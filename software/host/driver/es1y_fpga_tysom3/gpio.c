#include <stdint.h>

int main () {

  static volatile uint64_t tohost;
  static volatile uint64_t fromhost;
  static volatile uint64_t magicmem[8];
  static volatile uint64_t addr;
  static volatile uint64_t val;

  tohost = 0;
  addr = 0x0600000000;
  val = 0xface;
  *((uint64_t*) addr) = val;
  val = *((uint64_t*) addr);
  magicmem[0] = 106;
  magicmem[1] = val;
  __sync_synchronize();
  tohost = magicmem;
  __sync_synchronize();
  while (fromhost == 0) {};
  fromhost = 0;

  tohost = 0;
  addr = 0x0600100000;
  val = 0xbeef;
  *((uint64_t*) addr) = val;
  val = *((uint64_t*) addr);
  magicmem[0] = 106;
  magicmem[1] = val;
  __sync_synchronize();
  tohost = magicmem;
  __sync_synchronize();
  while (fromhost == 0) {};
  fromhost = 0;

  tohost = 0;
  addr = 0x0600000000;
  //val = 0xface;
  //*((uint64_t*) addr) = val;
  val = *((uint64_t*) addr);
  magicmem[0] = 106;
  magicmem[1] = val;
  __sync_synchronize();
  tohost = magicmem;
  __sync_synchronize();
  while (fromhost == 0) {};
  fromhost = 0;

  tohost = 1;
  __sync_synchronize();
  while(1) {};
}
