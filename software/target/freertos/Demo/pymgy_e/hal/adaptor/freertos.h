#ifndef HAL_ADAPTOR_FREERTOS_H
#define HAL_ADAPTOR_FREERTOS_H

void vPortasmHANDLE_INTERRUPT(void);
void vPortasmAddInterruptCount(void);
void vPortasmSubInterruptCount(void);
unsigned int uPortasmIsInterrupt(void);

#endif /* HAL_ADAPTOR_FREERTOS_H */
