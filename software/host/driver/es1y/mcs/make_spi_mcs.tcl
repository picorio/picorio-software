# file copy -force C:/vcu118_pcie/vcu118_pcie_x16_gen3_ex/vcu118_pcie_x16_gen3_ex.runs/impl_1/xilinx_pcie4_uscale_ep.bit .
set BIT_FILE ./pcie_dma_repl.runs/impl_1/xilinx_dma_pcie_ep.bit
set OUT_FILE ./pcie_dma_repl.runs/impl_1/xilinx_dma_pcie_ep.mcs
write_cfgmem -force -format MCS -size 32 -interface SPIx8 -loadbit "up 0x00000000 ${BIT_FILE}" ${OUT_FILE}
