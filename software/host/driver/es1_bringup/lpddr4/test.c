#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "mnPmuSramMsgBlock_lpddr4.h"

int main()
{
  PMU_SMB_LPDDR4_1D_t cfg;
  memset(&cfg, 0, sizeof(cfg));

  cfg.Reserved00           = (1 << 7); // 0x1 = use optimal csr settings for T28 circuitry
                              // MsgMisc[0] MTESTEnable
                              //      0x1 = Pulse primary digital test output bump at the end of each major training stage. This enables observation of training stage completion by observing the digital test output.
                              //      0x0 = Do not pulse primary digital test output bump
                              // 
                              // MsgMisc[1] SimulationOnlyReset
                              //      0x1 = Verilog only simulation option to shorten duration of DRAM reset pulse length to 1ns. 
                              //                Must never be set to 1 in silicon.
                              //      0x0 = Use reset pulse length specifed by JEDEC standard
                              // 
                              // MsgMisc[2] SimulationOnlyTraining
                              //      0x1 = Verilog only simulation option to shorten the duration of the training steps by performing fewer iterations. 
                              //                Must never be set to 1 in silicon.
                              //      0x0 = Use standard training duration.
                              // 
                              // MsgMisc[3] Disable Boot Clock 
                              //      0x1 = Disable boot frequency clock when initializing DRAM. (not recommended)
                              //      0x0 = Use Boot Frequency Clock 
                              // 
                              // MsgMisc[4] Suppress streaming messages, including assertions, regardless of HdtCtrl setting.
                              //            Stage Completion messages, as well as training completion and error messages are
                              //            Still sent depending on HdtCtrl setting.
                              // 
                              // MsgMisc[5] PerByteMaxRdLat
                              //      0x1 = Each DBYTE will return dfi_rddata_valid at the lowest possible latency. This may result in unaligned data between bytes to be returned to the DFI.
                              //      0x0 = Every DBYTE will return  dfi_rddata_valid simultaneously. This will ensure that data bytes will return aligned accesses to the DFI.
                              // 
  cfg.MsgMisc              = (1 << 4); // Suppress streaming messages
  cfg.Pstate               = 0x0;
  cfg.PllBypassEn          = 0x0;
  cfg.DRAMFreq             = 0x960; // DDR2400
  cfg.DfiFreqRatio         = 0x1; // 1:1
  cfg.BPZNResVal           = 0x0;
  cfg.PhyOdtImpedance      = 0x0;
  cfg.PhyDrvImpedance      = 0x0;
  cfg.PhyVref              = 0x40; // 50% of VDDQ (what value make sense here?)
  cfg.Lp4Misc              = 0x0;

                              //    SequenceCtrl[0] = Run DevInit - Device/phy initialization. Should always be set.
                              //    SequenceCtrl[1] = Run WrLvl - Write leveling
                              //    SequenceCtrl[2] = Run RxEn - Read gate training
                              //    SequenceCtrl[3] = Run RdDQS1D - 1d read dqs training
                              //    SequenceCtrl[4] = Run WrDQ1D - 1d write dq training
                              //    SequenceCtrl[5] = RFU, must be zero
                              //    SequenceCtrl[6] = RFU, must be zero
                              //    SequenceCtrl[7] = RFU, must be zero
                              //    SequenceCtrl[8] = Run RdDeskew - Per lane read dq deskew training
                              //    SequenceCtrl[9] = Run MxRdLat - Max read latency training
                              //    SequenceCtrl[11-10] = RFU, must be zero
                              //    SequenceCtrl[12]      = Run LPCA - CA Training
                              //    SequenceCtrl[15-13] = RFU, must be zero

  cfg.SequenceCtrl         = (1 <<  0) |
                             (1 <<  1) |
                             (1 <<  2) |
                             (1 <<  3) |
                             (1 <<  4) |
                             (1 <<  8) |
                             (1 <<  9) |
                             (1 << 12);
  cfg.HdtCtrl              = 0xff; // 0xFF = Firmware completion messages only
  cfg.DFIMRLMargin         = 0x1;
  cfg.UseBroadcastMR       = 0x1; // It is recommended in most LPDDR4 system configurations to set this to 1.
  cfg.Lp4Quickboot         = 0x0;
  cfg.CATrainOpt           = 0x1;
  cfg.X8Mode               = 0x0;
  cfg.PhyConfigOverride    = 0x0;
  cfg.EnabledDQsChA        = 0x10; // 16 bits
  cfg.CsPresentChA         = 0x3; // 0x3 = CS0 and CS1 are populated with DRAM
  cfg.MR1_A0               = 0x0; // Value to be programmed in DRAM Mode Register 1 {Channel A, Rank 0}
  cfg.MR2_A0               = 0x0;
  cfg.MR3_A0               = 0x0;
  cfg.MR4_A0               = 0x0;
  cfg.MR11_A0              = 0x0;
  cfg.MR12_A0              = 0x0;
  cfg.MR13_A0              = 0x0;
  cfg.MR14_A0              = 0x0;
  cfg.MR16_A0              = 0x0;
  cfg.MR17_A0              = 0x0;
  cfg.MR22_A0              = 0x0;
  cfg.MR24_A0              = 0x0;
  cfg.MR1_A1               = 0x0;
  cfg.MR2_A1               = 0x0;
  cfg.MR3_A1               = 0x0;
  cfg.MR4_A1               = 0x0;
  cfg.MR11_A1              = 0x0;
  cfg.MR12_A1              = 0x0;
  cfg.MR13_A1              = 0x0;
  cfg.MR14_A1              = 0x0;
  cfg.MR16_A1              = 0x0;
  cfg.MR17_A1              = 0x0;
  cfg.MR22_A1              = 0x0;
  cfg.MR24_A1              = 0x0;
  cfg.CATerminatingRankChA = 0x1; // 0x1 = Rank 1 is terminating rank
  cfg.EnabledDQsChB        = 0x10;
  cfg.CsPresentChB         = 0x0; // 0x0 = No chip selects are populated with DRAM
  cfg.MR1_B0               = 0x0;
  cfg.MR2_B0               = 0x0;
  cfg.MR3_B0               = 0x0;
  cfg.MR4_B0               = 0x0;
  cfg.MR11_B0              = 0x0;
  cfg.MR12_B0              = 0x0;
  cfg.MR13_B0              = 0x0;
  cfg.MR14_B0              = 0x0;
  cfg.MR16_B0              = 0x0;
  cfg.MR17_B0              = 0x0;
  cfg.MR22_B0              = 0x0;
  cfg.MR24_B0              = 0x0;
  cfg.MR1_B1               = 0x0;
  cfg.MR2_B1               = 0x0;
  cfg.MR3_B1               = 0x0;
  cfg.MR4_B1               = 0x0;
  cfg.MR11_B1              = 0x0;
  cfg.MR12_B1              = 0x0;
  cfg.MR13_B1              = 0x0;
  cfg.MR14_B1              = 0x0;
  cfg.MR16_B1              = 0x0;
  cfg.MR17_B1              = 0x0;
  cfg.MR22_B1              = 0x0;
  cfg.MR24_B1              = 0x0;
  cfg.CATerminatingRankChB = 0x0;

  uint8_t * p = (uint8_t*)&cfg;
  for (int i = 0; i < sizeof(cfg); i += 2) {
    printf("apb_wr(32'h%x, 16'h%02x%02x);\n", 0x54000 + i/2, p[i+1], p[i]);
  }
  return 0x0;
}
