#!/usr/bin/env bash

i="1"

export PATH=/work/home/lpcui/work2/pygmy/tool/riscv/bin/:$PATH
WORK_DIR="/work/home/lpcui/work2/pygmy/tool/driver/pygmy_e"
cd $WORK_DIR
FAIL_DIR=fail_rerun_`date "+%Y%m%d"`
mkdir $FAIL_DIR
echo "FAIL_DIR = $FAIL_DIR"
source ../../../env/sourceme
make reset0
while true; do
  CUR_DATE=`date "+%Y-%m-%d-%H-%M-%S"`
  echo "CUR_DATE = $CUR_DATE"
  cd $WORK_DIR
  test=`sed -n "$i p" pygmy_e_rand_test.list`
  echo "test=$test"
  RAND_TEST="$PROJ_ROOT/rtl/png-32/sw/png32-test-generator/fpga32_elfs/$test"
  RAND_TEST_C="$PROJ_ROOT/rtl/png-32/sw/png32-test-generator/fpga32_c_files/$test.c"
  echo "RAND_TEST=$RAND_TEST"
  make -e orv_rand_test RAND_TEST=$RAND_TEST |& tee $test.log &
  PID=$!
  echo "PID=$PID"
  $PROJ_ROOT/verif/common/regression/fpga/job_check.sh -i $WORK_DIR/$test.log -p "PASSED" -f "FAILED" -t 100 
  RSLT=$?
  pkill -9 fesvr2socket.o
  pkill -9 fesvr2socket.o
  kill -9 $PID
  echo "RSLT=$RSLT"
  if [ "$RSLT" -gt "0" ]
  then
    if [ "$RSLT" -gt "2" ]
    then
      echo "$(date) RAND Test $test TIMEOUT, rerun = $rerun" >> $WORK_DIR/rand_test_fail.log
    else
      echo "$(date) RAND Test $test FAILED" >> $WORK_DIR/rand_test_fail.log
    fi
    cp $RAND_TEST   ./$FAIL_DIR/$test.$CUR_DATE
    cp $RAND_TEST_C ./$FAIL_DIR/$test.c.$CUR_DATE
    cp $test.log    ./$FAIL_DIR/$test.log.$CUR_DATE
  else
    echo "$(date) RAND Test $test passed" >> $WORK_DIR/rand_test_pass.log
  fi
  echo "i = $i"
  if [ "$i" -ge "19" ]
  then
      i="1"
      cd $PROJ_ROOT/rtl/png-32/sw/png32-test-generator/
      ./create32_fpga.sh
      cd $PROJ_ROOT/rtl/png-32/sw/png32-test-generator/fpga32_check
      grep -rn FAIL * > ../fail_log_$CUR_DATE
  else
      i=$(($i+1))
  fi
  cd $WORK_DIR
 make reset0
done
