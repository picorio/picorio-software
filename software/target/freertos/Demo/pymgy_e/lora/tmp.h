#ifndef LORA_TMP_H
#define LORA_TMP_H

// TODO:
#define LORA_CLASS_C
#define REGION_CN470

#include "gpio.h"
#include "task.h" // TODO: for freertos

typedef enum {
	LORA_READ,
	LORA_WRITE,
}LoraRW_t;

typedef enum
{
    SPI_1,
    SPI_2,
}SpiId_t;

#if 0
typedef struct Spi_s
{
    SpiId_t SpiId;
    Gpio_t Mosi;
    Gpio_t Miso;
    Gpio_t Sclk;
    Gpio_t Nss;
}Spi_t;
#endif

typedef union Version_u
{
    struct Version_s
    {
        uint8_t Rfu;
        uint8_t Revision;
        uint8_t Minor;
        uint8_t Major;
    }Fields;
    uint32_t Value;
}Version_t;
#if 1
#define CRITICAL_SECTION_BEGIN( )	taskENTER_CRITICAL()
#define CRITICAL_SECTION_END( )		taskEXIT_CRITICAL()
#else
#define CRITICAL_SECTION_BEGIN( )	do{}while(0)
#define CRITICAL_SECTION_END( )		do{}while(0)
#endif
uint8_t BoardGetBatteryLevel( void );

uint32_t BoardGetRandomSeed( void );

void BoardGetUniqueId( uint8_t *id );

void DelayMs( uint32_t ms );

void PingpongMain( void );

int classAMain( void );

void BoardInitMcu( void );

void BoardDeInitMcu( void );

void BoardLowPowerHandler( void );

#endif /* LORA_TMP_H */

