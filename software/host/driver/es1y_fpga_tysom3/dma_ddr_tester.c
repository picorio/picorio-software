#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <map>

#include "socket_client.h"
#include "common.h"
#include "common-socket.h"
#include "station_orv32.h"
#include "pygmy_es1y_addr_translater.h"

int main(int argc, char * argv[]) {
  uint64_t addr = 0;
  uint64_t start_addr = 0;
  uint64_t wdata = 0;
  uint64_t rdata = 0;

  std::map<uint64_t, uint64_t> sb;
  std::map<uint64_t, uint64_t>::iterator it;

  unsigned short serverPort = 0;
  struct sockaddr_in serverAddr;
  int clientSock;
  for (int i = 1; i < argc; i++) {
    if (strncmp(argv[i], "serverPort=", 11) == 0) {
      serverPort = atoi(argv[i] + 11);
    }
  }
  if (serverPort == 0) {
    serverPort = 8081; // Default
  }
  printf ("serverPort = %0d\n", serverPort);
  setup(serverPort, &serverAddr, &clientSock);

  int pass = 1;
  int wrn = 0;
  int cnt = 0;
  int dma_cmd_vld = 0;

  do_write(&serverAddr, &clientSock, STATION_CACHE_S2B_RSTN_ADDR_0, 1);
  do_write(&serverAddr, &clientSock, STATION_CACHE_S2B_RSTN_ADDR_1, 1);
  //do_write(&serverAddr, &clientSock, STATION_CACHE_S2B_RSTN_ADDR_2, 1);
  //do_write(&serverAddr, &clientSock, STATION_CACHE_S2B_RSTN_ADDR_3, 1);
  //do_write(&serverAddr, &clientSock, STATION_CACHE_S2B_RSTN_ADDR_3, 1);
  do_write(&serverAddr, &clientSock, STATION_CACHE_S2B_CTRL_REG_ADDR_0, 0x1000000);
  do_write(&serverAddr, &clientSock, STATION_CACHE_S2B_CTRL_REG_ADDR_1, 0x1000000);
  //do_write(&serverAddr, &clientSock, STATION_CACHE_S2B_CTRL_REG_ADDR_2, 0x1000000);
  //do_write(&serverAddr, &clientSock, STATION_CACHE_S2B_CTRL_REG_ADDR_3, 0x1000000);

//  for (int i = 0;  i < 10; i++) {
  while (pass == 1) {
    //if ((cnt == 0) || (cnt == 100000)) {
    //  start_addr = (rand() % 80000000) & 0xfffff000;  // 2GB address range, zero-out lower 12 bit address
    //  start_addr = start_addr + 0x80000000;           // DDR address range: 0x80000000 - 0xffffffff
    //  cnt = 0;
    //}

    wrn = rand() % 2;
    start_addr = 0x80000000;

    if ((wrn == 1) || (sb.size() == 0)) {
      // Do write
      addr = ((rand() % 0x100) << 21) + start_addr;  // Generate lower 12 bit address for torture
      wdata = rand();
      wdata = (wdata << 32) + rand();
      do_write(&serverAddr, &clientSock, STATION_DMA_S2B_DMA_DEBUG_ADDR_ADDR, addr);
      do_write(&serverAddr, &clientSock, STATION_DMA_S2B_DMA_DEBUG_REQ_TYPE_ADDR, 2);
      do_write(&serverAddr, &clientSock, STATION_DMA_S2B_DMA_DEBUG_WR_DATA_ADDR, wdata);
      dma_cmd_vld = 1;
      do_write(&serverAddr, &clientSock, STATION_DMA_DMA_DEBUG_CMD_VLD_ADDR, dma_cmd_vld);
      while (dma_cmd_vld == 1)
        dma_cmd_vld = do_read(&serverAddr, &clientSock, STATION_DMA_DMA_DEBUG_CMD_VLD_ADDR);
      printf("WR 0x%010lx 0x%016lx\n", addr, wdata);
      cnt++;

      // Update scoreboard
      sb[addr] = wdata;
    } else {
      // Do read
      //addr = ((rand() % 0x100000) << 3) + start_addr;
      it = sb.begin();
      std::advance(it, rand() % sb.size());

      do_write(&serverAddr, &clientSock, STATION_DMA_S2B_DMA_DEBUG_ADDR_ADDR, it->first);
      do_write(&serverAddr, &clientSock, STATION_DMA_S2B_DMA_DEBUG_REQ_TYPE_ADDR, 0);
      dma_cmd_vld = 1;
      do_write(&serverAddr, &clientSock, STATION_DMA_DMA_DEBUG_CMD_VLD_ADDR, dma_cmd_vld);
      while (dma_cmd_vld == 1)
        dma_cmd_vld = do_read(&serverAddr, &clientSock, STATION_DMA_DMA_DEBUG_CMD_VLD_ADDR);
      rdata = do_read(&serverAddr, &clientSock, STATION_DMA_B2S_DMA_DEBUG_RDATA_ADDR);

      printf("RD 0x%010lx 0x%016lx\n", it->first, it->second);

      // Data check
      if (it->second != rdata) {
        pass = 0;
        printf("FAIL: addr = 0x%010lx, data_exp = 0x%016lx, rdata = 0x%016lx\n", it->first, it->second, rdata);
      }
    }
  }
}
