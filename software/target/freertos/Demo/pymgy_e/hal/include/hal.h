#ifndef HAL_INCLUDE_HAL_H
#define HAL_INCLUDE_HAL_H

#include "system_config.h"
#include "soc_config.h"
#include "bsp_config.h"

#if 1  // TODO: will be removed
#include "FreeRTOS.h"
#endif

#include "core.h"
#include "io.h"
#include "common.h"
#include "rtlgen.h"
#include "util.h"

#include "tmp.h" // TODO: will be removed

/*!
 * @discussion get uptime since system boot.
 * @return the current uptime value, unit: ms.
 */
unsigned int rvHal_get_uptime(void);

/*!
 * @discussion time delay when CPU polls in deadloop.
 * @param ms delay unit: ms.
 */
void rvHal_delay(unsigned int ms);

/*!
 * @discussion time sleep when the current task enter sleep state.
 * @param ms sleep unit: ms.
 */
void rvHal_sleep(unsigned int ms);

/*!
 * @discussion initialize RiVAI HAL IO function including interrupt, uart, gpio, i2c, spi...
 */
void rvHal_init(void);

/*!
 * @discussion application entry API as a callback
 */
void rvHalCB_app_entry(void);
#endif /* HAL_INCLUDE_HAL_H */

