#!/bin/bash
#
# Script to build RISC-V ISA simulator, proxy kernel, and GNU toolchain.
# Tools will be installed to $RISCV.

## # export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
#export PATH=/usr/local/bin/:/usr/sbin:/usr/bin:/sbin:/bin:$PATH

#export RISCV=/work/tools/ours-gnu-toolchain/riscv/
export MAKEFLAGS="$MAKEFLAGS -j16"
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:/usr/lib/pkgconfig
export PATH=$RISCV/bin:$PATH
export LD_LIBRARY_PATH=$RISCV/lib:/usr/local/lib64:/usr/local/lib
export CC=gcc
export CXX=g++


echo "Starting RISC-V Toolchain build process"

which gcc
gcc --version
if [ "$1" = "" ]; then
  echo "Please pass project name as argument. Example: ./build-fesvr.sh es1y"
  exit
else
  PROJ=$1
fi

export PROJECT_NAME=$PROJ

if [[ "$1" == "es1y" ]]; then
PROJ_NUM=0
elif [[ "$1" == "es1x" ]]; then
PROJ_NUM=1
elif [[ "$1" == "demo" ]]; then
PROJ_NUM=2
elif [[ "$1" == "pygmy_e" ]]; then
PROJ_NUM=3
fi


. build.common

# CFLAGS='-std=c99' build_project riscv-openocd --prefix=$RISCV --enable-remote-bitbang --enable-jtag_vpi --disable-werror
# build_project riscv-openocd --prefix=$RISCV --enable-remote-bitbang --enable-jtag_vpi --disable-werror
# build_project riscv-fesvr --prefix=$RISCV
# build_project riscv-isa-sim --prefix=$RISCV --with-fesvr=$RISCV
# build_project riscv-gnu-toolchain --prefix=$RISCV
# cd riscv-gnu-toolchain/build; make -j16 linux
# CC= CXX= build_project riscv-pk --prefix=$RISCV --host=riscv64-unknown-elf
# build_project riscv-tests --prefix=$RISCV/riscv64-unknown-elf
# cd ../riscv-linux; CROSS_COMPILE=riscv64-unknown-linux-gnu- make -j32 ARCH=riscv vmlinux

#cd riscv-pk/build; ../configure --with-payload=/work/users/jwang/tmp/riscv-linux/vmlinux --host=riscv64-unknown-linux-gnu; make bbl
#cd ../../

COM_INC="${PROJ_ROOT}/software/common/include"

#COM_CFLAGS=-I${PROJ_ROOT}/subproj/${PROJ}/rtl_gen/ -DPROJ_NUM=${PROJ_NUM}

COM_CFLAGS="-I${COM_INC}/ -I${COM_INC}/${PLATFORM_NAME}/rtl_gen/ -DPROJ_NUM=${PROJ_NUM}"

CFLAGS="${COM_CFLAGS}" CPPFLAGS="${COM_CFLAGS}" build_project riscv-fesvr --prefix=$RISCV --proj=$PROJECT_NAME
#build_project riscv-isa-sim --prefix=$RISCV --with-fesvr=$RISCV
#build_project riscv-gnu-toolchain --prefix=$RISCV

echo -e "\\nRISC-V Toolchain installation completed!"
