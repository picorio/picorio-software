
import re, sys

f = open ("run_hello_new.log", 'r')

dc_addr = dict()
cnt = 0
for l in f:
  cnt = cnt + 1
  m = re.match(r".*address ([^ ]*)", l)
  if m:
    if m.group(1) in dc_addr.keys():
      dc_addr[m.group(1)].append(cnt)
    else:
      dc_addr[m.group(1)] = [cnt]

for key in dc_addr.keys():
  if len(dc_addr[key]) > 1:
    print key, dc_addr[key]
