#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include "station_cache.h"
#include "pcie_driver.h"

int main() {
 uint64_t rdata;
 fprintf(stderr, "Start Zeroing Out TAG RAM for L2\n");
   for (int way = 0; way < 8; way ++) {
     for (int idx = 0; idx < 512; idx ++) {
       int index = ((way << 2) << 9) + idx;
       do_write(STATION_CACHE_DBG_VLDRAM_ADDR_0 + index * 32, 0);
       do_write(STATION_CACHE_DBG_VLDRAM_ADDR_1 + index * 32, 0);
       do_write(STATION_CACHE_DBG_VLDRAM_ADDR_2 + index * 32, 0);
       do_write(STATION_CACHE_DBG_VLDRAM_ADDR_3 + index * 32, 0);
     }
   }
 fprintf(stderr, "Done Zeroing Out TAG RAM for L2\n");
}
