//-----------------------------------------------------------------------------
//
// (c) Copyright 2012-2012 Xilinx, Inc. All rights reserved.
//
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
//
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
//
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
//
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
//
//-----------------------------------------------------------------------------
//
// Project    : The Xilinx PCI Express DMA 
// File       : xilinx_dma_pcie_ep.sv
// Version    : 4.1
//-----------------------------------------------------------------------------
`timescale 1ps / 1ps

module xilinx_dma_pcie_ep #
  (
   parameter PL_LINK_CAP_MAX_LINK_WIDTH          = 16,            // 1- X1; 2 - X2; 4 - X4; 8 - X8
   parameter PL_SIM_FAST_LINK_TRAINING           = "FALSE",      // Simulation Speedup
   parameter PL_LINK_CAP_MAX_LINK_SPEED          = 4,             // 1- GEN1; 2 - GEN2; 4 - GEN3
   parameter C_DATA_WIDTH                        = 512 ,
   parameter EXT_PIPE_SIM                        = "FALSE",  // This Parameter has effect on selecting Enable External PIPE Interface in GUI.
   parameter C_ROOT_PORT                         = "FALSE",      // PCIe block is in root port mode
   parameter C_DEVICE_NUMBER                     = 0,            // Device number for Root Port configurations only
   parameter AXIS_CCIX_RX_TDATA_WIDTH     = 256, 
   parameter AXIS_CCIX_TX_TDATA_WIDTH     = 256,
   parameter AXIS_CCIX_RX_TUSER_WIDTH     = 46,
   parameter AXIS_CCIX_TX_TUSER_WIDTH     = 46
   )
   (
    output [(PL_LINK_CAP_MAX_LINK_WIDTH - 1) : 0] pci_exp_txp,
    output [(PL_LINK_CAP_MAX_LINK_WIDTH - 1) : 0] pci_exp_txn,
    input [(PL_LINK_CAP_MAX_LINK_WIDTH - 1) : 0]  pci_exp_rxp,
    input [(PL_LINK_CAP_MAX_LINK_WIDTH - 1) : 0]  pci_exp_rxn,

//VU9P_TUL_EX_String= FALSE



    input                                        free_run_clock_p_in,
    input                                        free_run_clock_n_in,

    output 					 led_0,
    output 					 led_1,
    output 					 led_2,
    output 					 led_3,
    input 					 sys_clk_p,
    input 					 sys_clk_n,
    input 					 sys_rst_n
 );

   //-----------------------------------------------------------------------------------------------------------------------

   
   // Local Parameters derived from user selection
   localparam integer 				   USER_CLK_FREQ         = ((PL_LINK_CAP_MAX_LINK_SPEED == 3'h4) ? 5 : 4);
   localparam TCQ = 1;
   localparam C_S_AXI_ID_WIDTH = 4; 
   localparam C_M_AXI_ID_WIDTH = 4; 
   localparam C_S_AXI_DATA_WIDTH = C_DATA_WIDTH;
   localparam C_M_AXI_DATA_WIDTH = C_DATA_WIDTH;
   localparam C_S_AXI_ADDR_WIDTH = 64;
   localparam C_M_AXI_ADDR_WIDTH = 64;
   localparam C_NUM_USR_IRQ	 = 1;
   
   wire 					   user_lnk_up;
   
   //----------------------------------------------------------------------------------------------------------------//
   //  AXI Interface                                                                                                 //
   //----------------------------------------------------------------------------------------------------------------//
   
   wire 					   user_clk;
   wire 					   user_resetn;
   
  // Wires for Avery HOT/WARM and COLD RESET
   wire 					   avy_sys_rst_n_c;
   wire 					   avy_cfg_hot_reset_out;
   reg 						   avy_sys_rst_n_g;
   reg 						   avy_cfg_hot_reset_out_g;
   assign avy_sys_rst_n_c = avy_sys_rst_n_g;
   assign avy_cfg_hot_reset_out = avy_cfg_hot_reset_out_g;
   initial begin 
      avy_sys_rst_n_g = 1;
      avy_cfg_hot_reset_out_g =0;
   end
   


  //----------------------------------------------------------------------------------------------------------------//
  //    System(SYS) Interface                                                                                       //
  //----------------------------------------------------------------------------------------------------------------//

    wire                                    sys_clk;
    wire                                    sys_clk_gt;
    wire                                    sys_rst_n_c;

  // User Clock LED Heartbeat
     reg [25:0] 			     user_clk_heartbeat;
     reg [((2*C_NUM_USR_IRQ)-1):0]		usr_irq_function_number=0;
     reg [C_NUM_USR_IRQ-1:0] 		     usr_irq_req = 0;
     wire [C_NUM_USR_IRQ-1:0] 		     usr_irq_ack;

      //-- AXI Master Write Address Channel
     wire [C_M_AXI_ADDR_WIDTH-1:0] m_axi_awaddr;
     wire [C_M_AXI_ID_WIDTH-1:0] m_axi_awid;
     wire [2:0] 		 m_axi_awprot;
     wire [1:0] 		 m_axi_awburst;
     wire [2:0] 		 m_axi_awsize;
     wire [3:0] 		 m_axi_awcache;
     wire [7:0] 		 m_axi_awlen;
     wire 			 m_axi_awlock;
     wire 			 m_axi_awvalid;
     wire 			 m_axi_awready;

     //-- AXI Master Write Data Channel
     wire [C_M_AXI_DATA_WIDTH-1:0]     m_axi_wdata;
     wire [(C_M_AXI_DATA_WIDTH/8)-1:0] m_axi_wstrb;
     wire 			       m_axi_wlast;
     wire 			       m_axi_wvalid;
     wire 			       m_axi_wready;
     //-- AXI Master Write Response Channel
     wire 			       m_axi_bvalid;
     wire 			       m_axi_bready;
     wire [C_M_AXI_ID_WIDTH-1 : 0]     m_axi_bid ;
     wire [1:0]                        m_axi_bresp ;

     //-- AXI Master Read Address Channel
     wire [C_M_AXI_ID_WIDTH-1 : 0]     m_axi_arid;
     wire [C_M_AXI_ADDR_WIDTH-1:0]     m_axi_araddr;
     wire [7:0]                        m_axi_arlen;
     wire [2:0]                        m_axi_arsize;
     wire [1:0]                        m_axi_arburst;
     wire [2:0] 		       m_axi_arprot;
     wire 			       m_axi_arvalid;
     wire 			       m_axi_arready;
     wire 			       m_axi_arlock;
     wire [3:0] 		       m_axi_arcache;

     //-- AXI Master Read Data Channel
     wire [C_M_AXI_ID_WIDTH-1 : 0]   m_axi_rid;
     wire [C_M_AXI_DATA_WIDTH-1:0]   m_axi_rdata;
     wire [1:0] 		     m_axi_rresp;
     wire 			     m_axi_rvalid;
     wire 			     m_axi_rready;

/*
// declare wires for AXI bypass
     reg 			                m_axib_wready;
     reg 			                m_axib_wvalid;
     reg [C_M_AXI_DATA_WIDTH-1:0]   m_axib_wdata;
     reg			                m_axib_rready;
     reg 			                m_axib_rvalid;
     reg [C_M_AXI_DATA_WIDTH-1:0]   m_axib_rdata;
     reg [C_M_AXI_ADDR_WIDTH-1:0]   m_axib_awaddr;
     reg [C_M_AXI_ADDR_WIDTH-1:0]   m_axib_araddr;
*/



    wire [2:0]    msi_vector_width;
    wire          msi_enable;
    wire [3:0]                  leds;

 wire free_run_clock;

  wire   clk_100MHz_locked;
  
  // Clocking for the 7-segment display module.
  clk_wiz_0 mem_clk_inst(
    // Clock in ports
    .clk_in1_p  (free_run_clock_p_in),          // input clk_in1_p
    .clk_in1_n  (free_run_clock_n_in),          // input clk_in1_n
    // Reset port
     //.reset      (!sys_rst_n_c),
    // Clock out ports
    .clk_out1   (free_run_clock),             // output clk_out1
    // Status and control signals
    .locked     (clk_100MHz_locked)         // output locked
  );


    
  wire [5:0]                          cfg_ltssm_state;

  // Ref clock buffer
  IBUFDS_GTE4 # (.REFCLK_HROW_CK_SEL(2'b00)) refclk_ibuf (.O(sys_clk_gt), .ODIV2(sys_clk), .I(sys_clk_p), .CEB(1'b0), .IB(sys_clk_n));
  // Reset buffer
  IBUF   sys_reset_n_ibuf (.O(sys_rst_n_c), .I(sys_rst_n));
  // LED 0 pysically resides in the reconfiguable area for Tandem with 
  // Field Updates designs so the OBUF must included in the app hierarchy.
  assign led_0 = leds[0];
  // LEDs 1-3 physically reside in the stage1 region for Tandem with Field 
  // Updates designs so the OBUF must be instantiated at the top-level and
  // added to the stage1 region
  OBUF led_1_obuf (.O(led_1), .I(leds[1]));
  OBUF led_2_obuf (.O(led_2), .I(leds[2]));
  OBUF led_3_obuf (.O(led_3), .I(leds[3]));

     



//
//



  // Core Top Level Wrapper
  xdma_0 xdma_0_i 
     (
      //---------------------------------------------------------------------------------------//
      //  PCI Express (pci_exp) Interface                                                      //
      //---------------------------------------------------------------------------------------//
      .sys_rst_n       ( sys_rst_n_c ),
      .sys_clk         ( sys_clk ),
      .sys_clk_gt      ( sys_clk_gt),
      
      // Tx
      .pci_exp_txn     ( pci_exp_txn ),
      .pci_exp_txp     ( pci_exp_txp ),
      
      // Rx
      .pci_exp_rxn     ( pci_exp_rxn ),
      .pci_exp_rxp     ( pci_exp_rxp ),


       // AXI MM Interface
      .m_axi_awid      (m_axi_awid  ),
      .m_axi_awaddr    (m_axi_awaddr),
      .m_axi_awlen     (m_axi_awlen),
      .m_axi_awsize    (m_axi_awsize),
      .m_axi_awburst   (m_axi_awburst),
      .m_axi_awprot    (m_axi_awprot),
      .m_axi_awvalid   (m_axi_awvalid),
      .m_axi_awready   (m_axi_awready),
      .m_axi_awlock    (m_axi_awlock),
      .m_axi_awcache   (m_axi_awcache),
      .m_axi_wdata     (m_axi_wdata),
      .m_axi_wstrb     (m_axi_wstrb),
      .m_axi_wlast     (m_axi_wlast),
      .m_axi_wvalid    (m_axi_wvalid),
      .m_axi_wready    (m_axi_wready),
      .m_axi_bid       (m_axi_bid),
      .m_axi_bresp     (m_axi_bresp),
      .m_axi_bvalid    (m_axi_bvalid),
      .m_axi_bready    (m_axi_bready),
      .m_axi_arid      (m_axi_arid),
      .m_axi_araddr    (m_axi_araddr),
      .m_axi_arlen     (m_axi_arlen),
      .m_axi_arsize    (m_axi_arsize),
      .m_axi_arburst   (m_axi_arburst),
      .m_axi_arprot    (m_axi_arprot),
      .m_axi_arvalid   (m_axi_arvalid),
      .m_axi_arready   (m_axi_arready),
      .m_axi_arlock    (m_axi_arlock),
      .m_axi_arcache   (m_axi_arcache),
      .m_axi_rid       (m_axi_rid),
      .m_axi_rdata     (m_axi_rdata),
      .m_axi_rresp     (m_axi_rresp),
//      .m_axi_rlast     (m_axi_rlast),
// rlast was originally hooked up to the axi bram
// however, when we switched out for pcie2oursring we have no rlast signal
// we can't leave it floating or else vivado implementation will fail
// so we tie it to high because we are not using burst anyways
      .m_axi_rlast     (1'b1),
      .m_axi_rvalid    (m_axi_rvalid),
      .m_axi_rready    (m_axi_rready),



      .usr_irq_req       (usr_irq_req),
      .usr_irq_ack       (usr_irq_ack),
      .msi_enable        (msi_enable),
      .msi_vector_width  (msi_vector_width),


     // Config managemnet interface
      .cfg_mgmt_addr  ( 19'b0 ),
      .cfg_mgmt_write ( 1'b0 ),
      .cfg_mgmt_write_data ( 32'b0 ),
      .cfg_mgmt_byte_enable ( 4'b0 ),
      .cfg_mgmt_read  ( 1'b0 ),
      .cfg_mgmt_read_data (),
      .cfg_mgmt_read_write_done (),


// axi bypass interface

/*
      .m_axib_wready                                (m_axib_wready),
      .m_axib_wvalid                                (m_axib_wvalid),
      .m_axib_wdata                                 (m_axib_wdata),
      .m_axib_rready                                (m_axib_rready),
      .m_axib_rvalid                                (m_axib_rvalid),
      .m_axib_rdata                                 (m_axib_rdata),
      .m_axib_awaddr                                (m_axib_awaddr),
      .m_axib_araddr                                (m_axib_araddr),
*/

      //-- AXI Global
      .axi_aclk        ( user_clk ),
      .axi_aresetn     ( user_resetn ),
  





      .user_lnk_up     ( user_lnk_up )
    );


  // XDMA taget application
/*
  xdma_app #(
    .C_M_AXI_ID_WIDTH(C_M_AXI_ID_WIDTH)
  ) xdma_app_i (



      // AXI Memory Mapped interface
      .s_axi_awid      (m_axi_awid),
      .s_axi_awaddr    (m_axi_awaddr),
      .s_axi_awlen     (m_axi_awlen),
      .s_axi_awsize    (m_axi_awsize),
      .s_axi_awburst   (m_axi_awburst),
      .s_axi_awvalid   (m_axi_awvalid),
      .s_axi_awready   (m_axi_awready),

      .s_axi_wdata     (m_axi_wdata),
      .s_axi_wstrb     (m_axi_wstrb),
      .s_axi_wlast     (m_axi_wlast),
      .s_axi_wvalid    (m_axi_wvalid),
      .s_axi_wready    (m_axi_wready),

      .s_axi_bid       (m_axi_bid),
      .s_axi_bresp     (m_axi_bresp),
      .s_axi_bvalid    (m_axi_bvalid),
      .s_axi_bready    (m_axi_bready),

      .s_axi_arid      (m_axi_arid),
      .s_axi_araddr    (m_axi_araddr),
      .s_axi_arlen     (m_axi_arlen),
      .s_axi_arsize    (m_axi_arsize),
      .s_axi_arburst   (m_axi_arburst),
      .s_axi_arvalid   (m_axi_arvalid),
      .s_axi_arready   (m_axi_arready),

      .s_axi_rid       (m_axi_rid),
      .s_axi_rdata     (m_axi_rdata),
      .s_axi_rresp     (m_axi_rresp),
      .s_axi_rlast     (m_axi_rlast),
      .s_axi_rvalid    (m_axi_rvalid),
      .s_axi_rready    (m_axi_rready),


      .user_clk(user_clk),
      .user_resetn(user_resetn),
      .user_lnk_up(user_lnk_up),
      .sys_rst_n(sys_rst_n_c),

      .leds(leds)
  );
*/
// ila to monitor axi
  ila_0 ila_dma (
    .clk        (user_clk),
    .probe0     (m_axi_wready),
    .probe1     (m_axi_awaddr),
    .probe2     (2'b0),
    .probe3     (m_axi_wvalid),
    .probe4     (m_axi_rready),
    .probe5     (m_axi_araddr),
    .probe6     (m_axi_rvalid),
    .probe7     (1'b0),
    .probe8     (1'b0),
    .probe9     (1'b0),
    .probe10    (m_axi_wdata),
    .probe11    (1'b0),
    .probe12    (1'b0),
    .probe13    (2'b0),
    .probe14    (m_axi_rdata),

    .probe15    (64'b0),
    .probe16    (1'b0),
    .probe17    (3'b0),
    .probe18    (3'b0),
    .probe19    (4'b0),
    .probe20    (4'b0),
    .probe21    (8'b0),
    .probe22    (1'b0),
    .probe23    (3'b0),
    .probe24    (2'b0),
    .probe25    (4'b0),
    .probe26    (1'b0),
    .probe27    (8'b0),
    .probe28    (3'b0),
    .probe29    (2'b0),
    .probe30    (1'b0),
    .probe31    (4'b0),
    .probe32    (4'b0),
    .probe33    (4'b0),
    .probe34    (4'b0),
    .probe35    (1'b0),
    .probe36    (4'b0),
    .probe37    (4'b0),
    .probe38    (4'b0),
    .probe39    (1'b0),
    .probe40    (1'b0),
    .probe41    (1'b0),
    .probe42    (1'b0),
    .probe43    (1'b0)
    );

/*
  ila_0 ila_bypass (
    .clk        (user_clk),
    .probe0     (m_axib_wready),
    .probe1     (m_axib_awaddr),
    .probe2     (2'b0),
    .probe3     (m_axib_wvalid),
    .probe4     (m_axib_rready),
    .probe5     (m_axib_araddr),
    .probe6     (m_axib_rvalid),
    .probe7     (1'b0),
    .probe8     (1'b0),
    .probe9     (1'b0),
    .probe10    (m_axib_wdata),
    .probe11    (1'b0),
    .probe12    (1'b0),
    .probe13    (2'b0),
    .probe14    (m_axib_rdata),

    .probe15    (64'b0),
    .probe16    (1'b0),
    .probe17    (3'b0),
    .probe18    (3'b0),
    .probe19    (4'b0),
    .probe20    (4'b0),
    .probe21    (8'b0),
    .probe22    (1'b0),
    .probe23    (3'b0),
    .probe24    (2'b0),
    .probe25    (4'b0),
    .probe26    (1'b0),
    .probe27    (8'b0),
    .probe28    (3'b0),
    .probe29    (2'b0),
    .probe30    (1'b0),
    .probe31    (4'b0),
    .probe32    (4'b0),
    .probe33    (4'b0),
    .probe34    (4'b0),
    .probe35    (1'b0),
    .probe36    (4'b0),
    .probe37    (4'b0),
    .probe38    (4'b0),
    .probe39    (1'b0),
    .probe40    (1'b0),
    .probe41    (1'b0),
    .probe42    (1'b0),
    .probe43    (1'b0)
    );
*/

pcie2oursring pcie2oursring_i (
  .slave_awvalid     (m_axi_awvalid),
  .slave_awaddr      (m_axi_awaddr),
  .slave_awid        (m_axi_awid),
  .slave_awready     (m_axi_awready),
  
  .slave_wvalid      (m_axi_wvalid),
  .slave_wdata       (m_axi_wdata),
  .slave_wready      (m_axi_wready),
  
  .slave_bvalid      (m_axi_bvalid),
  .slave_bid         (m_axi_bid),
  .slave_bresp       (m_axi_bresp),
  .slave_bready      (m_axi_bready),
  
  .slave_arvalid     (m_axi_arvalid),
  .slave_araddr      (m_axi_araddr),
  .slave_arid        (m_axi_arid),
  .slave_arready     (m_axi_arready),
  
  .slave_rvalid      (m_axi_rvalid),
  .slave_rid         (m_axi_rid),
  .slave_rdata       (m_axi_rdata),
  .slave_rresp       (m_axi_rresp),
  .slave_rready      (m_axi_rready),

//  .or_awvalid
//  .or_aw
//  .or_awready
//  
//  .or_wvalid
//  .or_w
//  .or_wready
//  
//  .or_bvalid
//  .or_b
//  .or_bready
//  
//  .or_arvalid
//  .or_ar
//  .or_arready
//  
//  .or_rvalid
//  .or_r
//  .or_rready

  .clk               (user_clk),
  .rstn              (user_resetn)
  );

endmodule
