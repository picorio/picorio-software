Run the script with ‘python3 main.py --proj <project> [--driver <driver>]’ on Linux and '<python.exe> main.py --proj <project> [--driver <driver>]' on Windows.

An overview of the architectural layering for the core register packing/unpacking functionality:

GUI -> User -> Physical
=======================
read [reg name] -> read_reg_name -> reg.read

write [reg name] -> write_reg_name -> reg.write

update [reg name] [value] -> update_reg -> reg.update
update [reg name].[field name] [value] -> update_reg_field -> reg.update_field

show [reg name] -> show_reg -> reg.get_value, reg.get_staged_value
show [reg name] -> show_reg_field -> reg.get_field_value, reg.get_staged_field_value
=======================

Following these cases, the read function was overloaded to increase flexibility to the user and add compatibility with memory:

GUI -> User -> Physical
=======================
read [reg addr] -> read_reg_addr -> reg.read
read [mem addr] -> read_mem_addr -> mem.read
read [mem addr] [target] -> read_mem_addr -> mem.read
read [mem addr] [range] -> read_mem_range -> mem.read (loop)
read [mem addr] [range] [target] -> read_mem_range -> mem.read (loop)
=======================

Sequence of updating a value:

read [reg name] : required to get the current value first
<show [reg name]>
update [reg name] : required to update a shadow copy before write to the real hardware
<show [reg name]>
write [reg name] : required to write the new value to the real hardware


