#include "FreeRTOS.h"
#include "common.h"
#include "hal.h"

static void __rvHal_clk_init(void)
{
	int pclk_scale = FREQ_SCALE_PCLK;

	if (pclk_scale <= 1) {
		write32(STATION_BYP_S2B_SLOW_IO_CLKDIV_DIVCLK_SEL_ADDR, 0);
	} else {
#ifdef BOARD_FPGA_104
		if (pclk_scale > 12)
			pclk_scale = 12;
#endif
		write32(STATION_BYP_S2B_SLOW_IO_CLKDIV_DIVCLK_SEL_ADDR, 1);
		write32(STATION_BYP_S2B_SLOW_IO_CLKDIV_HALF_DIV_LESS_1_ADDR, (pclk_scale >> 1) -1);
	}
}

unsigned int rvHal_get_uptime(void)
{
	return xTaskGetTickCount() * portTICK_PERIOD_MS;
}

void rvHal_delay(unsigned int ms) // TODO: os hook, need be set in advance
{
#if 0
	unsigned int tick = pdMS_TO_TICKS(ms);
	unsigned int tickstart = xTaskGetTickCount();
//printf("tick: %u %u\n", tickstart, tick);
	while((xTaskGetTickCount() - tickstart) < tick)
	{
	}
//printf("tick: %u %u\n", xTaskGetTickCount(), tick);
//#else
	int i;
	unsigned int count = 100*ms;// TODO:
	for (i=0; i<count; i++) {

	}
#endif
}

void rvHal_sleep(unsigned int ms)
{
	vTaskDelay(pdMS_TO_TICKS(ms)); // TODO: os hook, need be set in advance
}

void rvHal_init(void)
{
	__rvHal_clk_init();
	__rvHal_irq_ext_init();
#ifdef CONSOLE_USE_UART
	__rvHal_uart_init();
#endif
#if 1
	__rvHal_gpio_init();
	__rvHal_i2c_init();
	__rvHal_spi_init();
#if 1
	__rvHal_rtc_init();
	vTimer1Init();
	tickless_init();
#endif
#endif
}

