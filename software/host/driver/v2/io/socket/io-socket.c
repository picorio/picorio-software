#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>

#include "socket_client.h"
#include "common-socket.h"
#include "common.h"

struct socket_channel_t {
	struct io_channel_t io;

	unsigned short serverPort;
	int clientSock;
	struct sockaddr_in serverAddr;
};

static int socket_parse_args(struct io_channel_t *io, int argc, char const **argv)
{
	struct socket_channel_t *socket = (struct socket_channel_t *)io_get_priv_channel(io);

	unsigned short serverPort = 0;

	for (int i = 1; i < argc; i++) {
		if (strncmp(argv[i], "serverPort=", 11) == 0) {
			serverPort = atoi(argv[i] + 11);
		}
	}

	if (serverPort == 0) {
		serverPort = 9900; // Default
	}

	printf ("serverPort = %0d\n", serverPort);

	socket->serverPort = serverPort;
	return 0;
}

static int socket_setup(struct io_channel_t *io)
{
	struct socket_channel_t *socket = (struct socket_channel_t *)io_get_priv_channel(io);

	setup(socket->serverPort, &(socket->serverAddr), &(socket->clientSock));
	return 0;
}

static void socket_reset(struct io_channel_t *io)
{
	struct socket_channel_t *socket = (struct socket_channel_t *)io_get_priv_channel(io);

	/* for FPGA */
	do_write(&(socket->serverAddr), &(socket->clientSock), 0x7ffff000, 0); // set reset
	printf("Set RESET\n");
	do_write(&(socket->serverAddr), &(socket->clientSock), 0x7ffff000, 1); // release reset
	printf("Release RESET\n");
}

static void socket_do_write(struct io_channel_t *io, uint64_t addr, uint64_t data)
{
	struct socket_channel_t *socket = (struct socket_channel_t *)io_get_priv_channel(io);
//printf("%s: addr(0x%lx), data(0x%lx) ... \n", __func__, addr, data);

	do_write(&(socket->serverAddr), &(socket->clientSock), addr, data);
}

static void socket_do_write_burst(struct io_channel_t *io, uint64_t addr, uint64_t *data, int len)
{
	struct socket_channel_t *socket = (struct socket_channel_t *)io_get_priv_channel(io);

	do_write_burst(&(socket->serverAddr), &(socket->clientSock), addr, data, len);
}

static uint64_t socket_do_read(struct io_channel_t *io, uint64_t addr)
{
	struct socket_channel_t *socket = (struct socket_channel_t *)io_get_priv_channel(io);

	return do_read(&(socket->serverAddr), &(socket->clientSock), addr);
}

static void socket_close(struct io_channel_t *io)
{
	struct socket_channel_t *socket = (struct socket_channel_t *)io_get_priv_channel(io);

	free(socket->io.ops);
	socket->io.ops = NULL;
	free(socket);
}

#if 0 /* different C standard version */
 /* for  socket */
static struct io_ops_t io_ops = {
	.parse_args = socket_parse_args,
	.setup = socket_setup,
	.do_status_check = NULL,
	.do_write = socket_do_write,
	.do_write_burst = socket_do_write_burst,
	.do_read = socket_do_read,
	.close = socket_close,
};
#else
static void io_ops_init(struct io_ops_t *ops)
{
	ops->parse_args = socket_parse_args;
	ops->setup = socket_setup;
	ops->reset = socket_reset;
	ops->do_status_check = NULL;
	ops->do_write = socket_do_write;
	ops->do_write_burst = socket_do_write_burst;
	ops->do_read = socket_do_read;
	ops->close = socket_close;
}
#endif

static struct socket_channel_t socket_channel;

struct io_channel_t * io_open(void)
{
	struct io_ops_t *ops = (struct io_ops_t *)malloc(sizeof(*ops));
	if (NULL == ops)
		return NULL;

	struct socket_channel_t *socket = (struct socket_channel_t *)malloc(sizeof(*socket));
	if (NULL == socket) {
		free(ops);
		return NULL;
	}
	memset(socket, 0, sizeof(*socket));

	io_ops_init(ops);
	socket->io.ops = ops;
	socket->io.priv = socket;

	return &(socket->io);
}

