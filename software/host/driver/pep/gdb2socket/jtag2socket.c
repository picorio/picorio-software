#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <iostream>
#include <unistd.h>

#include "debug_tick.h"
#include "gdb2socket/remote_bitbang.h"
#include "gdb2socket/fake_dmstatus.h"
#include "socket_client.h"
#include "common-socket.h"
#include "common.h"
#include "pygmy_es1y_addr_translater.h"

#include "../../../beak/riscv/opcodes.h"
#include "../../../beak/riscv/encoding.h"
#include "../../../beak/riscv/debug_defines.h"
#include "../../../beak/riscv/debug_rom_defines.h"
#include "gdb2socket/jtag2socket.h"

static time_t ltime;

static void process_reset(struct sockaddr_in *serverAddr, int *clientSock)
{
	do_write(serverAddr, clientSock, 0x7ffff000, 0); // set reset
	//printf("Set RESET\n");
	do_write(serverAddr, clientSock, 0x7ffff000, 1); // release reset
	//printf("Release RESET\n");
}


bool bp_hit(bp_t* bp, uint32_t addr, unsigned *index, unsigned empty_index) {
	for (int i=0; i<4; i++) {
		if (bp[i].addr == addr) {
			if (bp[i].enable) {
				//there is an enabled breakpoint
				*index = i; 
				printf("--HART0 ::: hit breakpoint = %d, addr = %x\n", *index, addr);
				return true; 
			} else {
				//there is an disabled breakpoint
				//*index = i;
				return false; 
			}
		}
	}

	*index = empty_index; //index = 4 for new breakpoint, move it to empty index
	//printf("--HART0 ::: not exist breakpoint = %d, addr = %x\n", *index, addr);
	return false;
}

void bp_push(bp_t* bp, bp_t* bp_new, unsigned index, unsigned* empty_index) {
	//printf("--HART0 ::: push breakpiont %d\n", index);
	bp[index] = *bp_new;

	if (*empty_index >= 3) 
		*empty_index = 0;
	else 
		*empty_index = *empty_index + 1; 
}

void bp_del(bp_t* bp, unsigned index, unsigned* empty_index) {

	//printf("--HART0 ::: delete breakpoint %d \n", index);
	for (int i=0; i<4; i++) {
		//move
		if (i == 3) {
			bp[i] = {0};
		} else if (i >= index) {
			//printf("--HART0 ::: move = %d\n", i);
			bp[i] = bp[i+1];
		}
	}
	*empty_index = *empty_index - 1; 
}

void bp_set(struct sockaddr_in *serverAddr, int *clientSock, bp_t* bp) {
	if (bp[0].enable) {
		do_write(serverAddr, clientSock, STATION_ORV32_S2B_BP_PC_0_ADDR, bp[0].addr);
		do_write(serverAddr, clientSock, STATION_ORV32_S2B_EN_BP_PC_0_ADDR, 1);
		printf("--HART0 ::: set breakpoint %d, addr = %x\n", 0, bp[0].addr);
	} else {
		do_write(serverAddr, clientSock, STATION_ORV32_S2B_BP_PC_0_ADDR, 0);
		do_write(serverAddr, clientSock, STATION_ORV32_S2B_EN_BP_PC_0_ADDR, 0);
		printf("--HART0 ::: remove breakpoint %d\n", 0);
	}

	if (bp[1].enable) {
		do_write(serverAddr, clientSock, STATION_ORV32_S2B_BP_PC_1_ADDR, bp[1].addr);
		do_write(serverAddr, clientSock, STATION_ORV32_S2B_EN_BP_PC_1_ADDR, 1);
		printf("--HART0 ::: set breakpoint %d, addr = %x\n", 1, bp[1].addr);
	} else {
		do_write(serverAddr, clientSock, STATION_ORV32_S2B_BP_PC_1_ADDR, 0);
		do_write(serverAddr, clientSock, STATION_ORV32_S2B_EN_BP_PC_1_ADDR, 0);
		printf("--HART0 ::: remove breakpoint %d\n", 1);
	}

	if (bp[2].enable) {
		do_write(serverAddr, clientSock, STATION_ORV32_S2B_BP_PC_2_ADDR, bp[2].addr);
		do_write(serverAddr, clientSock, STATION_ORV32_S2B_EN_BP_PC_2_ADDR, 1);
		printf("--HART0 ::: set breakpoint %d, addr = %x\n", 2, bp[2].addr);
	} else {
		do_write(serverAddr, clientSock, STATION_ORV32_S2B_BP_PC_2_ADDR, 0);
		do_write(serverAddr, clientSock, STATION_ORV32_S2B_EN_BP_PC_2_ADDR, 0);
		printf("--HART0 ::: remove breakpoint %d\n", 2);
	}

	if (bp[3].enable) {
		do_write(serverAddr, clientSock, STATION_ORV32_S2B_BP_PC_3_ADDR, bp[3].addr);
		do_write(serverAddr, clientSock, STATION_ORV32_S2B_EN_BP_PC_3_ADDR, 1);
		printf("--HART0 ::: set breakpoint %d, addr = %x\n", 3, bp[3].addr);
	} else {
		do_write(serverAddr, clientSock, STATION_ORV32_S2B_BP_PC_3_ADDR, 0);
		do_write(serverAddr, clientSock, STATION_ORV32_S2B_EN_BP_PC_3_ADDR, 0);
		printf("--HART0 ::: remove breakpoint %d\n", 3);
	}
}

void enter_debug_mode(dcsr_t *dcsr, uint8_t cause)
{
	dcsr->cause = cause;
  	dcsr->prv = PRV_M;
}

uint16_t read_mem16(struct sockaddr_in *serverAddr, int *clientSock, uint32_t addr)
{
	int mux;
	uint64_t data;
	uint16_t data16;

	if ((addr & 0x1) == 0) {
		mux = (addr >> 1) & 0x3; 
	} else {
		//printf("read mem16 address misalign\n");
	}
	addr &= ~0x7;

	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_ADDR_ADDR, addr);
	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_REQ_TYPE_ADDR, 0);
	data = do_read(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_RD_DATA_ADDR);

	if (mux == 0)
		data16 = data & 0xffff;
	else if (mux == 1)
		data16 = (data >> 16) & 0xffff;
	else if (mux == 2)
		data16 = (data >> 32) & 0xffff;
	else
		data16 = (data >> 48) & 0xffff;

	return data16;
}

void write_mem16(struct sockaddr_in *serverAddr, int *clientSock, uint32_t addr, uint16_t value)
{
	uint64_t data;
	uint64_t value_ext = value;
	bool mux;

	if ((addr & 0x1) == 0) {
		mux = (addr >> 1) & 0x3; 
	} else {
		//printf("write mem16 address misalign\n");
	}
	addr &= ~0x7;

	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_ADDR_ADDR, addr);
	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_REQ_TYPE_ADDR, 0);
	data = do_read(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_RD_DATA_ADDR);

	if (mux == 0) {
		data = (data & ~0xffff) | value_ext;
	} else if (mux == 1) {
		data = (data & ~(0xffff << 16)) | value_ext << 16;
	} else if (mux == 2) {
		data = (data & ~(0xffff << 32)) | value_ext << 32;
	} else {
		data = (data & ~(0xffff << 48)) | value_ext << 48;
	}
	
	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_ADDR_ADDR, addr);
	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_REQ_TYPE_ADDR, 2);
	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_WR_DATA_ADDR, data);
}

uint32_t read_mem32(struct sockaddr_in *serverAddr, int *clientSock, uint32_t addr)
{
	bool mux;
	uint64_t data;
	uint32_t data32;

	if ((addr & 0x3) == 0) {
		mux = (addr >> 2) & 1;
	} else {
		//printf("read mem32 address misalign\n");
	}
	addr &= ~0x7;

	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_ADDR_ADDR, addr);
	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_REQ_TYPE_ADDR, 0);
	data = do_read(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_RD_DATA_ADDR);
	if (mux)
		data32 = (data >> 32) & 0xffffffff;
	else
		data32 = data & 0xffffffff;

	return data32;
}

void write_mem32(struct sockaddr_in *serverAddr, int *clientSock, uint32_t addr, uint32_t value)
{
	uint64_t data;
	uint64_t value_ext = value;
	bool mux;

	if ((addr& 0x3) == 0) {
		mux = (addr >> 2) & 1;
	} else {
		//printf("write mem32 address misalign\n");
	}
	addr &= ~0x7;

	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_ADDR_ADDR, addr);
	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_REQ_TYPE_ADDR, 0);
	data = do_read(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_RD_DATA_ADDR);
	if (mux) {
		data = (data & 0xffffffff) | value_ext << 32;
	} else {
		data = (data & ~0xffffffff) | value_ext;
	}

	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_ADDR_ADDR, addr);
	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_REQ_TYPE_ADDR, 2);
	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_WR_DATA_ADDR, data);
}

uint64_t read_mem64(struct sockaddr_in *serverAddr, int *clientSock, uint32_t addr)
{
	uint64_t data;

	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_ADDR_ADDR, addr);
	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_REQ_TYPE_ADDR, 0);
	data = do_read(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_RD_DATA_ADDR);
	return data;
}

void write_mem64(struct sockaddr_in *serverAddr, int *clientSock, uint32_t addr, uint32_t value)
{
	uint64_t data;

	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_ADDR_ADDR, addr);
	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_REQ_TYPE_ADDR, 2);
	do_write(serverAddr, clientSock, STATION_DMA_DMA_DEBUG_WR_DATA_ADDR, data);
}

void release_orv32_reset(struct sockaddr_in *serverAddr, int *clientSock)
{
    ltime = time(NULL);
    fprintf(stderr, "Start Releasing ORV32 Reset @ %s\n", asctime(localtime(&ltime)));
    uint64_t rdata;
    write_mem32(serverAddr, clientSock, STATION_ORV32_S2B_EARLY_RSTN_ADDR, 1);
    rdata = read_mem32(serverAddr, clientSock, STATION_SLOW_IO_SCU_S2ICG_BLOCKS_ADDR);
    write_mem32(serverAddr, clientSock, STATION_SLOW_IO_SCU_S2ICG_BLOCKS_ADDR, (rdata | 0x4));
	//printf("--release ::: write Addr 0x%llx with Data 0x%llx\n", STATION_SLOW_IO_SCU_S2ICG_BLOCKS_ADDR, (rdata | 0x4));
    rdata = read_mem32(serverAddr, clientSock, STATION_SLOW_IO_SCU_S2FRST_BLOCKS_ADDR);
    write_mem32(serverAddr, clientSock, STATION_SLOW_IO_SCU_S2FRST_BLOCKS_ADDR, (rdata | 0x4));
	//printf("--release ::: write Addr 0x%llx with Data 0x%llx\n", STATION_SLOW_IO_SCU_S2FRST_BLOCKS_ADDR,(rdata | 0x4));
    rdata = 0;
    while (((rdata >> 2) & 0x1) != 0x1) {
      rdata = read_mem32(serverAddr, clientSock, STATION_SLOW_IO_SCU_FRST2S_BLOCKS_ADDR);
	  //printf("--release ::: read Addr 0x%llx with Data 0x%llx\n", STATION_SLOW_IO_SCU_FRST2S_BLOCKS_ADDR, rdata);
    }
    ltime = time(NULL);
    fprintf(stderr, "Done Releasing ORV32 Reset @ %s\n", asctime(localtime(&ltime)));
}


int main (int argc, char const *argv[])
{
	std::string status;
	debug_module_t*	  dm  = NULL;jtag_dtm_t*		  tap = NULL;
	remote_bitbang_t* rbb = NULL;

	int            state = 0; //0: IDLE, 1: WR_ADDR, 2:WR_DATA 3:RD
	int            rcvd_addr = 0;
	int            rcvd_data = 0;
	uint64_t       data = 0;
	uint64_t       addr = 0;
	uint64_t       ret_data = 0;
	uint64_t       tmp_data = 0;

	int            dma_cmd_vld = 0;
	int            is_incremental = 0;
	uint64_t       burst_data[256];
	int            is_burst = 0;
	int            burst_idx = 0;
	uint8_t*	   debug_rom[4];
	uint8_t		   init_rom = 1;
	uint8_t*	   set_debug_rom = &init_rom;
	dcsr_t		   dcsr = {0};
	bp_t		   bp[10] = {0};
	uint32_t	   _pc = 0;
	unsigned	   bp_run_hit = 0;
	bool		   step_run_hit = false;
	unsigned	   idx;
	unsigned	   empty_idx = 0;
	unsigned	   just_update_pc = 0;
	int			   release = 0;

	unsigned short serverPort = 0;
	struct sockaddr_in serverAddr;
	int clientSock;
	for (int i = 1; i < argc; i++) {
	if (strncmp(argv[i], "serverPort=", 11) == 0) {
	  serverPort = atoi(argv[i] + 11);
	}
	}
	if (serverPort == 0) {
	serverPort = 9900; // Default
	}
	//printf ("serverPort = %0d\n", serverPort);
	setup(serverPort, &serverAddr, &clientSock);
	//printf ("ready to new rbb\n");
	//dm = dm_new();
	//dm_new(dm);
    //rbb = rbb_new();
	////printf("dm function::::%p\n", &(dm->dmi_read));
	
	if (dm == NULL) {
		dm = new debug_module_t();
	}
	if (tap == NULL) {
		tap = new jtag_dtm_t(dm, 0);
	}
	if (rbb == NULL) {
		rbb = new remote_bitbang_t(tap);
	}
	//rbb->tap->set_debug_module(dm);

	//printf("rbb-tap-dm::::%p\n", rbb);
	//printf("rbb-tap-dm::::%p\n", rbb->tap);
	//printf("rbb-tap-dm::::%p\n", rbb->tap->dm);

	//do_status_check(&serverAddr, &clientSock);

	printf ("finish to new rbb\n");

	while (1) {
		//open a remote_bitbang server to connect with openocd
		rbb->tick();

		//to check where the program goes
		//when breakpoint or step is enbale
		//we need to know if the core is halted
		if (!dm->hart_state[0].halted) {
			if ((bp[0].enable || bp[1].enable || bp[2].enable || bp[3].enable) && !bp_run_hit) {
				uint32_t temp_pc = do_read(&serverAddr, &clientSock, STATION_ORV32_IF_PC_ADDR); 
				unsigned temp_idx;
				if (bp_hit(bp, temp_pc - 4 , &temp_idx, empty_idx)) {
					dm->store(DEBUG_ROM_HALTED, 4, set_debug_rom);
					printf("--HART0 ::: breakpoint %d halted @ addr = %x\n", temp_idx, (temp_pc - 4));
					printf("-----------------------HART BREAKPOINT HALTED-----------------------\n");

					enter_debug_mode(&dcsr, DCSR_CAUSE_SWBP);
					bp_run_hit = temp_idx + 1;
					_pc = temp_pc;
				}
			} else if (dcsr.step && !step_run_hit) {
				uint32_t temp_pc = do_read(&serverAddr, &clientSock, STATION_ORV32_IF_PC_ADDR); 
				if (_pc != temp_pc)
					dm->store(DEBUG_ROM_HALTED, 4, set_debug_rom);
					printf("-----------------------HART STEP HALTED-----------------------\n");
					enter_debug_mode(&dcsr, DCSR_CAUSE_STEP);

					step_run_hit = true;
					_pc = temp_pc;
			}
		}

		if (dm->dmcontrol.haltreq && !dm->hart_state[0].halted) {
			int stall = 1;
			do_write(&serverAddr, &clientSock, STATION_ORV32_DEBUG_INFO_ENABLE_ADDR, 1);
			do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_DEBUG_STALL_ADDR, 1);
			printf("--HART0 ::: halted, Addr 0x%llx with Data 0x%llx\n", STATION_ORV32_S2B_DEBUG_STALL_ADDR, 1);

			//release reset
			while (!stall) {
				stall = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_DEBUG_STALL_ADDR);
			}
			//do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_RSTN_ADDR, 1); // release reset
			if (!release) { 
				release = 1;
				release_orv32_reset(&serverAddr, &clientSock);
			}
			//new method to release reset, refer to fesvr
			printf("-----------------------HART HAS HALTED-----------------------\n");

			uint64_t data = 0;
			uint32_t cnt = 0;
			while (cnt < 256) {
				data = do_read(&serverAddr, &clientSock, STATION_ORV32_B2S_DEBUG_STALL_OUT_ADDR);
				//printf("--read::: read Addr 0x%llx with Data 0x%llx\n", STATION_ORV32_B2S_DEBUG_STALL_OUT_ADDR, data);
				if (data != 0) {
					dm->store(DEBUG_ROM_HALTED, 4, set_debug_rom);
					enter_debug_mode(&dcsr, DCSR_CAUSE_DEBUGINT);
					break;
				}
				cnt ++;//timeout when wait 256 cycles
			}
		}
		if (dm->dmcontrol.resumereq & !dm->hart_state[0].resumeack) {
			if (!dm->hart_state[0].halted)
				printf("--HART0 ::: the hart is not halted\n");

			//if step flag is set, keep stall flag = 1;
			if (!dcsr.step) {
				do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_DEBUG_STALL_ADDR, 0);
			} else {
				do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_DEBUG_STALL_ADDR, 1);
			}
			do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_DEBUG_RESUME_ADDR, 1);
			printf("--HART0 ::: resume, Addr 0x%llx with Data 0x%llx\n", STATION_ORV32_S2B_DEBUG_RESUME_ADDR, 1);

			//if step flag is set, we need to notice DM that target is halted after resume ack;
			dm->store(DEBUG_ROM_RESUMING, 4, set_debug_rom);
			bp_run_hit = 0;
			step_run_hit = false;
			printf("-----------------------HART HAS RESUMED-------------------------\n");
		}

		// Park loop to check if abstract command go flag is assert.
		// Read abstract data or program buffer(not supported yet).
		// Then clean the flag.
		if ((dm->debug_rom_flags[0] & (1 << DEBUG_ROM_FLAG_GO)) == 1) {

			//printf("--HART0 ::: get abstract from debug_rom_flag\n");
			int i = 0;

			//move dm_csr to dm_rom for cpu access
			//but we don't have dm_rom now, just bypass the data to dest;
			//dm->store(dm->debug_data_start, 4, dm->dmdata);
			uint32_t command;
			uint32_t fake_s0 = 0;
			uint32_t fake_s1 = 0;
			uint32_t match_opcode = 0;
			uint32_t imm12 = 0;
			uint32_t imm5  = 0;
			uint32_t imm7  = 0;
			uint32_t rd	   = 0;
			uint32_t rs1   = 0;
			uint32_t rs2   = 0;

			while ((dm->read32(dm->debug_abstract, i) != MATCH_EBREAK) && (i <= 48)) {
				////printf("HART0 go ::: get abstract from debug_rom_flag, index=%d\n",i);

				command = dm->read32(dm->debug_abstract, i);
				printf("--HART0 ::: command = 0x%x\n", command);
				match_opcode = command & 0xffff;
				imm12 = (command >> 20) & 0xfff;

				if ((match_opcode & MASK_LH) == MATCH_LH) {
					//we don't have dm_rom now, just bypass the data to dest;

					rd = (command >> 7) & 0x1f;
					imm12 = (command >> 20) & 0xfff;

					if (imm12 >= 0x380 && imm12 < 0x384) {
						fake_s0 = dm->read32(dm->dmdata, 0);
						//printf("--HART0 ::: excuting abstract lh to dmdata0 = 0x%x\n", fake_s0);
					} else if (imm12 >= 0x384 && imm12 < 0x388) {
						fake_s1 = dm->read32(dm->dmdata, 1);
						//printf("--HART0 ::: excuting abstract lh to dmdata1 = 0x%x\n", fake_s1);
					} else {
						fake_s0 = read_mem16(&serverAddr, &clientSock, fake_s1);
						printf("--HART0 ::: read16 dma addr = 0x%x, data = 0x%x\n",fake_s1, fake_s0);
					}
				} else if ((match_opcode & MASK_SH) == MATCH_SH) {
					//we don't have dm_rom now, just bypass the data to dest;

					imm5 = (command >> 7) & 0x1f;
					imm7 = (command >> 25) & 0x3f;
					imm12 = (imm7 << 5 | imm5) & 0xfff;

					if (imm12 >= 0x380 && imm12 < 0x384) {
						//printf("--HART0 ::: excuting abstract sh to dmdata0 = 0x%x\n", fake_s0);
						dm->write32(dm->dmdata, 0 , fake_s0);
					} else if (imm12 >= 0x384 && imm12 < 0x388) {
						//printf("--HART0 ::: excuting abstract sh to dmdata1 = 0x%x\n", fake_s1);
						dm->write32(dm->dmdata, 1 , fake_s1);
					} else {
						printf("--HART0 ::: write16 dma addr = 0x%x, data = 0x%x\n", fake_s1, fake_s0);
						if (bp[4].data == 0x73) {
							if (fake_s1 == bp[4].addr + 2) {
								if (fake_s0 == 0x10) {
									if (bp_hit(bp, bp[4].addr, &idx, empty_idx)) {
										printf("--HART0 ::: duplicated beakpoint %d, addr = %x\n", idx, bp[4].addr);
									} else {
										bp_push(bp, &bp[4], idx, &empty_idx);
										bp_set(&serverAddr, &clientSock, bp);
									}
								} else {
									write_mem16(&serverAddr, &clientSock, bp[4].addr, bp[4].data);
									write_mem16(&serverAddr, &clientSock, fake_s1, fake_s0);

									//delete breakpoint
									if (bp_hit(bp, bp[4].addr, &idx, empty_idx)) {
										bp_del(bp, idx, &empty_idx);
										bp_set(&serverAddr, &clientSock, bp);
										printf("--HART0 ::: delete beakpoint %d, addr = %x\n", idx, bp[4].addr);
									}
								}

							} else {
								write_mem16(&serverAddr, &clientSock, bp[4].addr, bp[4].data);
								write_mem16(&serverAddr, &clientSock, fake_s1, fake_s0);
							}
							bp[4] = {0};
						} else if (fake_s0 == 0x73) {
							//gdb wants to write ebeak to debug_rom
							bp[4].addr = fake_s1;
							bp[4].data = fake_s0;
							bp[4].enable = 1;
							printf("--HART0 ::: seem like a ebreak, store internal first\n");

						} else {
							write_mem16(&serverAddr, &clientSock, fake_s1, fake_s0);

							//delete breakpoint
							if (bp_hit(bp, fake_s1, &idx, empty_idx)) {
								bp_del(bp, idx, &empty_idx);
								bp_set(&serverAddr, &clientSock, bp);
								printf("--HART0 ::: delete beakpoint %d, addr = %x\n", idx, fake_s1);
							}
						}
					}

				} else if ((match_opcode & MASK_LW) == MATCH_LW) {
					//we don't have dm_rom now, just bypass the data to dest;

					rd = (command >> 7) & 0x1f;
					imm12 = (command >> 20) & 0xfff;

					if (imm12 >= 0x380 && imm12 < 0x384) {
						fake_s0 = dm->read32(dm->dmdata, 0);
						//printf("--HART0 ::: excuting abstract lw to dmdata0 = 0x%x\n", fake_s0);
					} else if (imm12 >= 0x384 && imm12 < 0x388) {
						fake_s1 = dm->read32(dm->dmdata, 1);
						//printf("--HART0 ::: excuting abstract lw to dmdata1 = 0x%x\n", fake_s1);
					} else {
						fake_s0 = read_mem32(&serverAddr, &clientSock, fake_s1);
						printf("--HART0 ::: read32 dma addr = 0x%x, data = 0x%x\n",fake_s1, fake_s0);
					}

				} else if ((match_opcode & MASK_SW) == MATCH_SW) {
					//we don't have dm_rom now, just bypass the data to dest;

					imm5 = (command >> 7) & 0x1f;
					imm7 = (command >> 25) & 0x3f;
					imm12 = (imm7 << 5 | imm5) & 0xfff;

					if (imm12 >= 0x380 && imm12 < 0x384) {
						//printf("--HART0 ::: excuting abstract sw to dmdata0 = 0x%x\n", fake_s0);
						dm->write32(dm->dmdata, 0 , fake_s0);
					} else if (imm12 >= 0x384 && imm12 < 0x388) {
						//printf("--HART0 ::: excuting abstract sw to dmdata1 = 0x%x\n", fake_s1);
						dm->write32(dm->dmdata, 1 , fake_s1);
					} else {
						printf("--HART0 ::: writ32 dma addr = 0x%x, data = 0x%x\n", fake_s1, fake_s0);
						write_mem32(&serverAddr, &clientSock, fake_s1, fake_s0);
					}
				} else if ((match_opcode & MASK_CSRRW) == MATCH_CSRRW) {
					if (imm12 == CSR_DPC){
						//printf("--HART0 ::: write to csr == 0x%x\n", CSR_DPC);
						//set station pc
						do_write(&serverAddr, &clientSock, STATION_ORV32_S2B_CFG_RST_PC_ADDR, fake_s0);
						just_update_pc = 3;
						printf("--HART0 ::: write to csr == 0x%x, address = 0x%x\n", CSR_DPC, fake_s0);
					} else if (imm12 == CSR_MSTATUS){
						//printf("--HART0 ::: write to csr == 0x%x\n", CSR_MSTATUS);
						do_write(&serverAddr, &clientSock, STATION_ORV32_MSTATUS_ADDR, fake_s0);
					} else if (imm12 == CSR_MISA){
						printf("--HART0 ::: write to read-only csr == 0x%x\n", CSR_MISA);
					} else if (imm12 == CSR_MCYCLE){
						//printf("--HART0 ::: write to csr == 0x%x\n", CSR_MCYCLE);
						do_write(&serverAddr, &clientSock, STATION_ORV32_MCYCLE_ADDR, fake_s0);
					} else if (imm12 == CSR_MINSTRET){
						//printf("--HART0 ::: write to csr == 0x%x\n", CSR_MINSTRET);
						do_write(&serverAddr, &clientSock, STATION_ORV32_MINSTRET_ADDR, fake_s0);
					} else if (imm12 == CSR_MEPC){
						//printf("--HART0 ::: write to csr == 0x%x\n", CSR_MEPC);
						do_write(&serverAddr, &clientSock, STATION_ORV32_MEPC_ADDR, fake_s0);
					} else if (imm12 == CSR_DCSR){
						//printf("--HART0 ::: write to csr == 0x%x\n", CSR_DCSR);
						dcsr.prv = get_field(fake_s0, DCSR_PRV);
					 	dcsr.step = get_field(fake_s0, DCSR_STEP);
					 	// TODO: ndreset and fullreset
					 	dcsr.ebreakm = get_field(fake_s0, DCSR_EBREAKM);
					 	dcsr.ebreakh = get_field(fake_s0, DCSR_EBREAKH);
					 	dcsr.ebreaks = get_field(fake_s0, DCSR_EBREAKS);
					 	dcsr.ebreaku = get_field(fake_s0, DCSR_EBREAKU);
					 	dcsr.halt = get_field(fake_s0, DCSR_HALT);
					} else {
						//printf("--HART0 ::: write to not supported csr == 0x%x\n", imm12);
					}

				} else if ((match_opcode & MASK_CSRRS) == MATCH_CSRRS) {
					if (imm12 == CSR_DPC){
						if (just_update_pc) {
							//when write to cfg_rst_pc, the read back pc = write_pc -4
							//I haven't figure out why
							fake_s0 = do_read(&serverAddr, &clientSock, STATION_ORV32_S2B_CFG_RST_PC_ADDR);
							//fake_s0 = fake_s0 + 4;
							just_update_pc --;
							printf("--HART0 ::: read S2B_CFG_RST_PC == 0x%x\n", fake_s0);
						} else if (dcsr.ebreakm && bp_run_hit) {
							fake_s0 = do_read(&serverAddr, &clientSock, STATION_ORV32_IF_PC_ADDR);
							fake_s0 = fake_s0 - 4;
							printf("--HART0 ::: breakpoint %d, read EBREAK_PC == 0x%x\n", bp_run_hit, fake_s0);
						} else {
							fake_s0 = do_read(&serverAddr, &clientSock, STATION_ORV32_IF_PC_ADDR);
							printf("--HART0 ::: read IF_PC == 0x%x\n", fake_s0);
						}
					} else if (imm12 == CSR_MSTATUS){
						fake_s0 = do_read(&serverAddr, &clientSock, STATION_ORV32_MSTATUS_ADDR);
						//printf("--HART0 ::: read MSTATUS == 0x%x\n", fake_s0);
					} else if (imm12 == CSR_MISA){
						fake_s0 = 0x40001104;
					} else if (imm12 == CSR_MCYCLE){
						fake_s0 = do_read(&serverAddr, &clientSock, STATION_ORV32_MCYCLE_ADDR);
						//printf("--HART0 ::: read MCYCLE == 0x%x\n", fake_s0);
					} else if (imm12 == CSR_MINSTRET){
						fake_s0 = do_read(&serverAddr, &clientSock, STATION_ORV32_MINSTRET_ADDR);
						//printf("--HART0 ::: read MINSTRET == 0x%x\n", fake_s0);
					} else if (imm12 == CSR_MEPC){
						fake_s0 = do_read(&serverAddr, &clientSock, STATION_ORV32_MEPC_ADDR);
						//printf("--HART0 ::: read MEPC == 0x%x\n", fake_s0);
					} else if (imm12 == CSR_DCSR){
						//printf("--HART0 ::: read to csr == 0x%x\n", CSR_DCSR);
						fake_s0 = set_field(fake_s0, DCSR_XDEBUGVER, 1);
						fake_s0 = set_field(fake_s0, DCSR_EBREAKM, dcsr.ebreakm);
						fake_s0 = set_field(fake_s0, DCSR_EBREAKH, dcsr.ebreakh);
						fake_s0 = set_field(fake_s0, DCSR_EBREAKS, dcsr.ebreaks);
						fake_s0 = set_field(fake_s0, DCSR_EBREAKU, dcsr.ebreaku);
						fake_s0 = set_field(fake_s0, DCSR_STOPCYCLE, 0);
						fake_s0 = set_field(fake_s0, DCSR_STOPTIME, 0);
						fake_s0 = set_field(fake_s0, DCSR_CAUSE, dcsr.cause);
						fake_s0 = set_field(fake_s0, DCSR_STEP, dcsr.step);
						fake_s0 = set_field(fake_s0, DCSR_PRV, dcsr.prv);
					} else {
						//printf("--HART0 ::: not supported csr == 0x%x\n", imm12);
					}
				}

				i++;
			}
			//finish abstracts command
			dm->store(DEBUG_ROM_GOING, 4, set_debug_rom);
			dm->store(DEBUG_ROM_HALTED, 4, set_debug_rom);
		}

	}
   	

}
