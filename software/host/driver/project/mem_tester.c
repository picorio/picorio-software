#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <iostream>

#include "ftd2xx.h"
#include "libft4222.h"
#include "debug_tick.h"
#include "common.h"
#include "pygmy_es1_addr_translater.h"

#define LOOP_COUNT 1000000

int mem_test (FT4222_SPIMode mode, FT_HANDLE ftHandle, std::string target) {
  uint64_t addr;
  uint64_t data;
  uint64_t rcvd;
  for (int i = 0; i < LOOP_COUNT; i++) {
    addr = (random() % 0xfffff) << 3;
//    addr = (i % 0xfffff) << 3;
    data = ((0x0ul + random()) << 32) + random();
    fprintf(stderr, "Writing 0x%lx to address 0x%lx\n", data, addr);
    do_write(mode, ftHandle, addr2obaddr(target, addr), data);
    rcvd = do_read(mode, ftHandle, addr2obaddr(target, addr));
    fprintf(stderr, "Reading address 0x%lx, get 0x%lx\n", addr, rcvd);
    if (rcvd != data) {
      fprintf(stderr, ">>> FAIL at address 0x%lx\n", addr);
      return 1;
    } else {
      fprintf(stderr, ">>> PASS at address 0x%lx\n", addr);
    }
  }
  return 0;
}

int main (int argc, char const *argv[])
{
  unsigned seed = 1; // Default seed
  int result;
  std::string status;
  FT4222_SPIMode mode = SPI_IO_SINGLE;

  FT_HANDLE ftHandle = setup(mode, CLK_DIV_64, CLK_IDLE_LOW, CLK_LEADING, 1);
  if (ftHandle == NULL)
    return 0;

  do_status_check(mode, ftHandle);

  if (argc < 2) {
    // Print Usage
  } else {
    for (int i = 0; i < argc; i++) {
      if (std::strncmp(argv[i], "seed=", 5) == 0) {
        seed = strtol(argv[i] + 5, NULL, 16);
        break;
      }
    }

    fprintf(stderr, "Using seed = %x\n", seed);
    srand(seed);
    for (int i = 0; i < argc; i++) {
      if (std::strcmp(argv[i], "l2") == 0) {
        fprintf(stderr, "Start testing using L2DA\n");
        // Drop L2 Reset
        do_write(mode, ftHandle, 0x8000000000 + 8 * 0x1a, 0x0);
        result = mem_test(mode, ftHandle, "l2_data");
        if (result != 0x0) {
          fprintf(stderr, "Test L2 FAILED!\n");
        } else {
          fprintf(stderr, "Test L2 PASSED!\n");
        }
      } else if (std::strcmp(argv[i], "dt") == 0) {
        // Drop L2 Reset
        do_write(mode, ftHandle, 0x8000000000 + 8 * 0x1a, 0x0);
        fprintf(stderr, "Start testing using DT\n");
        result = mem_test(mode, ftHandle, "dt");
        if (result != 0x0) {
          fprintf(stderr, "Test DT FAILED!\n");
        } else {
          fprintf(stderr, "Test DT PASSED!\n");
        }
      } else if (std::strcmp(argv[i], "dma") == 0) {
        // Drop L2 Reset
        do_write(mode, ftHandle, 0x8000000000 + 8 * 0x1a, 0x0);
        fprintf(stderr, "Start testing using DMA\n");
        result = mem_test(mode, ftHandle, "dma");
        if (result != 0x0) {
          fprintf(stderr, "Test DMA FAILED!\n");
        } else {
          fprintf(stderr, "Test DMA PASSED!\n");
        }
      }
    }
  }
    

  FT4222_UnInitialize(ftHandle);
  FT_Close(ftHandle);
  return result;
}
