#include <string.h>
#include <math.h> // TODO: floor
#include "FreeRTOS.h"
#include "common.h"
#include "rtlgen.h"
#include "hal.h"
#include "clib.h"

#define DW_IC_CON		0x0
#define DW_IC_TAR		0x4
#define DW_IC_SAR		0x8
#define DW_IC_HS_MADDR		0xc
#define DW_IC_DATA_CMD		0x10
#define DW_IC_SS_SCL_HCNT	0x14
#define DW_IC_SS_SCL_LCNT	0x18
#define DW_IC_FS_SCL_HCNT	0x1c
#define DW_IC_FS_SCL_LCNT	0x20
#define DW_IC_HS_SCL_HCNT	0x24
#define DW_IC_HS_SCL_LCNT	0x28
#define DW_IC_INTR_STAT		0x2c
#define DW_IC_INTR_MASK		0x30
#define DW_IC_RAW_INTR_STAT	0x34
#define DW_IC_RX_TL		0x38
#define DW_IC_TX_TL		0x3c
#define DW_IC_CLR_INTR		0x40
#define DW_IC_CLR_RX_UNDER	0x44
#define DW_IC_CLR_RX_OVER	0x48
#define DW_IC_CLR_TX_OVER	0x4c
#define DW_IC_CLR_RD_REQ	0x50
#define DW_IC_CLR_TX_ABRT	0x54
#define DW_IC_CLR_RX_DONE	0x58
#define DW_IC_CLR_ACTIVITY	0x5c
#define DW_IC_CLR_STOP_DET	0x60
#define DW_IC_CLR_START_DET	0x64
#define DW_IC_CLR_GEN_CALL	0x68
#define DW_IC_ENABLE		0x6c
#define DW_IC_STATUS		0x70
#define DW_IC_TXFLR		0x74
#define DW_IC_RXFLR		0x78
#define DW_IC_SDA_HOLD		0x7c
#define DW_IC_TX_ABRT_SOURCE	0x80
#define DW_IC_ENABLE_STATUS	0x9c
#define DW_IC_FS_SPKLEN		0xa0
#define DW_IC_HS_SPKLEN		0xa4
#define DW_IC_CLR_RESTART_DET	0xa8
#define DW_IC_COMP_PARAM_1	0xf4
#define DW_IC_COMP_VERSION	0xf8

#define DW_IC_CON_MASTER		0x1
#define DW_IC_CON_SPEED_STD		0x2
#define DW_IC_CON_SPEED_FAST		0x4
#define DW_IC_CON_SPEED_HIGH		0x6
#define DW_IC_CON_SPEED_MASK		0x6
#define DW_IC_CON_10BITADDR_SLAVE		0x8
#define DW_IC_CON_10BITADDR_MASTER	0x10
#define DW_IC_CON_RESTART_EN		0x20
#define DW_IC_CON_SLAVE_DISABLE		0x40
#define DW_IC_CON_STOP_DET_IFADDRESSED		0x80
#define DW_IC_CON_TX_EMPTY_CTRL		0x100
#define DW_IC_CON_RX_FIFO_FULL_HLD_CTRL		0x200

#define DW_IC_STATUS_ACTIVITY		0x1
#define DW_IC_STATUS_TFE		BIT(2)
#define DW_IC_STATUS_MASTER_ACTIVITY	BIT(5)
#define DW_IC_STATUS_SLAVE_ACTIVITY	BIT(6)

#define DW_IC_INTR_RX_UNDER	0x001
#define DW_IC_INTR_RX_OVER	0x002
#define DW_IC_INTR_RX_FULL	0x004
#define DW_IC_INTR_TX_OVER	0x008
#define DW_IC_INTR_TX_EMPTY	0x010
#define DW_IC_INTR_RD_REQ	0x020
#define DW_IC_INTR_TX_ABRT	0x040
#define DW_IC_INTR_RX_DONE	0x080
#define DW_IC_INTR_ACTIVITY	0x100
#define DW_IC_INTR_STOP_DET	0x200
#define DW_IC_INTR_START_DET	0x400
#define DW_IC_INTR_GEN_CALL	0x800
#define DW_IC_INTR_RESTART_DET	0x1000

#define DW_IC_INTR_DEFAULT_MASK		(DW_IC_INTR_RX_FULL | \
					 DW_IC_INTR_TX_ABRT | \
					 DW_IC_INTR_STOP_DET)
#define DW_IC_INTR_MASTER_MASK		(DW_IC_INTR_DEFAULT_MASK | \
					 DW_IC_INTR_TX_EMPTY)
#define DW_IC_INTR_SLAVE_MASK		(DW_IC_INTR_DEFAULT_MASK | \
					 DW_IC_INTR_RX_DONE | \
					 DW_IC_INTR_RX_UNDER | \
					 DW_IC_INTR_RD_REQ)

#define STATUS_IDLE			0x0
#define STATUS_WRITE_IN_PROGRESS	0x1
#define STATUS_READ_IN_PROGRESS		0x2

#define I2C_CLK					FREQ_PCLK

enum{
	I2C_FREQ_SS			= 100000,
	I2C_FREQ_FS			= 400000,
	I2C_FREQ_FSPLUS		= 1000000,
	I2C_FREQ_HS			= 3400000,
};
#define I2C_FREQ_FS_IS_PLUS	0

static unsigned int i2c_dw_scl_lcnt(unsigned int ic_clk, unsigned int tLOW, unsigned int tf, int offset)
{
	/*
	 * Conditional expression:
	 *
	 *   IC_[FS]S_SCL_LCNT + 1 >= IC_CLK * (tLOW + tf)
	 *
	 * DW I2C core starts counting the SCL CNTs for the LOW period
	 * of the SCL clock (tLOW) as soon as it pulls the SCL line.
	 * In order to meet the tLOW timing spec, we need to take into
	 * account the fall time of SCL signal (tf).  Default tf value
	 * should be 0.3 us, for safety.
	 */

	int val = ((ic_clk * (tLOW + tf) + 500000) / 1000000) + offset;
	/* 
	The minimum valid value is 5; hardware prevents values less than this being written, and if attempted, results in 5 being set. 
	*/
	return (val < 7) ? 5 : (val - 1);
}

static unsigned int i2c_dw_scl_hcnt(unsigned int ic_clk, unsigned int tSYMBOL, unsigned int tf, int cond, int offset)
{
	int val;

	/*
	 * DesignWare I2C core doesn't seem to have solid strategy to meet
	 * the tHD;STA timing spec.  Configuring _HCNT based on tHIGH spec
	 * will result in violation of the tHD;STA spec.
	 */
	if (cond) {
		/*
		 * Conditional expression:
		 *
		 *   IC_[FS]S_SCL_HCNT + (1+4+3) >= IC_CLK * tHIGH
		 *
		 * This is based on the DW manuals, and represents an ideal
		 * configuration.  The resulting I2C bus speed will be
		 * faster than any of the others.
		 *
		 * If your hardware is free from tHD;STA issue, try this one.
		 */
		val = (ic_clk * tSYMBOL + 500000) / 1000000 + offset;
		/* 
		The minimum valid value is 6; hardware prevents values less than this being written, and if attempted results in 6 being set.
		*/
		return (val < 15) ? 6 : (val - 8);
	}
	/*
	 * Conditional expression:
	 *
	 *   IC_[FS]S_SCL_HCNT + 3 >= IC_CLK * (tHD;STA + tf)
	 *
	 * This is just experimental rule; the tHD;STA period turned
	 * out to be proportinal to (_HCNT + 3).  With this setting,
	 * we could meet both tHIGH and tHD;STA timing specs.
	 *
	 * If unsure, you'd better to take this alternative.
	 *
	 * The reason why we need to take into account "tf" here,
	 * is the same as described in i2c_dw_scl_lcnt().
	 */

	val = (ic_clk * (tSYMBOL + tf) + 500000) / 1000000 + offset;
	/*
	The minimum valid value is 6; hardware prevents values less than this being written, and if attempted results in 6 being set.
	*/
	return (val < 10) ? 6 : (val - 3);
}

#define I2C_SCL_LCNT(freq)	((I2C_CLK/(freq)) >> 1)

static unsigned int reg_i2c = STATION_SLOW_IO_I2C0_BLOCK_REG_ADDR;

struct i2c_dev_t {
	int					cmd_complete;
	unsigned int		status;

	unsigned int		tx_fifo_depth;
	unsigned int		rx_fifo_depth;	

	struct i2c_msg		*msgs;
	int					msgs_num;
	unsigned int		rx_buf_len;
	unsigned char		*rx_buf;
	unsigned char		*tx_buf;
	unsigned int		tx_buf_len;
	int					msg_write_idx;
	int					msg_read_idx;
	int					rx_outstanding;

	int (*master_xfer)(struct i2c_dev_t *, struct i2c_msg *, int);
};

static struct i2c_dev_t i2c_dev;

static unsigned int i2c_dw_read_clear_intrbits(void)
{
	unsigned int stat;

	/*
	 * The IC_INTR_STAT register just indicates "enabled" interrupts.
	 * Ths unmasked raw version of interrupt status bits are available
	 * in the IC_RAW_INTR_STAT register.
	 *
	 * That is,
	 *   stat = dw_readl(IC_INTR_STAT);
	 * equals to,
	 *   stat = dw_readl(IC_RAW_INTR_STAT) & dw_readl(IC_INTR_MASK);
	 *
	 * The raw version might be useful for debugging purposes.
	 */
	stat = read32(reg_i2c+DW_IC_INTR_STAT);

	/*
	 * Do not use the IC_CLR_INTR register to clear interrupts, or
	 * you'll miss some interrupts, triggered during the period from
	 * dw_readl(IC_INTR_STAT) to dw_readl(IC_CLR_INTR).
	 *
	 * Instead, use the separately-prepared IC_CLR_* registers.
	 */
	if (stat & DW_IC_INTR_RX_UNDER)
		read32(reg_i2c+DW_IC_CLR_RX_UNDER);
	if (stat & DW_IC_INTR_RX_OVER)
		read32(reg_i2c+DW_IC_CLR_RX_OVER);
	if (stat & DW_IC_INTR_TX_OVER)
		read32(reg_i2c+DW_IC_CLR_TX_OVER);
	if (stat & DW_IC_INTR_RD_REQ)
		read32(reg_i2c+DW_IC_CLR_RD_REQ);
	if (stat & DW_IC_INTR_TX_ABRT) {
		/*
		 * The IC_TX_ABRT_SOURCE register is cleared whenever
		 * the IC_CLR_TX_ABRT is read.  Preserve it beforehand.
		 */
		printf("TX_ABRT src: 0x%x\n", read32(reg_i2c+DW_IC_TX_ABRT_SOURCE));
		read32(reg_i2c+DW_IC_CLR_TX_ABRT);
	}
	if (stat & DW_IC_INTR_RX_DONE)
		read32(reg_i2c+DW_IC_CLR_RX_DONE);
	if (stat & DW_IC_INTR_ACTIVITY)
		read32(reg_i2c+DW_IC_CLR_ACTIVITY);
	if (stat & DW_IC_INTR_STOP_DET)
		read32(reg_i2c+DW_IC_CLR_STOP_DET);
	if (stat & DW_IC_INTR_START_DET)
		read32(reg_i2c+DW_IC_CLR_START_DET);
	if (stat & DW_IC_INTR_GEN_CALL)
		read32(reg_i2c+DW_IC_CLR_GEN_CALL);

	return stat;
}

static void
i2c_dw_read(struct i2c_dev_t *dev)
{
	struct i2c_msg *msgs = dev->msgs;
	int rx_valid;

	for (; dev->msg_read_idx < dev->msgs_num; dev->msg_read_idx++) {
		unsigned int len;
		unsigned char *buf;

		if (!(msgs[dev->msg_read_idx].flags & I2C_M_RD))
			continue;

		if (!(dev->status & STATUS_READ_IN_PROGRESS)) {
			len = msgs[dev->msg_read_idx].len;
			buf = msgs[dev->msg_read_idx].buf;
		} else {
			len = dev->rx_buf_len;
			buf = dev->rx_buf;
		}

		rx_valid = read32(reg_i2c+DW_IC_RXFLR);

		for (; len > 0 && rx_valid > 0; len--, rx_valid--) {
			*buf = read32(reg_i2c+DW_IC_DATA_CMD);
			buf++;
			dev->rx_outstanding--;
		}

		if (len > 0) {
			dev->status |= STATUS_READ_IN_PROGRESS;
			dev->rx_buf_len = len;
			dev->rx_buf = buf;
			return;
		} else
			dev->status &= ~STATUS_READ_IN_PROGRESS;
	}
}

/*
 * Initiate (and continue) low level master read/write transaction.
 * This function is only called from i2c_dw_isr, and pumping i2c_msg
 * messages into the tx buffer.  Even if the size of i2c_msg data is
 * longer than the size of the tx buffer, it handles everything.
 */
static void
i2c_dw_xfer_msg(struct i2c_dev_t *dev)
{
	struct i2c_msg *msgs = dev->msgs;
	unsigned int intr_mask;
	int tx_limit, rx_limit;
	unsigned int buf_len = dev->tx_buf_len;
	unsigned char *buf = dev->tx_buf;
	int need_restart = 0;

	intr_mask = DW_IC_INTR_MASTER_MASK;

	for (; dev->msg_write_idx < dev->msgs_num; dev->msg_write_idx++) {
		unsigned int flags = msgs[dev->msg_write_idx].flags;
		if (!(dev->status & STATUS_WRITE_IN_PROGRESS)) {
			/* new i2c_msg */
			buf = msgs[dev->msg_write_idx].buf;
			buf_len = msgs[dev->msg_write_idx].len;

			/* If both IC_EMPTYFIFO_HOLD_MASTER_EN and
			 * IC_RESTART_EN are set, we must manually
			 * set restart bit between messages.
			 */
			if ((1/*dev->master_cfg & DW_IC_CON_RESTART_EN*/) &&
					(dev->msg_write_idx > 0)) // TODO: restart
				need_restart = 1;
		}

		tx_limit = dev->tx_fifo_depth - read32(reg_i2c+DW_IC_TXFLR);
		rx_limit = dev->rx_fifo_depth - read32(reg_i2c+DW_IC_RXFLR);

		while (buf_len > 0 && tx_limit > 0 && rx_limit > 0) {
			unsigned int cmd = 0;

			/*
			 * If IC_EMPTYFIFO_HOLD_MASTER_EN is set we must
			 * manually set the stop bit. However, it cannot be
			 * detected from the registers so we set it always
			 * when writing/reading the last byte.
			 */
			// TODO: IC_EMPTYFIFO_HOLD_MASTER_EN is disabled by default, so can remove them...
			/*
			 * i2c-core always sets the buffer length of
			 * I2C_FUNC_SMBUS_BLOCK_DATA to 1. The length will
			 * be adjusted when receiving the first byte.
			 * Thus we can't stop the transaction here.
			 */
			if (dev->msg_write_idx == dev->msgs_num - 1 &&
			    buf_len == 1 && !(flags & I2C_M_RECV_LEN))
				cmd |= BIT(9);

			if (need_restart) {
				cmd |= BIT(10);
				need_restart = 0;
			}

			if (msgs[dev->msg_write_idx].flags & I2C_M_RD) {

				/* Avoid rx buffer overrun */
				if (dev->rx_outstanding >= dev->rx_fifo_depth)
					break;

				write32(reg_i2c+DW_IC_DATA_CMD, cmd | 0x100);
				rx_limit--;
				dev->rx_outstanding++;
			} else {
				//printf("i2c tx buf: 0x%x\n", *buf);
				write32(reg_i2c+DW_IC_DATA_CMD, cmd | *buf++);
			}
			tx_limit--; buf_len--;
		}

		dev->tx_buf = buf;
		dev->tx_buf_len = buf_len;

		/*
		 * Because we don't know the buffer length in the
		 * I2C_FUNC_SMBUS_BLOCK_DATA case, we can't stop
		 * the transaction here.
		 */
		if (buf_len > 0 || flags & I2C_M_RECV_LEN) {
			/* more bytes to be written */
			dev->status |= STATUS_WRITE_IN_PROGRESS;
			break;
		} else
			dev->status &= ~STATUS_WRITE_IN_PROGRESS;
	}

	/*
	 * If i2c_msg index search is completed, we don't need TX_EMPTY
	 * interrupt any more.
	 */
	if (dev->msg_write_idx == dev->msgs_num)
		intr_mask &= ~DW_IC_INTR_TX_EMPTY;

	write32(reg_i2c+DW_IC_INTR_MASK, intr_mask);
}

static void i2c_dw_irq_handler_master(struct i2c_dev_t *dev)
{
	unsigned int stat;

	stat = i2c_dw_read_clear_intrbits();

	//printf("%s, stat: 0x%x\n", __func__, stat);

	if (stat & DW_IC_INTR_TX_ABRT) {
		dev->status = STATUS_IDLE;

		/*
		 * Anytime TX_ABRT is set, the contents of the tx/rx
		 * buffers are flushed. Make sure to skip them.
		 */
		write32(reg_i2c+DW_IC_INTR_MASK, 0);
		goto tx_aborted;
	}

	if (stat & DW_IC_INTR_RX_FULL)
		i2c_dw_read(dev);

	if (stat & DW_IC_INTR_TX_EMPTY)
		i2c_dw_xfer_msg(dev);

	/*
	 * No need to modify or disable the interrupt mask here.
	 * i2c_dw_xfer_msg() will take care of it according to
	 * the current transmit status.
	 */

tx_aborted:
	if (stat & (DW_IC_INTR_TX_ABRT | DW_IC_INTR_STOP_DET))
		dev->cmd_complete = 1;
}

static void i2c_irq_handler(void)
{
	struct i2c_dev_t *dev = &i2c_dev;
	unsigned int stat, enabled;

	enabled = read32(reg_i2c+DW_IC_ENABLE);
	stat = read32(reg_i2c+DW_IC_RAW_INTR_STAT);

	//printf("%s, enable: 0x%x, raw_stat: 0x%x\n", __func__, enabled, stat);

	if (!enabled || !(stat & ~DW_IC_INTR_ACTIVITY))
		return;

	i2c_dw_irq_handler_master(dev);
}

static struct irq_ext_handler_t ext_irq_i2c_handler = {
	.hook		= i2c_irq_handler,
};

static void i2c_dw_xfer_init(struct i2c_dev_t *dev)
{
	struct i2c_msg *msgs = dev->msgs;

	/* Disable the adapter */
	write32(reg_i2c+DW_IC_ENABLE, 0);// TODO:

	/*
	 * Set the slave (target) address and enable 10-bit addressing mode
	 * if applicable.
	 */
	write32(reg_i2c+DW_IC_TAR, msgs[dev->msg_write_idx].addr);

	/* Enforce disabled interrupts (due to HW issues) */
	write32(reg_i2c+DW_IC_INTR_MASK, 0);

	/* Enable the adapter */
	write32(reg_i2c+DW_IC_ENABLE, 1);

	/* Dummy read to avoid the register getting stuck on Bay Trail */
	read32(reg_i2c+DW_IC_ENABLE_STATUS);

	/* Clear and enable interrupts */
	read32(reg_i2c+DW_IC_CLR_INTR);
	write32(reg_i2c+DW_IC_INTR_MASK, DW_IC_INTR_MASTER_MASK);
}

static int
i2c_dw_xfer(struct i2c_dev_t *dev, struct i2c_msg *msgs, int num)
{
	/* i2c_dw_wait_bus_not_busy() */
	while (read32(reg_i2c+DW_IC_STATUS) & DW_IC_STATUS_ACTIVITY) {
		// TODO: set timeout
	}

	dev->msgs = msgs;
	dev->msgs_num = num;
	dev->msg_write_idx = 0;
	dev->msg_read_idx = 0;
	dev->status = STATUS_IDLE;
	dev->cmd_complete = 0;
	dev->rx_outstanding = 0;

	/* Start the transfers */
	i2c_dw_xfer_init(dev);	

	/* Wait for tx to complete */
	while (0 == dev->cmd_complete) {
		// TODO: wfe
	}

	return 0;
}

int rvHal_i2c_transfer(unsigned int dev_num, struct i2c_msg *msgs, int num)
{
	struct i2c_dev_t *dev = &i2c_dev; // TODO: from dev_num

	return dev->master_xfer(dev, msgs, num);
}

static void i2c_configure_scl(int fast_plus)
{
	unsigned int i2c_clk_khz = I2C_CLK/1000;

	write32(reg_i2c+DW_IC_SS_SCL_LCNT, i2c_dw_scl_lcnt(i2c_clk_khz, 4700, 300, 0));
	write32(reg_i2c+DW_IC_SS_SCL_HCNT, i2c_dw_scl_hcnt(i2c_clk_khz, 4000, 300, 0, 0));

	write32(reg_i2c+DW_IC_FS_SCL_LCNT, i2c_dw_scl_lcnt(i2c_clk_khz, 1300, 300, 0));
	write32(reg_i2c+DW_IC_FS_SCL_HCNT, i2c_dw_scl_hcnt(i2c_clk_khz, 600, 300, 0, 0));

	write32(reg_i2c+DW_IC_HS_SCL_LCNT, 0); // TODO:
	write32(reg_i2c+DW_IC_HS_SCL_HCNT, 0);
}

static void i2c_configure_fifo(void)
{
	struct i2c_dev_t *dev = &i2c_dev;
	unsigned int param = read32(reg_i2c+DW_IC_COMP_PARAM_1);

	dev->tx_fifo_depth = ((param >> 16) & 0xff) + 1;
	dev->rx_fifo_depth = ((param >> 8)  & 0xff) + 1;

	write32(reg_i2c+DW_IC_TX_TL, dev->tx_fifo_depth >> 1);
	write32(reg_i2c+DW_IC_RX_TL, 0);
}

static void i2c_init_hook(void)
{
	i2c_dev.master_xfer = i2c_dw_xfer;
	__rvHal_request_irq_ext(NUM_IRQ_EXT_I2C, &ext_irq_i2c_handler);
}

void __rvHal_i2c_init(void)
{
	unsigned int value;

	value = DW_IC_CON_MASTER 
		| DW_IC_CON_SPEED_FAST 
		| DW_IC_CON_RESTART_EN 
		| DW_IC_CON_SLAVE_DISABLE;

	write32(reg_i2c+DW_IC_CON, value);

	/* configure slave address */
//	write32(reg_i2c+DW_IC_TAR, I2C_ADR); // TODO: move to xfer...

	write32(reg_i2c+DW_IC_HS_MADDR, 0); /* for HS */

	i2c_configure_scl(I2C_FREQ_FS_IS_PLUS); 
	write32(reg_i2c+DW_IC_SDA_HOLD, 2);
	
	write32(reg_i2c+DW_IC_FS_SPKLEN, 1);
	write32(reg_i2c+DW_IC_HS_SPKLEN, 1);

	write32(reg_i2c+DW_IC_INTR_MASK, 0);

	i2c_configure_fifo();

	i2c_init_hook();
}
