#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <iostream>
#include <unistd.h>

#include "common.h"
#include "pygmy_es1y_addr_translater.h"

int main (int argc, char const *argv[])
{
  int reset = 0;
  std::map<std::string, uint64_t> debug_map;
  int ret = 0;
  char *str_slowio_clkdiv = "slowio_clkdiv=";
  int len_slowio_clkdiv = strlen(str_slowio_clkdiv);
  int slowio_clkdiv = 0;

  struct io_channel_t * io = io_open();
  if (NULL == io)
  	return -1;

  ret = io_parse_args(io, argc, argv);
  if (ret)
  	return -1;
  ret = io_setup(io);
  if (ret)
  	return -1;
  ret = io_do_status_check(io);
  if (ret)
  	return -1;

  debug_map.insert(std::pair<std::string, uint64_t>("rst_pc", STATION_ORV32_S2B_CFG_RST_PC_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("bp_if_pc_0", STATION_ORV32_S2B_BP_IF_PC_0_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("bp_if_pc_1", STATION_ORV32_S2B_BP_IF_PC_1_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("bp_if_pc_2", STATION_ORV32_S2B_BP_IF_PC_2_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("bp_if_pc_3", STATION_ORV32_S2B_BP_IF_PC_3_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("bp_wb_pc_0", STATION_ORV32_S2B_BP_WB_PC_0_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("bp_wb_pc_1", STATION_ORV32_S2B_BP_WB_PC_1_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("bp_wb_pc_2", STATION_ORV32_S2B_BP_WB_PC_2_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("bp_wb_pc_3", STATION_ORV32_S2B_BP_WB_PC_3_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("if_pc", STATION_ORV32_IF_PC_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("wb_pc", STATION_ORV32_WB_PC_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("mcycle", STATION_ORV32_MCYCLE_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("minstret", STATION_ORV32_MINSTRET_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("mstatus", STATION_ORV32_MSTATUS_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("mcause", STATION_ORV32_MCAUSE_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("mepc", STATION_ORV32_MEPC_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("hpmcounter_3", STATION_ORV32_HPMCOUNTER_3_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("hpmcounter_4", STATION_ORV32_HPMCOUNTER_4_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("hpmcounter_5", STATION_ORV32_HPMCOUNTER_5_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("hpmcounter_6", STATION_ORV32_HPMCOUNTER_6_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("hpmcounter_7", STATION_ORV32_HPMCOUNTER_7_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("hpmcounter_8", STATION_ORV32_HPMCOUNTER_8_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("hpmcounter_9", STATION_ORV32_HPMCOUNTER_9_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("hpmcounter_10", STATION_ORV32_HPMCOUNTER_10_ADDR));
  //debug_map.insert(std::pair<std::string, uint64_t>("lfsr_seed", STATION_ORV32_S2B_CFG_LFSR_SEED_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("early_rstn", STATION_ORV32_S2B_EARLY_RSTN_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("rstn", STATION_ORV32_S2B_RSTN_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ext_event", STATION_ORV32_S2B_EXT_EVENT_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("debug_stall", STATION_ORV32_S2B_DEBUG_STALL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("b2s_debug_stall_out", STATION_ORV32_B2S_DEBUG_STALL_OUT_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("debug_resume", STATION_ORV32_S2B_DEBUG_RESUME_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("en_hpmcounter", STATION_ORV32_S2B_CFG_EN_HPMCOUNTER_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("pwr_on", STATION_ORV32_S2B_CFG_PWR_ON_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("sleep", STATION_ORV32_S2B_CFG_SLEEP_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("bypass_ic", STATION_ORV32_S2B_CFG_BYPASS_IC_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("s2icg_clk_en", STATION_ORV32_S2ICG_CLK_EN_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("itb_en", STATION_ORV32_S2B_CFG_ITB_EN_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("itb_wrap_around", STATION_ORV32_S2B_CFG_ITB_WRAP_AROUND_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("en_bp_if_pc_0", STATION_ORV32_S2B_EN_BP_IF_PC_0_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("en_bp_if_pc_1", STATION_ORV32_S2B_EN_BP_IF_PC_1_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("en_bp_if_pc_2", STATION_ORV32_S2B_EN_BP_IF_PC_2_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("en_bp_if_pc_3", STATION_ORV32_S2B_EN_BP_IF_PC_3_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("en_bp_wb_pc_0", STATION_ORV32_S2B_EN_BP_WB_PC_0_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("en_bp_wb_pc_1", STATION_ORV32_S2B_EN_BP_WB_PC_1_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("en_bp_wb_pc_2", STATION_ORV32_S2B_EN_BP_WB_PC_2_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("en_bp_wb_pc_3", STATION_ORV32_S2B_EN_BP_WB_PC_3_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("if_stall", STATION_ORV32_IF_STALL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("if_kill", STATION_ORV32_IF_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("if_valid", STATION_ORV32_IF_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("if_ready", STATION_ORV32_IF_READY_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("id_stall", STATION_ORV32_ID_STALL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("id_kill", STATION_ORV32_ID_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("id_valid", STATION_ORV32_ID_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("id_ready", STATION_ORV32_ID_READY_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ex_stall", STATION_ORV32_EX_STALL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ex_kill", STATION_ORV32_EX_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ex_valid", STATION_ORV32_EX_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ex_ready", STATION_ORV32_EX_READY_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ma_stall", STATION_ORV32_MA_STALL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ma_kill", STATION_ORV32_MA_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ma_valid", STATION_ORV32_MA_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ma_ready", STATION_ORV32_MA_READY_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("wb_valid", STATION_ORV32_WB_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("wb_ready", STATION_ORV32_WB_READY_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("cs2if_kill", STATION_ORV32_CS2IF_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("cs2id_kill", STATION_ORV32_CS2ID_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("cs2ex_kill", STATION_ORV32_CS2EX_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("cs2ma_kill", STATION_ORV32_CS2MA_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("is_wfe", STATION_ORV32_IS_WFE_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ma2if_npc_valid", STATION_ORV32_MA2IF_NPC_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ex2if_kill", STATION_ORV32_EX2IF_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ex2id_kill", STATION_ORV32_EX2ID_KILL_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("branch_solved", STATION_ORV32_BRANCH_SOLVED_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("if2ic_pc", STATION_ORV32_IF2IC_PC_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("if2id_excp_valid", STATION_ORV32_IF2ID_EXCP_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("id2ex_excp_valid", STATION_ORV32_ID2EX_EXCP_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ex2ma_excp_valid", STATION_ORV32_EX2MA_EXCP_VALID_ADDR));
  debug_map.insert(std::pair<std::string, uint64_t>("ma2cs_excp_valid", STATION_ORV32_MA2CS_EXCP_VALID_ADDR));

  for (int i = 1; i < argc; i++) {
	if (strncmp(argv[i], "reset", 5) == 0) { /* for script usage and dont need enter the internal handshake mode */
		reset = 1;
	}
    if (strncmp(argv[i], str_slowio_clkdiv, len_slowio_clkdiv) == 0) {
         slowio_clkdiv = atoi(argv[i] + len_slowio_clkdiv);
         printf("slowio_clkdiv: %d\n", slowio_clkdiv);
    }
  }

  if (slowio_clkdiv) { /* use for debug-spi-base : bypass pclk config via low speed spi */
        if (slowio_clkdiv <= 1) {
                io_do_write(io, STATION_BYP_S2B_SLOW_IO_CLKDIV_DIVCLK_SEL_ADDR, 0);
        } else {
                io_do_write(io, STATION_BYP_S2B_SLOW_IO_CLKDIV_DIVCLK_SEL_ADDR, 1);
                io_do_write(io, STATION_BYP_S2B_SLOW_IO_CLKDIV_HALF_DIV_LESS_1_ADDR, (slowio_clkdiv >> 1) -1);
        }
        return 0;
  }

  if (reset) {
      io_reset(io);
	  return 0;
  }

  while (1) {
    std::string cmd;
    std::cout << "Please enter command: (All Data in HEX no matter 0x is added or not)\n: ";
    std::getline(std::cin, cmd);
    std::string delimiter = " ";
    size_t pos = 0;
    std::vector <std::string> tokenQ;
    int dma_cmd_vld = 0;
  
    while ((pos = cmd.find(delimiter)) != std::string::npos) {
      tokenQ.push_back(cmd.substr(0, pos));
      cmd.erase(0, pos + delimiter.length());
    }
    if (!cmd.empty())
      tokenQ.push_back(cmd);
  
    if ((tokenQ.size() == 1) && (tokenQ[0] == "status")) {
      //do_status_check(mode, ftHandle);
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "setpc")) {
      io_do_write(io, STATION_ORV32_S2B_CFG_RST_PC_ADDR, 0x80000000);
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "list_debug")) {
      for (std::map<std::string, uint64_t>::iterator it = debug_map.begin(); it != debug_map.end(); ++it) {
        printf("\t%s\n", it->first.c_str());
      }
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "reset")) {
      io_reset(io);
    } else if ((tokenQ.size() == 1) && (debug_map.count(tokenQ[0]) > 0)) {
      uint64_t addr = debug_map[tokenQ[0]];
      uint64_t data = io_do_read(io, addr);
      printf("Do Read to Addr 0x%llx (%s), Got Data 0x%llx\n", addr, tokenQ[0].c_str(), data);
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "test_ddr")) {
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "release_vp0_reset")) {
      io_do_write(io, STATION_ORV32_S2B_RSTN_ADDR, 1);
    } else if ((tokenQ.size() == 2) && (tokenQ[0] == "step")) {
      char * p;
      uint32_t cnt = strtoul(tokenQ[1].c_str(), & p, 10);
      uint64_t pc;
      for (int i = 0; i < cnt; i ++) {
        io_do_write(io, STATION_ORV32_S2B_DEBUG_RESUME_ADDR, 1);
        pc = io_do_read(io, STATION_ORV32_WB_PC_ADDR);
        fprintf(stderr, "pc = 0x%llx\n", pc);
      }
    } else if ((tokenQ.size() == 0) || (tokenQ[0] == "help") || (tokenQ[0] == "h")) {
      std::cout << "This is Help Info\n";
    } else if ((tokenQ[0] == "quit") || (tokenQ[0] == "q") || (tokenQ[0] == "exit")) {
      break;
    } else if ((tokenQ.size() == 3) && (tokenQ[0] == "read")) {
      char * p;
      uint64_t data = 0;
      uint64_t addr = strtoul(tokenQ[1].c_str(), & p, 16);
      std::string target = tokenQ[2];
      if (*p == 0) {
        if (target == "rb") {
          data = io_do_read(io, addr);
        } else if (target == "dma") {
          io_do_write(io, STATION_DMA_DMA_DEBUG_ADDR_ADDR, addr);
          io_do_write(io, STATION_DMA_DMA_DEBUG_REQ_TYPE_ADDR, 0);
          data = io_do_read(io, STATION_DMA_DMA_DEBUG_RD_DATA_ADDR);
        } else if (target == "dt") {
          //io_do_write(io, STATION_DT_DBG_ADDR_ADDR, addr);
          //data = io_do_read(io, STATION_DT_DBG_DATA_ADDR);
        } else {
          data = io_do_read(io, addr2oraddr(target, addr));
        }
        printf("Do Read to Addr 0x%llx, Got Data 0x%llx\n", addr, data);
      }
    } else if ((tokenQ.size() == 4) && (tokenQ[0] == "init")) {
      char * p;
      uint64_t addr = strtoul(tokenQ[1].c_str(), & p, 16);
      uint64_t data = strtoul(tokenQ[2].c_str(), & p, 16);
      std::string target = tokenQ[3];
      if (*p == 0) {
        if (target == "rb") {
          io_do_write(io, addr, data);
        } else if (target == "dma") {
          io_do_write(io, STATION_DMA_DMA_DEBUG_ADDR_ADDR, addr);
          io_do_write(io, STATION_DMA_DMA_DEBUG_REQ_TYPE_ADDR, 2);
          io_do_write(io, STATION_DMA_DMA_DEBUG_WR_DATA_ADDR, data);
        } else if (target == "dt") {
          //io_do_write(io, STATION_DT_DBG_ADDR_ADDR, addr);
          //io_do_write(io, STATION_DT_DBG_DATA_ADDR, data);
        } else {
          io_do_write(io, addr2oraddr("l2_vld",  addr), 0xffffffffffffffff);
          io_do_write(io, addr2oraddr("l2_tag",  addr), (addr >> 16));
          io_do_write(io, addr2oraddr("l2_data", addr), data);
        }
        printf("Do Write to Addr 0x%llx with Data 0x%llx\n", addr, data);
      }
    } else if ((tokenQ.size() == 4) && (tokenQ[0] == "write")) {
      char * p;
      uint64_t addr = strtoul(tokenQ[1].c_str(), & p, 16);
      uint64_t data = strtoul(tokenQ[2].c_str(), & p, 16);
      std::string target = tokenQ[3];
      if (*p == 0) {
        if (target == "rb") {
          io_do_write(io, addr, data);
        } else if (target == "dma") {
          io_do_write(io, STATION_DMA_DMA_DEBUG_ADDR_ADDR, addr);
          io_do_write(io, STATION_DMA_DMA_DEBUG_REQ_TYPE_ADDR, 2);
          io_do_write(io, STATION_DMA_DMA_DEBUG_WR_DATA_ADDR, data);
        } else if (target == "dt") {
          //io_do_write(io, STATION_DT_DBG_ADDR_ADDR, addr);
          //io_do_write(io, STATION_DT_DBG_DATA_ADDR, data);
        } else {
          io_do_write(io, addr2oraddr(target, addr), data);
        }
        printf("Do Write to Addr 0x%llx with Data 0x%llx\n", addr, data);
      }
    } else if ((tokenQ.size() == 4) && (tokenQ[0] == "dump")) {
      char * p;
      uint64_t data = 0;
      uint64_t addr_lo = strtoul(tokenQ[1].c_str(), & p, 16);
      uint64_t addr_hi = strtoul(tokenQ[2].c_str(), & p, 16);
      std::string target = tokenQ[3];
      if (*p == 0) {
        for (uint64_t addr = addr_lo; addr <= addr_hi; addr += 8) {
          if (target == "rb") {
            data = io_do_read(io, addr);
          } else if (target == "dma") {
            io_do_write(io, STATION_DMA_DMA_DEBUG_ADDR_ADDR, addr);
            io_do_write(io, STATION_DMA_DMA_DEBUG_REQ_TYPE_ADDR, 0);
            data = io_do_read(io, STATION_DMA_DMA_DEBUG_RD_DATA_ADDR);
          } else if (target == "dt") {
            //io_do_write(io, STATION_DT_DBG_ADDR_ADDR, addr);
            //data = io_do_read(io, STATION_DT_DBG_DATA_ADDR);
          } else {
            data = io_do_read(io, addr2oraddr(target, addr));
          }
          printf("0x%llx: 0x%08llx\n", addr + 0, (data >>  0) & 0xffffffff);
          printf("0x%llx: 0x%08llx\n", addr + 4, (data >> 32) & 0xffffffff);
        }
      }
    } else {
      std::cout << "Unrecognized Command; Please use help or h to see supported command list.\n";
    }
  }

  io_close(io);
  return 0;
}
