// for uint*_t data types
#include <stdint.h>
// for open() on device files
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

// for malloc
#include <stdlib.h>

// for print statements
#include <stdio.h>

#include "pcie_driver.h"

//#define DEBUG

// shove a 64bit number into an 8 bit buffer
void dataToBuffer(uint8_t *buf, uint64_t data) {
	int i = 0;
	for(i = 0; i<8; i++) {
#ifdef DEBUG
//		fprintf(stderr,"%016lx\n", (data >> i*8) & 0XFF );
#endif
	        *buf = (data >> i*8) & 0xFF;
		buf++;
	}
}

// PCIE
int fpga_fd_rd = 0;
int fpga_fd_wr = 0;

// size here should be the number of elements in dataArr
// user needs to guarantee buf is big enough to hold the entire dataArr
void dataArrToBuffer(uint8_t *buf, uint64_t *dataArr, uint16_t size) {
	int i = 0;
	for(i=0; i<size; i++) {
		dataToBuffer(buf+(i*8), dataArr[i]);
	}
}

void printBuffer(uint8_t *buf, uint8_t size) {
	int i = 0;
        for(i=size - 1; i >= 0; i--)
        {
        	fprintf(stderr,"%02x", buf[i]);
        }
}

int compareBuffers(uint8_t *buf1, uint8_t *buf2, int size){
	int i = 0;
	for(i = 0; i < size; i++) {
		if(buf1[i] != buf2[i]) {
			return 0;
		}
	}
	return 1;
}

// make sure writes are aligned with AXI DATA BUS WIDTH or the driver will shift things out of order on the data channel to make it align
// eg for 512 bit bus, addresses should be multiples of 0x40
// addr should not be larger than 40bit
// data going out to AXI is little endian and should be a buf created with
// uint8_t * buf = (uint8_t *) malloc (sizeof(uint8_t) * AXI_DATA_BUS_WIDTH_IN_BYTES);
int pcie_write(uint64_t addr, uint8_t *buf)
{
        if (fpga_fd_wr == 0)
	  fpga_fd_wr = open(WDEVICE_NAME_DEFAULT, O_RDWR);
	uint8_t rc = 0;
#ifdef DEBUG
	fprintf(stderr,"Writing data 0x");
	printBuffer(buf, AXI_DATA_BUS_WIDTH_IN_BYTES);
        fprintf(stderr,"\n");
#endif
	// set the address of the AXI channel
	if (addr != 0)
	{
		rc = lseek(fpga_fd_wr, addr, SEEK_SET);
#ifdef DEBUG
		fprintf(stderr,"Seeked %x bytes, addr = %x fpga_fd_wr = %lx\n", rc, addr, fpga_fd_wr);
#endif

	}
	rc = write(fpga_fd_wr, buf, AXI_DATA_BUS_WIDTH_IN_BYTES);
#ifdef DEBUG
	fprintf(stderr,"Wrote %d bytes\n", rc);
#endif

        //fsync(fpga_fd_wr);
        //fflush(fpga_fd_wr);
	close(fpga_fd_wr);
        fpga_fd_wr = 0;
	return rc;
}

void pcie_read(uint64_t addr, uint8_t* buf) {
        if (fpga_fd_rd == 0)
	  fpga_fd_rd = open(RDEVICE_NAME_DEFAULT, O_RDWR | O_NONBLOCK);
	uint8_t rc = 0;
	if (addr != 0)
	{
		rc = lseek(fpga_fd_rd, addr, SEEK_SET);
#ifdef DEBUG
		fprintf(stderr,"Seeked %x bytes, fpga_fd_rd = %lx\n", rc, fpga_fd_rd);
#endif

	}

        rc = read(fpga_fd_rd, buf, AXI_DATA_BUS_WIDTH_IN_BYTES);

#ifdef DEBUG
	fprintf(stderr,"Read addr %x \n", addr);
	fprintf(stderr,"Read %d bytes\n", rc);
	fprintf(stderr,"Reading data %lx",*(uint64_t *)buf);
	printBuffer(buf, AXI_DATA_BUS_WIDTH_IN_BYTES);
        fprintf(stderr,"\n");
#endif

	//close(fpga_fd_rd);
}

int do_reset(uint64_t data) {
  uint64_t chksum = 0xABABABABABABABAB;
  uint64_t seq    = 0xDEDEDEDEDEDEDEDE;
  uint64_t dataArr[8] = {data, RST_REG_ADDR, chksum, seq, 0, 0, 0, 0};
  uint8_t * buf = (uint8_t *) malloc (sizeof(uint8_t) * AXI_DATA_BUS_WIDTH_IN_BYTES);
  dataArrToBuffer(buf, dataArr, 8);

   // write to command register
   pcie_write(RST_REG_ADDR, buf);
}

int do_write_pcie(uint64_t addr, uint64_t data) {
        // fprintf(stderr,"do_write: addr = %llx, data = %llx\n", addr, data);
	// format command packet
	// 63:0 -> data
	// 128:64 -> addr
	// 192:129 -> chksum
	// 256:193 -> seq
	// 512:257 -> unused
	uint64_t chksum = 0xABABABABABABABAB;
	uint64_t seq    = 0xDEDEDEDEDEDEDEDE;
	uint64_t dataArr[8] = {addr, data, chksum, seq, 0, 0, 0, 0};
   	//fprintf(stderr,"%lx %lx\n",dataArr[0],dataArr[1]);
	uint8_t * buf = (uint8_t *) malloc (sizeof(uint8_t) * AXI_DATA_BUS_WIDTH_IN_BYTES);
        dataArrToBuffer(buf, dataArr, 8);
  
	// write to command register
	pcie_write(CMD_REG_ADDR, buf);
	// write to execute register
	pcie_write(EXE_REG_ADDR, buf);

	free(buf);
}

uint64_t do_read_pcie(uint64_t addr) {
        // fprintf(stderr,"do_read: addr = %llx\n", addr);
	// format command packet
	// 63:0 -> fake_data
	// 128:64 -> addr
	// 192:129 -> chksum
	// 256:193 -> seq
	// 512:257 -> unuseda
	uint64_t rdata;
	uint64_t chksum = 0xCDCDCDCDCDCDCDCD;
	uint64_t seq    = 0xEFEFEFEFEFEFEFEF;
	uint64_t dataArr[8] = {addr, 0xf415ed474, chksum, seq, 0, 0, 0, 0};
	uint8_t * buf = (uint8_t *) malloc (sizeof(uint8_t) * AXI_DATA_BUS_WIDTH_IN_BYTES);
        dataArrToBuffer(buf, dataArr, 8);

	// write to command register
	pcie_write(CMD_REG_ADDR, buf);
        for (int i = 0; i < 64; i++) {
          buf[i] = 0;
        }
	// write to execute register
 	pcie_read(EXE_REG_ADDR, buf);
	rdata = *((uint64_t *) buf);
        //for (int i = 0; i < 64; i++) {
        //  fprintf(stderr,"buf[%d] = %x\n", i, buf[i]);
        //}
  	//printf  ("rdata=%lx\n", rdata );
        //fprintf(stderr,"do_read: addr = %llx, rdata = %llx\n", addr, rdata);
	free(buf);
	return rdata;
}


