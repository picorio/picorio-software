"""
    Abstract class for spi interface
    Author: Wilson Ko
    Date created: 12/19/2017
    Python version: 3.6
    Copyright 2017, OURS Technology Inc.
"""
import abc


class SPI(abc.ABC):

    def __init__(self):
        pass

    @abc.abstractmethod
    def driver_write(self, addr, data):
        pass

    @abc.abstractmethod
    def driver_read(self, addr):
        pass
