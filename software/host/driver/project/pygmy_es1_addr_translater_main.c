#include "pygmy_es1_addr_translater.h"
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <stdint.h>
#include <sstream>

int main (int argc, char* argv[]) {
  if (argc != 3) {
    printf("Bad Command! Correct Command: pygmy_es1_addr_translater_main.o TARGET ADDR\n");
  } else {
    std::stringstream ss;
    std::string target;
    ss << argv[1];
    ss >> target;
    printf("0x%lx\n", addr2obaddr(target, strtol(argv[2], NULL, 16)));
  }
}
