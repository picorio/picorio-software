/*
 * This test application is to read/write data directly from/to the device 
 * from userspace. 
 * 
 */
#include "gpio_dev_mem.h"
int fd;
struct stat sb;
void *rst_ptr;
void *spi_ptr;
void *bram_ptr;
void *ddr_ptr;
uint64_t rst_page_addr;
uint64_t spi_page_addr;
uint64_t bram_page_addr;
uint64_t ddr_page_addr;
uint64_t page_size;
void open_fd(){
  uint64_t rst_base_addr  = RST_ADDR_OFFSET;
  uint64_t spi_base_addr  = SPIM_ADDR_OFFSET_0;
  uint64_t bram_base_addr = BRAM_ADDR_OFFSET;
  uint64_t ddr_base_addr  = DDR_ADDR_OFFSET;

  page_size = sysconf(_SC_PAGESIZE);
  page_size = MEM_ALLOC_SIZE;
	printf("SERVER: access through /dev/mem. page size 0x%" PRIx64 "  pagesz: %lx\n",page_size ,getpagesize());
  fd = open("/dev/mem fail",O_RDWR|O_CREAT); 
  if(fd < 1) {
    perror("open /dev/mem fail");
    exit(EXIT_FAILURE);
  }
  if(fallocate(fd,0,DDR_ADDR_OFFSET,MEM_ALLOC_SIZE) < 0){
    perror("fallocate failed");
    exit(EXIT_FAILURE);
  }
  if(fstat(fd,&sb)< 0){
    perror("fstat failed");
    exit(EXIT_FAILURE);
  }
  printf("SERVER: file size: %x bytes\n",sb.st_size);
  //RST
  rst_page_addr = (rst_base_addr & (~(getpagesize())));
  if(rst_page_addr != rst_base_addr){
    close(fd);
    perror("rst_base_addr != rst_page_addr");
    exit(EXIT_FAILURE);
  }
  rst_ptr = mmap(NULL,getpagesize()*sizeof(uint64_t),PROT_READ|PROT_WRITE,MAP_SHARED,fd,rst_page_addr);
  if(rst_ptr == MAP_FAILED){
   close(fd);
   perror("SERVER: Error Mapping rst_ptr to the file");
   exit(EXIT_FAILURE);
  }
  //SPI
  spi_page_addr = (spi_base_addr & (~(2*getpagesize())));
  if(spi_page_addr != spi_base_addr){
    close(fd);
    printf("ERROR spi_base_addr: 0x%" PRIx64 " != spi_page_addr: 0x%" PRIx64 "\n",spi_base_addr,spi_page_addr);
    exit(EXIT_FAILURE);
  }
  spi_ptr = mmap(NULL,getpagesize()*2*sizeof(uint64_t),PROT_READ|PROT_WRITE,MAP_SHARED,fd,spi_page_addr);
  if(spi_ptr == MAP_FAILED){
   close(fd);
   perror("SERVER: Error Mapping spi_ptr to the file");
   exit(EXIT_FAILURE);
  }
  //BRAM
  bram_page_addr = (bram_base_addr & (~(getpagesize())));
  if(bram_page_addr != bram_base_addr){  
     close(fd);
     perror("bram_base_addr != bram_page_addr");
     exit(EXIT_FAILURE);
  }
  bram_ptr = mmap(NULL,getpagesize()*sizeof(uint64_t),PROT_READ|PROT_WRITE,MAP_SHARED,fd,bram_page_addr);
  if(bram_ptr == MAP_FAILED){
   close(fd);
   perror("SERVER: Error Mapping bram_ptr to the file");
   exit(EXIT_FAILURE);
  }
  //DDR
  ddr_page_addr = (ddr_base_addr & (~(page_size)));
  if(ddr_page_addr != ddr_base_addr){
     perror("ddr_base_addr != ddr_page_addr");
     close(fd);
     exit(EXIT_FAILURE);
  }
  ddr_ptr = mmap(NULL,(size_t)page_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd,ddr_page_addr);
  if(ddr_ptr == MAP_FAILED){
   close(fd);
   perror("SERVER: Error Mapping ddr_ptr to the file");
   exit(EXIT_FAILURE);
  } 
} 

void close_fd(){
	munmap(rst_ptr, getpagesize());
	munmap(spi_ptr,  2*getpagesize());
	munmap(bram_ptr, page_size);
	munmap(ddr_ptr, page_size);
  close(fd);
}

int do_write(uint64_t addr, uint64_t val, enum trgt_t trgt){
 	uint64_t page_offset;
  if(trgt == TRGT_RST){
	  page_offset = addr - rst_page_addr;
    *((uint64_t*)(rst_ptr + page_offset)) = val;
	  return 0;
  }
  else  if(trgt == TRGT_SPI){
	  page_offset = addr - spi_page_addr;
    //fprintf(stdout,"do_write: page_offset: 0x%" PRIx64 ", addr: 0x%" PRIx64 ", spi_page_addr: 0x%" PRIx64 "\n",page_offset,addr,spi_page_addr);
    *((uint64_t*)(spi_ptr + page_offset)) = val;
	  return 0;
  }
  else if(trgt == TRGT_BRAM){
	  page_offset = addr - bram_page_addr;
    *((uint64_t*)(bram_ptr + page_offset)) = val;
	  return 0;
  }
  else if(trgt == TRGT_DDR){
	  page_offset = addr - ddr_page_addr;
    //fprintf(stdout,"do_write: page_offset: 0x%" PRIx64 ", addr: 0x%" PRIx64 ", ddr_page_addr: 0x%" PRIx64 "\n",page_offset,addr,ddr_page_addr);
    *((uint64_t*)(ddr_ptr + page_offset)) = val;
	  return 0;
  }
  else {
		fprintf(stderr,"SERVER: do_write Illegal mode.\n");
    close(fd);
    return -1;
  }
}

uint64_t do_read(uint64_t addr,enum  trgt_t trgt){
  uint64_t val = 0;
	uint64_t page_offset;
  if(trgt == TRGT_RST) { 
	  page_offset = addr -rst_page_addr;
    val = *((uint64_t*)(rst_ptr + page_offset));
	  return val;
  }
  else  if(trgt == TRGT_SPI) { 
	  page_offset = addr -spi_page_addr;
    val = *((uint64_t*)(spi_ptr + page_offset));
	  return val;
  }
  else if(trgt == TRGT_BRAM) { 
	  page_offset = addr -bram_page_addr;
    val = *((uint64_t*)(bram_ptr + page_offset));
	  return val;
  }
  else if(trgt == TRGT_DDR) { 
	  page_offset = addr -ddr_page_addr;
    //fprintf(stdout,"do_read: page_offset: 0x%" PRIx64 ", addr: 0x%" PRIx64 ", ddr_page_addr: 0x%" PRIx64 "\n",page_offset,addr,ddr_page_addr);
    val = *((uint64_t*)(ddr_ptr + page_offset));
	  return val;
  }
  else {
    fprintf(stderr,"SERVER: do_read Illegal mode.\n");
    close(fd);
    return -1;
  }
}
