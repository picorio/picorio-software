#ifndef CLIB_H
#define CLIB_H

#include "system_config.h"

#define BUF_SIZE_PRINTF	64
#define BUF_SIZE_DUMP	72

struct putchar_data {
	char *buf;
	int bufsize;
	int buflen;
	int end;
};

void exit(int code);
int printf(const char* fmt, ...);
int sprintf(char* str, const char* fmt, ...);
int snprintf(char * str, size_t size, const char *fmt, ...);
int ddprintf(struct putchar_data *pdata, const char* fmt, ...);
#endif /* CLIB_H */

