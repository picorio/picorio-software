#ifndef HAL_CONFIG_PYGMY_E_SOC_CONFIG_H
#define HAL_CONFIG_PYGMY_E_SOC_CONFIG_H

enum  RVHAL_spi_num {
	NUM_SPIM_0 = 0,
	NUM_SPIM_1,
	NUM_SPIM_2,
	NUM_SPIM_MAX,
};

enum  RVHAL_i2c_num {
	NUM_I2C_0 = 0,
	NUM_I2C_1,
	NUM_I2C_2,
	NUM_I2C_MAX,
};

/*
 * interrupt number list
 * get them by script from soc_config later
 * 
ifndef NO_SSP_TOP
plic_u/irq_in[0]                    ssp_top_u/qspim_u_intr
plic_u/irq_in[1]                    ssp_top_u/sspim0_u_intr
plic_u/irq_in[2]                    ssp_top_u/sspim1_u_intr
plic_u/irq_in[3]                    ssp_top_u/sspim2_u_intr
plic_u/irq_in[4]                    ssp_top_u/spis_u_intr
plic_u/irq_in[5]                    ssp_top_u/uart0_u_intr
plic_u/irq_in[6]                    ssp_top_u/uart1_u_intr
plic_u/irq_in[7]                    ssp_top_u/uart2_u_intr
plic_u/irq_in[8]                    ssp_top_u/uart3_u_intr
plic_u/irq_in[9]                    ssp_top_u/i2sm_u_intr
plic_u/irq_in[10]                   ssp_top_u/i2ss0_u_intr
plic_u/irq_in[11]                   ssp_top_u/i2ss1_u_intr
plic_u/irq_in[12]                   ssp_top_u/i2c0_u_intr
plic_u/irq_in[13]                   ssp_top_u/i2c1_u_intr
plic_u/irq_in[14]                   ssp_top_u/i2c2_u_intr
plic_u/irq_in[15]                   ssp_top_u/gpio_u_intr
plic_u/irq_in[16]                   ssp_top_u/rtc_u_intr
plic_u/irq_in[17]                   ssp_top_u/timers_u_intr
plic_u/irq_in[18]                   ssp_top_u/wdt_u_intr
plic_u/irq_in[21]                   ssp_top_u/scu_u_intr
ssp_top_u/fast_io_intr              'b0
endif
ifndef NO_SDIO
plic_u/irq_in[19]                   sdio_u/b2s_intr
plic_u/irq_in[20]                   sdio_u/b2s_wakeup_intr
endif
*/
enum RVHAL_irq_ext_num {
	NUM_IRQ_EXT_SPI = 1,
	NUM_IRQ_EXT_I2C = 12,
	NUM_IRQ_EXT_GPIO = 15,
	NUM_IRQ_EXT_RTC = 16,
	NUM_IRQ_EXT_TIMERS = 17,
	NUM_IRQ_EXT_MAX = 22 // TODO:
};

#endif /* HAL_CONFIG_PYGMY_E_SOC_CONFIG_H */

