

#include <stdint.h>

static uint64_t tohost = 0;
static uint64_t fromhost = 0;
static uint64_t magicmem[8];

void print2 (int i, int64_t * addr) {
  magicmem[0] = 106;
  magicmem[1] = i;
  magicmem[2] = addr;
  tohost = magicmem;
  __sync_synchronize();
  while (fromhost == 1);
  fromhost = 0;
}

void stop () {
  tohost = 1;
  __sync_synchronize();
  while (fromhost == 1);
  fromhost = 0;
}

#define tsize 130000

void main (void) {

  int addr = 0;
  static int64_t weights[tsize] = {1};

  for (int i = 0; i < tsize; i++) {
    weights[i] = ((0UL + i) << 32) + i;
  }

  for (int i = 0; i < tsize; i++) {
    if (((weights[i] >> 32) != i) || ((weights[i] & 0xffffffff) != i)) {
      print2(i, &weights[i]);
    }
  }

  stop();
}
