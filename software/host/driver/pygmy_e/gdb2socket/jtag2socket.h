/*************************************************************************
	> File Name: jtag2socket.h
	> Author: jzyang
	> Mail: jianzhi.yang@rivai.com 
	> Created Time: Wed 03 Jun 2020 10:08:20 AM CST
 ************************************************************************/

typedef struct
 {
   uint8_t prv;
   bool step;
   bool ebreakm;
   bool ebreakh;
   bool ebreaks;
   bool ebreaku;
   bool halt;
   uint8_t cause;
 } dcsr_t;

typedef struct
{
	uint32_t enable;
	uint32_t addr;
	uint32_t data;
} bp_t;
