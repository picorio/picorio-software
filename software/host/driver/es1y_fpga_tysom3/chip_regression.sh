#!/usr/bin/env bash

i="$1" # initial test index
j="$2" # chip id
rerun="0"

cd $PROJ_ROOT/tool/driver/es1_bringup/
python serial_reset.py
mkdir -p $PROJ_ROOT/tool/driver/es1_bringup/test_log/chip_$2/
export PATH=/work/users/tian/pygmy-es2/tool/riscv/bin/:$PATH

while true; do
  if [ "$rerun" -gt "0" ]
  then
    echo "Rerun, do not generate new tests"
  else
    cd $PROJ_ROOT/tool/riscv-tests/torture/
    make test$i
  fi
  cd $PROJ_ROOT/tool/driver/es1_bringup
  cp $PROJ_ROOT/tool/riscv-tests/torture/output/test$i $PROJ_ROOT/tool/driver/es1_bringup/test_log/chip_$2/
  make test$i |& tee $PROJ_ROOT/tool/driver/es1_bringup/test_log/chip_$2/test$i.log &
  PID=$!
  $PROJ_ROOT/verif/common/regression/fpga/job_check.sh -i $PROJ_ROOT/tool/driver/es1_bringup/test_log/chip_$2/test$i.log -p "PASSED" -f "FAILED" -t 60
  RSLT=$?
  pkill -9 fesvr2spim.o
  pkill -9 fesvr2spim.o
  kill -9 $PID
  if [ "$RSLT" -gt "0" ]
  then
    if [ "$RSLT" -gt "2" ]
    then
      echo "$(date) Torture Test $i TIMEOUT, rerun = $rerun" >> $PROJ_ROOT/tool/driver/es1_bringup/test_log/chip_$2/torture.log
      #mailx -r fpga@ours-tech.com -s "Test $i Failed!!!" tian@ours-tech.com < /work/users/tian/for_jenkins/fpga/test$i.log
      rerun=$(($rerun+1))
      #rerun="0" # disable rerun for now
    else
      echo "$(date) Torture Test $i FAILED" >> /work/users/tian/for_jenkins/fpga/torture.log
      i=$(($i+1))
      rerun="0"
    fi
  else
    echo "$(date) Torture Test $i passed" >> $PROJ_ROOT/tool/driver/es1_bringup/test_log/chip_$2/torture.log
    i=$(($i+1))
    rerun="0"
  fi
  cd $PROJ_ROOT/tool/driver/es1_bringup/
  python serial_reset.py
done
