
from dbg_defines import DRV

if (DRV == "SPI_FT4222"):
  from ftdi_ft4222h import FTDI_FT4222H
  driver = FTDI_FT4222H(FTDI_FT4222H.SPIMode.SPI_IO_SINGLE, FTDI_FT4222H.SPIClock.CLK_DIV_64, FTDI_FT4222H.SPICPol.CLK_IDLE_LOW, FTDI_FT4222H.SPICPha.CLK_LEADING, False)

elif (DRV == "DBG2FESVR"):
  from dbg2fesvr import DBG2FESVR
  driver = DBG2FESVR()

elif (DRV == "PCIE2FPGA"):
  from pcie2fpga import PCIE2FPGA
  driver = PCIE2FPGA()

else:
  print('Starting with no driver...')
  driver = None
