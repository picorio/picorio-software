#include "FreeRTOS.h"
#include "task.h"
#include <string.h>
#include "timers.h"
#include "common.h"
#include "core.h"
#include "hal.h"
#include "io.h"
#include "rtlgen.h"
#include "util.h"
#include "portmacro.h"
#include "hal_sleep_manager.h"
#include "hal_sleep_driver.h"
#include "hal_rtc.h"

#define TICKLESS_DEBUG 0
#define RTC_CALIBRATION_ENABLE  0
#define GIVE_OWN_BIT   1
#define TICKLESS_TIME_ACCURACY_DEBUG   1

#if TICKLESS_DEBUG == 1
uint32_t bAbort = 0;
uint32_t workaround = 0;
#endif

#if RTC_CALIBRATION_ENABLE
uint32_t rtc_clock;
uint32_t rtc_clock_now;
#endif

volatile uint8_t ticklessStatus = 0;
uint32_t ticklessSTDBYL1Count   = 0;
uint32_t ticklessSTDBYL2Count   = 0;
uint32_t ticklessSTPCount   = 0;
uint32_t ticklessWFICount  = 0;
uint32_t ticklessTryCount  = 0;
extern void (*ptr_tickless_cb)(void*);

uint32_t ulAST_Reload_ms = 0;
uint32_t ulRealCompletedCnt;
uint32_t ulCompletedTick;

static uint32_t TimeStampCounter;
static uint32_t TimeStampSystick;

float RTC_Freq = 32.768f;

#if TICKLESS_TIME_ACCURACY_DEBUG
#define TICKLESS_DEBUG_TICKS  10000
extern uint32_t ulCompleteTickPeriods;
extern uint32_t ulCompletedSysTickDecrements;
static TimerHandle_t timer1 = NULL;
static TimerHandle_t timer2 = NULL;
#endif /* configUSE_TICKLESS_IDLE == 2 */

extern void vSetWakupTimerInterrupt(uint32_t ms); 
extern void vWakupTimerInterruptDisable(void);
extern void vWakupTimerInterruptEnable(void);
extern void vTimer1Disable(void);
extern void vSetWkupTimer1(uint32_t ms);
extern void rvHal_EnterStandbyL1(void);
extern void rvHal_EnterStandbyL2(void);
void hal_sleep_manager_enter_sleep(uint32_t sleep_time_ms);
void hal_sleep_manager_enter_stop(uint32_t sleep_time_ms);
void hal_sleep_manager_enter_standbyl1(uint32_t sleep_time_ms);
void hal_sleep_manager_enter_standbyl2(uint32_t sleep_time_ms);

inline static void rvHal_SetSleepStopMode(void)
{
	(*(uint64_t *) STATION_SLOW_IO_SCU_LP_MODE_ADDR) = 0x3;
}

inline static void rvHal_SetSleepSleepMode(void)
{
	(*(uint64_t *) STATION_SLOW_IO_SCU_LP_MODE_ADDR) = 0x0;
}

void hal_sleep_manager_enter_sleep_mode(hal_sleep_mode_t mode, uint32_t sleep_time_ms)
{
	if(mode >= HAL_SLEEP_MODE_NUMBER)
		return ;

    	RVHAL_OS_LOG_DEBUG("%s: %d mode = %lu, sleep_time_ms = %lu \r\n", __func__, __LINE__, mode, sleep_time_ms);
	switch(mode){
		case HAL_SLEEP_MODE_SLEEP:
			hal_sleep_manager_enter_sleep(sleep_time_ms);
			break;
		case HAL_SLEEP_MODE_STOP:
			hal_sleep_manager_enter_stop(sleep_time_ms);
			break;
		case HAL_SLEEP_MODE_STANDBYL1:
			hal_sleep_manager_enter_standbyl1(sleep_time_ms);
			break;
		case HAL_SLEEP_MODE_STANDBYL2:
			hal_sleep_manager_enter_standbyl2(sleep_time_ms);
			break;
		default:
			break;
	}
	return;	
}

void hal_sleep_manager_enter_sleep(uint32_t sleep_time_ms)
{
	vSetWakupTimerInterrupt(sleep_time_ms);
	// rvHal_SetSleepSleepMode();
	__asm volatile("wfi":::);
}

void hal_sleep_manager_enter_stop(uint32_t sleep_time_ms)
{
	vWakupTimerInterruptDisable();
	//rvHal_SetWakupRtcInterrupt(sleep_time_ms);
	//vSetWakupTimerInterrupt(sleep_time_ms);
	vSetWkupTimer1(sleep_time_ms);
	rvHal_SetSleepStopMode();
	//fence: dmb.st, isb
	//__asm volatile("fence w,w":::);
	//__asm volatile("fence.i":::);
       	//__asm volatile("fence r,r":::);
	//wfi
	__asm volatile("wfi":::);
	//after wakeup
	//rvHal_RtcDisable();
	vTimer1Disable();
	vWakupTimerInterruptEnable();
}

void hal_sleep_manager_enter_standbyl1(uint32_t sleep_time_ms)
{
	vWakupTimerInterruptDisable();
	//rvHal_SetWakupRtcInterrupt(sleep_time_ms);
	vSetWkupTimer1(sleep_time_ms);
	rvHal_SetSleepStopMode();
	rvHal_EnterStandbyL1();
	//rvHal_RtcDisable();
	vTimer1Disable();
	vWakupTimerInterruptEnable();
}

void hal_sleep_manager_enter_standbyl2(uint32_t sleep_time_ms)
{
	vWakupTimerInterruptDisable();
	//rvHal_SetWakupRtcInterrupt(sleep_time_ms);
	vSetWkupTimer1(sleep_time_ms);
	rvHal_SetSleepStopMode();
	rvHal_EnterStandbyL2();
	//rvHal_RtcDisable();
	vTimer1Disable();
	vWakupTimerInterruptEnable();
}

static void tickless_debug_timer_callback(TimerHandle_t expiredTimer)
{
    RVHAL_OS_LOG_DEBUG("sleep statistic info: tick:%u standbyl2:%u standbyl1:%u, stop:%u sleep:%u\n",
        (unsigned int)xTaskGetTickCount(),
        (unsigned int)ticklessSTDBYL2Count,
        (unsigned int)ticklessSTDBYL1Count,
        (unsigned int)ticklessSTPCount,
        (unsigned int)ticklessWFICount);
}

static void tickless_dummy_debug_timer_callback(TimerHandle_t expiredTimer)
{
}

extern unsigned int rvHal_irq_check_pending(void);

static void check_wakeup_src(void)
{
        unsigned int mtime_pending = 0;
        //check plic pending;
        rvHal_irq_check_pending();

        //check mtime wakeup pending;
        __asm volatile("csrr %0,mip":"=r"(mtime_pending));
    RVHAL_OS_LOG_DEBUG("mtime_pending = 0x%x\n", mtime_pending);

        return ;
}


/*
 *function: enter different lpm according xExpectedIdleTime
 *para: 
 *	xExpectedIdleTime: the time stay at idle state
 *		note: xExpectedIdleTime >= configEXPECTED_IDLE_TIME_BEFORE_SLEEP = 2, 
 *		      so, PlatformIdleOverhead should equal 0;
 * */
void vPortSuppressTicksAndSleep(TickType_t xExpectedIdleTime)
{
#if RTC_CALIBRATION_ENABLE
    static bool calibration_done = false;
#endif

    TickType_t xModifiableIdleTime;
    uint32_t nowCount;
    uint32_t nowTick;
    hal_sleep_manager_status_t ret;
    hal_sleep_mode_t mode;

    ticklessStatus = 30;
    RVHAL_OS_LOG_DEBUG("%s: %d xExpectedIdleTime = %lu \r\n", __func__, __LINE__, xExpectedIdleTime);
#if RTC_CALIBRATION_ENABLE
    if (calibration_done == false) {
        hal_rtc_get_f32k_frequency(&rtc_clock_now);
        if (rtc_clock_now == rtc_clock) {
            calibration_done = true;
            RTC_Freq =  ((float)(rtc_clock)/1000);
            RVHAL_OS_LOG_DEBUG("calibration done, %u, %f\n", rtc_clock, RTC_Freq);
        }
        rtc_clock = rtc_clock_now; 
    }
#endif

    /* Enter a critical section but don't use the taskENTER_CRITICAL()
       method as that will mask interrupts that should exit sleep mode. */
    portDISABLE_INTERRUPTS();
    rvHal_RtcGetCnt(&TimeStampCounter);
    vBackupCompenstateTime();

    ticklessStatus = 31;

    /* If a context switch is pending or a task is waiting for the scheduler
    to be unsuspended then abandon the low power entry. */
    if (eTaskConfirmSleepModeStatus() == eAbortSleep) {
        ticklessStatus = 32;
#if TICKLESS_DEBUG == 1
        bAbort = 1;
#endif
        /* Restart from whatever is left in the count register to complete
        this tick period. */
        //SysTick->LOAD = SysTick->VAL;

        /* Restart SysTick. */
        //SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;

        /* Reset the reload register to the value required for normal tick
        periods. */
        //SysTick->LOAD = ulTimerCountsForOneTick - 1UL;

        /* Re-enable interrupts - see comments above the cpsid instruction()
        above. */
	portENABLE_INTERRUPTS();

        ticklessStatus = 33;

        return;
    } else {
        ticklessStatus = 35;

        if ((xExpectedIdleTime >= StandbyL2Criteria) && (tickless_is_sleep_locked() == true)) {
            ulAST_Reload_ms = xExpectedIdleTime - 1 - StandbyL2ResumeOverhead;
            mode = HAL_SLEEP_MODE_STANDBYL2;
	} else if ((xExpectedIdleTime >= StandbyL1Criteria) && (tickless_is_sleep_locked() == true)) {
            ulAST_Reload_ms = xExpectedIdleTime - 1 - StandbyL1ResumeOverhead;
            mode = HAL_SLEEP_MODE_STANDBYL1;
        } else if (xExpectedIdleTime >= StopCriteria && (tickless_is_sleep_locked() == true)) {
            ulAST_Reload_ms = xExpectedIdleTime - 1 - StopResumeOverhead;
            mode = HAL_SLEEP_MODE_STOP;
        } else {
            ulAST_Reload_ms = xExpectedIdleTime - 1 - PlatformIdleOverhead;
            mode = HAL_SLEEP_MODE_SLEEP;
        }
        ticklessStatus = 36;

        /* Sleep until something happens.  configPRE_SLEEP_PROCESSING() can
        set its parameter to 0 to indicate that its implementation contains
        its own wait for interrupt or wait for event instruction, and so wfi
        should not be executed again.  However, the original expected idle
        time variable must remain unmodified, so a copy is taken. */
        xModifiableIdleTime = xExpectedIdleTime;

        configPRE_SLEEP_PROCESSING(xModifiableIdleTime);
        if (xModifiableIdleTime > 0) {
#ifdef HAL_WDT_PROTECTION_ENABLED
            hal_wdt_feed(HAL_WDT_FEED_MAGIC);
#endif
            ticklessStatus = 39;

            hal_sleep_manager_enter_sleep_mode(mode, ulAST_Reload_ms);
            ticklessStatus = 40;

            if (mode == HAL_SLEEP_MODE_SLEEP) {
                ticklessWFICount++;
            } else if (mode == HAL_SLEEP_MODE_STOP) {
                ticklessSTPCount++;
            } else if (mode == HAL_SLEEP_MODE_STANDBYL1) {
                ticklessSTDBYL1Count++;
            } else if (mode == HAL_SLEEP_MODE_STANDBYL2) {
                ticklessSTDBYL2Count++;
            } else {
                RVHAL_OS_LOG_DEBUG("unknown sleep mode %d\n", mode);
            }
            ticklessStatus = 44;

#ifdef HAL_WDT_PROTECTION_ENABLED
            hal_wdt_feed(HAL_WDT_FEED_MAGIC);
#endif
            ticklessStatus = 45;
        }
        configPOST_SLEEP_PROCESSING(xExpectedIdleTime);

        ticklessStatus = 46;

        //calculate time(systick) to jump
        rvHal_RtcGetCnt(&nowCount);
        ticklessStatus = 48;
        // get counter distance from last record
        if (nowCount >= TimeStampCounter) {
            ulRealCompletedCnt = nowCount - TimeStampCounter;
        } else {
            ulRealCompletedCnt = nowCount + (0xFFFFFFFF - TimeStampCounter);
        }

		// convert rtc counter to ms
		ulRealCompletedCnt = ulRealCompletedCnt * configTICK_RATE_HZ / (RTC_FREQ / PRESCALAR_CFG);

        RVHAL_OS_LOG_DEBUG("%s: %d, ulRealCompletedCnt = %lu nowCount = %lu  TimeStampCounter = %lu \r\n", __func__, __LINE__, ulRealCompletedCnt, nowCount, TimeStampCounter);
        __rvHal_irq_ext_restore();
        check_wakeup_src();
        //Limit OS Tick Compensation Value
        if (ulRealCompletedCnt > (xExpectedIdleTime - 1)) {
            ulRealCompletedCnt = xExpectedIdleTime - 1;
       }

        ticklessStatus = 49;

        //vTaskStepTick(xExpectedIdleTime * configTICK_RATE_HZ / 1000);
        vTaskStepTick(ulRealCompletedCnt);
	    // vSetCompenstateTime(ulRealCompletedCnt);

        ticklessStatus = 50;
	portENABLE_INTERRUPTS();
    }

    ticklessStatus = 51;

#if TICKLESS_DEBUG == 1
    RVHAL_OS_LOG_DEBUG("xExpectedIdleTime = %lu ms\n", xExpectedIdleTime);
    RVHAL_OS_LOG_DEBUG("bAbort = %lu\n", bAbort);
    RVHAL_OS_LOG_DEBUG("workaround = %lu\n", workaround);
    RVHAL_OS_LOG_DEBUG("ulAST_Reload_ms = %lu ms\n", ulAST_Reload_ms);
    bAbort = 0;
    workaround = 0;
    RVHAL_OS_LOG_DEBUG("tick: %lu  \n", (unsigned int)xTaskGetTickCount());
#endif
}

void tickless_init()
{
    timer1 = xTimerCreate("tickless_debug_timer",
                         TICKLESS_DEBUG_TICKS,
                         true,
                         NULL,
                         tickless_debug_timer_callback);

    if (timer1 == NULL) {
        RVHAL_OS_LOG_DEBUG("tickless_debug_timer create fail\n");
    } else {
        if (xTimerStart(timer1, 0) != pdPASS) {
            RVHAL_OS_LOG_DEBUG("tickless_debug_timer fail\n");
        } else {
            RVHAL_OS_LOG_DEBUG("tickless_debug_timer start\n");
        }
    }

}

uint32_t save_and_set_interrupt_mask(void)
{
	return 0;
}
void restore_interrupt_mask(uint32_t mask)
{
	return;
}

uint32_t tickless_handle_array[MAX_SLEEP_HANDLE];
uint32_t tickless_handle_ref_count[MAX_SLEEP_HANDLE];
tickless_ds_lock_struct ds_lock;

uint8_t tickless_set_sleep_handle(const char *handle_name)
{
    uint8_t handle_index = 0;
    uint32_t _savedMask;

    _savedMask = save_and_set_interrupt_mask();
    for (handle_index = 0; handle_index < MAX_SLEEP_HANDLE; handle_index++) {
        if (tickless_handle_array[handle_index] == 0) {
            ds_lock.handleCount++;
            tickless_handle_array[handle_index] = (uint32_t)handle_name;
            break;
        }
    }

    if (handle_index >= MAX_SLEEP_HANDLE) {
        RVHAL_OS_LOG_DEBUG("Fatal error, cannot get sleep handle\r\n");
        handle_index = INVALID_SLEEP_HANDLE;
    }
    restore_interrupt_mask(_savedMask);

    return handle_index;
}

tickless_status_t tickless_release_sleep_handle(uint8_t handle)
{
    uint32_t _savedMask;
    _savedMask = save_and_set_interrupt_mask();
    ds_lock.handleCount--;
    tickless_handle_array[handle] = 0;
    restore_interrupt_mask(_savedMask);

    return TICKLESS_OK;
}

tickless_status_t tickless_lock_sleep(uint8_t handle_index)
{
    if (handle_index < MAX_SLEEP_HANDLE) {
        uint32_t _savedMask;
        _savedMask = save_and_set_interrupt_mask();
        tickless_handle_ref_count[handle_index]++;
        ds_lock.sleepDisable |= (1 << handle_index);
        restore_interrupt_mask(_savedMask);
    }
    return TICKLESS_OK;
}

tickless_status_t tickless_unlock_sleep(uint8_t handle_index)
{
    if (handle_index < MAX_SLEEP_HANDLE) {
        uint32_t _savedMask;
        _savedMask = save_and_set_interrupt_mask();
        if (tickless_handle_ref_count[handle_index] != 0) {
            tickless_handle_ref_count[handle_index]--;
            if (tickless_handle_ref_count[handle_index] == 0) {
                ds_lock.sleepDisable &= ~(1 << handle_index);
            }
        } else {
            RVHAL_OS_LOG_DEBUG("lock has already released\r\n");
        }
        restore_interrupt_mask(_savedMask);
    }
    return TICKLESS_OK;
}

uint32_t tickless_get_lock_status(void)
{
    uint32_t lock;
    uint32_t _savedMask;

    _savedMask = save_and_set_interrupt_mask();
    lock = ds_lock.sleepDisable;
    restore_interrupt_mask(_savedMask);

    return lock;
}

bool tickless_is_sleep_locked(void)
{
    uint32_t _savedMask;
    bool lock;

    _savedMask = save_and_set_interrupt_mask();
    lock = ds_lock.sleepDisable != 0 ? true : false;
    restore_interrupt_mask(_savedMask);

    return lock;
}

