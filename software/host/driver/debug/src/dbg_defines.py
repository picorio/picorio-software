#!/usr/bin/env python3

# Global variable definitions

import re
import sys
import operator
import argparse
import os

projects = ['es1', 'es1x', 'es1y', 'anyka']
DRV_LIST = ["SPI_FT4222", "DBG2FESVR", "PCIE2FPGA"]
project = None
DRV = None

def parse_args():
  parser = argparse.ArgumentParser()
  parser.add_argument('--proj', dest='proj', type = str,
                      help='Project directory to use', required=True, choices = projects)
  parser.add_argument('--driver', dest= 'drv', type = str,
                      default = '', help='Driver to use', choices = DRV_LIST)

  args = parser.parse_args()
  global project
  global DRV
  project = args.proj
  DRV = args.drv
  if project:
    sys.path.append(os.path.join(os.getcwd(), "../%s/" % (project)))
  if DRV:
    sys.path.append(os.path.abspath(os.path.join(os.getcwd(), "../%s" % project)))

  global PHY_ADDR_WIDTH
  global RING_ADDR_WIDTH
  global MAX_RING_ADDR
  if project == 'es1':
    PHY_ADDR_WIDTH  = 40
    RING_ADDR_WIDTH = 25
    MAX_RING_ADDR = 0
  elif project == 'es1x':
    PHY_ADDR_WIDTH  = 32
    RING_ADDR_WIDTH = 25
    MAX_RING_ADDR = 0
  elif project == 'anyka':
    PHY_ADDR_WIDTH  = 32
    RING_ADDR_WIDTH = 25
  elif project == 'es1y':
    PHY_ADDR_WIDTH  = 32
    RING_ADDR_WIDTH = 25
    MAX_RING_ADDR = 0x1ffffff
  elif project == 'pygmy_e':
    PHY_ADDR_WIDTH  = 32
    RING_ADDR_WIDTH = 25
    MAX_RING_ADDR = 0x1ffffff

MEM_DEFAULT_TARGET = 'DMA'
MEM_TARGET_LIST = ['DMA', 'L2', 'DRAM']

DATA_WIDTH = 64

UNINITIALIZED = -1

# Register Location & Direct
# Located in STATION block
STATION = "STATION"
# Located in BLK
BLK = "BLOCK"
# Direct access
DIRECT = "DR"
# Indirect Access
INDIRECT = "IDR"

# SW Access Type
RW = "RW"
RO = "RO"
WO = "WO"

# HW Writable
WR_YES = "YES"
WR_NO = "NO"
WR_NA = "NA"

# TYPE
LOGIC = "LOGIC"

def convert_width(WIDTH):
  s = WIDTH.replace(" ", "")
  m = re.match(r'([0-9]+)(GB|MB|KB|B|$)', WIDTH)
  if m:
    if m.group(2) == "":
      return int(m.group(1))
    elif m.group(2) == "B":
      return int(m.group(1)) * 8
    elif m.group(2) == "KB":
      return int(m.group(1)) * 1024 * 8
    elif m.group(2) == "MB":
      return int(m.group(1)) * 1024 * 1024 * 8
    elif m.group(2) == "GB":
      return int(m.group(1)) * 1024 * 1024 * 1024 * 8
  else:
    sys.exit("FATAL: convert_width failed, WIDTH = %s" % WIDTH)

def find_base(ls_regblk, parent):
  hier = parent.split(".")
  assert (len(hier) == 2), "%s is a wrong PARENT_BLK specification" % parent
  base = UNINITIALIZED
  size = UNINITIALIZED
  for regblk in ls_regblk:
    if regblk.OWNER == hier[0]:
      for reg in regblk.REGLIST:
        if reg.NAME == hier[1]:
          base = reg.OFFSET
          size = reg.WIDTH_IN_BITS
  return (base, size)

def printVal2Python(val):
  s = ""
  if type(val) == str:
    s = "\"%s\"" % (val)
  elif type(val) == int:
    s = "%d" % (val)
  elif type(val) == list:
    s = "[%s]" % (", ".join(printVal2Python(x) for x in val))
  return (s)

def autogen_header():
  return '''
 ////////////////////////////////////////////
 // This is an auto-generated file         //
 // Support: Tian Lan (tian@ours-tech.com) //
 ////////////////////////////////////////////

'''

def company_header():
  return '''
 /*************************************************************************
 *
 *  OURS Technology, Inc CONFIDENTIAL
 *
 *
 *  2017 OURS Technology, Inc
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of OURS Technology Inc and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to OURS Technology Inc
 * and its suppliers and may be covered by U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from OURS Technology Inc.
 *
 * Author: tian@ours-tech.com
 *
 **************************************************************************/

'''

def set_phy_addr_width(width):
  global PHY_ADDR_WIDTH
  PHY_ADDR_WIDTH = width

def set_data_width(width):
  global DATA_WIDTH
  DATA_WIDTH = width

def set_ring_addr_width(width):
  global RING_ADDR_WIDTH
  RING_ADDR_WIDTH = width

def set_max_ring_addr(addr):
  global MAX_RING_ADDR
  MAX_RING_ADDR = addr

