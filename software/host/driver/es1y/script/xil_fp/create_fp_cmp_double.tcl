##################################################################
# CREATE IP XIL_fp_cmp_double
##################################################################

set floating_point XIL_fp_cmp_double
create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name $floating_point

set_property -dict { 
  CONFIG.Operation_Type {Compare}
  CONFIG.C_Compare_Operation {Condition_Code}
  CONFIG.A_Precision_Type {Double}
  CONFIG.C_A_Exponent_Width {11}
  CONFIG.C_A_Fraction_Width {53}
  CONFIG.Result_Precision_Type {Custom}
  CONFIG.C_Result_Exponent_Width {4}
  CONFIG.C_Result_Fraction_Width {0}
  CONFIG.C_Accum_Msb {32}
  CONFIG.C_Accum_Lsb {-31}
  CONFIG.C_Accum_Input_Msb {32}
  CONFIG.C_Mult_Usage {No_Usage}
  CONFIG.Flow_Control {NonBlocking}
  CONFIG.Has_RESULT_TREADY {false}
  CONFIG.C_Latency {2}
  CONFIG.C_Rate {1}
  CONFIG.Has_ACLKEN {true}
  CONFIG.Has_ARESETn {true}
} [get_ips $floating_point]

##################################################################

