#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <map>
//#include <time.h>
#include "common.h"
#include "debug_tick.h"
#include "pygmy_es1y_addr_translater.h"
#include "socket_client.h"
#include "common-socket.h"
//#define MEM_TESTER_DMA
uint64_t get_random_num()
{
    uint64_t rad = 0;  
    srand((int)time(NULL)); 
    rad = rand(); 
    return rad;
	
}
int main(int argc, char const *argv[]) {
  uint64_t addr = 0;
  uint64_t start_addr = 0;
  uint64_t wdata = 0;
  uint64_t rdata = 0;

  std::map<uint64_t, uint64_t> sb;
  std::map<uint64_t, uint64_t>::iterator it;


  //setup socket
  unsigned short serverPort = 0;
  struct sockaddr_in serverAddr;
  int clientSock;
  for (int i = 1; i < argc; i++) {
    if (strncmp(argv[i], "serverPort=", 11) == 0) {
      serverPort = atoi(argv[i] + 11);
    }
  }
  if (serverPort == 0) {
    serverPort = 8800; // Default
  }
  printf ("serverPort = %0d\n", serverPort);
  setup(serverPort, &serverAddr, &clientSock);

  do_write(&serverAddr, &clientSock, STATION_DT_DBG_ADDR_ADDR, addr);
  do_write(&serverAddr, &clientSock,STATION_CACHE_S2ICG_CLK_EN_ADDR_1,1);
  do_write(&serverAddr, &clientSock,STATION_CACHE_S2B_RSTN_ADDR_0, 1);
  do_write(&serverAddr, &clientSock,STATION_CACHE_S2B_RSTN_ADDR_1, 1);
  start_addr = 0x80000000;
  while (1) {
      // Do write
      addr = (get_random_num()%(2*1024*1024/8) << 3) + start_addr;  // Generate lower 12 bit address for torture
      //addr = (get_random_num()%(4*1024)) + start_addr;  // Generate lower 12 bit address for torture
      wdata = (get_random_num()<<32)+get_random_num()%0xffffffff;
      //L2
      //do_write(&serverAddr, &clientSock, addr2oraddr("l2_data", addr), wdata);
      //rdata =  do_read(&serverAddr, &clientSock, addr2oraddr("l2_data", addr));
      //DDR
      
      do_write(&serverAddr, &clientSock, STATION_DT_DBG_ADDR_ADDR, addr);
      do_write(&serverAddr, &clientSock, STATION_DT_DBG_DATA_ADDR, wdata);
      do_write(&serverAddr, &clientSock, STATION_DT_DBG_ADDR_ADDR, addr);
      rdata = do_read(&serverAddr, &clientSock, STATION_DT_DBG_DATA_ADDR);

      if(wdata != rdata){
	printf("FAIL: addr = 0x%08lx, data_exp = 0x%016lx, rdata = 0x%016lx\n", addr, wdata, rdata);
      }
      sb[addr]=wdata;
      printf("WR 0x%08lx 0x%016lx\n", addr, wdata);
      if(sb.size()>1){
      	it = sb.begin();
        std::advance(it, 1);
        //DDR
        do_write(&serverAddr, &clientSock, STATION_DT_DBG_ADDR_ADDR, it->first);
      	rdata =  do_read(&serverAddr, &clientSock, STATION_DT_DBG_DATA_ADDR);
        //L2
        //rdata =  do_read(&serverAddr, &clientSock, addr2oraddr("l2_data", it->first));
      	printf("RD 0x%08lx 0x%016lx\n", it->first, it->second);

      	// Data check
      	if (it->second != rdata) {
        	printf("FAIL: addr = 0x%08lx, data_exp = 0x%016lx, rdata = 0x%016lx\n", it->first, it->second, rdata);
       		break;
      }
   }
  }
}
