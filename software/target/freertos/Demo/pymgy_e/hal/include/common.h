#ifndef HAL_INCLUDE_COMMON_H
#define HAL_INCLUDE_COMMON_H

#include "hal_define.h"

#define ARRAY_SIZE(x) 			(sizeof(x) / sizeof((x)[0]))

#define MIN( a, b ) ( ( ( a ) < ( b ) ) ? ( a ) : ( b ) )
#define MAX( a, b ) ( ( ( a ) > ( b ) ) ? ( a ) : ( b ) )

#define write32(addr,value)		(*((volatile unsigned int *)(addr)))=(value)
#define read32(addr)			(*((volatile unsigned int *)(addr)))

#define write64(addr,value)		(*((volatile unsigned long long *)(addr)))=(value)
#define read64(addr)			(*((volatile unsigned long long *)(addr)))

#define BITS_PER_LONG 32

#ifndef BITS_PER_LONG_LONG
#define BITS_PER_LONG_LONG 64
#endif

#define BIT(nr)			(1UL << (nr))
#define BIT_ULL(nr)		(1ULL << (nr))
#define BIT_MASK(nr)		(1UL << ((nr) % BITS_PER_LONG))
#define BIT_WORD(nr)		((nr) / BITS_PER_LONG)
#define BIT_ULL_MASK(nr)	(1ULL << ((nr) % BITS_PER_LONG_LONG))
#define BIT_ULL_WORD(nr)	((nr) / BITS_PER_LONG_LONG)
#define BITS_PER_BYTE		8

/*
 * Create a contiguous bitmask starting at bit position @l and ending at
 * position @h. For example
 * GENMASK_ULL(39, 21) gives us the 64bit vector 0x000000ffffe00000.
 */
#define GENMASK(h, l) \
	(((~0UL) - (1UL << (l)) + 1) & (~0UL >> (BITS_PER_LONG - 1 - (h))))

#define GENMASK_ULL(h, l) \
	(((~0ULL) - (1ULL << (l)) + 1) & \
	 (~0ULL >> (BITS_PER_LONG_LONG - 1 - (h))))

#define ___constant_swab32(x) ((u32)(                         \
         (((u32)(x) & (u32)0x000000ffUL) << 24) |            \
         (((u32)(x) & (u32)0x0000ff00UL) <<  8) |            \
         (((u32)(x) & (u32)0x00ff0000UL) >>  8) |            \
         (((u32)(x) & (u32)0xff000000UL) >> 24)))

#if 1 /* for spi flash driver from kernel */ // TODO:
/**
 * list_is_last - tests whether @list is the last entry in list @head
 * @list: the entry to test
 * @head: the head of the list
 */
#include "list.h" // TODO: from freertos
static inline int list_is_last(const struct xLIST_ITEM *list, struct xLIST_ITEM *head)
{
	return list->pxNext == head;
}

#ifndef offsetof
#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#endif


#ifndef container_of
/**
 * container_of - cast a member of a structure out to the containing structure
 * @ptr:	the pointer to the member.
 * @type:	the type of the container struct this is embedded in.
 * @member:	the name of the member within the struct.
 *
 */
#define container_of(ptr, type, member) ({			\
	const typeof(((type *)0)->member) * __mptr = (ptr);	\
	(type *)((char *)__mptr - offsetof(type, member)); })
#endif

/**
 * list_entry - get the struct for this entry
 * @ptr:	the &struct list_head pointer.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the list_head within the struct.
 */
#define list_entry(ptr, type, member) \
	container_of(ptr, type, member)

/**
 * list_first_entry - get the first element from a list
 * @ptr:	the list head to take the element from.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the list_head within the struct.
 *
 * Note, that list is expected to be not empty.
 */
#define list_first_entry(ptr, type, member) \
	list_entry((ptr)->pxNext, type, member)

/**
 * list_last_entry - get the last element from a list
 * @ptr:	the list head to take the element from.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the list_head within the struct.
 *
 * Note, that list is expected to be not empty.
 */
#define list_last_entry(ptr, type, member) \
	list_entry((ptr)->pxPrevious, type, member)

/**
 * list_first_entry_or_null - get the first element from a list
 * @ptr:	the list head to take the element from.
 * @type:	the type of the struct this is embedded in.
 * @member:	the name of the list_head within the struct.
 *
 * Note that if the list is empty, it returns NULL.
 */
#define list_first_entry_or_null(ptr, type, member) \
	(!list_empty(ptr) ? list_first_entry(ptr, type, member) : NULL)

/**
 * list_pxNext_entry - get the pxNext element in list
 * @pos:	the type * to cursor
 * @member:	the name of the list_head within the struct.
 */
#define list_pxNext_entry(pos, member) \
	list_entry((pos)->member.pxNext, typeof(*(pos)), member)

/**
 * list_pxPrevious_entry - get the pxPrevious element in list
 * @pos:	the type * to cursor
 * @member:	the name of the list_head within the struct.
 */
#define list_pxPrevious_entry(pos, member) \
	list_entry((pos)->member.pxPrevious, typeof(*(pos)), member)

/**
 * list_for_each	-	iterate over a list
 * @pos:	the &struct list_head to use as a loop cursor.
 * @head:	the head for your list.
 */
#define list_for_each(pos, head) \
	for (pos = (head)->pxNext; pos != (head); pos = pos->pxNext)

/**
 * list_for_each_pxPrevious	-	iterate over a list backwards
 * @pos:	the &struct list_head to use as a loop cursor.
 * @head:	the head for your list.
 */
#define list_for_each_pxPrevious(pos, head) \
	for (pos = (head)->pxPrevious; pos != (head); pos = pos->pxPrevious)

/**
 * list_for_each_safe - iterate over a list safe against removal of list entry
 * @pos:	the &struct list_head to use as a loop cursor.
 * @n:		another &struct list_head to use as temporary storage
 * @head:	the head for your list.
 */
#define list_for_each_safe(pos, n, head) \
	for (pos = (head)->pxNext, n = pos->pxNext; pos != (head); \
		pos = n, n = pos->pxNext)

/**
 * list_for_each_pxPrevious_safe - iterate over a list backwards safe against removal of list entry
 * @pos:	the &struct list_head to use as a loop cursor.
 * @n:		another &struct list_head to use as temporary storage
 * @head:	the head for your list.
 */
#define list_for_each_pxPrevious_safe(pos, n, head) \
	for (pos = (head)->pxPrevious, n = pos->pxPrevious; \
	     pos != (head); \
	     pos = n, n = pos->pxPrevious)

/**
 * list_for_each_entry	-	iterate over list of given type
 * @pos:	the type * to use as a loop cursor.
 * @head:	the head for your list.
 * @member:	the name of the list_head within the struct.
 */
#define list_for_each_entry(pos, head, member)				\
	for (pos = list_first_entry(head, typeof(*pos), member);	\
	     &pos->member != (head);					\
	     pos = list_pxNext_entry(pos, member))
#endif

void srand1( uint32_t seed );
int32_t randr( int32_t min, int32_t max );
void memcpy1( uint8_t *dst, const uint8_t *src, uint16_t size );
void memset1( uint8_t *dst, uint8_t value, uint16_t size );
void memcpyr( uint8_t *dst, const uint8_t *src, uint16_t size );
void delay_cnt(int delay_cnt);
#endif /* HAL_INCLUDE_COMMON_H */

