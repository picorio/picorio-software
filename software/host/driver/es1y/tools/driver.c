
#include "ftd2xx.h"
#include "libft4222.h"
#include "spi_driver.h"
#include "pcie_driver.h"
#include "driver.h"

FT4222_SPIMode mode;
FT_HANDLE ftHandle;

uint64_t do_read (uint64_t addr, int target) {
  if (target == 0) {
    return do_read_pcie(addr);
  } else {
    printf("do_read: mode = %d, ftHandle = %d\n", mode, ftHandle);
    return do_read_spi(mode, ftHandle, addr);
  }
}

int do_write (uint64_t addr, uint64_t data, int target) {
  if (target == 0) {
    return do_write_pcie(addr, data);
  } else {
    printf("do_write: mode = %d, ftHandle = %d\n", mode, ftHandle);
    do_write_spi(mode, ftHandle, addr, data);
    return 0;
  }
}

void setup() {
  mode = SPI_IO_SINGLE;
  FT4222_SPIClock div  = CLK_DIV_512;
  FT4222_SPICPOL  cpol = CLK_IDLE_LOW;
  FT4222_SPICPHA  cpha = CLK_LEADING;
  ftHandle = setup_spi(mode, div, cpol, cpha);
}

void do_status_check() {
  do_status_check_spi(mode, ftHandle);
}

