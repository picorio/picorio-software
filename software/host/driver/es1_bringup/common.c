#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <inttypes.h>
#include "ftd2xx.h"
#include "libft4222.h"

inline std::string DeviceFlagToString(DWORD flags)
{
  std::string msg;
  msg += (flags & 0x1) ? "DEVICE_OPEN" : "DEVICE_CLOSED";
  msg += ", ";
  msg += (flags & 0x2) ? "High-speed USB" : "Full-speed USB";
  return msg;
}

std::vector< FT_DEVICE_LIST_INFO_NODE > ListFtUsbDevices()
{
  std::vector< FT_DEVICE_LIST_INFO_NODE > g_FT4222DevList;
  
  DWORD numOfDevices = 0;
  FT_STATUS status = FT_CreateDeviceInfoList(&numOfDevices);
  for (DWORD iDev = 0; iDev < numOfDevices; ++iDev)
  {
    FT_DEVICE_LIST_INFO_NODE devInfo;
    memset(&devInfo, 0, sizeof(devInfo));

    status = FT_GetDeviceInfoDetail(iDev,
             &devInfo.Flags, &devInfo.Type, &devInfo.ID, &devInfo.LocId,
             devInfo.SerialNumber, devInfo.Description, &devInfo.ftHandle);

    if (FT_OK == status)
    {
      printf("Dev %d:\n", iDev);
      printf("  Flags        = 0x%x, (%s)\n", devInfo.Flags,
                  DeviceFlagToString(devInfo.Flags).c_str());
      printf("  Type         = 0x%x\n",       devInfo.Type);
      printf("  ID           = 0x%x\n",       devInfo.ID);
      printf("  LocId        = 0x%x\n",       devInfo.LocId);
      printf("  SerialNumber = %s\n",         devInfo.SerialNumber);
      printf("  Description  = %s\n",         devInfo.Description);
      printf("  ftHandle     = 0x%x\n",       devInfo.ftHandle);

      const std::string desc = devInfo.Description;
      if (desc == "FT4222" || desc == "FT4222 A")
      {
        g_FT4222DevList.push_back(devInfo);
      }
    }
  }
  return g_FT4222DevList;
}

FT_HANDLE setup(FT4222_SPIMode mode, FT4222_SPIClock div, FT4222_SPICPOL cpol, FT4222_SPICPHA cpha) {

  std::vector< FT_DEVICE_LIST_INFO_NODE > g_FT4222DevList;

  g_FT4222DevList = ListFtUsbDevices();

  if (g_FT4222DevList.empty())
  {
    printf("No FT4222 device is found!\n");
    return NULL;
  }

  FT_HANDLE ftHandle = NULL;
  FT_STATUS ftStatus;
  for (int i = 0; i < g_FT4222DevList.size(); i++) {
    ftStatus = FT_OpenEx((PVOID)g_FT4222DevList[i].LocId,
                         FT_OPEN_BY_LOCATION, &ftHandle);
    if (FT_OK != ftStatus) {
      printf("Open a FT4222 device (index = %0d) failed!\n", i);
      continue;
    }

    // Check Product ID
    printf("Open FT4222 device (index = %0d)!\n", i);
    break;
  }

  ftStatus = FT4222_SPIMaster_Init(ftHandle, mode, div,
                                   cpol, cpha, 0x01);
  if (FT_OK != ftStatus)
  {
    printf("Init FT4222 as SPI master device failed!\n");
    return NULL;
  }
  return ftHandle;
}

void do_status_check(FT4222_SPIMode mode, FT_HANDLE ftHandle) {
  if (mode != SPI_IO_SINGLE) {
    uint8 *  readBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint8 *  writeBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint8    singleWriteBytes = 1;
    uint16   multiWriteBytes = 0;
    uint16   multiReadBytes = 1;
    uint32 * sizeOfRead = (uint32 *) malloc (sizeof(uint32));
    FT_STATUS ftStatus;

    writeBuffer[0] = 0xaa;
    readBuffer [0] = 0;

    ftStatus = FT4222_SPIMaster_MultiReadWrite(ftHandle, readBuffer,
                                               writeBuffer, singleWriteBytes,
                                               multiWriteBytes, multiReadBytes,
                                               sizeOfRead);
    if (FT_OK != ftStatus) {
      printf("do_status_check failed!\n");
    } else if (readBuffer[0] != 0x57) {
      printf("do_status_check failed! Status is not 0x57! Status = 0x%x\n", readBuffer[0]);
      abort();
    }
    //printf ("Status Check Passed\n");
  } else {
    uint8 *  readBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint8 *  writeBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint16 * sizeTransferred = (uint16 *) malloc (sizeof(uint16));
    BOOL     isEndTransaction = 1;
    uint16   sizeToTransfer = 2;
    FT_STATUS ftStatus;

    * sizeTransferred = 0;
    writeBuffer[0] = 0xaa;
    readBuffer [1] = 0;

    ftStatus = FT4222_SPIMaster_SingleReadWrite(ftHandle, readBuffer,
                                                writeBuffer, sizeToTransfer,
                                                sizeTransferred, isEndTransaction);

    if (FT_OK != ftStatus) {
      printf("do_status_check failed!\n");
    } else if (readBuffer[1] != 0x57) {
      printf("do_status_check failed! Status is not 0x57! Status = 0x%x\n", readBuffer[1]);
      abort();
    }
    //printf ("Status Check Passed\n");
  }
}

uint64 do_read(FT4222_SPIMode mode, FT_HANDLE ftHandle, uint64 addr) {
  if (mode != SPI_IO_SINGLE) {
    uint8 *  readBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint8 *  writeBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint8    singleWriteBytes = 1;
    uint16   multiWriteBytes = 6;
    uint16   multiReadBytes = 9;
    uint32 * sizeOfRead = (uint32 *) malloc (sizeof(uint32));
    FT_STATUS ftStatus;
    uint8    status = 0;
    uint64   ret_data = 0;
    
    writeBuffer[ 0] = 0x05; // Read
    writeBuffer[ 1] = (addr >> 32) & 0xff;
    writeBuffer[ 2] = (addr >> 24) & 0xff;
    writeBuffer[ 3] = (addr >> 16) & 0xff;
    writeBuffer[ 4] = (addr >>  8) & 0xff;
    writeBuffer[ 5] = ((addr & 0xffffffffffffffff) >>  0) & 0xff;
    //writeBuffer[ 5] = ((addr & 0xfffffffffffffff8) >>  0) & 0xff;
    writeBuffer[ 6] = 0xa5; // Turn around byte: 1010 0101

    do {
      *sizeOfRead = 0;
      for (int i = 0; i < 9; i++)
        readBuffer[i] = 0;
      ftStatus = FT4222_SPIMaster_MultiReadWrite(ftHandle, readBuffer,
                                                 writeBuffer, singleWriteBytes,
                                                 multiWriteBytes, multiReadBytes,
                                                 sizeOfRead);
      if ((FT_OK != ftStatus) || (*sizeOfRead != multiReadBytes)) {
        printf("do_read failed!\n");
      } else {
        status = readBuffer[8];
      }
      //for (int i = 0; i < 9; i ++)
      //  printf("readBuffer[%d] = 0x%x\n", i, readBuffer[i]);
      //printf("status = %x\n", status);
    } while ((status & 0x0f) != 0);

    for (int i = 0; i < 8; i++) {
      ret_data = ret_data << 8;
      ret_data = ret_data | readBuffer[i];
    }
    //printf ("Done Reading address 0x%llx, Got data 0x%llx\n", addr, ret_data);
    return ret_data;
  } else {
    uint8 *  readBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint8 *  writeBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint16 * sizeTransferred = (uint16 *) malloc (sizeof(uint16));
    BOOL     isEndTransaction = 1;
    uint16   sizeToTransfer = 16;
    FT_STATUS ftStatus;
    uint8    status = 0;
    uint64   ret_data = 0;

    * sizeTransferred = 0;
    writeBuffer[ 0] = 0x05; // Read
    writeBuffer[ 1] = (addr >> 32) & 0xff;
    writeBuffer[ 2] = (addr >> 24) & 0xff;
    writeBuffer[ 3] = (addr >> 16) & 0xff;
    writeBuffer[ 4] = (addr >>  8) & 0xff;
    //writeBuffer[ 5] = ((addr & 0xfffffffffffffff8) >>  0) & 0xff;
    writeBuffer[ 5] = ((addr & 0xffffffffffffffff) >>  0) & 0xff;
    writeBuffer[ 6] = 0xa5; // Turn around byte: 1010 0101
    writeBuffer[ 7] = 0x0;
    writeBuffer[ 8] = 0x0;
    writeBuffer[ 9] = 0x0;
    writeBuffer[10] = 0x0;
    writeBuffer[11] = 0x0;
    writeBuffer[12] = 0x0;
    writeBuffer[13] = 0x0;
    writeBuffer[14] = 0x0;
    writeBuffer[15] = 0x0;

    do {
      //fprintf (stderr, "Try Reading address 0x%llx\n", addr);
      *sizeTransferred = 0;
      for (int i = 0; i < 16; i++)
        readBuffer[i] = 0;
      ftStatus = FT4222_SPIMaster_SingleReadWrite(ftHandle, readBuffer,
                                                  writeBuffer, sizeToTransfer,
                                                  sizeTransferred, isEndTransaction);
      if (FT_OK != ftStatus) {
        printf("do_read failed!\n");
      } else {
        status = readBuffer[15];
      }
      // for (int i = 7; i < 16; i ++)
      //   printf("readBuffer[%d] = 0x%x\n", i, readBuffer[i]);
      // printf("readBuffer[15] = 0x%x\n", readBuffer[15]);
    } while ((status & 0x0f) != 0);

    for (int i = 0; i < 8; i++) {
      ret_data = ret_data << 8;
      ret_data = ret_data | readBuffer[i + 7];
    }
    //fprintf (stderr, "Done Reading address 0x%010llx, Got data 0x%016llx\n", addr, ret_data);
    return ret_data;
  }
}

void do_write(FT4222_SPIMode mode, FT_HANDLE ftHandle, uint64 addr, uint64 data) {
  if (mode != SPI_IO_SINGLE) {
    uint8 *  readBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint8 *  writeBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint8    singleWriteBytes = 1;
    uint16   multiWriteBytes = 13;
    uint16   multiReadBytes = 0;
    uint32 * sizeOfRead = (uint32 *) malloc (sizeof(uint32));
    FT_STATUS ftStatus;
    
    writeBuffer[ 0] = 0x04; // Write
    writeBuffer[ 1] = (addr >> 32) & 0xff;
    writeBuffer[ 2] = (addr >> 24) & 0xff;
    writeBuffer[ 3] = (addr >> 16) & 0xff;
    writeBuffer[ 4] = (addr >>  8) & 0xff;
    writeBuffer[ 5] = (addr >>  0) & 0xff;
    writeBuffer[ 6] = (data >> 56) & 0xff;
    writeBuffer[ 7] = (data >> 48) & 0xff;
    writeBuffer[ 8] = (data >> 40) & 0xff;
    writeBuffer[ 9] = (data >> 32) & 0xff;
    writeBuffer[10] = (data >> 24) & 0xff;
    writeBuffer[11] = (data >> 16) & 0xff;
    writeBuffer[12] = (data >>  8) & 0xff;
    writeBuffer[13] = (data >>  0) & 0xff;

    //for (int i = 0; i < 14; i ++)
    //  printf("writeBuffer[%d] = 0x%x\n", i, writeBuffer[i]);

    ftStatus = FT4222_SPIMaster_MultiReadWrite(ftHandle, readBuffer,
                                               writeBuffer, singleWriteBytes,
                                               multiWriteBytes, multiReadBytes,
                                               sizeOfRead);
    if (FT_OK != ftStatus) {
      printf("do_write failed!\n");
    } else {
      // No Timeout Case for WRITE for now
    }
    //printf ("Done Writing data 0x%lx to address 0x%lx\n", data, addr);
  } else {
    uint8 *  readBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint8 *  writeBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint16 * sizeTransferred = (uint16 *) malloc (sizeof(uint16));
    BOOL     isEndTransaction = 1;
    uint16   sizeToTransfer = 14;
    FT_STATUS ftStatus;

    * sizeTransferred = 0;
    writeBuffer[ 0] = 0x04; // Write
    writeBuffer[ 1] = (addr >> 32) & 0xff;
    writeBuffer[ 2] = (addr >> 24) & 0xff;
    writeBuffer[ 3] = (addr >> 16) & 0xff;
    writeBuffer[ 4] = (addr >>  8) & 0xff;
    writeBuffer[ 5] = (addr >>  0) & 0xff;
    writeBuffer[ 6] = (data >> 56) & 0xff;
    writeBuffer[ 7] = (data >> 48) & 0xff;
    writeBuffer[ 8] = (data >> 40) & 0xff;
    writeBuffer[ 9] = (data >> 32) & 0xff;
    writeBuffer[10] = (data >> 24) & 0xff;
    writeBuffer[11] = (data >> 16) & 0xff;
    writeBuffer[12] = (data >>  8) & 0xff;
    writeBuffer[13] = (data >>  0) & 0xff;

//for (int i = 0; i < 2; i++) {
    ftStatus = FT4222_SPIMaster_SingleReadWrite(ftHandle, readBuffer,
                                                writeBuffer, sizeToTransfer,
                                                sizeTransferred, isEndTransaction);

    if (FT_OK != ftStatus) {
      printf("do_write failed!\n");
    } else {
      // No Timeout Case for WRITE for now
    }
    //fprintf (stderr, "Done Writing data 0x%lx to address 0x%lx\n", data, addr);
//}
//for (int i = 0; i < 2; i++) {
//    if ((((addr >> 32) & 0xff) != 0xa8) && (((addr >> 32) & 0xff) != 0xa0) && ((addr >> 36) != 0x8)) {
//      uint64 d;
//      d = do_read(mode, ftHandle, addr);
//      if (d != data) {
//        printf ("Warning: Mismatch, cc addr = 0x%" PRIx64 ", expect = 0x%" PRIx64 ", receive = 0x%" PRIx64 "\n", addr, data, d);
//        //exit(1);
//      }
//    }
//}
  }
}

