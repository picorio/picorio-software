#include "hal.h"
#include "clib.h"
#include "timers.h"

#define SPIM_DEBUG_BASE_ADDR (0x100)

#define SPIM_CTRLR0_ADDR (0x0)
#define SPIM_CTRLR1_ADDR (0x4)
#define SPIM_SSIENR_ADDR (0x8)
#define SPIM_MWCR_ADDR (0xc)
#define SPIM_SER_ADDR (0x10)
#define SPIM_BAUDR_ADDR (0x14)
#define SPIM_TXFTLR_ADDR (0x18)
#define SPIM_RXFTLR_ADDR (0x1c)
#define SPIM_TXFLR_ADDR (0x20)
#define SPIM_RXFLR_ADDR (0x24)
#define SPIM_SR_ADDR (0x28)
#define SPIM_IMR_ADDR (0x2c)
#define SPIM_ISR_ADDR (0x30)
#define SPIM_RISR_ADDR (0x34)
#define SPIM_TXOICR_ADDR (0x38)
#define SPIM_RXOICR_ADDR (0x3c)
#define SPIM_RXUICR_ADDR (0x40)
#define SPIM_MSTICR_ADDR (0x44)
#define SPIM_ICR_ADDR (0x48)
#define SPIM_DMACR_ADDR (0x4c)
#define SPIM_DMATDLR_ADDR (0x50)
#define SPIM_DMARDLR_ADDR (0x54)
#define SPIM_IDR_ADDR (0x58)
#define SPIM_SSI_VERSION_ID_ADDR (0x5c)
#define SPIM_DR0_ADDR (0x60)
#define SPIM_DR1_ADDR (0x64)
#define SPIM_DR2_ADDR (0x68)
#define SPIM_DR3_ADDR (0x6c)
#define SPIM_DR4_ADDR (0x70)
#define SPIM_DR5_ADDR (0x74)
#define SPIM_DR6_ADDR (0x78)
#define SPIM_DR7_ADDR (0x7c)
#define SPIM_DR8_ADDR (0x80)
#define SPIM_DR9_ADDR (0x84)
#define SPIM_DR10_ADDR (0x88)
#define SPIM_DR11_ADDR (0x8c)
#define SPIM_DR12_ADDR (0x90)
#define SPIM_DR13_ADDR (0x94)
#define SPIM_DR14_ADDR (0x98)
#define SPIM_DR15_ADDR (0x9c)
#define SPIM_DR16_ADDR (0xa0)
#define SPIM_DR17_ADDR (0xa4)
#define SPIM_DR18_ADDR (0xa8)
#define SPIM_DR19_ADDR (0xac)
#define SPIM_DR20_ADDR (0xb0)
#define SPIM_DR21_ADDR (0xb4)
#define SPIM_DR22_ADDR (0xb8)
#define SPIM_DR23_ADDR (0xbc)
#define SPIM_DR24_ADDR (0xc0)
#define SPIM_DR25_ADDR (0xc4)
#define SPIM_DR26_ADDR (0xc8)
#define SPIM_DR27_ADDR (0xcc)
#define SPIM_DR28_ADDR (0xd0)
#define SPIM_DR29_ADDR (0xd4)
#define SPIM_DR30_ADDR (0xd8)
#define SPIM_DR31_ADDR (0xdc)
#define SPIM_DR32_ADDR (0xe0)
#define SPIM_DR33_ADDR (0xe4)
#define SPIM_DR34_ADDR (0xe8)
#define SPIM_DR35_ADDR (0xec)
#define SPIM_RX_SAMPLE_DLY_ADDR (0xf0)
#define SPIM_SPI_CTRLR0_ADDR (0xf4)
#define SPIM_TXD_DRIVE_EDGE_ADDR (0xf8)
#define SPIM_RSVD_ADDR (0xfc)

#define SPIM_VALID_NUM(num)		((num) < (NUM_SPIM_MAX))

static unsigned int reg_spim = STATION_SLOW_IO_SSPIM1_BLOCK_REG_ADDR;

static void spi_delay(int delay_cnt)
{
	while(delay_cnt>0)
	{
		delay_cnt--;		
	}
}

#if 1
#if 0
unsigned int rvHal_spim_rw( unsigned int addr )
{
#if 1
        // TODO: 8/16/32BIT!!32bit here...
        // Loop while DR register is not empty
        while ( ( read32(reg_spim+SPIM_SR_ADDR) & BIT(2)) == 0x00 );
//		while(( read32(reg_spim+SPIM_SR_ADDR) & BIT(1)) != 0)
        // Send byte through the SPI peripheral
        write32(reg_spim+SPIM_DR0_ADDR, addr);
#endif
        // Wait to receive a byte
        while ( (read32(reg_spim+SPIM_SR_ADDR) & BIT(3)) == 0x00 );
        // Return the byte read from the SPI bus
        return read32(reg_spim+SPIM_DR0_ADDR);
}

unsigned int rvHal_spim_w( unsigned int addr, unsigned int data )
{
#if 1
        // TODO: 8/16/32BIT!!32bit here...
        // Loop while DR register is not empty
        while ( ( read32(reg_spim+SPIM_SR_ADDR) & BIT(2)) == 0x00 );

        // Send byte through the SPI peripheral
        write32(reg_spim+SPIM_DR0_ADDR, ((addr|0x80)<<8)|data);
#endif
        // Wait to receive a byte
        while ( (read32(reg_spim+SPIM_SR_ADDR) & BIT(3)) == 0x00 );
        // Return the byte read from the SPI bus
        return read32(reg_spim+SPIM_DR0_ADDR);
}
#endif

#if 0//try to best to tx/rx much more bytes, but software & hardware are async... and speed is mismatch

void rvHal_spim_w1( unsigned char addr, unsigned int *buffer)
{
#if 1
	int i = 0, pad = 0, num= 0, left = 8;
        while ( ( read32(reg_spim+SPIM_SR_ADDR) & BIT(0)) );

	while (read32(reg_spim+SPIM_SR_ADDR) & BIT(1)) {
		write32(reg_spim+SPIM_DR0_ADDR, (0==i)?(addr|0x80):0xaa);
		i++;
	}
	write32(reg_spim+SPIM_SER_ADDR, 1);
	while(1){
		pad = read32(reg_spim+SPIM_TXFLR_ADDR);
		if (0 == pad) {printf("tx over...\n");break;}
		num = 8 - pad;
		if (0<num && 0<left) {
			pad = MIN(num, left);
			for (i=0; i<pad; i++)
				write32(reg_spim+SPIM_DR0_ADDR, 0xaa);
			if (left > num) left -= num; else break;
		}
	}

        while ( ( read32(reg_spim+SPIM_SR_ADDR) & BIT(0)) );
//printf("sr: 0x%x\n", read32(reg_spim+SPIM_SR_ADDR)); 0x1e
        // Loop while DR register is not empty
		i=0;
        while ( ( read32(reg_spim+SPIM_SR_ADDR) & BIT(3)) && i<16) {
//		while(( read32(reg_spim+SPIM_SR_ADDR) & BIT(1)) != 0)
        // Send byte through the SPI peripheral
        	buffer[i++] = read32(reg_spim+SPIM_DR0_ADDR);
        }
#endif
	write32(reg_spim+SPIM_SER_ADDR, 0);
//printf("rx cnt: %d\n", i);
        // Return the byte read from the SPI bus
 //       return read32(reg_spim+SPIM_DR0_ADDR);
//printf("once: %d\n", once);
}

void rvHal_spim_r1( unsigned char addr, unsigned int *buffer)
{
#if 1
	int i = 0, once = 0, rx_idx = 0, rx_num = 0, rx_left = 16;
	int flr = 0, num= 0, left = 8;
	int tx_over = 0;

	write32(reg_spim+SPIM_SER_ADDR, 0);
        while ( ( read32(reg_spim+SPIM_SR_ADDR) & BIT(0)) );

//		printf("left: 0x%x\n", read32(reg_spim+SPIM_TXFLR_ADDR));
	while (read32(reg_spim+SPIM_SR_ADDR) & BIT(1)) {
		write32(reg_spim+SPIM_DR0_ADDR, (0==once)?(addr):0);
		once++;
	}
	write32(reg_spim+SPIM_SER_ADDR, 1);
	while(1){
        flr = read32(reg_spim+SPIM_RXFLR_ADDR);
//printf("rx flr: %u...\n", flr);
		if (0 < flr) {
			if (0 < rx_left) {
				flr = MIN(flr, rx_left);
				rx_left-=flr;
				while(flr--)
					buffer[rx_idx++] = read32(reg_spim+SPIM_DR0_ADDR);
			} else break;
		}
		
		if (tx_over)
			continue;

		flr = read32(reg_spim+SPIM_TXFLR_ADDR);
		if (0 == flr) {printf("tx over...\n");tx_over=1;continue;}
		num = 8 - flr;
		if (0<num && 0<left) {
			flr = MIN(num, left);
			for (i=0; i<flr; i++)
				write32(reg_spim+SPIM_DR0_ADDR, 0);
			if (left > num) left -= num; else {tx_over=1;continue;}
		}
//printf("rx %u...\n", read32(reg_spim+SPIM_RXFLR_ADDR));
	}
//		i=0;
#if 0
        while ( 1) {
			if ( read32(reg_spim+SPIM_SR_ADDR) & BIT(4)) {
				for (i=0;i<8;i++)
					buffer[i] = read32(reg_spim+SPIM_DR0_ADDR);
				break;
			}
        }
//#else
        while ( 1) {
			if ( read32(reg_spim+SPIM_RXFLR_ADDR)) {
				for (i=0;i<8;i++)
					buffer[i] = read32(reg_spim+SPIM_DR0_ADDR);
				break;
			}
        }		
#endif
#if 0
        while ( ( read32(reg_spim+SPIM_SR_ADDR) & BIT(0)) );
//printf("sr: 0x%x\n", read32(reg_spim+SPIM_SR_ADDR)); 0x1e
        // Loop while DR register is not empty

        while ( ( read32(reg_spim+SPIM_SR_ADDR) & BIT(3)) && rx_idx<16) {
//		while(( read32(reg_spim+SPIM_SR_ADDR) & BIT(1)) != 0)
        // Send byte through the SPI peripheral
        	buffer[rx_idx++] = read32(reg_spim+SPIM_DR0_ADDR);
        }
#endif
#endif
	write32(reg_spim+SPIM_SER_ADDR, 0);
        // Return the byte read from the SPI bus
 //       return read32(reg_spim+SPIM_DR0_ADDR);
printf("rx ttl: %d\n", rx_idx);
}
//#else
void rvHal_spim_r( unsigned char addr, unsigned int *buffer)
{
#if 1
	int i = 0, once = 0;
        while ( ( read32(reg_spim+SPIM_SR_ADDR) & BIT(0)) );

	while (read32(reg_spim+SPIM_SR_ADDR) & BIT(1)) {
		write32(reg_spim+SPIM_DR0_ADDR, (0==once)?addr<<24:0);
		once++;
	}
	write32(reg_spim+SPIM_SER_ADDR, 1);
	while (read32(reg_spim+SPIM_SR_ADDR) & BIT(1) && i<8) {
		write32(reg_spim+SPIM_DR0_ADDR, 0);i++;
	}
        while ( ( read32(reg_spim+SPIM_SR_ADDR) & BIT(0)) );

        // Loop while DR register is not empty
        while ( ( read32(reg_spim+SPIM_SR_ADDR) & BIT(3)) && i<16) {
//		while(( read32(reg_spim+SPIM_SR_ADDR) & BIT(1)) != 0)
        // Send byte through the SPI peripheral
        	buffer[i++] = read32(reg_spim+SPIM_DR0_ADDR);
        }
#endif
	write32(reg_spim+SPIM_SER_ADDR, 0);
        // Return the byte read from the SPI bus
 //       return read32(reg_spim+SPIM_DR0_ADDR);
}
#endif
#define SPIM_SR_BUSY	BIT(0)
#define SPIM_SR_TFNF	BIT(1)
#define SPIM_SR_TFE		BIT(2)
#define SPIM_SR_RFNE	BIT(3)
#if 0
void rvHal_spim_w( unsigned short addr, unsigned char *buffer, unsigned char size)
{
#if 1
	int i = 0, once = 0;
        while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	while (read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_TFNF) {
		write32(reg_spim+SPIM_DR0_ADDR, (0==once)?addr<<24:0);
		once++;
	}
	write32(reg_spim+SPIM_SER_ADDR, 1);

        while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

        // Loop while DR register is not empty
        while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_RFNE) && i<16) {
//		while(( read32(reg_spim+SPIM_SR_ADDR) & BIT(1)) != 0)
        // Send byte through the SPI peripheral
        	buffer[i++] = read32(reg_spim+SPIM_DR0_ADDR);
        }
#endif
	write32(reg_spim+SPIM_SER_ADDR, 0);
        // Return the byte read from the SPI bus
 //       return read32(reg_spim+SPIM_DR0_ADDR);
}
#endif

//#define SHIFT_IN_32BIT(shift)				((3 - (shift)) << 3)
#define BYTE_VALUE_IN_32BIT(value, shift)	(((value) >> (shift)) & 0xff)
#if 0// change dfs dynamic: need re-enable SPIM_SSIENR_ADDR that will clear all fifo data !!
void rvHal_spim_read( unsigned short addr, unsigned char *buffer, unsigned char size)
{
	unsigned char num32 = (size >> 2) + 1;
	unsigned int value32 = 0;
	int i = 0, once = 0, shift = 0;

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	while ((read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_TFNF) && (num32--)) {
		write32(reg_spim+SPIM_DR0_ADDR, (0==once)?addr<<24:0);
		once++;
	}
//printf("once: %d\n", once);
	write32(reg_spim+SPIM_SER_ADDR, 1);

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	once = 0;
	num32 = (size >> 2) + 1;
	while ( (( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_RFNE) ) && (num32--)) {
		value32 = read32(reg_spim+SPIM_DR0_ADDR);
//printf("rx %d value32=0x%x\n", i, value32);
		shift = (once++) ? 24 : 16;//!(once++);
		for (; (0 <= shift) && (i < size) ; i++, shift-=8) {
			buffer[i] = BYTE_VALUE_IN_32BIT(value32, shift);
//printf("2 value32=0x%x, buffer[%d]=0x%x, shift=%d\n", value32, i, buffer[i], shift);
		}
	}

	write32(reg_spim+SPIM_SER_ADDR, 0);
//printf("test=%d, %d, %d, %d\n", test, i, shift, once);
}

void rvHal_spim_write( unsigned short addr, unsigned char *buffer, unsigned char size)
{
	
////////////
	unsigned char loop_cnt = (size >> 2) + 1;
	unsigned char byte_cnt = size - ((size >> 2) << 2);
	unsigned int dfs = 0;
	unsigned int value32 = 0;
	int i = 0, once = 0, shift = 0, dfs32 = 1, reg_ctrlr0 = 0;

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	while ((read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_TFNF) && (loop_cnt--)) {
		if (0 < loop_cnt) {
			if (once) {
				shift = 24;
				value32 = 0;
			} else {
				shift = 16;
				value32 = (addr | 0x80) << 24;
				once = 1;
			}

			for (; (0 <= shift) && (i < size) ; i++, shift-=8) {
				value32 |= (buffer[i] << shift);
			}
//printf("w i: %d, value32 : 0x%x\n", i, value32);
			write32(reg_spim+SPIM_DR0_ADDR, value32);
		} else {
			if (once) {
				shift = byte_cnt << 3;
				value32 = 0;
			} else {
				shift = (byte_cnt - 1 ) << 3; // TODO: byte_cnt => 1
				value32 = (addr | 0x80) << (shift + 8);
				once = 1;
			}

			for (; (0 <= shift) && (i < size) ; i++, shift-=8) {
				value32 |= (buffer[i] << shift);
			}
			
			dfs = (8 * byte_cnt + 7) << 16;
			if (0x1f0000 != dfs) {
				dfs32 = 0;
			}
			reg_ctrlr0 = read32(reg_spim+SPIM_CTRLR0_ADDR);

//	write32(reg_spim+SPIM_SSIENR_ADDR, 0x0);
			write32(reg_spim+SPIM_CTRLR0_ADDR, (reg_ctrlr0 & ~0x1f0000) | dfs);			
//	write32(reg_spim+SPIM_SSIENR_ADDR, 0x1);
//			spi_delay(1000);
//printf("w i: %d, value32 : 0x%x - 0x%x - 0x%x\n", i, value32, dfs, (reg_ctrlr0 & ~0x1f0000) | dfs);
//rvHal_delay(1000);
			write32(reg_spim+SPIM_DR0_ADDR, value32);
		}
	}
//printf("once: %d\n", once);
	write32(reg_spim+SPIM_SER_ADDR, 1);
#if 1
	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_RFNE) ) {
		value32 = read32(reg_spim+SPIM_DR0_ADDR);
	}
	value32++; /* must add this line to avoid compiler omit this read */

	write32(reg_spim+SPIM_SER_ADDR, 0);
#endif
	if (0 == dfs32)
		write32(reg_spim+SPIM_CTRLR0_ADDR, (reg_ctrlr0 | 0x1f0000));
//printf("test=%d, %d, %d, %d\n", test, i, shift, once);
}
#else
void rvHal_spim_read( unsigned short addr, unsigned char *buffer, unsigned char size)
{
	unsigned char num32 = (size >> 2) + 1;
	unsigned int value32 = 0;
	int i = 0, once = 0, shift = 0;

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	while ((read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_TFNF) && (num32--)) {
		write32(reg_spim+SPIM_DR0_ADDR, (0==once)?addr<<24:0);
		once++;
	}
//printf("once: %d\n", once);
	write32(reg_spim+SPIM_SER_ADDR, 1);

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	once = 0;
	num32 = (size >> 2) + 1;
	while ( (( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_RFNE) ) && (num32--)) {
		value32 = read32(reg_spim+SPIM_DR0_ADDR);
//printf("rx %d value32=0x%x\n", i, value32);
		shift = (once++) ? 24 : 16;//!(once++);
		for (; (0 <= shift) && (i < size) ; i++, shift-=8) {
			buffer[i] = BYTE_VALUE_IN_32BIT(value32, shift);
//printf("2 value32=0x%x, buffer[%d]=0x%x, shift=%d\n", value32, i, buffer[i], shift);
		}
	}

	write32(reg_spim+SPIM_SER_ADDR, 0);
//printf("test=%d, %d, %d, %d\n", test, i, shift, once);
}

#if 0
void rvHal_spim_write( unsigned short addr, unsigned char *buffer, unsigned char size)
{
	unsigned char num32 = (size >> 2) + 1;
	unsigned int value32 = 0;
	int i = 0, once = 0, shift = 0;

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	while ((read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_TFNF) && (num32--)) {
		if (once) {
			shift = 24;
			value32 = 0;
		} else {
			shift = 16;
			value32 = (addr | 0x80) << 24;
			once = 1;
		}

		for (; (0 <= shift) && (i < size) ; i++, shift-=8) {
			value32 |= (buffer[i] << shift);
		}

printf("tx %d value32=0x%x\n", i, value32);
		write32(reg_spim+SPIM_DR0_ADDR, value32);
	}
//printf("once: %d\n", once);
	write32(reg_spim+SPIM_SER_ADDR, 1);
#if 1
	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_RFNE) ) {
		value32 = read32(reg_spim+SPIM_DR0_ADDR);
	}
	value32++; /* must add this line to avoid compiler omit this read */

	write32(reg_spim+SPIM_SER_ADDR, 0);
#endif
//printf("test=%d, %d, %d, %d\n", test, i, shift, once);
}
#else // read firstly to avoid overwrite 0 for left bis in 32bit width.
void rvHal_spim_write( unsigned short addr, unsigned char *buffer, unsigned char size)
{
	unsigned char num32 = (size >> 2) + 1;
	unsigned int value32 = 0;
	int i = 0, once = 0, shift = 0;

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	while ((read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_TFNF) && (num32--)) {
		if (once) {
			shift = 24;
			value32 = 0;
		} else {
			shift = 16;
			value32 = (addr | 0x80) << 24;
			once = 1;
		}

		for (; (0 <= shift) && (i < size) ; i++, shift-=8) {
			value32 |= (buffer[i] << shift);
		}

//printf("tx %d value32=0x%x\n", i, value32);
		write32(reg_spim+SPIM_DR0_ADDR, value32);
	}
//printf("once: %d\n", once);
	write32(reg_spim+SPIM_SER_ADDR, 1);
#if 1
	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_RFNE) ) {
		value32 = read32(reg_spim+SPIM_DR0_ADDR);
	}
	value32++; /* must add this line to avoid compiler omit this read */

	write32(reg_spim+SPIM_SER_ADDR, 0);
#endif
//printf("test=%d, %d, %d, %d\n", test, i, shift, once);
}
#endif
#endif

#if 1
void rvHal_spim_read_1262( unsigned char *abuffer, unsigned char asize, unsigned char *buffer, unsigned char size)
{
	unsigned char tsize = asize+size;
	unsigned char num32 = ((tsize-1) >> 2) + 1;
	unsigned int value32 = 0;
	int i = 0, j, once = 0, shift = 0;

	RVHAL_ASSERT_PARAM((NULL != abuffer) && (asize < 5) && (tsize <= RVHAL_SPIM_FIFO_SIZE_MAX));

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

        while ((read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_TFNF) && (num32--)) {
                value32 = 0;
                if (0 == once) {
                        for (j=0, shift=24; j<asize; j++, shift-=8)
                                value32 |= (abuffer[j] << shift);
                        once = 1;
                }

//printf("write abuffer: 0x%x\n", value32);
		write32(reg_spim+SPIM_DR0_ADDR, value32);
        }
//printf("once: %d\n", once);
        write32(reg_spim+SPIM_SER_ADDR, 1);

        while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

        once = 0;
        num32 = ((tsize-1) >> 2) + 1;

//printf("read %d-%d-%d, sr:0x%x-0x%x\n", once, num32, asize, read32(reg_spim+SPIM_SR_ADDR), read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_RFNE);
	
	if ((4 == asize) && (( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_RFNE) )) {
		once = 1;
		num32--;
		value32 = read32(reg_spim+SPIM_DR0_ADDR); // TODO: gcc
//printf("read abuffer0: 0x%x\n", value32);
	}
	while ( (( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_RFNE) ) && (num32--)) {
        value32 = read32(reg_spim+SPIM_DR0_ADDR);  // TODO: gcc
//printf("read abuffer: 0x%x\n", value32);
		if (NULL != buffer) {
			shift = (once++) ? 24: shift;//!(once++);
	        for (; (0 <= shift) && (i < size) ; i++, shift-=8)
                buffer[i] = BYTE_VALUE_IN_32BIT(value32, shift);
		}
    }

        write32(reg_spim+SPIM_SER_ADDR, 0);
//printf("test=%d, %d, %d, %d\n", test, i, shift, once);
}

void rvHal_spim_write_1262( unsigned char *abuffer, unsigned char asize, unsigned char *buffer, unsigned char size)
{
	unsigned char tsize = asize+size;
	unsigned char num32 = ((tsize-1) >> 2) + 1;
	unsigned int value32 = 0;
	int i = 0, j, once = 0, shift = 0;

	RVHAL_ASSERT_PARAM((NULL != abuffer) && (asize < 4) && (tsize <= RVHAL_SPIM_FIFO_SIZE_MAX));// TODO: all args

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	while ((read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_TFNF) && (num32--)) {
		value32 = 0;
		if (once) {
			shift = 24;
		} else {
			for (j=0, shift=24; j<asize; j++, shift-=8)
				value32 |= (abuffer[j] << shift);
			once = 1;
		}

		if (NULL != buffer) {
			for (; (0 <= shift) && (i < size) ; i++, shift-=8)
				value32 |= (buffer[i] << shift);
		}

//printf("tx %d value32=0x%x\n", num32, value32);
		write32(reg_spim+SPIM_DR0_ADDR, value32);
	}
//printf("once: %d\n", once);
	write32(reg_spim+SPIM_SER_ADDR, 1);
#if 1
	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_RFNE) ) {
		value32 = read32(reg_spim+SPIM_DR0_ADDR);
	}
	value32++; /* must add this line to avoid compiler omit this read */

	write32(reg_spim+SPIM_SER_ADDR, 0);
#endif
//printf("test=%d, %d, %d, %d\n", test, i, shift, once);
}
#endif

#if 0
void rvHal_spim_write_snps(unsigned char *abuffer, unsigned char asize, unsigned char *dbuffer, unsigned char dsize)
{
	int i = 0;
	unsigned char num_entry, num_byte, size = asize;
	unsigned char *buffer = abuffer;
	unsigned int value32 = 0;

	if ((NULL == abuffer) || (0 == asize)) {
		buffer = dbuffer;
		size = dsize;
	}

	num_entry = size >> 2;
	num_byte = size - (num_entry << 2);
	num_entry++;

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	while ((read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_TFNF) && (num_entry--)) {
		value32 = abuffer[i];
		write32(reg_spim+SPIM_DR0_ADDR, value32);
	}

	write32(reg_spim+SPIM_SER_ADDR, 1);

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_RFNE) ) {
		value32 = read32(reg_spim+SPIM_DR0_ADDR);
	}
	value32++; /* must add this line to avoid compiler omit this read */

	write32(reg_spim+SPIM_SER_ADDR, 0);
}

void rvHal_spim_read_snps(unsigned char *abuffer, unsigned char asize, unsigned char *dbuffer, unsigned char dsize)
{
	int i = 0;
	unsigned char num_entry, num_aentry, num_abyte, size;
	unsigned int value32 = 0;

	RVHAL_ASSERT_PARAM((NULL != abuffer) && (0 != asize));

	size = asize + dsize;
	num_entry = size >> 2;
	num_aentry = asize >> 2;
	num_abyte = asize - (num_aentry << 2);

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	do {
		if (!(read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_TFNF)) {
			break;
		}

		for (; i<asize; i++) {
			value32 |= abuffer[i] << (24 - (i << 3));
		}

		write32(reg_spim+SPIM_DR0_ADDR, value32);
	} while(num_entry--);

	write32(reg_spim+SPIM_SER_ADDR, 1);

	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );

	once = 0;
	num32 = (size >> 2) + 1;
	while ( (( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_RFNE) ) && (num32--)) {
		value32 = read32(reg_spim+SPIM_DR0_ADDR);
//printf("rx %d value32=0x%x\n", i, value32);
		shift = (once++) ? 24 : 16;//!(once++);
		for (; (0 <= shift) && (i < size) ; i++, shift-=8) {
			buffer[i] = BYTE_VALUE_IN_32BIT(value32, shift);
//printf("2 value32=0x%x, buffer[%d]=0x%x, shift=%d\n", value32, i, buffer[i], shift);
		}
	}

	write32(reg_spim+SPIM_SER_ADDR, 0);
//printf("test=%d, %d, %d, %d\n", test, i, shift, once);
}
#endif
#if 0
void LoraWriteBuffer( uint16_t addr, uint8_t *buffer, uint8_t size )
{
	uint8_t *wb = buffer;
	uint8_t rb[3];

	if (31 < size) { // TODO:
		RVHAL_LORA_LOG_ERR("%s: size it too big: %u\n", __func__, size);
	} else if (0 != addr) {
		RVHAL_ASSERT_PARAM(1 == size);
		*rb = *wb;
		rvHal_spim_read(addr+1, rb+1, 2);
		wb = rb;
		size = sizeof(rb);
	}

	rvHal_spim_write(addr, wb, size);

	//RVHAL_LORA_LOG_DEBUG("addr[ 0x%02x ] <--w--   value[ 0x%02x ] \n", addr, buffer[0]);
}
#endif
static void vSpiTestTask( void *pvParameters )
{
//	printf("ready to test gpio for dead loop, every 5s toggle\n");
	int i = 0;
//	unsigned int data = 0;
#if 0
    write32(reg_gpio+GPIO_SWPORTA_DDR,0xc0000000);
	write32(reg_gpio+GPIO_SWPORTA_DR, 0x00000000);
//    rvHal_delay( 10 );
//	write32(reg_gpio+GPIO_SWPORTA_DR, 0x00000000);

    // Wait 1 ms
    rvHal_delay( 1 );

    // Configure RESET as input
//    GpioInit( &SX1276.Reset, RADIO_RESET, PIN_INPUT, PIN_PUSH_PULL, PIN_NO_PULL, 1 );
//	write32(reg_gpio+GPIO_SWPORTA_DR, 0x40000000);
    write32(reg_gpio+GPIO_SWPORTA_DDR,0x80000000);

    // Wait 6 ms
    rvHal_delay( 6 );
#endif
//	for (i=0; i<5; i++)
//	while(1)
//		rvHal_spim_rw(0x42);
//		rvHal_spim_rw(0x32);
//		rvHal_spim_rw(0xff);

#if 1
#if 1
		unsigned char num = 4;
		unsigned char buffer_w[64];
		unsigned char buffer_r[64];
		unsigned char addr = 0, data = 0;
for (i=0; i<64;i ++) {
	buffer_w[i] = 0x88+i;
}

//for (i=1; i<32;i ++) 
	{
//printf("\ni=%d\n", i);
		//LoraWriteBuffer(addr, buffer_w, num);
}
#if 1
//			rvHal_spim_w1(0x0, buffer);
//			rvHal_spim_write(addr, buffer_w, num);
//printf("\n");
//		for (i=0; i<num; i++, addr++)
		unsigned char *buf = buffer_r;
		for (addr=0x62; addr<0x65; addr++)
//		while(1)
		{
//			spi_delay(1000);
			rvHal_spim_read(addr, &data, 1);
//			rvHal_spim_w(i, 0xaa);
//			data=rvHal_spim_rw(0);

//			data = rvHal_spim_rw(0);

			//data++;
			printf("1addr[ 0x%x ] spi rx data: 0x%x\n",  addr, data);
//			buffer[0] = 0;
//	    	rvHal_delay( 5 );
		}

		rvHal_spim_read(0x62, buffer_r, 3);
		printf("2addr[ 0x%x ] spi rx data: 0x%x - 0x%x - 0x%x\n",  0x62, buffer_r[0], buffer_r[1], buffer_r[2]);

		data = 0xc;
		rvHal_spim_write(0x62, &data, 1);

		rvHal_spim_read(0x62, buffer_r, 3);
		printf("2addr[ 0x%x ] spi rx data: 0x%x - 0x%x - 0x%x\n",  0x62, buffer_r[0], buffer_r[1], buffer_r[2]);
#endif
#if 0
addr = 6;
buf = buffer_r;
buffer_r[0] = 0x6c;
		rvHal_spim_write(addr, buffer_r, 1);
			spi_delay(1000);
		for (i=0; i<3; i++, addr++)
//		while(1)
		{
			spi_delay(1000);
			rvHal_spim_read(addr, buf++, 1);
			printf("2addr[ 0x%x ] spi rx data: 0x%x\n",  addr, buffer_r[i]);
		}
#endif
#else
		while(1)
		{
			rvHal_spim_rw(0x42);

			data = rvHal_spim_rw(0);

			printf("%d, spi rx data: 0x%x\n", ++i, data);
	    	rvHal_delay( 5 );
		}
#endif
#else
//	int n =10;
	while(1) {
	    rvHal_spim_rw( 0xff );
//		spi_delay(500);
//printf("spi1\n");
//		  rvHal_sleep(5000);
//printf("spi0\n");
	    rvHal_spim_rw( 0x0 );
//		  rvHal_sleep(5000);
//		printf("spi rx data: 0x%x\n", rvHal_spim_rw(0));
	}
#endif
}
#else
static void vSpiTestTask( void *pvParameters )
{
	PingpongMain();
}
#endif

void __rvHal_spi_init(void)
{ 
	int i; 
//printf("reg_spim: 0x%x\n", reg_spim);
	write32(reg_spim+SPIM_SSIENR_ADDR, 0x0);
#if 1
	write32(reg_spim+SPIM_CTRLR0_ADDR, 0x1f0000);
#else
	write32(SPIM_CTRLR0_ADDR, 0x1f0000);
#endif
	write32(reg_spim+SPIM_BAUDR_ADDR, 8);// TODO:

	write32(reg_spim+SPIM_TXFTLR_ADDR, 0);
	write32(reg_spim+SPIM_RXFTLR_ADDR, 8);
	write32(reg_spim+SPIM_SER_ADDR, 0);
	write32(reg_spim+SPIM_IMR_ADDR, 0);
	//write32(SPIM_SPI_CTRLR0_ADDR, 0x180a);
	//write32(SPIM_SPI_CTRLR0_ADDR, 0x1a0a);
	write32(reg_spim+SPIM_SSIENR_ADDR, 1);

	spi_delay(1000);
//    GpioInit( &DIO2, 29, PIN_OUTPUT, PIN_PUSH_PULL, PIN_PULL_UP, 1);
//	vGpioTest();
#if 0//test only
	printf("spi tx start\n");

	for (i=0; i<1; i++)
	{
		rvHal_spim_rw(0x42);

		spi_delay(500);

		printf("spi rx data: 0x%x\n", rvHal_spim_rw(0));
		spi_delay(500);
	}
#else
	//printf("startup spiTask: 0x%x\n", read32(reg_spim+SPIM_CTRLR0_ADDR));

#if 0
	xTaskCreate( vSpiTestTask, "SpiTest", configMINIMAL_STACK_SIZE, NULL, ( configMAX_PRIORITIES - 2 ), NULL );

#endif
	//while(1) {}
#endif
}
