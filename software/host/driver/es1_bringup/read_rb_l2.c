#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <iostream>

#include "ftd2xx.h"
#include "libft4222.h"
#include "debug_tick.h"
#include "common.h"
#include "pygmy_es1_addr_translater.h"

#include <time.h>

int main () {
  std::string status;
  FT4222_SPIMode mode = SPI_IO_SINGLE;

  FT_HANDLE ftHandle = setup(mode, CLK_DIV_64, CLK_IDLE_LOW, CLK_LEADING);
  if (ftHandle == NULL)
    return 0;

  do_status_check(mode, ftHandle);

  // L2
  for (int i = 0x40000000; i < 0x40001050; i+=8) {
    do_read(mode, ftHandle, 0x8000000000 + 0x1B * 8);
  }

}
