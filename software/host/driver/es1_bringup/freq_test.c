
#include <stdint.h>

static volatile uint64_t tohost = 0;
static volatile uint64_t fromhost = 0;
static volatile uint64_t magicmem[8] = {0};

void main (void) {
  uint32_t loop = 1000000;
  uint64_t start_cycle = 0;
  uint64_t end_cycle = 0;

  magicmem[0] = 106;
  magicmem[1] = start_cycle;
  magicmem[2] = end_cycle;

  tohost = magicmem;
  while (fromhost == 0);
  fromhost = 0;

  asm volatile (
      "rdcycle %0\n\t"
      :"=r" (start_cycle)
      :
      );
  for (int i = 0; i < loop; i++) {
    asm volatile (
        "nop \n\t"
        );
  }
  asm volatile (
      "rdcycle %0\n\t"
      :"=r" (end_cycle)
      :
      );

  magicmem[0] = 106;
  magicmem[1] = start_cycle;
  magicmem[2] = end_cycle;

  tohost = magicmem;
  while (fromhost == 0);
  fromhost = 0;

  tohost = 1;
}
