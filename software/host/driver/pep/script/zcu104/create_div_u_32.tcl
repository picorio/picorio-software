##################################################################
# CREATE IP div_u_32
##################################################################

set div_gen div_u_32
create_ip -name div_gen -vendor xilinx.com -library ip -version 5.1 -module_name $div_gen

set_property -dict { 
  CONFIG.dividend_and_quotient_width {32}
  CONFIG.divisor_width {32}
  CONFIG.fractional_width {32}
  CONFIG.operand_sign {Unsigned}
  CONFIG.FlowControl {Blocking}
  CONFIG.latency {35}
  CONFIG.ACLKEN {true}
} [get_ips $div_gen]

##################################################################

