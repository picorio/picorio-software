// for uint*_t data types
#include <stdint.h>
// for print statements
#include <inttypes.h>
#include <stdio.h>
// for ctrl-c interrupt handling
#include <csignal>
// write to files
#include <cstdio>

#include "pcie_driver.h"
#include "station_vp.h"

#define WORD_SIZE AXI_DATA_BUS_WIDTH_IN_BYTES //size in bytes
#define MEMORY_SIZE 0x80000 // 512kb bram
#define NUM_ADDRESSES MEMORY_SIZE/WORD_SIZE

#define DEBUG

using namespace std;

int main(int argc, char* argv[]) {
  printf("Settint Early Reset to vp0...\n");
  do_write(STATION_VP_S2B_EARLY_RSTN_ADDR_0, 1);
}

