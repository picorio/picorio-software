/*
 * Rivai HAL adaptor for FreeRTOS Kernel V10.2.1
 *
 * reference:
 * http://www.FreeRTOS.org
 * http://aws.amazon.com/freertos
 *
 */

#include "hal.h"
#include "adaptor/freertos.h"

static unsigned int prvInterruptCount = 0;

void vPortasmHANDLE_INTERRUPT(void)
{
	rvHal_irq_ext_interrupt();
}

void vPortasmAddInterruptCount(void)
{
	prvInterruptCount++;
}

void vPortasmSubInterruptCount(void)
{
	if (0 != prvInterruptCount) /* for exception... todo */
		prvInterruptCount--;
}

unsigned int uPortasmIsInterrupt(void)
{
	return prvInterruptCount;
}