// for scoreboard hashing
#include <map>
// for uint*_t data types
#include <stdint.h>
// for print statements
#include <inttypes.h>
#include <stdio.h>
// for ctrl-c interrupt handling
#include <csignal>
// write to files
#include <cstdio>

#include "pcie_driver.c"
#define WORD_SIZE AXI_DATA_BUS_WIDTH_IN_BYTES //size in bytes
#define MEMORY_SIZE 0x80000 // 512kb bram
#define NUM_ADDRESSES MEMORY_SIZE/WORD_SIZE

#define DEBUG

int interruptReceived = 0;

void dumpScoreboard(std::map<uint32_t, uint8_t*> sb){
  printf("Dumping scoreboard to sb.log\n");
  int i = 0;
  FILE *of;
  of = fopen("sb.log", "w");
	std::map<uint32_t, uint8_t *>::iterator  it=sb.begin();

  while(it != sb.end()){
    //printf("%x %p\n", it->first, it->second);
    fprintf(of, "ADDR: %08x DATA: ", it->first);
          for(i=WORD_SIZE - 1; i >= 0; i--)
          {
            fprintf(of, "%02x", it->second[i]);
          }
            fprintf(of, "\n");
    it++;
  }

  fclose(of);
}

void signalHandler( int signum) {
  printf("Ctrl-C received, cleaning up...\n");
  interruptReceived = 1;
}

// rand has a max of 32bits
// cobble together a random 64bit piece of data horribly
uint64_t rand64(){
  return ((uint64_t)rand() << 32) | rand();
}

void randArr(uint8_t *buf, int size){
  int i = 0;
  for(i=0; i < size; i++) {
    buf[i] = rand64();
  }
}

uint8_t *mallocWord() {
  return (uint8_t *) malloc(sizeof(uint8_t)*WORD_SIZE);
}

uint8_t *mallocAndRandWord() {
  uint8_t *w = mallocWord();
  randArr(w, WORD_SIZE);
  return w;
}

int main() {
  std::signal(SIGINT, signalHandler);
  std::map<uint32_t, uint8_t*> sb;
  uint8_t * dataArr;
  uint32_t addr, weight;
  int i = 0;
  srand(time(0)); //seed rng
  printf("Running pcie torture test...\n");

  while(1)
  {
    if(interruptReceived) {
      dumpScoreboard(sb);
      return 0;
    }
    // make sure address is bound to available memory and aligned
    addr = (rand()%NUM_ADDRESSES) * WORD_SIZE;

    weight = rand()%100 ;
    if(weight < 49) { //do write
      // record in scoreboard
      // if sb[addr] exists, randomize the dataArr
      if(sb.find(addr) != sb.end()) {
        randArr(sb[addr], WORD_SIZE);
      } else {
        sb[addr] = mallocAndRandWord();
      }

      // write to pcie
#ifdef DEBUG
        printf("Writing addr: %08x -- ", addr); printBuffer(sb[addr], WORD_SIZE); printf("\n");
#endif
      pcie_write(addr, sb[addr]);
    }
    else { //do read
      // read from pcie
      dataArr = pcie_read(addr);  //pcie_read will malloc a new buf, be sure to free it if the data exists
#ifdef DEBUG
        printf("Reading addr: %08x -- ", addr); printBuffer(dataArr, WORD_SIZE); printf("\n");
#endif
      // check if it exists in scoreboard
      if(sb.find(addr) != sb.end()){
#ifdef DEBUG
        //purposefully cause it to fail to test if it actually can fail
        //dataArr[0] += 1;
#endif
        if(!compareBuffers(dataArr, sb[addr], WORD_SIZE)) {  // error if it doesn't match
            printf("ERROR: Data at addr %08x did not match\n", addr);
            printf("Scoreboard: "); printBuffer(sb[addr], WORD_SIZE); printf("\n");
            printf("FPGA readi: "); printBuffer(dataArr, WORD_SIZE);  printf("\n");
            dumpScoreboard(sb);
            return 0;
        }
#ifdef DEBUG
        printf("Exists in scoreboard and matches...\n");
#endif
        free(dataArr);
      } else {  // else put the value into the scoreboard
        sb[addr] = dataArr;
#ifdef DEBUG
        printf("Doesnt exist in scoreboard, adding...\n");
#endif
      }
    }
  }
      dumpScoreboard(sb);
}

