#!/usr/bin/env python3

import cmd
import os
import argparse
import time
from gui import *

class LIB(GUI):
  def __init__(self, user):
    super().__init__(user)

    # config_vcore
    self.config_vcore_parser = argparse.ArgumentParser(description="Config VCore", prog="config_vcore")
    self.config_vcore_parser.add_argument('vcore_id', type=str, help='VCore under configuration',   choices=['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', 'all'])
    #self.config_vcore_parser.add_argument('feature',  type=str, help='Feature under configuration', choices=['clk', 'pwr', 'lane_pwr'])
    #self.config_vcore_parser.add_argument('value',    type=str, help='On or Off',                   choices=['on', 'off'])
    self.config_vcore_subparser = self.config_vcore_parser.add_subparsers()
    self.config_vcore_subparser.required = True
    self.config_vcore_subparser.dest = 'feature'
    self.config_vcore_subparser_clk = self.config_vcore_subparser.add_parser('clk', help='Config Clock')
    self.config_vcore_subparser_clk.add_argument(     'value', type=str, help='On or Off',   choices=['on', 'off'])
    self.config_vcore_subparser_pwr = self.config_vcore_subparser.add_parser('pwr', help='Config Power')
    self.config_vcore_subparser_pwr.add_argument(     'value', type=str, help='On or Off',   choices=['on', 'off'])
    self.config_vcore_subparser_lane_pwr = self.config_vcore_subparser.add_parser('lane_pwr', help='Config Lane Power')
    self.config_vcore_subparser_lane_pwr.add_argument('value', type=int, help='Power Level', choices=[0, 1, 2, 3])

    # config_l2
    self.config_l2_parser = argparse.ArgumentParser(description="Config L2", prog="config_l2")
    self.config_l2_subparser = self.config_l2_parser.add_subparsers()
    self.config_l2_subparser.required = True
    self.config_l2_subparser.dest = 'feature'
    self.config_l2_parser_pwr = self.config_l2_subparser.add_parser('pwr', help="Config Power")
    self.config_l2_parser_pwr.add_argument('way_id',  type=str, help='Bank under configuration',    choices=['0', '1', '2', '3', '4', '5', '6', '7', 'all'])
    self.config_l2_parser_pwr.add_argument('value',   type=str, help='On or Sleep or PwrDown',      choices=['on', 'sleep', 'off'])
    self.config_l2_parser_vls = self.config_l2_subparser.add_parser('vls', help="Config VLS")
    self.config_l2_parser_vls.add_argument('way_id',  type=int, help='Bank under configuration',    choices=range(8))
    self.config_l2_parser_vls.add_argument('value',   type=int, help='VLS configuration',           choices=[1, 2, 4, 8])

    # config_ddr
    self.config_ddr_parser = argparse.ArgumentParser(description="Config DDR", prog="config_ddr")
    self.config_ddr_subparser = self.config_ddr_parser.add_subparsers()
    self.config_ddr_subparser.required = True
    self.config_ddr_subparser.dest = 'feature'
    self.config_ddr_parser_pwr = self.config_ddr_subparser.add_parser('pwr', help='Config Power')
    self.config_ddr_parser_pwr.add_argument('value', type=str, help='On or Off', choices=['on', 'off'])

    # config_pll
    self.config_pll_parser = argparse.ArgumentParser(description="Config PLL", prog="config_pll")
    self.config_pll_subparser = self.config_pll_parser.add_subparsers()
    self.config_pll_subparser.required = True
    self.config_pll_subparser.dest = 'feature'
    self.config_pll_parser_pwr = self.config_pll_subparser.add_parser('pwr', help='Config Power')
    self.config_pll_parser_pwr.add_argument('value', type=str, help='On or Off', choices=['on', 'off'])

    # config_mp
    self.config_mp_parser = argparse.ArgumentParser(description="Config MP", prog="config_mp")
    self.config_mp_subparser = self.config_mp_parser.add_subparsers()
    self.config_mp_subparser.required = True
    self.config_mp_subparser.dest = 'feature'
    self.config_mp_parser_pwr = self.config_mp_subparser.add_parser('pwr', help='Config Power')
    self.config_mp_parser_pwr.add_argument('value', type=str, help='On or Off', choices=['on', 'off'])


  def do_reset(self, *args):
    os.system("python %s/../../es1_bringup/serial_reset.py" % (os.getcwd()))
    time.sleep(3)

  def help_reset(self):
    print("Reset the entire chip")

  def do_config_vcore(self, *args):
    arg = self.parse_command(self.get_curr_func_name(), *args)
    if (arg != None):
      # self.do_read("rb2mp_brkpt")
      # self.do_update("rb2mp_brkpt 0x5")
      # self.do_show("rb2mp_brkpt")
      # self.do_write("rb2mp_brkpt")
      if arg.vcore_id == 'all':
        vcore_ids = range(12)
      else:
        vcore_ids = [arg.vcore_id]

      for vcore_id in vcore_ids:
        reg_name = 'rb2vc_' + str(vcore_id).zfill(2)
        if arg.feature == 'lane_pwr':
          field_name = 'lane_pwr_domain_on'
          value = arg.value
        else:
          field_name = arg.feature + '_on'
          if arg.value == 'on':
            value = 1
          else:
            value = 0
        print('Writing vcore_id ' + str(vcore_id))
        self.do_read(reg_name)
        time.sleep(0.1)
        self.do_update(reg_name + '.' + field_name + ' ' + str(value))
        time.sleep(0.1)
        self.do_show(reg_name)
        time.sleep(0.1)
        self.do_show(reg_name + '.' + field_name)
        time.sleep(0.1)
        self.do_write(reg_name)
        print('')
        time.sleep(2)
      

  def help_config_vcore(self):
    print(self.command_help(self.get_curr_func_name()))

  def do_config_l2(self, *args):
    # work around, do not know how to specify required for subparsers
    # if (len(args[0].split()) == 0):
    #   print("error: please pass required arguments")
    #   self.help_config_l2()
    # else:
    arg = self.parse_command(self.get_curr_func_name(), *args)
    if (arg != None):
      if arg.feature == 'pwr':
        reg_name = 'rb2l2_' + arg.feature

        if arg.way_id == 'all':
          way_ids = range(8)
        else:
          way_ids = [arg.way_id]

        values = {
          'on'    : 0,
          'sleep' : 1,
          'off'   : 2,
        }
        for way_id in way_ids:
          field_name = 'way' + str(way_id) + '_pwr_state'
          print('Writing way_id ' + str(way_id))
          self.do_read(reg_name)
          time.sleep(0.1)
          self.do_update(reg_name + '.' + field_name + ' ' + str(values[arg.value]))
          time.sleep(0.1)
          self.do_show(reg_name)
          time.sleep(0.1)
          self.do_show(reg_name + '.' + field_name)
          time.sleep(0.1)
          self.do_write(reg_name)
          print('')
          time.sleep(2)

      elif arg.feature == 'vls':
        print('config_l2 lvs: command not yet configured')
        pass

  def help_config_l2(self):
    print(self.command_help(self.get_curr_func_name()))

  def do_config_ddr(self, *args):
    arg = self.parse_command(self.get_curr_func_name(), *args)
    if (arg != None):
      if arg.feature == 'pwr':
        reg_name = 'rb2ddr_0'
        field_name = 'ddr_pwr_on'
        if arg.value == 'on':
          value = 1
        else:
          value = 0
        self.do_read(reg_name)
        time.sleep(0.1)
        self.do_update(reg_name + '.' + field_name + ' ' + str(value))
        time.sleep(0.1)
        self.do_show(reg_name)
        time.sleep(0.1)
        self.do_show(reg_name + '.' + field_name)
        time.sleep(0.1)
        self.do_write(reg_name)
        print('')
        time.sleep(2)

  def help_config_ddr(self):
    print(self.command_help(self.get_curr_func_name()))

  def do_config_pll(self, *args):
    arg = self.parse_command(self.get_curr_func_name(), *args)
    if (arg != None):
      if arg.feature == 'pwr':
        reg_name = 'rb2pll'
        field_name = 'pll_pwrdn'
        if arg.value == 'on':
          value = 0
        else:
          value = 1
        self.do_read(reg_name)
        time.sleep(0.1)
        self.do_update(reg_name + '.' + field_name + ' ' + str(value))
        time.sleep(0.1)
        self.do_show(reg_name)
        time.sleep(0.1)
        self.do_show(reg_name + '.' + field_name)
        time.sleep(0.1)
        self.do_write(reg_name)
        print('')
        time.sleep(2)

  def help_config_pll(self):
    print(self.command_help(self.get_curr_func_name()))

  def do_config_mp(self, *args):
    arg = self.parse_command(self.get_curr_func_name(), *args)
    if (arg != None):
      if arg.feature == 'pwr':
        reg_name = 'rb2mp'
        field_name = 'pwr_on'
        if arg.value == 'on':
          value = 1
        else:
          value = 0
        self.do_read(reg_name)
        time.sleep(0.1)
        self.do_update(reg_name + '.' + field_name + ' ' + str(value))
        time.sleep(0.1)
        self.do_show(reg_name)
        time.sleep(0.1)
        self.do_show(reg_name + '.' + field_name)
        time.sleep(0.1)
        self.do_write(reg_name)
        print('')
        time.sleep(2)

  def help_config_mp(self):
    print(self.command_help(self.get_curr_func_name()))
