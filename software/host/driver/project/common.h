#ifndef _COMMON_H
#define _COMMON_H
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>

#include "ftd2xx.h"
#include "libft4222.h"
#include "fpga/socket_server.h"
#include "socket_client.h"

inline std::string DeviceFlagToString(DWORD flags);
std::vector< FT_DEVICE_LIST_INFO_NODE > ListFtUsbDevices();

FT_HANDLE setup(FT4222_SPIMode mode, FT4222_SPIClock div, FT4222_SPICPOL cpol, FT4222_SPICPHA cpha, DWORD rpid, enum drvr_type_t drvr_mode=SPI);

void do_status_check(FT4222_SPIMode mode, FT_HANDLE ftHandle);
void do_write(FT4222_SPIMode mode, FT_HANDLE ftHandle, uint64 addr, uint64 data);
uint64 do_read(FT4222_SPIMode mode, FT_HANDLE ftHandle, uint64 addr);
#endif
