//#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>

#include "ftd2xx.h"
#include "libft4222.h"
#include "common.h"
std::vector< FT_DEVICE_LIST_INFO_NODE > g_FT4222DevList;

int main (int argc, char const *argv[])
{
  std::vector< FT_DEVICE_LIST_INFO_NODE > g_FT4222DevList;

  g_FT4222DevList = ListFtUsbDevices();

  if (g_FT4222DevList.empty())
  {
    printf("No FT4222 device is found!\n");
    return NULL;
  }

  LPWORD          id;
  if (argc != 2) {
    printf("Usage: ./set_dongle_id.o id=X\n");
    abort();
  } else if (strncmp(argv[1], "id=", 3) != 0) {
    printf("Usage: ./set_dongle_id.o id=X\n");
    abort();
  } else {
    id = atoi(argv[1] + 3);
  }

  FT_STATUS	ftStatus;
  FT_HANDLE	ftHandle0;
  int iport;
  static FT_PROGRAM_DATA Data;
  static FT_DEVICE ftDevice;
  DWORD libraryVersion = 0;
  int retCode = 0;

  ftStatus = FT_OpenEx((PVOID)g_FT4222DevList[0].LocId,
                       FT_OPEN_BY_LOCATION, &ftHandle0);
  if(ftStatus != FT_OK) {
  	/* 
  		This can fail if the ftdi_sio driver is loaded
  	 	use lsmod to check this and rmmod ftdi_sio to remove
  		also rmmod usbserial
  	 */
  	printf("FT_Open(%d) failed\n", iport);
  	return 1;
  }
  
  printf("FT_Open succeeded.  Handle is %p\n", ftHandle0);
  
  ftStatus = FT_GetDeviceInfo(ftHandle0,
                              &ftDevice,
                              NULL,
                              NULL,
                              NULL,
                              NULL); 
  if (ftStatus != FT_OK) 
  { 
  	printf("FT_GetDeviceType FAILED!\n");
  	retCode = 1;
  	goto exit;
  }  
  
  printf("FT_GetDeviceInfo succeeded.  Device is type %d.\n", 
         (int)ftDevice);
  
  /* MUST set Signature1 and 2 before calling FT_EE_Read */
  Data.Signature1 = 0x00000000;
  Data.Signature2 = 0xffffffff;
  Data.Manufacturer = (char *)malloc(256); /* E.g "FTDI" */
  Data.ManufacturerId = (char *)malloc(256); /* E.g. "FT" */
  Data.Description = (char *)malloc(256); /* E.g. "USB HS Serial Converter" */
  Data.SerialNumber = (char *)malloc(256); /* E.g. "FT000001" if fixed, or NULL */
  if (Data.Manufacturer == NULL ||
      Data.ManufacturerId == NULL ||
      Data.Description == NULL ||
      Data.SerialNumber == NULL)
  {
  	printf("Failed to allocate memory.\n");
  	retCode = 1;
  	goto exit;
  }
  
  ftStatus = FT_EE_Read(ftHandle0, &Data);
  if(ftStatus != FT_OK) {
  	printf("FT_EE_Read failed\n");
  	retCode = 1;
  	goto exit;
  }
  
  printf("FT_EE_Read succeeded.\n\n");
  	
  printf("Signature1 = %d\n", (int)Data.Signature1);			
  printf("Signature2 = %d\n", (int)Data.Signature2);			
  printf("Version = %d\n", (int)Data.Version);				
  							
  printf("VendorId = 0x%04X\n", Data.VendorId);				
  printf("ProductId = 0x%04X\n", Data.ProductId);
  printf("Manufacturer = %s\n", Data.Manufacturer);			
  printf("ManufacturerId = %s\n", Data.ManufacturerId);		
  printf("Description = %s\n", Data.Description);			
  printf("SerialNumber = %s\n", Data.SerialNumber);			
  printf("MaxPower = %d\n", Data.MaxPower);				
  printf("PnP = %d\n", Data.PnP) ;					
  printf("SelfPowered = %d\n", Data.SelfPowered);			
  printf("RemoteWakeup = %d\n", Data.RemoteWakeup);			
  
  Data.SerialNumber[0] = '1';
  ftStatus = FT_EE_Program(ftHandle0, &Data);
  if(ftStatus != FT_OK) {
  	printf("FT_EE_Program failed, ftStatus = %d\n", ftStatus);
  	retCode = 1;
  	goto exit;
  }
 
  ftStatus = FT_EE_Read(ftHandle0, &Data);
  if(ftStatus != FT_OK) {
  	printf("FT_EE_Read failed\n");
  	retCode = 1;
  	goto exit;
  }
  
  printf("FT_EE_Read succeeded.\n\n");
  	
  printf("Signature1 = %d\n", (int)Data.Signature1);			
  printf("Signature2 = %d\n", (int)Data.Signature2);			
  printf("Version = %d\n", (int)Data.Version);				
  							
  printf("VendorId = 0x%04X\n", Data.VendorId);				
  printf("ProductId = 0x%04X\n", Data.ProductId);
  printf("Manufacturer = %s\n", Data.Manufacturer);			
  printf("ManufacturerId = %s\n", Data.ManufacturerId);		
  printf("Description = %s\n", Data.Description);			
  printf("SerialNumber = %s\n", Data.SerialNumber);			
  printf("MaxPower = %d\n", Data.MaxPower);				
  printf("PnP = %d\n", Data.PnP) ;					
  printf("SelfPowered = %d\n", Data.SelfPowered);			
  printf("RemoteWakeup = %d\n", Data.RemoteWakeup);			
 
  exit:
    free(Data.Manufacturer);
    free(Data.ManufacturerId);
    free(Data.Description);
    free(Data.SerialNumber);
    FT_Close(ftHandle0);
    printf("Returning %d\n", retCode);
    return retCode;
}
