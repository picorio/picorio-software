#include "FreeRTOS.h"
#include "portmacro.h"
#include "rtlgen.h"
#if 1
#define BULK_LEN (32)
static uint64_t data_buf[4];
void dma_init(uint32_t len)
{
    int chnl_id = 0;
    //(*(uint64_t *) (STATION_DMA_S2B_DMA_THREAD_GATHER_GRPDEPTH_ADDR__DEPTH_0 + (chnl_id * 8))) =  8;
    //(*(uint64_t *) (STATION_DMA_S2B_DMA_THREAD_GATHER_STRIDE_ADDR__DEPTH_0 + (chnl_id * 8))) =  8;
    //(*(uint64_t *) (STATION_DMA_S2B_DMA_THREAD_USE_8B_ALIGN_ADDR__DEPTH_0 + (chnl_id * 8))) =  1;
    (*(uint64_t *) (STATION_DMA_S2B_DMA_THREAD_LENGTH_IN_BYTES_ADDR__DEPTH_0 + (chnl_id * 8))) =  len;
    //(*(uint64_t *) (STATION_DMA_S2B_DMA_THREAD_RPT_CNT_LESS_1_ADDR__DEPTH_0 + (chnl_id * 8))) =  1;
}

void dma_transfer2l1(uint32_t dst_addr, uint32_t src_addr)
{
	int chnl_id = 0;
	uint64_t cmd_vld = 1;
	(*(uint64_t *) (STATION_DMA_S2B_DMA_THREAD_SRC_ADDR_ADDR__DEPTH_0 + (chnl_id * 8))) =  src_addr;
	(*(uint64_t *) (STATION_DMA_S2B_DMA_THREAD_DST_ADDR_ADDR__DEPTH_0 + (chnl_id * 8))) =  dst_addr;
	//(*(uint64_t *) (STATION_DMA_S2B_DMA_THREAD_CBUF_SIZE_ADDR__DEPTH_0 + (chnl_id * 8))) =  8;
	//(*(uint64_t *) (STATION_DMA_S2B_DMA_THREAD_CBUF_THOLD_ADDR__DEPTH_0 + (chnl_id * 8))) =  8;
	//(*(uint64_t *) (STATION_DMA_DMA_THREAD_CBUF_RP_ADDR_ADDR__DEPTH_0 + (chnl_id * 8))) =  (dst_addr + 1);
	//(*(uint64_t *) (STATION_DMA_DMA_THREAD_CBUF_WP_ADDR_ADDR__DEPTH_0 + (chnl_id * 8))) =  dst_addr;
	//(*(uint64_t *) (STATION_DMA_S2B_DMA_THREAD_CBUF_MODE_ADDR__DEPTH_0 + (chnl_id * 8))) =  1;
	//(*(uint64_t *) (STATION_DMA_DMA_THREAD0_DATA_AVAIL_SRC_SEL_ADDR + (chnl_id * 8))) =  1;
	(*(uint64_t *) (STATION_DMA_DMA_THREAD_CMD_VLD_ADDR__DEPTH_0 + (chnl_id * 8))) = 1;
	//uint32_t wp = (*(uint64_t *)(STATION_DMA_DMA_THREAD_CBUF_WP_ADDR_ADDR__DEPTH_0));
	//uint32_t rp = (*(uint64_t *)(STATION_DMA_DMA_THREAD_CBUF_RP_ADDR_ADDR__DEPTH_0));
	//uint32_t full = (*(uint64_t *)(STATION_DMA_DMA_THREAD_CBUF_FULL_SEEN_ADDR__DEPTH_0));
	cmd_vld = (*(uint64_t *)(STATION_DMA_DMA_THREAD_CMD_VLD_ADDR__DEPTH_0));
	while(1 == cmd_vld)
		cmd_vld = (*(uint64_t *)(STATION_DMA_DMA_THREAD_CMD_VLD_ADDR__DEPTH_0));

	//uint32_t cbufsize = (*(uint64_t *)(STATION_DMA_S2B_DMA_THREAD_CBUF_SIZE_ADDR__DEPTH_0));
	//uint32_t cbufthold = (*(uint64_t *)(STATION_DMA_S2B_DMA_THREAD_CBUF_THOLD_ADDR__DEPTH_0));
	//wait util transfer done? 
	//
	//printf("%s, %d : cbufsize = %x, cbufthold = %x\n", __func__, __LINE__, cbufsize, cbufthold);
	//printf("%s, %d : wp = %lx rp = %lx full = %x, cmd_vld = %x\n", __func__, __LINE__, wp, rp, full, cmd_vld);
	return ;
}

uint64_t dma_transfer2cpu(uint32_t src_addr)
{
	int chnl_id = 0;
	uint64_t data;
	uint64_t cmd_vld = 1;
	(*(uint64_t *) (STATION_DMA_S2B_DMA_THREAD_SRC_ADDR_ADDR__DEPTH_0 + (chnl_id * 8))) =  src_addr;
	(*(uint64_t *) (STATION_DMA_S2B_DMA_THREAD_DST_ADDR_ADDR__DEPTH_0 + (chnl_id * 8))) =  &(data_buf[0]);
	//(*(uint64_t *) (STATION_DMA_S2B_DMA_THREAD_CBUF_SIZE_ADDR__DEPTH_0 + (chnl_id * 8))) =  8;
	//(*(uint64_t *) (STATION_DMA_S2B_DMA_THREAD_CBUF_THOLD_ADDR__DEPTH_0 + (chnl_id * 8))) =  8;
	//(*(uint64_t *) (STATION_DMA_DMA_THREAD_CBUF_RP_ADDR_ADDR__DEPTH_0 + (chnl_id * 8))) =  (&data + 1);
	//(*(uint64_t *) (STATION_DMA_DMA_THREAD_CBUF_WP_ADDR_ADDR__DEPTH_0 + (chnl_id * 8))) =  &data;
	//(*(uint64_t *) (STATION_DMA_S2B_DMA_THREAD_CBUF_MODE_ADDR__DEPTH_0 + (chnl_id * 8))) =  1;
	//(*(uint64_t *) (STATION_DMA_DMA_THREAD0_DATA_AVAIL_SRC_SEL_ADDR + (chnl_id * 8))) =  1;
	(*(uint64_t *) (STATION_DMA_DMA_THREAD_CMD_VLD_ADDR__DEPTH_0 + (chnl_id * 8))) = 1;
	//uint32_t wp = (*(uint64_t *)(STATION_DMA_DMA_THREAD_CBUF_WP_ADDR_ADDR__DEPTH_0));
	//uint32_t rp = (*(uint64_t *)(STATION_DMA_DMA_THREAD_CBUF_RP_ADDR_ADDR__DEPTH_0));
	//uint32_t full = (*(uint64_t *)(STATION_DMA_DMA_THREAD_CBUF_FULL_SEEN_ADDR__DEPTH_0));
	cmd_vld = (*(uint64_t *)(STATION_DMA_DMA_THREAD_CMD_VLD_ADDR__DEPTH_0));
	while(1 == cmd_vld)
		cmd_vld = (*(uint64_t *)(STATION_DMA_DMA_THREAD_CMD_VLD_ADDR__DEPTH_0));
	//uint32_t cbufsize = (*(uint64_t *)(STATION_DMA_S2B_DMA_THREAD_CBUF_SIZE_ADDR__DEPTH_0));
	//uint32_t cbufthold = (*(uint64_t *)(STATION_DMA_S2B_DMA_THREAD_CBUF_THOLD_ADDR__DEPTH_0));

	//wait util transfer done? 
	//printf("%s, %d : cbufsize = %x, cbufthold = %x\n", __func__, __LINE__, cbufsize, cbufthold);
	//printf("%s, %d : wp = %lx rp = %lx full = %x, cmd_vld = %x\n", __func__, __LINE__, wp, rp, full, cmd_vld);
	return data;
}
#endif
#if 1
static void show_data(uint32_t addr, uint32_t len)
{
	for(int i = 0; i < len; )
	{
		printf("data addr: %lx, content: %llx \n", addr + i, *(uint64_t *)(addr+i));
		i+=8;
	}
	return ;
}
static void inline preload_inst(uint32_t addr)
{
	uint32_t wayid = addr & 0x1000; 
	uint32_t idx = (addr & 0xfe0) >> 5;
	uint32_t data_addr = (addr & 0xff8);
	uint64_t data =  (*(uint64_t *)addr);

	printf("addr= %lx, idx = %lx, data_addr = %lx, data = %llx \n", addr, idx<<3, data_addr, data);
	show_data(addr, BULK_LEN);
	if (wayid == 0) {
		*(uint32_t *) (STATION_ORV32_IC_TAG_WAY_0_ADDR + 8 * idx) = (0x008 << 20 | (addr >> 12 & 0x000fffff));
		printf("instr addr: %lx, content: %lx \n", STATION_ORV32_IC_TAG_WAY_0_ADDR + 8 * idx, (0x008 << 20 | (addr >> 12 & 0x000fffff)));
		printf("after write: content: %lx \n", *(uint32_t *) (STATION_ORV32_IC_TAG_WAY_0_ADDR + 8 * idx) );
		//*(uint32_t *) (STATION_ORV32_IC_DATA_WAY_0_ADDR + data_addr) = data;
		dma_transfer2l1(STATION_ORV32_IC_DATA_WAY_0_ADDR + data_addr, addr);
		printf("data addr: %lx, content: %lx \n", STATION_ORV32_IC_DATA_WAY_0_ADDR + data_addr, data);
		dma_transfer2cpu(STATION_ORV32_IC_DATA_WAY_0_ADDR + data_addr);
		show_data((uint32_t)(&data_buf[0]), BULK_LEN);
	} else {
		*(uint64_t *) (STATION_ORV32_IC_TAG_WAY_1_ADDR + 8 * idx) = (0x008 << 20 | (addr >> 12 & 0x000fffff));
		*(uint64_t *) (STATION_ORV32_IC_DATA_WAY_1_ADDR + data_addr) = data;
	}
}
#endif
void rvHal_EnterStandbyL1()
{
	/*need backup L2 cache content and shutdown L2 cache*/
	/*preload next code into L1 cache*/
	void *begin = &&LOAD_START;
	void *end = &&LOAD_END;
	uint32_t i = 0;
	register uint64_t cache_cfg0_normal = *(uint64_t *)STATION_CACHE_S2B_CTRL_REG_ADDR_0;
	register uint64_t cache_cfg0_sleep;
	register uint64_t cache_cfg1_normal = *(uint64_t *)STATION_CACHE_S2B_CTRL_REG_ADDR_1;
	register uint64_t cache_cfg1_sleep;
	cache_cfg0_sleep = cache_cfg0_normal & 0xfffff0000fffffff;
	cache_cfg0_sleep |= 0xfffff5555fffffff;
	cache_cfg1_sleep = cache_cfg1_normal & 0xfffff0000fffffff;
	cache_cfg1_sleep |= 0xfffff5555fffffff;
	printf("begin = %lx, end = %lx \n", begin, end);
	dma_init(BULK_LEN);
	for(i = begin; i<(end); )
	{
		preload_inst(i);
		i+=BULK_LEN;
	}
	//begin, end need align to 32bytes
	__asm volatile(".align 5":::);
LOAD_START:	
	/*L2 cache enter sleep mode*/
	*(uint64_t *) (STATION_CACHE_S2B_CTRL_REG_ADDR_0) = cache_cfg0_sleep;
	*(uint64_t *) (STATION_CACHE_S2B_CTRL_REG_ADDR_1) = cache_cfg1_sleep;

	//fence: dmb.st, isb, notice, fence will let L1 cache been clean & invalidate
	//for pygmy_e, without out of order, so do not need isb;
	//for pygmy_e, for io-map address space, do not need dsb for sync reason. 
	//__asm volatile("fence w,w":::);
	//__asm volatile("fence.i":::);
       	//__asm volatile("fence r,r":::);
	__asm volatile("wfi":::);
	//after wakeup
	/*L2 cache exit sleep mode*/
	*(uint64_t *) (STATION_CACHE_S2B_CTRL_REG_ADDR_0) = cache_cfg0_normal;
	*(uint64_t *) (STATION_CACHE_S2B_CTRL_REG_ADDR_1) = cache_cfg1_normal;
LOAD_END:
	return;	
}

static void inline backup_l2(void)
{
	printf("%s called. \n", __func__);
	return ;
}
static inline void restore_l2(void)
{
	printf("%s called. \n", __func__);
	return ;
}

void rvHal_EnterStandbyL2()
{
	/*need backup L2 cache content and shutdown L2 cache*/
	/*preload next code into L1 cache*/
	void *begin = &&LOAD_START;
	void *end = &&LOAD_END;
	register uint64_t cache_cfg0_normal = *(uint64_t *)STATION_CACHE_S2B_CTRL_REG_ADDR_0;
	register uint64_t cache_cfg0_pwrdwn;
	register uint64_t cache_cfg1_normal = *(uint64_t *)STATION_CACHE_S2B_CTRL_REG_ADDR_1;
	register uint64_t cache_cfg1_pwrdwn;
	cache_cfg0_pwrdwn = cache_cfg0_normal & 0xfffff0000fffffff;
	cache_cfg0_pwrdwn |= 0xfffff5555fffffff;
	cache_cfg1_pwrdwn = cache_cfg1_normal & 0xfffff0000fffffff;
	cache_cfg1_pwrdwn |= 0xfffff5555fffffff;
	uint32_t i = 0;
	/*backup L2 cache to flash*/
	backup_l2();
	printf("begin = %lx, end = %lx \n", begin, end);
	dma_init(BULK_LEN);
	for(i = begin; i<(end); )
	{
		preload_inst(i);
		i+=BULK_LEN;
	}
	//begin, end need align to 32bytes 
	__asm volatile(".align 5":::);
LOAD_START:

	/*L2 cache enter sleep mode*/
	*(uint64_t *) (STATION_CACHE_S2B_CTRL_REG_ADDR_0) = cache_cfg0_pwrdwn;
	*(uint64_t *) (STATION_CACHE_S2B_CTRL_REG_ADDR_1) = cache_cfg1_pwrdwn;

	//fence: dmb.st, isb
	__asm volatile("wfi":::);
	//after wakeup
	/*L2 cache exit sleep mode*/
	*(uint64_t *) (STATION_CACHE_S2B_CTRL_REG_ADDR_0) = cache_cfg0_normal;
	*(uint64_t *) (STATION_CACHE_S2B_CTRL_REG_ADDR_1) = cache_cfg1_normal;
	/*restore L2 cache from flash*/
	restore_l2();
LOAD_END:
	return;	
}
