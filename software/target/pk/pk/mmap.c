#include "mmap.h"
#include "atomic.h"
#include "pk.h"
#include "boot.h"
#include "bits.h"
#include "mtrap.h"
#include <stdint.h>
#include <errno.h>

//#define DEBUG_MMAP
#ifdef DEBUG_MMAP
#define mm_printk(args...) printk(args)
#define huge_printk(huge, format, arg...)                         \
    if (huge) {   \
        mm_printk("%s: " format, __func__, ##arg);      \
    }
#else
#define mm_printk(args...)
#define huge_printk(huge, format, arg...) 
#endif

typedef struct {
  uintptr_t addr;
  size_t length;
  file_t* file;
  size_t offset;
  unsigned refcnt;
  int prot;
} vmr_t;

#define MAX_VMR (RISCV_PGSIZE / sizeof(vmr_t))
static spinlock_t vm_lock = SPINLOCK_INIT;
static vmr_t* vmrs;

uintptr_t first_free_paddr;
static uintptr_t first_free_page;
static size_t next_free_page;
static size_t free_pages;

int demand_paging = 1; // unless -p flag is given

enum {
    PS_MIN = 0, 
	PS_4K = PS_MIN, /* SV32 */
	PS_2M, /* SV39, SV48 */
#if 0
	PS_4M, /* SV32 */
	PS_1G, /* SV39, SV48 */
	PS_512G, /* SV48 */
#endif
	PS_HUGE = PS_2M,
	PS_MAX,
};

static ps_info psinfo[PS_MAX] = {
/* PS_4K */  {RISCV_PGSIZE, 0, 0},
/* PS_2M */	{RISCV_PGSIZE << RISCV_PGLEVEL_BITS, 1, 0}
};

#if 1//rongdebug: single thread limitation in pk
static int hugetlb_range(uintptr_t addr)
{
	if ((addr < current.mmap_max_hugetlb) && (current.mmap_min_hugetlb <= addr))
		return 1;
	return 0;
}

static ps_info *get_ps_by_vaddr(uintptr_t vaddr)
{
  ps_info *ps = NULL;

  if (hugetlb_range(vaddr)) {
    ps = &psinfo[PS_HUGE];
  } else {
    ps = &psinfo[PS_MIN];
  }

  return ps;
}

static ps_info *get_ps_by_flags(int flags)
{
  ps_info *ps = NULL;

  if (flags & MAP_HUGETLB) {
    ps = &psinfo[PS_HUGE];
  } else {
    ps = &psinfo[PS_MIN];
  }

  return ps;
}

ps_info *get_ps_min(void)
{
  return &psinfo[PS_MIN];
}

static int ps_min(ps_info *ps)
{
  return (ps->page_size == RISCV_PGSIZE);
}

ps_info *get_ps_huge(void)
{
  return &psinfo[PS_HUGE];
}

static size_t get_mmap_min(ps_info *ps)
{
  return (ps_min(ps) ? current.brk : current.mmap_min_hugetlb);
}

static size_t get_mmap_max(ps_info *ps)
{
  return (ps_min(ps) ? current.mmap_max : current.mmap_max_hugetlb);
}

static int align_page_size(uintptr_t addr, size_t pszie)
{
  return !(addr & (pszie -1));
}
#endif

static uintptr_t __page_alloc()
{
  kassert(next_free_page != free_pages);
  uintptr_t addr = first_free_page + RISCV_PGSIZE * next_free_page++;
  memset((void*)addr, 0, RISCV_PGSIZE);
  return addr;
}

static vmr_t* __vmr_alloc(uintptr_t addr, size_t length, file_t* file,
                          size_t offset, unsigned refcnt, int prot)
{
  if (!vmrs) {
    spinlock_lock(&vm_lock);
      if (!vmrs)
        vmrs = (vmr_t*)__page_alloc();
    spinlock_unlock(&vm_lock);
  }

  for (vmr_t* v = vmrs; v < vmrs + MAX_VMR; v++) {
    if (v->refcnt == 0) {
      if (file)
        file_incref(file);
      v->addr = addr;
      v->length = length;
      v->file = file;
      v->offset = offset;
      v->refcnt = refcnt;
      v->prot = prot;
      return v;
    }
  }
  return NULL;
}

static void __vmr_decref(vmr_t* v, unsigned dec)
{
  if ((v->refcnt -= dec) == 0)
  {
    if (v->file)
      file_decref(v->file);
  }
}

static size_t pte_ppn(pte_t pte, size_t step)
{
  return pte >> get_pte_ppn_shift(step);
}

static uintptr_t ppn(uintptr_t addr, size_t step)
{
  return addr >> get_page_shift(step);
}

static uintptr_t ppn_min(uintptr_t addr)
{
  return addr >> RISCV_PGSHIFT;
}

static size_t pt_idx(uintptr_t addr, int level)
{
  size_t idx = addr >> (RISCV_PGLEVEL_BITS*level + RISCV_PGSHIFT);
  return idx & ((1 << RISCV_PGLEVEL_BITS) - 1);
}

static pte_t* __walk_create(uintptr_t addr, size_t step);

static pte_t* __attribute__((noinline)) __continue_walk_create(uintptr_t addr, pte_t* pte, size_t step)
{
  *pte = ptd_create(ppn_min(__page_alloc()), get_ps_min()->pte_step);
  return __walk_create(addr, step);
}

static pte_t* __walk_internal(uintptr_t addr, int create, size_t step)
{
  size_t ps_min_step = get_ps_min()->pte_step;
  pte_t* t = root_page_table;
  for (int i = (VA_BITS - RISCV_PGSHIFT) / RISCV_PGLEVEL_BITS - 1; i > step; i--) {
    size_t idx = pt_idx(addr, i);
    if (unlikely(!(t[idx] & PTE_V)))
      return create ? __continue_walk_create(addr, &t[idx], step) : 0;
    t = (pte_t*)(pte_ppn(t[idx], ps_min_step) << get_page_shift(ps_min_step)); /* non-leaf PTE : 4K, regardless of hugetlb */
  }
  return &t[pt_idx(addr, step)];
}

static pte_t* __walk(uintptr_t addr, size_t step)
{
  return __walk_internal(addr, 0, step);
}

static pte_t* __walk_create(uintptr_t addr, size_t step)
{
  return __walk_internal(addr, 1, step);
}

static int __va_avail(uintptr_t vaddr, size_t step)
{
  pte_t* pte = __walk(vaddr, step);
  return pte == 0 || *pte == 0;
}

static uintptr_t __vm_alloc(size_t npage, ps_info *ps)
{
  size_t psize = ps->page_size;
  size_t step = ps->pte_step;
  uintptr_t start = get_mmap_min(ps), end = get_mmap_max(ps) - npage*psize;

  for (uintptr_t a = start; a <= end; a += psize)
  {
    if (!__va_avail(a, step))
      continue;

    uintptr_t first = a, last = a + (npage-1) * psize;
    for (a = last; a > first && __va_avail(a, step); a -= psize)
      ;
    if (a > first)
      continue;
    return a;
  }
  return 0;
}

static inline pte_t prot_to_type(int prot, int user)
{
  pte_t pte = 0;
  if (prot & PROT_READ) pte |= PTE_R | PTE_A;
  if (prot & PROT_WRITE) pte |= PTE_W | PTE_D;
  if (prot & PROT_EXEC) pte |= PTE_X | PTE_A;
  if (pte == 0) pte = PTE_R;
  if (user) pte |= PTE_U;
  return pte;
}

int __valid_user_range(uintptr_t vaddr, size_t len, size_t mmap_max)
{
  if (vaddr + len < vaddr)
    return 0;
  return vaddr + len <= mmap_max;
}

static int __handle_page_fault(uintptr_t vaddr, int prot, ps_info *ps)
{
  uintptr_t ovaddr = vaddr;
  int huge = hugetlb_range(vaddr);

  size_t step = ps->pte_step;
  size_t pgshift = get_page_shift(step);

  uintptr_t vpn = vaddr >> pgshift;
  vaddr = vpn << pgshift;

  pte_t* pte = __walk(vaddr, step);

  huge_printk(huge, "vaddr: 0x%x, vpn: 0x%x, step: %d, pgshift: %d, vaddr0: 0x%x *pte: 0x%x\n", ovaddr, vpn, step, pgshift, vaddr, *pte);

  if (pte == 0 || *pte == 0 || !__valid_user_range(vaddr, 1, get_mmap_max(ps)))
    return -1;
  else if (!(*pte & PTE_V))
  {
    uintptr_t ppn = vpn + ps->ppn_first;

    vmr_t* v = (vmr_t*)*pte;
    *pte = pte_create(ppn, step, prot_to_type(PROT_READ|PROT_WRITE, 0));
    flush_tlb();
    if (v->file) // TODO: notice: hugetlb does NOT support file now.
    {
      size_t flen = MIN(RISCV_PGSIZE, v->length - (vaddr - v->addr));
      ssize_t ret = file_pread(v->file, (void*)vaddr, flen, vaddr - v->addr + v->offset);
      kassert(ret > 0);
      if (ret < RISCV_PGSIZE)
        memset((void*)vaddr + ret, 0, RISCV_PGSIZE - ret);
    }
    else {
      memset((void*)vaddr, 0, ps->page_size);
    }

    __vmr_decref(v, 1);
    *pte = pte_create(ppn, step, prot_to_type(v->prot, 1));
    huge_printk(huge, "vpn: 0x%x, ppn: 0x%x, *pte: 0x%x, paddr:0x%x\n", vpn, ppn, *pte, (ppn<<get_page_shift(step)) |(ovaddr & (ps->page_size-1)));
  }

  pte_t perms = pte_create(0, step, prot_to_type(prot, 1));
  if ((*pte & perms) != perms) {
	mm_printk("perms error\n");
    return -1;
  }

  flush_tlb();
  return 0;
}

int handle_page_fault(uintptr_t vaddr, int prot)
{
  ps_info *ps = get_ps_by_vaddr(vaddr);

  spinlock_lock(&vm_lock);
    int ret = __handle_page_fault(vaddr, prot , ps);
  spinlock_unlock(&vm_lock);
  return ret;
}

static void __do_munmap(uintptr_t addr, size_t len, ps_info *ps)
{
  for (uintptr_t a = addr; a < addr + len; a += ps->page_size)
  {
    pte_t* pte = __walk(a, ps->pte_step);
    if (pte == 0 || *pte == 0)
      continue;

    if (!(*pte & PTE_V))
      __vmr_decref((vmr_t*)*pte, 1);

    *pte = 0;
  }
  flush_tlb(); // TODO: shootdown
}

uintptr_t __do_mmap(uintptr_t addr, size_t length, int prot, int flags, file_t* f, off_t offset, ps_info *ps)
{
  size_t psize = ps->page_size;
  size_t npage = (length-1)/psize+1;

  if (flags & MAP_FIXED)
  {
    if (!align_page_size(addr, psize) || !__valid_user_range(addr, length, get_mmap_max(ps))) {
      return (uintptr_t)-1;
    }
  }
  else if ((addr = __vm_alloc(npage, ps)) == 0) {
      return (uintptr_t)-1;
  }

  vmr_t* v = __vmr_alloc(addr, length, f, offset, npage, prot);
  if (!v) {
      return (uintptr_t)-1;
  }

  for (uintptr_t a = addr; a < addr + length; a += psize)
  {
    pte_t* pte = __walk_create(a, ps->pte_step);
    kassert(pte);

    if (*pte)
      __do_munmap(a, psize, ps);
#if 0
	if ((addr < 0x40000000) && (0x3f800000 <= addr))
	    mm_printk("do_mmap create pte: addr:0x%x, a:0x%x, step:%d\n", addr, a, ps->pte_step);
#endif
    *pte = (pte_t)v;
  }

  if (!demand_paging || (flags & MAP_POPULATE))
       for (uintptr_t a = addr; a < addr + length; a += psize)
      kassert(__handle_page_fault(a, prot, ps) == 0);

  return addr;
}

int do_munmap(uintptr_t addr, size_t length)
{
  ps_info *ps = get_ps_by_vaddr(addr);

  if (!align_page_size(addr, ps->page_size) || !__valid_user_range(addr, length, get_mmap_max(ps)))
    return -EINVAL;

  spinlock_lock(&vm_lock);
    __do_munmap(addr, length, ps);
  spinlock_unlock(&vm_lock);

  return 0;
}

uintptr_t do_mmap(uintptr_t addr, size_t length, int prot, int flags, int fd, off_t offset)
{
  ps_info *ps = get_ps_by_flags(flags);

  mm_printk("[MMAP] addr: 0x%x len: 0x%x, %d, brk:0x%x \r\n", 
		addr, length, (length-1)/ps->page_size+1, current.brk);

  if (!(flags & MAP_PRIVATE) || length == 0 || (offset & (ps->page_size-1)))
    return -EINVAL;

  file_t* f = NULL;
  if (!(flags & MAP_ANONYMOUS) && (f = file_get(fd)) == NULL)
    return -EBADF;

  spinlock_lock(&vm_lock);
    addr = __do_mmap(addr, length, prot, flags, f, offset, ps);

  if (ps_min(ps) && (addr < current.brk_max))
      current.brk_max = addr;
  spinlock_unlock(&vm_lock);

  if (f) file_decref(f);
  return addr;
}

uintptr_t __do_brk(size_t addr)
{
  ps_info *ps = get_ps_min();

  uintptr_t newbrk = addr;
  if (addr < current.brk_min)
    newbrk = current.brk_min;
  else if (addr > current.brk_max)
    newbrk = current.brk_max;

  if (current.brk == 0)
    current.brk = ROUNDUP(current.brk_min, RISCV_PGSIZE);

  uintptr_t newbrk_page = ROUNDUP(newbrk, RISCV_PGSIZE);
  if (current.brk > newbrk_page)
    __do_munmap(newbrk_page, current.brk - newbrk_page, ps);
  else if (current.brk < newbrk_page)
    kassert(__do_mmap(current.brk, newbrk_page - current.brk, -1, MAP_FIXED|MAP_PRIVATE|MAP_ANONYMOUS, 0, 0, ps) == current.brk);

  current.brk = newbrk_page;

  return newbrk;
}

uintptr_t do_brk(size_t addr)
{
  spinlock_lock(&vm_lock);
    addr = __do_brk(addr);
  spinlock_unlock(&vm_lock);
  
  return addr;
}

uintptr_t do_mremap(uintptr_t addr, size_t old_size, size_t new_size, int flags)
{
  return -ENOSYS;
}

uintptr_t do_mprotect(uintptr_t addr, size_t length, int prot)
{
  uintptr_t res = 0;
  ps_info *ps = get_ps_by_vaddr(addr);
  size_t psize = ps->page_size;
  size_t step = ps->pte_step;

  if (!align_page_size(addr, psize))
    return -EINVAL;

  spinlock_lock(&vm_lock);
    for (uintptr_t a = addr; a < addr + length; a += psize)
    {
      pte_t* pte = __walk(a, step);
      if (pte == 0 || *pte == 0) {
        res = -ENOMEM;
        break;
      }
  
      if (!(*pte & PTE_V)) {
        vmr_t* v = (vmr_t*)*pte;
        if((v->prot ^ prot) & ~v->prot){
          //TODO:look at file to find perms
          res = -EACCES;
          break;
        }
        v->prot = prot;
      } else {
        if (!(*pte & PTE_U) ||
            ((prot & PROT_READ) && !(*pte & PTE_R)) ||
            ((prot & PROT_WRITE) && !(*pte & PTE_W)) ||
            ((prot & PROT_EXEC) && !(*pte & PTE_X))) {
          //TODO:look at file to find perms
          res = -EACCES;
          break;
        }
        *pte = pte_create(pte_ppn(*pte, step), step, prot_to_type(prot, 1));
      }
    }
  spinlock_unlock(&vm_lock);

  flush_tlb();
  return res;
}

void __map_kernel_range(uintptr_t vaddr, uintptr_t paddr, size_t len, int prot)
{
  ps_info *ps = get_ps_min();
  uintptr_t n = ROUNDUP(len, RISCV_PGSIZE) / RISCV_PGSIZE;
  uintptr_t offset = paddr - vaddr;
  for (uintptr_t a = vaddr, i = 0; i < n; i++, a += RISCV_PGSIZE)
  {
    pte_t* pte = __walk_create(a, ps->pte_step);
    kassert(pte);
    *pte = pte_create((a + offset) >> RISCV_PGSHIFT, ps->pte_step, prot_to_type(prot, 0));
  }
}

void populate_mapping(const void* start, size_t size, int prot)
{
  uintptr_t a0 = ROUNDDOWN((uintptr_t)start, RISCV_PGSIZE);
  for (uintptr_t a = a0; a < (uintptr_t)start+size; a += RISCV_PGSIZE)
  {
    if (prot & PROT_WRITE)
      atomic_add((int*)a, 0);
    else
      atomic_read((int*)a);
  }
}

uintptr_t pk_vm_init()
{
  // HTIF address signedness and va2pa macro both cap memory size to 2 GiB
  mem_size = MIN(mem_size, 1U << 31);
  size_t mem_pages = mem_size >> RISCV_PGSHIFT;
  free_pages = MAX(8, mem_pages >> (RISCV_PGLEVEL_BITS-1));
  
	//mm_printk("next: %d %d %d %d line:%d\r\n", next_free_page, RISCV_PGLEVEL_BITS, (VA_BITS - RISCV_PGSHIFT) / RISCV_PGLEVEL_BITS, MAX_VMR, __LINE__);
  //next: 0 9 3 102 line:411

  extern char _end;
  first_free_page = ROUNDUP((uintptr_t)&_end, RISCV_PGSIZE);
  first_free_paddr = first_free_page + free_pages * RISCV_PGSIZE;

  root_page_table = (void*)__page_alloc();
  __map_kernel_range(DRAM_BASE, DRAM_BASE, first_free_paddr - DRAM_BASE, PROT_READ|PROT_WRITE|PROT_EXEC);

#ifdef PK_USE_HUGETLB
  uintptr_t mem_size_hugetlb = mem_size >> 1; /* occupy half of memory : 1G */
#else
  uintptr_t mem_size_hugetlb = 0; /* occupy half of memory : 0 */
#endif
  uintptr_t mem_size_mintlb = mem_size - mem_size_hugetlb;
  uintptr_t kernel_range = first_free_paddr - DRAM_BASE;
  kassert(mem_size_mintlb > kernel_range);

  current.mmap_max = current.brk_max =
    MIN(DRAM_BASE, mem_size_mintlb - kernel_range);

#ifdef PK_USE_HUGETLB /* before __do_mmap() */
  current.mmap_max_hugetlb = ROUNDDOWN(MIN(DRAM_BASE, mem_size - kernel_range), psinfo[PS_HUGE].page_size);
  current.mmap_min_hugetlb = MIN(ROUNDUP(current.mmap_max, psinfo[PS_HUGE].page_size), current.mmap_max_hugetlb);

  psinfo[PS_HUGE].ppn_first = (ROUNDUP((mem_size_mintlb + DRAM_BASE), psinfo[PS_HUGE].page_size) - current.mmap_min_hugetlb) / psinfo[PS_HUGE].page_size;
#endif
  psinfo[PS_MIN].ppn_first = first_free_paddr / RISCV_PGSIZE;

  size_t stack_size = MIN(mem_pages >> 5, 2048) * RISCV_PGSIZE;
  size_t stack_bottom = __do_mmap(current.mmap_max - stack_size, stack_size, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_PRIVATE|MAP_ANONYMOUS|MAP_FIXED, 0, 0, get_ps_min());
  kassert(stack_bottom != (uintptr_t)-1);
  current.stack_top = stack_bottom + stack_size;

  flush_tlb();
  write_csr(sptbr, ((uintptr_t)root_page_table >> RISCV_PGSHIFT) | SPTBR_MODE_CHOICE);

  uintptr_t kernel_stack_top = __page_alloc() + RISCV_PGSIZE;

  mm_printk("mem: 0x%x %d 0x%x 0x%x 0x%x\r\n     0x%x 0x%x 0x%x 0x%x 0x%x %d\r\n     0x%x 0x%x %d %d 0x%x\r\n", 
  	mem_size, free_pages, first_free_page, first_free_paddr, DRAM_BASE, 
  	root_page_table, kernel_stack_top, current.mmap_max, stack_size, stack_bottom, next_free_page,
  	current.mmap_max_hugetlb, current.mmap_min_hugetlb, psinfo[PS_MIN].ppn_first, psinfo[PS_HUGE].ppn_first, kernel_range);

  return kernel_stack_top;
}
