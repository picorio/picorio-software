source ./script/file_to_list.tcl

# need to create a project targetting the correct SoC 
create_project -part xczu7ev-ffvc1156-2-e pygmy_e_fpga_zcu104

set_property source_mgmt_mode None [current_project]
# target the correct board as well, or when creating the Xilinx IPs errors will occur
set_property board_part xilinx.com:zcu104:part0:1.1 [current_project]

source ./script/zcu104/create_mult_ss_32.tcl
source ./script/zcu104/create_mult_uu_32.tcl
source ./script/zcu104/create_mult_su_32.tcl
source ./script/zcu104/create_div_s_32.tcl
source ./script/zcu104/create_div_u_32.tcl
source ./script/zcu104/create_zcu104_ddr4.tcl
source ./script/zcu104/create_xddr_cdc.tcl
source ./script/zcu104/create_jtag_bd.tcl

set git_root $env(PROJ_ROOT)

# read in an flist
set flist ${git_root}/rtl/flist/fpga_e.syn.f

# Create 'sources_1' fileset (if not found)
if {[string equal [get_filesets -quiet sources_1] ""]} {
   create_fileset -srcset sources_1
}
 # Set 'sources_1' fileset object
set obj [get_filesets sources_1]
set_property -name "verilog_define" -value "SYNTHESIS FPGA FPGA_SIM PYGMY_E EXT_OR_BRIDGE PYGMY_E" -objects $obj

foreach f [expand_file_list  $flist] {
  puts "Reading $f"
  read_verilog -library ours -sv $f
  add_files  -verbose -norecurse -scan_for_includes -fileset $obj $f
}
 
# import files will create a copy of the imported file and place it by default under directory
read_xdc  ${git_root}/subproj/pygmy_e_fpga/zcu104/fpga_e_zcu104.xdc

#create 'sim_1' fileset (if not found)
if {[string equal [get_filesets -quiet sim_1] ""]} {
  create_fileset -simset sim_1
}

# Set 'sim_1' fileset object
set obj [get_filesets sim_1]
set_property -name "verilog_define" -value "SYNTHESIS FPGA FPGA_SIM PYGMY_E EXT_OR_BRIDGE PYGMY_E" -objects $obj

# any read_* commands like read_verilog, read_xdc, will read by reference instead of making a copy like import_files does.
# therefore, you edit the original file path instead of having to edit the local imported one
# use whichever depending on how you want your workflow to be: either you dirty up your git repo or you remember to copy back to git later on

# change top level module
set_property top pygmy_e_fpga_zcu104 [current_fileset]
