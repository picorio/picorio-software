# file copy -force C:/vcu118_pcie/vcu118_pcie_x16_gen3_ex/vcu118_pcie_x16_gen3_ex.runs/impl_1/xilinx_pcie4_uscale_ep.bit .
set BIT_FILE ./fpga_es1y.runs/impl_1/pygmy_es1y_fpga.bit
set OUT_FILE ./fpga_es1y.runs/impl_1/pygmy_es1y_fpga.mcs
write_cfgmem -force -format MCS -size 128 -interface SPIx8 -loadbit "up 0x00000000 ${BIT_FILE}" ${OUT_FILE} -verbose
