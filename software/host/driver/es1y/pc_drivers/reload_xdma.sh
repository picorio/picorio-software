# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi


rmmod xdma

# the pcie identifier 0000:02:00.0 represents a physical pcie slot and doesnt change
# between reboots, so it should be okay to hardcode it
# you can find this information from "lspci -vvv -d 10ee:"

echo 1 > /sys/bus/pci/devices/0000:02:00.0/remove 
echo 1 > /sys/bus/pci/rescan
cd ./Xilinx_Answer_65444_Linux_Files_rel20180420/tests/
./Xilinx_Answer_65444_Linux_Files_rel20180420/tests/load_driver.sh
cd -
