// for uint*_t data types
#include <stdint.h>
// for print statements
#include <inttypes.h>
#include <stdio.h>
// for ctrl-c interrupt handling
#include <csignal>
// write to files
#include <cstdio>

#include "pcie_driver.h"

#define WORD_SIZE AXI_DATA_BUS_WIDTH_IN_BYTES //size in bytes
#define MEMORY_SIZE 0x80000 // 512kb bram
#define NUM_ADDRESSES MEMORY_SIZE/WORD_SIZE

#define DEBUG

using namespace std;

int main() {  
  uint32_t addr;
  uint64_t rddata;
  uint64_t wrdata;
  uint8_t * buf;
  char * test_name = new char;
  test_name = "reset_board";

  buf = (uint8_t *) malloc (sizeof(uint8_t) * AXI_DATA_BUS_WIDTH_IN_BYTES);

  printf("Running %s...\n",test_name);
  wrdata = 0x0000000000000000;
  do_reset(wrdata);
  sleep(1);

  wrdata = 0x0000000000000001;
  do_reset(wrdata);
  sleep(1);  
}

