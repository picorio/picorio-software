set module XIL_fp_mul

create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name $module
set_property -dict [list \
	CONFIG.Operation_Type {Multiply}  \
	CONFIG.A_Precision_Type {Half}  \
	CONFIG.C_Mult_Usage {Max_Usage}  \
	CONFIG.Axi_Optimize_Goal {Resources}  \
	CONFIG.Has_ACLKEN {true}  \
	CONFIG.Has_ARESETn {true}  \
	CONFIG.C_Has_UNDERFLOW {true}  \
	CONFIG.C_Has_OVERFLOW {true}  \
	CONFIG.C_Has_INVALID_OP {true}  \
	CONFIG.C_A_Exponent_Width {5}  \
	CONFIG.C_A_Fraction_Width {11}  \
	CONFIG.Result_Precision_Type {Half}  \
	CONFIG.C_Result_Exponent_Width {5}  \
	CONFIG.C_Result_Fraction_Width {11}  \
	CONFIG.C_Accum_Msb {32}  \
	CONFIG.C_Accum_Lsb {-24}  \
	CONFIG.C_Accum_Input_Msb {15}  \
	CONFIG.C_Latency {4}  \
	CONFIG.Maximum_Latency {false} \
	CONFIG.C_Rate {1}] [get_ips $module]
