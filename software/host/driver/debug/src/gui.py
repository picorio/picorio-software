#!/usr/bin/env python3

import cmd
import os
import re
import textwrap
import inspect
import glob
import time
from dbg_defines import *
from user import *
from physical import *

class GUI(cmd.Cmd):
    def __init__(self, user):
        super().__init__()
        self.intro = "OURS Debug System Version 0.1\n" # Arbitrary system title
        self.intro += "Type 'help' to see the list of commands.\n"
        self.intro += "Type 'help <cmd>' to find out more about <cmd>."
        self.prompt = '>> '
        self.doc_header = 'Available commands (type help <cmd>):'
        self.user = user
        self.alias = {}

    def help_help(self):
        help_str = """\
        help: help ([cmd])
            Displays available commands with 'help' and detailed help with
            'help <cmd>'.
        """
        print(textwrap.dedent(help_str))

    ## utility ##

    def is_hex(self, value):
        try:
            int(value, 16)
            return True
        except:
            return False

    def is_int(self, value):
        try:
            int(value)
            return True
        except:
            return False

    def has_dot(self, value):
        return (len(value.split('.')) == 2)

    ## exit ##
    def do_exit(self, *args):
        return True

    def help_exit(self):
        help_str = """\
        exit: exit
            Exits the GUI.
        """
        print(textwrap.dedent(help_str))

    ## clear ##
    def do_clear(self, *args):
        # from os import system, name
        # if (name == 'nt'):
        #     _ = system('cls')
        # else:
        #     _ = system('clear')
        print("\033[H\033[J")

    def help_clear(self):
        help_str = """\
        clear: clear
            Clears the console.
        """
        print(textwrap.dedent(help_str))

    ## read ##

    def do_read(self, *args):
        args_list = args[0].split()

        # read [reg addr]
        if (len(args_list) == 1 and self.is_hex(args_list[0])):
            addr = int(args_list[0], 16)
            data, field_data = self.user.read_reg_addr(addr)
            if (data != None and field_data != None):
                print(data)
                self.print_field_data(field_data)
                return

        # read [mem addr]
        if (len(args_list) == 1 and self.is_hex(args_list[0])):
            addr = int(args_list[0], 16)
            data = self.user.read_mem_addr(addr)
            if (data != None):
                print(data)
                return

        # read [reg name]
        if (len(args_list) == 1):
            name = args_list[0]
            data, field_data = self.user.read_reg_name(name)
            if (data != None and field_data != None):
                print(data)
                self.print_field_data(field_data)
                return

        # read [mem addr] [target]
        if (len(args_list) == 2 and self.is_hex(args_list[0])):
            addr = int(args_list[0], 16)
            target = args_list[1]
            data = self.user.read_mem_addr(addr, target)
            if (data != None):
                print(data)
                return

        # read [mem addr] [range]
        if (len(args_list) == 2 and self.is_hex(args_list[0]) and
            self.is_int(args_list[1])):
            addr = int(args_list[0], 16)
            range = int(args_list[1])
            data = self.user.read_mem_range(addr, range)
            if (data != None):
                print(data)
                return

        # read [mem addr] [range] [target]
        if (len(args_list) == 3 and self.is_hex(args_list[0]) and
            self.is_int(args_list[1])):
            addr = int(args_list[0], 16)
            range = int(args_list[1])
            target = args_list[2]
            data = self.user.read_mem_range(addr, range, target)
            if (data != None):
                print(data)
                return


        print('Invalid arguments')

    def print_field_data(self, field_data):
        name_list = ['FIELD']
        val_list = ['VALUE']
        for name, val in field_data.items():
            name_list.append(name)
            val_list.append(val)
            margin = str(len(max(name_list, key=len)))
            s = ''
            for c1, c2 in zip(name_list, val_list):
                if (type(c1) == str and type(c2) == str):
                    s += ('%-' + margin + 's %s') % (c1,c2) + '\n'
                else:
                    s += ('%-' + margin + 's 0x%x') % (c1,c2) + '\n'
        print(s)

    def help_read(self):
        help_str = """\
        read: read [reg name], read [reg addr]; read [mem addr] ([target]),
              read [mem addr] [range] ([target])
        Reads either a register or memory based on the input arguments. To read
        a register, provide either the register name or address. To read from
        memory, provide an address and optionally a target or provide an
        address, a range, and optionally a target. The default target is {default}.
        """.format(default=MEM_DEFAULT_TARGET)
        print(textwrap.dedent(help_str))

    def complete_read(self, text, line, begidx, endidx):
        arg_list = self.user.get_register_names()
        return [i for i in arg_list if i.startswith(text)]

    ## write ##

    def do_write(self, *args):
        args_list = args[0].split()

        # write [reg name]
        if (len(args_list) == 1):
            name = args_list[0]
            data = self.user.write_reg_name(name)
            if (data != None):
                print('Written')
                return

        print('Invalid arguments')

    def help_write(self):
        help_str = """\
        write: write [reg name]
        Writes the updates to the provided register.
        """
        print(textwrap.dedent(help_str))

    def complete_write(self, text, line, begidx, endidx):
        arg_list = self.user.get_register_names()
        return [i for i in arg_list if i.startswith(text)]

    ## update ##

    def do_update(self, *args):
        args_list = args[0].split()

        # update [reg name].[field name] [value]
        if (len(args_list) == 2 and self.has_dot(args_list[0]) and
            self.is_hex(args_list[1])):
            reg_name = args_list[0].split('.')[0]
            field_name = args_list[0].split('.')[1]
            value = int(args_list[1], 16)
            data = self.user.update_reg_field(reg_name, field_name, value)
            if (data != None):
                print('Updated')
                return

        # update [reg name] [value]
        if (len(args_list) == 2 and self.is_hex(args_list[1])):
            name = args_list[0]
            value = int(args_list[1], 16)
            data = self.user.update_reg(name, value)
            if (data != None):
                print('Updated')
                return

        print('Invalid arguments')

    def help_update(self):
        help_str = """\
        update: update [reg name] [value], update [reg name].[field name] [value]
        Updates the register or field with the provided value. The updates are
        moved to a staging environment; use the 'write' command to commit the
        updates to the register or field.
        """
        print(textwrap.dedent(help_str))

    def complete_update(self, text, line, begidx, endidx):
        if self.has_dot(text):
            entry = text.split('.')
            reg_name = entry[0]
            field_text = ''
            if len(entry) == 2:
                field_text = entry[1]
            reg = self.user.get_reg_name(reg_name)
            if (reg != None):
                arg_list = self.user.get_reg_field_names(reg)
                return [reg_name + '.' + i for i in arg_list if i.startswith(field_text)]
        else:
            arg_list = self.user.get_register_names()
            return [i for i in arg_list if i.startswith(text)]

    ## show ##

    def do_show(self, *args):
        args_list = args[0].split()

        # show [reg name].[field name]
        if (len(args_list) == 1 and self.has_dot(args_list[0])):
            reg_name = args_list[0].split('.')[0]
            field_name = args_list[0].split('.')[1]
            data, staged_data = self.user.show_reg_field(reg_name, field_name)
            if (data != None and staged_data != None):
                if (data == staged_data):
                    print(data + ' No update')
                else:
                    print(data + ' --> ' + staged_data)
                return

        # show [reg name]
        if (len(args_list) == 1):
            name = args_list[0]
            data, staged_data = self.user.show_reg(name)
            if (data != None and staged_data != None):
                if (data == staged_data):
                    print(data + ' No update')
                else:
                    print(data + ' --> ' + staged_data)
                return

        print('Invalid arguments')

    def help_show(self):
        help_str = """\
        show: show [reg name], show [reg name].[field name]
        Shows the difference between the most recent read for a register or
        field and any updates that have been made using the 'update' command.
        NOTE: The 'read' command should be called before 'show' for any
        register or field.
        """
        print(textwrap.dedent(help_str))

    def complete_show(self, text, line, begidx, endidx):
        if self.has_dot(text):
            entry = text.split('.')
            reg_name = entry[0]
            field_text = ''
            if len(entry) == 2:
                field_text = entry[1]
            reg = self.user.get_reg_name(reg_name)
            if (reg != None):
                arg_list = self.user.get_reg_field_names(reg)
                return [reg_name + '.' + i for i in arg_list if i.startswith(field_text)]
        else:
            arg_list = self.user.get_register_names()
            return [i for i in arg_list if i.startswith(text)]

    ## registers ##

    def do_registers(self, *args):
        print(self.user.get_register_info())

    def help_registers(self):
        help_str = """\
        registers: registers
            Prints a list of all register names and addresses.
        """
        print(textwrap.dedent(help_str))

    ## fields ##

    def do_fields(self, *args):
        args_list = args[0].split()

        if (len(args_list) == 0):
            self.help_fields()
            return
        # fields [reg name]
        if (len(args_list) == 1):
            name = args_list[0]
            data = self.user.get_field_info(name)
            if (data != None):
                print (data)
                return

        print('Invalid arguments')

    def complete_fields(self, text, line, begidx, endidx):
        arg_list = self.user.get_register_names()
        return [i for i in arg_list if i.startswith(text)]

    def help_fields(self):
        help_str = """\
        fields: fields [reg name]
            Prints the fields associated to the register.
        """
        print(textwrap.dedent(help_str))

    ## memory ##
    def do_memory(self, *args):
        print(self.user.get_memory_info())

    def help_memory(self):
        help_str = """\
        memory: memory
            Prints the start and range of the memory.
        """
        print(textwrap.dedent(help_str))

    ## misc ##
    def get_curr_func_name(self):
        # inspect.stack()[0][3] is always get_curr_func_name
        # use [1][3] to get the function name for prev one
        return inspect.stack()[1][3]
  
    def parse_command(self, func_name, *args):
        ls_arg = args[0].split()
        assert(func_name.startswith("do_")), print("Calling from wrong function: %s" % func_name)
        parser_name = func_name.replace("do_", "") + "_parser"
        assert(getattr(self, parser_name) != None), print("The parser specified %s is not created!" % parser_name)
        try:
            arg = getattr(self, parser_name).parse_args(ls_arg)
        except SystemExit:
            arg = None
        return arg
  
    def command_help(self, func_name):
        assert(func_name.startswith("help_")), print("Calling from wrong function: %s" % func_name)
        parser_name = func_name.replace("help_", "") + "_parser"
        assert(getattr(self, parser_name) != None), print("The parser specified %s is not created!" % parser_name)
        return getattr(self, parser_name).print_help()

    ## source batch script ##
    def do_source(self, *args):
        script_file = args[0]
        if not os.path.exists(script_file):
            print('Source file not found: ' + script_file)
        else:
            with open(script_file) as fh_script:
                for line in fh_script:
                    print('>>> ' + line, end='')
                    self.onecmd(line)
        return

    def help_source(self):
        help_str = """\
        source: source [script path]
        Run a batch script.
        """
        print(textwrap.dedent(help_str))

    def complete_source(self, text, line, begidx, endidx):
        before_arg = line.rfind(" ", 0, begidx)
        if before_arg == -1:
            return
        fixed = line[before_arg+1:begidx]
        arg = line[before_arg+1:endidx]
        completions = []
        for path in glob.glob(arg + '*'):
            if os.path.isdir(path) and path[-1] != os.sep:
                path = path + os.sep

            completions.append(path.replace(fixed, "", 1))
        return completions

    ## alias ##
    def do_alias(self, *args):
        args_list = args[0].split()
        if len(args_list) == 1:
            name = args_list[0]
            if name not in self.alias:
                print('No alias for "' + name + '" found')
            for command in self.alias[name]:
                print('>>> ' + command)
                self.onecmd(command)
                time.sleep(0.5)
        else:
            sep = ' '
            name = args_list[0]
            command = sep.join(args_list[1::])
            command_list = command.split(';')
            self.alias[name] = command_list
        
        
    def help_alias(self):
        help_str = """\
        help: alias [name] [command]
        Sets an alias for a command. The alias name must not include whitespace, or part of it will be counted as the command. Multiple commands should be deliminated with a semi-colon. Do not nest aliases.

              alias [name]
        Calls the alias that was previously set.
        """
        print(textwrap.dedent(help_str))

    def complete_alias(self, text, line, begidx, endidx):
        arg_list = self.alias.keys()
        return [i for i in arg_list if i.startswith(text)]

    ## gen station rtl ##
    def do_gen_station_rtl(self, *args):
        args_list = args[0].split()

        if (len(args_list) == 0):
            self.user.gen_station_rtl()
            return

        print('Invalid arguments')

    def help_gen_station_rtl(self):
        help_str = """\
        gen_station_rtl: gen_station_rtl
            Generates rtl for all register blocks in the STATION and BLOCK locations.
        """
        print(textwrap.dedent(help_str))

    ## gen addr map ##
    def do_gen_addr_map(self, *args):
        args_list = args[0].split()

        if (len(args_list) == 0):
            self.user.gen_addr_map()
            return
        
        print('Invalid arguments')

    def help_gen_addr_map(self):
        help_str = """\
        gen_addr_map: gen_addr_map
            Generates address mapping for all registers.
        """
        print(textwrap.dedent(help_str))
