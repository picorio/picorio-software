#!/bin/bash
#
# Script to build freertos.

if [[ x"$1" == x"pep" ]]; then
	PROJ=PEP
elif [[ x"$1" == x"pet" ]]; then
	PROJ=PET
	if [[ x"$2" == x"M25P80" ]]; then
		CFLAGS="-D_SPI_NOR_ -D_M25P80_ -D_SPI_MEM_ -D_SPI_CORE_ -D_SPI_DW_ -DTEST_DEMO_SPI_FLASH -D_M25P80_TEST_"
	fi
	CFLAGS+=-DPROJ=${PROJ} make all
elif [[ x"$1" == x"clean" ]]; then
	make clean
fi

