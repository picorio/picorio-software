** HW H2C = /dev/xdma0_h2c_0 bytecount = 64 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00000040, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 64
perf.iterations = 43895783
(data transferred = 2809330112 bytes)
perf.clock_cycle_count = 500323679
perf.data_cycle_count = 43895825
(data duty cycle = 8%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 64 bytes
perf.iterations = 43896049
(data transferred = 2809347136 bytes)
perf.clock_cycle_count = 500326645
perf.data_cycle_count = 43896049
(data duty cycle = 8%)
 data rate ***** bytes length = 64, rate = 0.087735 
perf.pending_count = 0
** HW H2C = /dev/xdma0_h2c_0 bytecount = 128 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00000080, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 128
perf.iterations = 42136689
(data transferred = 5393496192 bytes)
perf.clock_cycle_count = 500323211
perf.data_cycle_count = 84273452
(data duty cycle = 16%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 128 bytes
perf.iterations = 42136953
(data transferred = 5393529984 bytes)
perf.clock_cycle_count = 500326104
perf.data_cycle_count = 84273893
(data duty cycle = 16%)
 data rate ***** bytes length = 128, rate = 0.168438 
perf.pending_count = 0
** HW H2C = /dev/xdma0_h2c_0 bytecount = 256 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00000100, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 256
perf.iterations = 38041062
(data transferred = 9738511872 bytes)
perf.clock_cycle_count = 500324139
perf.data_cycle_count = 152164444
(data duty cycle = 30%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 256 bytes
perf.iterations = 38041324
(data transferred = 9738578944 bytes)
perf.clock_cycle_count = 500327342
perf.data_cycle_count = 152165287
(data duty cycle = 30%)
 data rate ***** bytes length = 256, rate = 0.304131 
perf.pending_count = 0
** HW H2C = /dev/xdma0_h2c_0 bytecount = 512 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00000200, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 512
perf.iterations = 24679940
(data transferred = 12636129280 bytes)
perf.clock_cycle_count = 500320976
perf.data_cycle_count = 197439784
(data duty cycle = 39%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 512 bytes
perf.iterations = 24680076
(data transferred = 12636198912 bytes)
perf.clock_cycle_count = 500323351
perf.data_cycle_count = 197440595
(data duty cycle = 39%)
 data rate ***** bytes length = 512, rate = 0.394626 
perf.pending_count = 0
** HW H2C = /dev/xdma0_h2c_0 bytecount = 1024 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00000400, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 1024
perf.iterations = 13010007
(data transferred = 13322247168 bytes)
perf.clock_cycle_count = 500319817
perf.data_cycle_count = 208160376
(data duty cycle = 41%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 1024 bytes
perf.iterations = 13010078
(data transferred = 13322319872 bytes)
perf.clock_cycle_count = 500322204
perf.data_cycle_count = 208161239
(data duty cycle = 41%)
 data rate ***** bytes length = 1024, rate = 0.416054 
perf.pending_count = 0
** HW H2C = /dev/xdma0_h2c_0 bytecount = 2048 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00000800, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 2048
perf.iterations = 6642726
(data transferred = 13604302848 bytes)
perf.clock_cycle_count = 500321701
perf.data_cycle_count = 212567508
(data duty cycle = 42%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 2048 bytes
perf.iterations = 6642764
(data transferred = 13604380672 bytes)
perf.clock_cycle_count = 500324250
perf.data_cycle_count = 212568463
(data duty cycle = 42%)
 data rate ***** bytes length = 2048, rate = 0.424861 
perf.pending_count = 0
** HW H2C = /dev/xdma0_h2c_0 bytecount = 4096 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00001000, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 4096
perf.iterations = 3354470
(data transferred = 13739909120 bytes)
perf.clock_cycle_count = 500323405
perf.data_cycle_count = 214686360
(data duty cycle = 42%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 4096 bytes
perf.iterations = 3354488
(data transferred = 13739982848 bytes)
perf.clock_cycle_count = 500325784
perf.data_cycle_count = 214687255
(data duty cycle = 42%)
 data rate ***** bytes length = 4096, rate = 0.429095 
perf.pending_count = 0
** HW H2C = /dev/xdma0_h2c_0 bytecount = 8192 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00002000, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 8192
perf.iterations = 1682608
(data transferred = 13783924736 bytes)
perf.clock_cycle_count = 500321714
perf.data_cycle_count = 215374096
(data duty cycle = 43%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 8192 bytes
perf.iterations = 1682617
(data transferred = 13783998464 bytes)
perf.clock_cycle_count = 500324109
perf.data_cycle_count = 215374999
(data duty cycle = 43%)
 data rate ***** bytes length = 8192, rate = 0.430471 
perf.pending_count = 0
** HW H2C = /dev/xdma0_h2c_0 bytecount = 16384 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00004000, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 16384
perf.iterations = 842284
(data transferred = 13799981056 bytes)
perf.clock_cycle_count = 500321918
perf.data_cycle_count = 215625132
(data duty cycle = 43%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 16384 bytes
perf.iterations = 842289
(data transferred = 13800062976 bytes)
perf.clock_cycle_count = 500324329
perf.data_cycle_count = 215626043
(data duty cycle = 43%)
 data rate ***** bytes length = 16384, rate = 0.430973 
perf.pending_count = 0
** HW H2C = /dev/xdma0_h2c_0 bytecount = 32768 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00008000, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 32768
perf.iterations = 421526
(data transferred = 13812563968 bytes)
perf.clock_cycle_count = 500321870
perf.data_cycle_count = 215821612
(data duty cycle = 43%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 32768 bytes
perf.iterations = 421528
(data transferred = 13812629504 bytes)
perf.clock_cycle_count = 500324592
perf.data_cycle_count = 215822660
(data duty cycle = 43%)
 data rate ***** bytes length = 32768, rate = 0.431365 
perf.pending_count = 0
** HW H2C = /dev/xdma0_h2c_0 bytecount = 65536 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00010000, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 65536
perf.iterations = 210860
(data transferred = 13818920960 bytes)
perf.clock_cycle_count = 500321216
perf.data_cycle_count = 215921740
(data duty cycle = 43%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 65536 bytes
perf.iterations = 210862
(data transferred = 13819052032 bytes)
perf.clock_cycle_count = 500323794
perf.data_cycle_count = 215922723
(data duty cycle = 43%)
 data rate ***** bytes length = 65536, rate = 0.431566 
perf.pending_count = 0
** HW H2C = /dev/xdma0_h2c_0 bytecount = 131072 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00020000, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 131072
perf.iterations = 105439
(data transferred = 13820100608 bytes)
perf.clock_cycle_count = 500320500
perf.data_cycle_count = 215939768
(data duty cycle = 43%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 131072 bytes
perf.iterations = 105439
(data transferred = 13820100608 bytes)
perf.clock_cycle_count = 500323142
perf.data_cycle_count = 215940783
(data duty cycle = 43%)
 data rate ***** bytes length = 131072, rate = 0.431603 
perf.pending_count = 0
** HW H2C = /dev/xdma0_h2c_0 bytecount = 262144 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00040000, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 262144
perf.iterations = 52722
(data transferred = 13820755968 bytes)
perf.clock_cycle_count = 500322204
perf.data_cycle_count = 215950104
(data duty cycle = 43%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 262144 bytes
perf.iterations = 52722
(data transferred = 13820755968 bytes)
perf.clock_cycle_count = 500324742
perf.data_cycle_count = 215951071
(data duty cycle = 43%)
 data rate ***** bytes length = 262144, rate = 0.431622 
perf.pending_count = 0
** HW H2C = /dev/xdma0_h2c_0 bytecount = 524288 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00080000, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 524288
perf.iterations = 26360
(data transferred = 13820231680 bytes)
perf.clock_cycle_count = 500322519
perf.data_cycle_count = 215948600
(data duty cycle = 43%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 524288 bytes
perf.iterations = 26361
(data transferred = 13820755968 bytes)
perf.clock_cycle_count = 500325160
perf.data_cycle_count = 215949612
(data duty cycle = 43%)
 data rate ***** bytes length = 524288, rate = 0.431619 
perf.pending_count = 0
** HW H2C = /dev/xdma0_h2c_0 bytecount = 1048576 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00100000, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 1048576
perf.iterations = 13181
(data transferred = 13821280256 bytes)
perf.clock_cycle_count = 500321802
perf.data_cycle_count = 215959588
(data duty cycle = 43%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 1048576 bytes
perf.iterations = 13181
(data transferred = 13821280256 bytes)
perf.clock_cycle_count = 500324397
perf.data_cycle_count = 215960579
(data duty cycle = 43%)
 data rate ***** bytes length = 1048576, rate = 0.431641 
perf.pending_count = 0
** HW H2C = /dev/xdma0_h2c_0 bytecount = 2097152 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00200000, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 2097152
perf.iterations = 6590
(data transferred = 13820231680 bytes)
perf.clock_cycle_count = 500321565
perf.data_cycle_count = 215960092
(data duty cycle = 43%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 2097152 bytes
perf.iterations = 6590
(data transferred = 13820231680 bytes)
perf.clock_cycle_count = 500324214
perf.data_cycle_count = 215961111
(data duty cycle = 43%)
 data rate ***** bytes length = 2097152, rate = 0.431642 
perf.pending_count = 0
** HW H2C = /dev/xdma0_h2c_0 bytecount = 4194304 and iteration = 1
'/dev/xdma0_h2c_0'
 count = 1
device = /dev/xdma0_h2c_0, size = 0x00400000, count = 1
IOCTL_XDMA_PERF_START succesful.
IOCTL_XDMA_PERF_GET succesful.
perf.transfer_size = 4194304
perf.iterations = 3295
(data transferred = 13820231680 bytes)
perf.clock_cycle_count = 500321845
perf.data_cycle_count = 215958168
(data duty cycle = 43%)
IOCTL_XDMA_PERF_STOP succesful.
perf.transfer_size = 4194304 bytes
perf.iterations = 3295
(data transferred = 13820231680 bytes)
perf.clock_cycle_count = 500324468
perf.data_cycle_count = 215959172
(data duty cycle = 43%)
 data rate ***** bytes length = 4194304, rate = 0.431638 
perf.pending_count = 0
