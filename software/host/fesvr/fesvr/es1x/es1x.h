#ifndef __HTIF_ES1X_H
#define __HTIF_ES1X_H
#include "htif_proj.h"

//class htif_proj;

class es1x : public htif_proj {
  typedef void (es1x::*func_pointer)(std::string key);
  private:
    // list of functions
    void bg() {return;};
    void io_test(std::string key);
    void program_pll(std::string key);
    void reset_ddr(std::string key);  
    void update_l2_configuration(std::string key);
    void release_reset_for_l2(std::string key);
    void test_l2(std::string key);
    void set_valid_ram_to_zero_for_l2(std::string key);
    void release_reset_for_mp_l1(std::string key);
    void test_l1(std::string key);
    void zero_out_l1_tag_ram(std::string key);
    void warmup_l1_cache(std::string key);
    void load_kernel(std::string key);
    void load_image(std::string key);
    void load_weight(std::string key);
    void alloc_stack(std::string key);
    void set_magic_mem_address(std::string key);
    void set_reset_pc(std::string key);
    void load_rom(std::string key);
    void dump_l2_after_loading(std::string key);
    void release_vcore_reset(std::string key);
    void mem_test(std::string key);
    void enable_dram_tester(std::string key);
    void release_orv_reset(std::string key);
    void trigger_afterwards(std::string key);
    struct func_t {
      std::string key;
      es1x::func_pointer ptr;
   
    };
    std::vector<func_t> boot_sequence;
  public:
    es1x(htif_t * ht) : htif_proj("es1x", ht){
        boot_sequence.push_back((func_t){"io_test"  ,&es1x::io_test});
        boot_sequence.push_back((func_t){"program_pll="  ,&es1x::program_pll});
        boot_sequence.push_back((func_t){"reset_ddr"  ,&es1x::reset_ddr});
        boot_sequence.push_back((func_t){"config_l2=", &es1x::update_l2_configuration});
        boot_sequence.push_back((func_t){"release_l2_reset", &es1x::release_reset_for_l2});
        boot_sequence.push_back((func_t){"test_l2", &es1x::test_l2});
        boot_sequence.push_back((func_t){"set_0_to_l2_vldram", &es1x::set_valid_ram_to_zero_for_l2});
        boot_sequence.push_back((func_t){"release_l1_reset", &es1x::release_reset_for_mp_l1});
        boot_sequence.push_back((func_t){"test_l1", &es1x::test_l1});
        boot_sequence.push_back((func_t){"set_0_to_l1_tagram", &es1x::zero_out_l1_tag_ram});
        boot_sequence.push_back((func_t){"l1_warmup", &es1x::warmup_l1_cache});
        boot_sequence.push_back((func_t){"load_pk", &es1x::load_kernel});
        boot_sequence.push_back((func_t){"load_image=", &es1x::load_image});
        boot_sequence.push_back((func_t){"load_weight=", &es1x::load_weight});
        boot_sequence.push_back((func_t){"alloc_sp=", &es1x::alloc_stack});
        boot_sequence.push_back((func_t){"set_magic_mem", &es1x::set_magic_mem_address});
        boot_sequence.push_back((func_t){"set_rst_pc=", &es1x::set_reset_pc});
        boot_sequence.push_back((func_t){"load_rom", &es1x::load_rom});
        boot_sequence.push_back((func_t){"dump_l2_after_loading=", &es1x::dump_l2_after_loading});
        boot_sequence.push_back((func_t){"release_vcore_reset", &es1x::release_vcore_reset});
        boot_sequence.push_back((func_t){"mem_test=", &es1x::mem_test});
        boot_sequence.push_back((func_t){"dram_tester_test", &es1x::enable_dram_tester});
        boot_sequence.push_back((func_t){"release_orv_reset", &es1x::release_orv_reset});
        boot_sequence.push_back((func_t){"trigger_afterwards", &es1x::trigger_afterwards});
        auto it = this->boot_sequence.begin();
        for(;it != this->boot_sequence.end(); ++it){
          this->ht->argmap.insert({it->key.c_str(), ""});
        }

    } 
  void setup();
  ~es1x();
  
  friend class htif_t;
};
#endif
