##################################################################
# CREATE IP mult_su_32
##################################################################

set mult_gen mult_su_32
create_ip -name mult_gen -vendor xilinx.com -library ip -version 12.0 -module_name $mult_gen

set_property -dict { 
  CONFIG.PortAWidth {32}
  CONFIG.PortBType {Unsigned}
  CONFIG.PortBWidth {32}
  CONFIG.Multiplier_Construction {Use_Mults}
  CONFIG.OutputWidthHigh {63}
  CONFIG.PipeStages {6}
  CONFIG.ClockEnable {true}
} [get_ips $mult_gen]

##################################################################

