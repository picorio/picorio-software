set module zcu104_ddr4_0
create_ip -name ddr4 -vendor xilinx.com -library ip -version 2.2 -module_name $module 
set_property -dict [list \
        CONFIG.C0_CLOCK_BOARD_INTERFACE {clk_300mhz} \
        CONFIG.C0_DDR4_BOARD_INTERFACE {ddr4_sdram} \
        CONFIG.RESET_BOARD_INTERFACE {reset} \
	CONFIG.C0.DDR4_TimePeriod {938} \
	CONFIG.C0.DDR4_InputClockPeriod {3335} \
	CONFIG.C0.DDR4_Specify_MandD {false} \
	CONFIG.C0.DDR4_CLKOUT0_DIVIDE {6} \
	CONFIG.C0.DDR4_MemoryType {SODIMMs} \
	CONFIG.C0.DDR4_MemoryPart {MTA8ATF51264HZ-2G1} \
	CONFIG.C0.DDR4_DataWidth {64} \
	CONFIG.C0.DDR4_AxiIDWidth {8} \
	CONFIG.C0.DDR4_AxiSelection {true} \
	CONFIG.C0.DDR4_CasLatency {15} \
	CONFIG.C0.DDR4_AxiDataWidth {512} \
	CONFIG.C0.DDR4_SELF_REFRESH {false} \
	CONFIG.C0.DDR4_SAVE_RESTORE {false} \
	CONFIG.ADDN_UI_CLKOUT1_FREQ_HZ {24}] [get_ips $module]
