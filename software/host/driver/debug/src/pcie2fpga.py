from ctypes import *
import os.path
import platform
import time

if platform.system() == 'Windows':
    from ctypes.wintypes import *

class PCIE2FPGA:
    def __init__(self):
        if platform.system() == 'Windows':
            sys.exit("Do not support Windows for PCIE2FPGA driver yet")
        else:
            self._pcie_drv = cdll.LoadLibrary(os.path.join(os.path.dirname(__file__), "libpcie.so"))

    def driver_write(self, addr, data):
        print("addr = %x, data = %x" % (addr, data))
        self._pcie_drv.do_write(c_ulonglong(addr), c_ulonglong(data))

    def driver_read(self, addr, retry=True):
        ret_data = self._pcie_drv.do_read(c_ulonglong(addr))
        resp_err = 0
        return ret_data, resp_err

