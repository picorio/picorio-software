/*!
 * \file      board-config.h
 *
 * \brief     Board configuration
 *
 * \copyright Revised BSD License, see section \ref LICENSE.
 *
 * \code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 *               ___ _____ _   ___ _  _____ ___  ___  ___ ___
 *              / __|_   _/_\ / __| |/ / __/ _ \| _ \/ __| __|
 *              \__ \ | |/ _ \ (__| ' <| _| (_) |   / (__| _|
 *              |___/ |_/_/ \_\___|_|\_\_| \___/|_|_\\___|___|
 *              embedded.connectivity.solutions===============
 *
 * \endcode
 *
 * \author    Miguel Luis ( Semtech )
 *
 * \author    Gregory Cristian ( Semtech )
 *
 * \author    Daniel Jaeckle ( STACKFORCE )
 *
 * \author    Johannes Bruder ( STACKFORCE )
 */
#ifndef __BOARD_CONFIG_H__
#define __BOARD_CONFIG_H__

#ifdef __cplusplus
extern "C"
{
#endif

/*!
 * Defines the time required for the TCXO to wakeup [ms].
 */
#if defined( SX1262MBXDAS )
#define BOARD_TCXO_WAKEUP_TIME                      5
#else
#define BOARD_TCXO_WAKEUP_TIME                      0
#endif

/*!
 * Board MCU pins definitions
 */
#define RADIO_RESET                                 15

#define RADIO_MOSI                                  0
#define RADIO_MISO                                  0
#define RADIO_SCLK                                  0

#if defined( SX1261MBXBAS ) || defined( SX1262MBXCAS ) || defined( SX1262MBXDAS )

#define RADIO_NSS                                   0
#define RADIO_BUSY                                  14
#define RADIO_DIO_1                                 27

#define RADIO_ANT_SWITCH_POWER                      0
#define RADIO_FREQ_SEL                              0
#define RADIO_XTAL_SEL                              0
#define RADIO_DEVICE_SEL                            0

#define LED_1                                       0
#define LED_2                                       0

// Debug pins definition.
#define RADIO_DBG_PIN_TX                            0
#define RADIO_DBG_PIN_RX                            0

#elif defined( SX1272MB2DAS) || defined( SX1276MB1LAS ) || defined( SX1276MB1MAS )

#define RADIO_NSS                                   0

#define RADIO_DIO_0                                 28
#define RADIO_DIO_1                                 0
#define RADIO_DIO_2                                 29
#define RADIO_DIO_3                                 0
#define RADIO_DIO_4                                 0
#define RADIO_DIO_5                                 0

#define RADIO_ANT_SWITCH                            31

#define LED_1                                       6
#define LED_2                                       7

// Debug pins definition.
#define RADIO_DBG_PIN_TX                            0
#define RADIO_DBG_PIN_RX                            0
#endif

#define OSC_LSE_IN                                  0
#define OSC_LSE_OUT                                 0

#define OSC_HSE_IN                                  0
#define OSC_HSE_OUT                                 0

#define SWCLK                                       0
#define SWDAT                                       0

#define I2C_SCL                                     0
#define I2C_SDA                                     0

#define UART_TX                                     0
#define UART_RX                                     0

#ifdef __cplusplus
}
#endif

#endif // __BOARD_CONFIG_H__
