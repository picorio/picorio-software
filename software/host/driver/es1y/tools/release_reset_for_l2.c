#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include "station_cache.h"
#include "pcie_driver.h"

int main() {
  uint64_t rdata;
  fprintf(stderr, "Start releasing reset for L2\n");
  do_write(STATION_CACHE_S2B_RSTN_ADDR_0, 1);
  do_write(STATION_CACHE_S2B_RSTN_ADDR_1, 1);
  do_write(STATION_CACHE_S2B_RSTN_ADDR_2, 1);
  do_write(STATION_CACHE_S2B_RSTN_ADDR_3, 1);
  while (rdata != 1) {
    rdata = do_read(STATION_CACHE_S2B_RSTN_ADDR_3);
  }
  fprintf(stderr, "Done Releasing Reset for L2\n");
}
