#ifndef HAL_INCLUDE_UTIL_H
#define HAL_INCLUDE_UTIL_H

#include "common.h"
#include "clib.h"

extern int rvhal_log_mask;
extern int rvhal_log_level;
extern int rvhal_dump_hex_on;

#define isprint(c) (((unsigned int)((c) - 0x20)) <= (0x7e - 0x20))

enum {
	RVHAL_FAIL = 0,
	RVHAL_OK,	
};

enum {
	RVHAL_ERR_NO = 0,
	RVHAL_ERR_SYS_PARAM_INVALID,
	RVHAL_ERR_SYS_DEV_BUSY,

	RVHAL_ERR_XXX_YYY_ZZZ, // TODO: XXX function, e.g. SPI
};

#define LOG_EMERG   0  /**< system is unusable */
#define LOG_ALERT   1  /**< action must be taken immediately */
#define LOG_CRIT    2  /**< critical condition */
#define LOG_ERR     3  /**< error conditions */
#define LOG_WARNING 4  /**< warning conditions */
#define LOG_NOTICE  5  /**< normal but significant condition */
#define LOG_INFO    6  /**< informational */
#define LOG_DEBUG   7  /**< debug-level messages */

#define RVHAL_LOG_COMP_ALL              0xFFFFFFFF
#define RVHAL_LOG_COMP_HAL              BIT(0)
#define RVHAL_LOG_COMP_OS            	BIT(1)
#define RVHAL_LOG_COMP_LORA            	BIT(2)

#define RVHAL_DUMP_HEX_ON_DEFAULT		0
#if 0
#define RVHAL_LOG_LEVEL_DEFAULT			LOG_DEBUG
#else
#define RVHAL_LOG_LEVEL_DEFAULT			LOG_INFO
#endif
#define RVHAL_LOG_MASK_DEFAULT			RVHAL_LOG_COMP_ALL

#if 1 // TODO:
/*!
 * @discussion dump buffer by hex and ascii format.
 * @param buf buffer pointer.
 * @param size buffer size.
 * @param description string without format.
 */
void rvHal_dump_hex ( unsigned char* buf, unsigned int size, char *description, ... );

/*!
 * @discussion assert API, print wrong messages and hang when fail.
 * @param file file name.
 * @param line line number.
 */
void rvHal_assert_failed(unsigned char *file, unsigned int line);
#define RVHAL_ASSERT_PARAM(expr) ((expr) ? (void)0U : rvHal_assert_failed((unsigned char *)__FILE__, __LINE__))

#define RVHAL_TRACE(fmt, a...) RVHAL_LOG(RVHAL_LOG_COMP_ALL, LOG_INFO, "[%9d]  " fmt, rvHal_get_uptime(), ##a)

#define RVHAL_LOG(Comp, Lvl, Fmt, Arg...)                               \
    do {                                                                \
        if (((Comp) & rvhal_log_mask) && ((Lvl) <= rvhal_log_level)) {  \
			printf(Fmt, ##Arg);	\
        }                                                               \
    } while (0)

#define RVHAL_DUMP_HEX(buf, n, Fmt, Arg...)                               \
    do {                                                                \
        if (rvhal_dump_hex_on) {  \
			rvHal_dump_hex(buf, n, Fmt, ##Arg);	\
        }                                                               \
    } while (0)
#else
#define RVHAL_ASSERT_PARAM(expr) ((void)0U)
#define RVHAL_LOG(Comp, Lvl, Fmt, Arg...)
#endif /* USE_FULL_ASSERT */


#define RVHAL_LOG_ALWAYS(Fmt, Arg...)                       \
    RVHAL_LOG(rvhal_log_mask, rvhal_log_level, Fmt, ##Arg)

#if 1
#define RVHAL_LOG_EMERG(Comp, Fmt, Arg...)      \
    RVHAL_LOG(Comp, LOG_EMERG, Fmt, ##Arg)
#define RVHAL_LOG_ALERT(Comp, Fmt, Arg...)      \
    RVHAL_LOG(Comp, LOG_ALERT, Fmt, ##Arg)
#define RVHAL_LOG_CRIT(Comp, Fmt, Arg...)       \
    RVHAL_LOG(Comp, LOG_CRIT, Fmt, ##Arg)
#define RVHAL_LOG_ERR(Comp, Fmt, Arg...)        \
    RVHAL_LOG(Comp, LOG_ERR, Fmt, ##Arg)
#define RVHAL_LOG_WARNING(Comp, Fmt, Arg...)    \
    RVHAL_LOG(Comp, LOG_WARNING, Fmt, ##Arg)
#define RVHAL_LOG_NOTICE(Comp, Fmt, Arg...)     \
    RVHAL_LOG(Comp, LOG_NOTICE, Fmt, ##Arg)
#define RVHAL_LOG_INFO(Comp, Fmt, Arg...)       \
    RVHAL_LOG(Comp, LOG_INFO, Fmt, ##Arg)
#define RVHAL_LOG_DEBUG(Comp, Fmt, Arg...)      \
    RVHAL_LOG(Comp, LOG_DEBUG, Fmt, ##Arg)
#endif

#define RVHAL_HAL_LOG_ERR(Fmt, Arg...)        \
    RVHAL_LOG(RVHAL_LOG_COMP_HAL, LOG_ERR, Fmt, ##Arg)
#define RVHAL_HAL_LOG_INFO(Fmt, Arg...)      \
    RVHAL_LOG(RVHAL_LOG_COMP_HAL, LOG_INFO, Fmt, ##Arg)
#define RVHAL_HAL_LOG_DEBUG(Fmt, Arg...)      \
    RVHAL_LOG(RVHAL_LOG_COMP_HAL, LOG_DEBUG, Fmt, ##Arg)

#define RVHAL_OS_LOG_ERR(Fmt, Arg...)        \
    RVHAL_LOG(RVHAL_LOG_COMP_OS, LOG_ERR, Fmt, ##Arg)
#define RVHAL_OS_LOG_INFO(Fmt, Arg...)      \
    RVHAL_LOG(RVHAL_LOG_COMP_OS, LOG_INFO, Fmt, ##Arg)
#define RVHAL_OS_LOG_DEBUG(Fmt, Arg...)      \
    RVHAL_LOG(RVHAL_LOG_COMP_OS, LOG_DEBUG, Fmt, ##Arg)

#define RVHAL_LORA_LOG_ERR(Fmt, Arg...)        \
    RVHAL_LOG(RVHAL_LOG_COMP_LORA, LOG_ERR, Fmt, ##Arg)
#define RVHAL_LORA_LOG_INFO(Fmt, Arg...)      \
    RVHAL_LOG(RVHAL_LOG_COMP_LORA, LOG_INFO, Fmt, ##Arg)
#define RVHAL_LORA_LOG_DEBUG(Fmt, Arg...)      \
    RVHAL_LOG(RVHAL_LOG_COMP_LORA, LOG_DEBUG, Fmt, ##Arg)

/*!
 * @discussion set log mask.
 * @param mask mask for each modules.
 */
void rvHal_set_log_mask(int mask);

/*!
 * @discussion set log level.
 * @param level log level.
 */
void rvHal_set_log_level(int level);
#endif /* HAL_INCLUDE_UTIL_H */

