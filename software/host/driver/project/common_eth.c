#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include "socket_client.c"

uint64_t do_read(uint64_t addr) {
  uint64_t ack;

  fprintf(stdout,"do_read: Sending Addr: 0x%" PRIx64 "\n",addr);
  send_cmd_eth("8081",addr,&ack);
  if(ack!= 0x0000000000000001) {
   fprintf(stderr,"do_read: Send Rd failed!\n");
   exit(EXIT_FAILURE);
  }
  return ack;
}

void do_write(uint64_t addr, uint64_t data) {
  uint64_t ack1;
  uint64_t ack2;
  fprintf(stdout,"do_write: Sending Addr: 0x%" PRIx64 "\n",addr);
  send_cmd_eth("8081",addr,&ack1);
  if(ack1 == 0x0000000000000001) {
    fprintf(stdout,"do_write: Sending Data: 0x%" PRIx64 "\n",data);
    send_cmd_eth("8081",data,&ack2);
    if(ack2 != 0x0000000000000001) {
      fprintf(stderr,"do_write: Send Data failed!\n");
      exit(EXIT_FAILURE);
    }
  }
  else {
    fprintf(stderr,"do_write: Send Addr failed! Got: 0x%" PRIx64 "\n",ack2);
    exit(EXIT_FAILURE);
  }
}

