// for uint*_t data types
#include <stdint.h>
// for print statements
#include <inttypes.h>
#include <stdio.h>
// for ctrl-c interrupt handling
#include <csignal>
// write to files
#include <cstdio>

#include "pcie_driver.h"
#include "station_vp.h"

#define WORD_SIZE AXI_DATA_BUS_WIDTH_IN_BYTES //size in bytes
#define MEMORY_SIZE 0x80000 // 512kb bram
#define NUM_ADDRESSES MEMORY_SIZE/WORD_SIZE

#define DEBUG

using namespace std;

int main(int argc, char* argv[]) {
  uint64_t pc;
  char * p;
  if (argc > 1)
    pc = strtoul(argv[1], & p, 0);
  else
    pc = 0x80000000;

  printf("Settint Reset PC to %lx...\n", pc);
  do_write(STATION_VP_S2B_CFG_RST_PC_ADDR_0, pc);
}

