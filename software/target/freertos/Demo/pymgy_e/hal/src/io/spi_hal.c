unsigned int rvHal_spim_rw(struct spim_cfg_t *cfg, unsigned int data)
{
	RVHAL_ASSERT_PARAM((cfg) && SPIM_VALID_NUM(cfg->num));

	unsigned int reg = spi_dev.spim[cfg->num].reg;

	while ( ( read32(reg + SPIM_SR_ADDR) & BIT(2)) == 0x00 );

	write32(reg + SPIM_DR0_ADDR, data);

	while ( (read32(reg + SPIM_SR_ADDR) & BIT(3)) == 0x00 );

	return read32(reg + SPIM_DR0_ADDR);
}

struct spi_dev_t {
	struct spim_dev_t spim[NUM_SPIM_MAX];
};

struct spim_cfg_t {
	unsigned char	num;
	unsigned char	dfs;
	unsigned short	sckdv;
};

struct spim_ops_t {
	void (*pinctrl)(int);
};

struct spim_dev_t {
	unsigned int reg;
	struct spim_cfg_t *cfg;
	struct spim_ops_t *ops;
};

static struct spi_dev_t spi_dev;

static int spim_register_param_validate(struct spim_cfg_t *cfg)
{
	int ret = RVHAL_ERR_SYS_PARAM_INVALID;

	if (NULL == cfg) {
		RVHAL_LOG_ERR("NULL spim cfg\n");
	} else if (!SPIM_VALID_NUM(cfg->num)) {
		RVHAL_LOG_ERR("invalid spim num: %u\n", cfg->num);
	} else { // TODO: others...
		ret = RVHAL_ERR_NO;
	}

	return ret;
}

static int spim_hw_register(struct spim_dev_t *dev)
{
	// enable clk: STATION_SLOW_IO_S2B_SSPIM1_SSI_CLK_EN_ADDR

}

static int spim_hw_unregister(struct spim_dev_t *dev)
{
	// disable clk: STATION_SLOW_IO_S2B_SSPIM1_SSI_CLK_EN_ADDR
}

int rvHal_spim_register(struct spim_cfg_t *cfg)
{ 
	int ret;
	unsigned char num;
	struct spim_dev_t *dev;

	ret = spim_register_param_validate(cfg);
	if (ret)
		return ret;

	num = cfg->num;
	dev = &(spi_dev.spim[num]);

	RVHAL_ENTER_CS();

	if (dev->cfg) {
		RVHAL_LOG_WARNING("spim%d is busy\n", num);
		return RVHAL_ERR_SYS_DEV_BUSY;
	}
	dev->cfg = cfg;

	spim_hw_register(dev);

	RVHAL_EXIT_CS();

	return RVHAL_ERR_NO;
}

void rvHal_spim_unregister(struct spim_cfg_t *cfg)
{ 
	struct spim_dev_t *dev;

	RVHAL_ASSERT_PARAM((cfg) && SPIM_VALID_NUM(cfg->num));

	dev = &(spi_dev.spim[cfg->num]);

	RVHAL_ENTER_CS();

	spim_hw_unregister(dev);

	dev->cfg = NULL;

	RVHAL_EXIT_CS();
}

void __rvHal_spi_init(void)
{ 
	memset(&spi_dev, 0, sizeof(spi_dev));	
}
