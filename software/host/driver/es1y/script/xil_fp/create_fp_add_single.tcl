##################################################################
# CREATE IP XIL_fp_add_single
##################################################################

set floating_point XIL_fp_add_single
create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name $floating_point

set_property -dict { 
  CONFIG.Add_Sub_Value {Add}
  CONFIG.Flow_Control {NonBlocking}
  CONFIG.Has_RESULT_TREADY {false}
  CONFIG.C_Latency {11}
  CONFIG.Has_ACLKEN {true}
  CONFIG.Has_ARESETn {true}
  CONFIG.C_Has_UNDERFLOW {true}
  CONFIG.C_Has_OVERFLOW {true}
  CONFIG.C_Has_INVALID_OP {true}
} [get_ips $floating_point]

##################################################################

