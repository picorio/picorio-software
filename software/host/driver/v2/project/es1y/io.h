#ifndef _PROJECT_PYGMYE_IO_H
#define _PROJECT_PYGMYE_IO_H

#if 0
#define offsetof(TYPE, MEMBER) ((size_t) &((TYPE *)0)->MEMBER)
#define container_of(ptr, type, member) ({			\
	const typeof( ((type *)0)->member ) *__mptr = (ptr);	\
	(type *)( (char *)__mptr - offsetof(type,member) );})
#else
#define container_of(ptr, type, member) (ptr)
#endif

#define io_get_priv_channel(io)	((io)->priv)

struct io_ops_t {
	int (*parse_args)(struct io_channel_t *io, int argc, char const **argv);
	int (*setup)(struct io_channel_t *io);
	void (*reset)(struct io_channel_t *io);
	int (*do_status_check)(struct io_channel_t *io);
	void (*do_write)(struct io_channel_t *io, uint64_t addr, uint64_t data);
	void (*do_write_burst)(struct io_channel_t *io, uint64_t addr, uint64_t* data, int len);
	uint64_t (*do_read)(struct io_channel_t *io, uint64_t addr);
	void (*close)(struct io_channel_t *io);
};

struct io_channel_t {
	struct io_ops_t	*ops;
	void *priv;
};

int io_parse_args(struct io_channel_t *io, int argc, char const **argv);
int io_setup(struct io_channel_t *io);
void io_reset(struct io_channel_t *io);
int io_do_status_check(struct io_channel_t *io);
void io_do_write(struct io_channel_t *io, uint64_t addr, uint64_t data);
void io_do_write_burst(struct io_channel_t *io, uint64_t addr, uint64_t* data, int len);
uint64_t io_do_read(struct io_channel_t *io, uint64_t addr);
void io_close(struct io_channel_t *io);

extern struct io_channel_t * io_open(void);
#endif
