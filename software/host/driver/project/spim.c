//#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>

#include "ftd2xx.h"
#include "libft4222.h"
#include "common.h"
std::vector< FT_DEVICE_LIST_INFO_NODE > g_FT4222DevList;

int main (int argc, char const *argv[])
{
  int doSingle = 1;
  uint8 *  readBuffer;
  uint8 *  writeBuffer;
  uint8    singleWriteBytes = 1;
  uint16   multiWriteBytes = 13;
  uint16   multiReadBytes = 8;
  uint32 * sizeOfRead;
  std::string status;

  uint16   sizeToTransfer = 23;
  uint16 * sizeTransferred;
  BOOL     isEndTransaction;

  FT_STATUS ftStatus;
  FT_HANDLE ftHandle;

  if (doSingle == 0)
    ftHandle = setup(SPI_IO_DUAL, CLK_DIV_16,
                                     CLK_IDLE_LOW, CLK_LEADING, 0);
  else
    ftHandle = setup(SPI_IO_SINGLE, CLK_DIV_16,
                                     CLK_IDLE_LOW, CLK_LEADING, 0);

  printf("doSingle = %0d!\n", doSingle);

  if (doSingle == 0) {
  
    writeBuffer = (uint8 *) malloc (100);
    readBuffer = (uint8 *) malloc (100);
    sizeOfRead = (uint32 *) malloc (1);
    multiWriteBytes = 0;
    multiReadBytes = 1;
  
    writeBuffer[0] = 0xaa;
  
    //while (1) {
      ftStatus = FT4222_SPIMaster_MultiReadWrite(ftHandle, readBuffer,
                                                 writeBuffer, singleWriteBytes,
                                                 multiWriteBytes, multiReadBytes,
                                                 sizeOfRead);
      printf("STATUS readBuffer[0] = %x\n", readBuffer[0]);
    //}

    writeBuffer[0] = 0x04;
    writeBuffer[1] = 0x00;
    writeBuffer[2] = 0x00;
    writeBuffer[3] = 0x00;
    writeBuffer[4] = 0x00;
    writeBuffer[5] = 0x00;
    writeBuffer[6] = 0x11;
    writeBuffer[7] = 0x22;
    writeBuffer[8] = 0x33;
    writeBuffer[9] = 0x44;
    writeBuffer[10] = 0x55;
    writeBuffer[11] = 0x66;
    writeBuffer[12] = 0x77;
    writeBuffer[13] = 0x88;

    multiWriteBytes = 13;
    multiReadBytes = 0;

    for (int i = 0; i < 100; i ++)
      readBuffer[i] = 0;
//    while (1) {
      ftStatus = FT4222_SPIMaster_MultiReadWrite(ftHandle, readBuffer,
                                                 writeBuffer, singleWriteBytes,
                                                 multiWriteBytes, multiReadBytes,
                                                 sizeOfRead);
//    }
    for (int i = 0; i < 9; i ++)
      printf("WRITE ADDR %0d readBuffer[%0d] = %x\n", i, i, readBuffer[i]);

    writeBuffer[0] = 0x05;
    writeBuffer[1] = 0x80;
    writeBuffer[2] = 0x00;
    writeBuffer[3] = 0x00;
    writeBuffer[4] = 0x00;
    writeBuffer[5] = 0x00;
    writeBuffer[6] = 0x48;

    multiWriteBytes = 6;
    multiReadBytes = 9;

    for (int i = 0; i < 100; i ++)
      readBuffer[i] = 0;
//    while (1) {
      ftStatus = FT4222_SPIMaster_MultiReadWrite(ftHandle, readBuffer,
                                                 writeBuffer, singleWriteBytes,
                                                 multiWriteBytes, multiReadBytes,
                                                 sizeOfRead);
//    }
    for (int i = 0; i < 9; i ++)
      printf("READ ADDR %0d readBuffer[%0d] = %x\n", i, i, readBuffer[i]);
}
  else {
    writeBuffer = (uint8 *) malloc (100);
    readBuffer = (uint8 *) malloc (100);
    sizeTransferred = (uint16 *) malloc (1);
    * sizeTransferred = 0;
    isEndTransaction = 1;
  
//    writeBuffer[0] = 0x04;
//    writeBuffer[1] = 0x00;
//    writeBuffer[2] = 0x11;
//    writeBuffer[3] = 0x22;
//    writeBuffer[4] = 0x33;
//    writeBuffer[5] = 0x88;
//    writeBuffer[6] = 0x55;
//    writeBuffer[7] = 0x55;
//    writeBuffer[8] = 0x88;
//    writeBuffer[9] = 0x88;
//    writeBuffer[10] = 0x55;
//    writeBuffer[11] = 0x55;
//    writeBuffer[12] = 0x88;
//    writeBuffer[13] = 0x88;
//  
//    while (1) {
//      ftStatus = FT4222_SPIMaster_SingleReadWrite(ftHandle, readBuffer,
//                                                  writeBuffer, sizeToTransfer,
//                                                  sizeTransferred, isEndTransaction);
//    }
//    writeBuffer[0] = 0x05;
//    writeBuffer[1] = 0x80;
//    writeBuffer[2] = 0x00;
//    writeBuffer[3] = 0x00;
//    writeBuffer[4] = 0x00;
//    writeBuffer[5] = 0x80;
//    writeBuffer[6] = 0x48;
//
//    sizeToTransfer = 1 + 5 + 1 + 9;
//    
//
//    for (int i = 0; i < 100; i ++)
//      readBuffer[i] = 0;
////    while (1) {
//      ftStatus = FT4222_SPIMaster_SingleReadWrite(ftHandle, readBuffer,
//                                                  writeBuffer, sizeToTransfer,
//                                                  sizeTransferred, isEndTransaction);
////    }
//    for (int i = 0; i < 16; i ++)
//      printf("READ ADDR %0d readBuffer[%0d] = %x\n", i, i, readBuffer[i]);


    writeBuffer[0] = 0xaa;
    readBuffer[1] = 0xaa;
    sizeToTransfer = 1 + 1;
    int cnt = 0;
    //while (1) {
      printf("Before\n");
      ftStatus = FT4222_SPIMaster_SingleReadWrite(ftHandle, readBuffer,
                                                  writeBuffer, sizeToTransfer,
                                                  sizeTransferred, isEndTransaction);
        printf("cnt = %0d, writeBuffer[0] = %x, readBuffer[1] = %x\n", cnt, writeBuffer[0], readBuffer[1]);
        cnt ++;
//      writeBuffer[0] ++;
    //}
  }

  if (FT_OK != ftStatus)
  {
    printf("MultiReadWrite failed!, ftStatus = %d\n", ftStatus);
    return 0;
  }

  FT4222_UnInitialize(ftHandle);
  FT_Close(ftHandle);
  return 0;
}
