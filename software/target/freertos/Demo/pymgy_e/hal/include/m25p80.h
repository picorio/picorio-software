/* RIVAI: GPL-2.0+ */

#ifndef __M25P80_H
#define __M25P80_H

/**
 * @discussion: Erase an address range on the nor chip.  The address range may extend
 * one or more erase sectors.
 * @param addr: the start address of the address range.
 * @param len: the len of the address range.
 * @return Return an non-zero error if there is a problem erasing.
 */
int m25p80_spi_nor_erase(u32 addr, u32 len);

/**
 * @discussion: Write an address range to the nor chip.
 * The address range may be any size provided
 * it is within the physical boundaries.
 * @param to: the nor chip destination's start address of the address range.
 * @param len: the nor chip destination's length of the address range.
 * @param buf: the src's start address.
 * @return: Return an non-zero error code if there is a problem when writing.
 */
int m25p80_spi_nor_write(loff_t to, size_t len,
	size_t *retlen, const u_char *buf);

/**
 * @discussion: read an address range from the nor chip.
 * The address range may be any size provided
 * it is within the physical boundaries.
 * @param from: the nor chip source's start address of the address range.
 * @param len: the nor chip source's length of the address range.
 * @param buf: the destination's start address.
 * @return: Return an non-zero error code if there is a problem when reading.
 */
int m25p80_spi_nor_read(loff_t from, size_t len,
			size_t *retlen, u_char *buf);

/**
 * @discussion: init dw spi controller and init m25p80 nor flash chip.
 * including: spi clk, spi io, m25p80 device;
 */
void m25p80_spi_nor_init(void);
#endif
