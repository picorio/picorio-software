// for uint*_t data types
#include <stdint.h>

#include "pcie_driver.c"
int main()
{
    uint64_t addr   = 0x200;
    uint64_t data   = 0xb0b0;
    uint64_t chksum = 0xCDCDCDCDCDCDCDCD;
    uint64_t seq    = 0xEFEFEFEFEFEFEFEF;
    uint64_t dataArr[8] = {0xf415ed474, addr, chksum, seq, 0, 0, 0, 0};
    uint8_t * buf = (uint8_t *) malloc (sizeof(uint8_t) * AXI_DATA_BUS_WIDTH_IN_BYTES);

    dataArrToBuffer(buf, dataArr, 8);
    pcie_read(addr);
//  do_read((uint64_t) 0x200);
}
