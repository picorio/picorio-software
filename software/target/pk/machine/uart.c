#include <string.h>
#include "uart.h"
#include "fdt.h"

#define UART_DLL_ADDR (0x0)
#define UART_THR_ADDR (0x0)
#define UART_RBR_ADDR (0x0)
#define UART_IER_ADDR (0x4)
#define UART_DLH_ADDR (0x4)
#define UART_FCR_ADDR (0x8)
#define UART_LCR_ADDR (0xc)
#define UART_MCR_ADDR (0x10)
#define UART_LSR_ADDR (0x14)

#define UART_LCR_DLAB		0x80 /* Divisor latch access bit */
#define UART_LSR_TEMT		0x40 /* Transmitter empty */
#define UART_LSR_THRE		0x20 /* Transmit-hold-register empty */
#define BOTH_EMPTY (UART_LSR_TEMT | UART_LSR_THRE)

#define DIV_ROUND_CLOSEST(x, divisor)(			\
{							\
	typeof(x) __x = x;				\
	typeof(divisor) __d = divisor;			\
	(((typeof(x))-1) > 0 ||				\
	 ((typeof(divisor))-1) > 0 ||			\
	 (((__x) > 0) == ((__d) > 0))) ?		\
		(((__x) + ((__d) / 2)) / (__d)) :	\
		(((__x) - ((__d) / 2)) / (__d));	\
}							\
)

volatile uint64_t uart;

#if __riscv_xlen == 64
#define write32(addr,value)		(*((volatile unsigned int *)(addr)))=(value)
#define read32(addr)			(*((volatile unsigned int *)(addr)))
#else
static inline void write32(uint32_t addr, uint32_t value)
{
	*((volatile unsigned int *)(addr)) = value;
}
static inline uint32_t read32(uint32_t addr)
{
	return *((volatile unsigned int *)(addr));
}
#endif

static inline uint32_t bswap(uint32_t x)
{
  uint32_t y = (x & 0x00FF00FF) <<  8 | (x & 0xFF00FF00) >>  8;
  uint32_t z = (y & 0x0000FFFF) << 16 | (y & 0xFFFF0000) >> 16;
  return z;
}

static void __uart_putchar(uint8_t ch)
{
  uint64_t reg = uart;
  unsigned int status;

  write32(reg+UART_THR_ADDR, ch);

  for (;;) {
  	status = read32(reg+UART_LSR_ADDR);
  	if ((status & BOTH_EMPTY) == BOTH_EMPTY)
  	  break;
  }
}

void uart_putchar(uint8_t ch)
{
  if ('\n' == ch) 
    __uart_putchar('\r');
  __uart_putchar(ch);
}

int uart_getchar()
{
  return 0;
}

struct uart_scan
{
  int console;
  int compat;
  int uartclk;
  int baudrate;
  uint64_t reg;
};

static void uart_setup(struct uart_scan *scan)
{
  uint64_t reg = scan->reg;
  int uart_clk = scan->uartclk;
  unsigned int divisor;
  unsigned char c;

  write32(reg+UART_LCR_ADDR, 0x3);
  write32(reg+UART_IER_ADDR, 0);
  write32(reg+UART_FCR_ADDR, 0);
  write32(reg+UART_MCR_ADDR, 3);

  divisor = DIV_ROUND_CLOSEST(scan->uartclk, 16 * scan->baudrate);
  c = read32(reg+UART_LCR_ADDR);
  write32(reg+UART_LCR_ADDR, c | UART_LCR_DLAB);
  write32(reg+UART_DLL_ADDR, divisor & 0xff);
  write32(reg+UART_DLH_ADDR, (divisor >> 8) & 0xff);
  write32(reg+UART_LCR_ADDR, c & ~UART_LCR_DLAB);
}

static void uart_open(const struct fdt_scan_node *node, void *extra)
{
  struct uart_scan *scan = (struct uart_scan *)extra;
  memset(scan, 0, sizeof(*scan));
}

static void uart_prop(const struct fdt_scan_prop *prop, void *extra)
{
  struct uart_scan *scan = (struct uart_scan *)extra;

  if (scan->console)
  	return;  

  if (!strcmp(prop->name, "compatible") && !strcmp((const char*)prop->value, "snps,dw-apb-uart")) {
    scan->compat = 1;
  } else if (!strcmp(prop->name, "reg")) {
    fdt_get_address(prop->node->parent, prop->value, &scan->reg);
  } else if (!strcmp(prop->name, "clock-frequency")) {
    scan->uartclk = bswap(prop->value[0]);
  } else if (!strcmp(prop->name, "baud")) {
    scan->baudrate = bswap(prop->value[0]);
  } else if (!strcmp(prop->name, "console")) {
    scan->console = 1;
  }
}

static void uart_done(const struct fdt_scan_node *node, void *extra)
{
  struct uart_scan *scan = (struct uart_scan *)extra;
  if (!scan->compat || !scan->reg || !scan->uartclk || !scan->baudrate || !scan->console || uart) return;

  uart = scan->reg;
  uart_setup(scan);
}

void query_uart(uintptr_t fdt)
{
  struct fdt_cb cb;
  struct uart_scan scan;

  memset(&cb, 0, sizeof(cb));
  cb.open = uart_open;
  cb.prop = uart_prop;
  cb.done = uart_done;
  cb.extra = &scan;

  fdt_scan(fdt, &cb);
}
