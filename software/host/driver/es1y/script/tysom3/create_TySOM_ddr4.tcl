set module TySOM_ddr4_0

create_ip -name ddr4 -vendor xilinx.com -library ip -version 2.2 -module_name  $module

set_property -dict [ list \
 CONFIG.C0.DDR4_AxiAddressWidth {29} \
 CONFIG.C0.DDR4_AxiDataWidth {64} \
 CONFIG.C0.DDR4_AxiIDWidth {8} \
 CONFIG.C0.DDR4_CLKOUT0_DIVIDE {7} \
 CONFIG.C0.DDR4_CasLatency {11} \
 CONFIG.C0.DDR4_CasWriteLatency {9} \
 CONFIG.C0.DDR4_CustomParts {no_file_loaded} \
 CONFIG.C0.DDR4_DataWidth {8} \
 CONFIG.C0.DDR4_InputClockPeriod {5000} \
 CONFIG.C0.DDR4_MemoryPart {MT40A512M8HX-107} \
 CONFIG.C0.DDR4_MemoryType {Components} \
 CONFIG.C0.DDR4_TimePeriod {1250} \
 CONFIG.C0.DDR4_isCustom {false} \
 CONFIG.ADDN_UI_CLKOUT1_FREQ_HZ {25} \
 CONFIG.C0.DDR4_AxiSelection {true} \
] [get_ips $module]
