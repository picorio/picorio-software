#if 1

//#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <unistd.h>

#include "ftd2xx.h"
#include "libft4222.h"
#include "common.h"
std::vector< FT_DEVICE_LIST_INFO_NODE > g_FT4222DevList;

static uint64 spi_do_read(FT4222_SPIMode mode, FT_HANDLE ftHandle, uint64 addr)
{
    uint8 *  readBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint8 *  writeBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint16 * sizeTransferred = (uint16 *) malloc (sizeof(uint16));
    BOOL     isEndTransaction = 0;
    uint16   sizeToTransfer = 16;
    FT_STATUS ftStatus;
    uint8    status = 0;
  int i;
    uint64   ret_data = 0;

    * sizeTransferred = 0;
#if 0
    writeBuffer[ 0] = 0x0; 
    writeBuffer[ 1] = 0x0;
    writeBuffer[ 2] = 0x0; // data7
    writeBuffer[ 3] = 0x0; // data6
    writeBuffer[ 4] = 0x0; // data5
    writeBuffer[ 5] = 0x0; // data4
    writeBuffer[ 6] = 0x0; // data3
    writeBuffer[ 7] = 0x0; // data2
    writeBuffer[ 8] = 0x0; // data1
    writeBuffer[ 9] = 0x0; // data0
#if 0// 1st
    writeBuffer[10] = addr & 0xffUL;//((addr & 0xffffffffffffffff) >>  0) & 0xff;
    writeBuffer[11] = (addr >> 8) & 0xff;
    writeBuffer[12] = (addr >> 16) & 0xff;
    writeBuffer[13] = (addr >> 24) & 0xff;
    writeBuffer[14] = (addr >> 32) & 0xff;
#else // 2nd
    writeBuffer[10] = (addr >> 32) & 0xffUL;//((addr & 0xffffffffffffffff) >>  0) & 0xff;
    writeBuffer[11] = (addr >> 24) & 0xff;
    writeBuffer[12] = (addr >> 16) & 0xff;
    writeBuffer[13] = (addr >> 8) & 0xff;
    writeBuffer[14] = (addr) & 0xff;
#endif
	writeBuffer[15] = 0x1;// Read
#else
    writeBuffer[ 0] = 0x01; // Read
#if 1 // 3rd
    writeBuffer[ 1] = (addr >> 32) & 0xff;
    writeBuffer[ 2] = (addr >> 24) & 0xff;
    writeBuffer[ 3] = (addr >> 16) & 0xff;
    writeBuffer[ 4] = (addr >>  8) & 0xff;
    writeBuffer[ 5] = addr & 0xffUL;//((addr & 0xffffffffffffffff) >>  0) & 0xff;
#else //4 // 4th
    writeBuffer[ 1] = addr & 0xffUL;
    writeBuffer[ 2] = (addr >> 8) & 0xff;
    writeBuffer[ 3] = (addr >> 16) & 0xff;
    writeBuffer[ 4] = (addr >>  24) & 0xff;
    writeBuffer[ 5] = (addr >> 32) & 0xff;//((addr & 0xffffffffffffffff) >>  0) & 0xff;
#endif
    writeBuffer[ 6] = 0x0; // Turn around byte: 1010 0101
    writeBuffer[ 7] = 0x0;
    writeBuffer[ 8] = 0x0;
    writeBuffer[ 9] = 0x0;
    writeBuffer[10] = 0x0;
    writeBuffer[11] = 0x0;
    writeBuffer[12] = 0x0;
    writeBuffer[13] = 0x0;
    writeBuffer[14] = 0x0;
    writeBuffer[15] = 0x0;
#endif

    fprintf (stderr, "Try Reading address 0x%llx\n", addr);
    *sizeTransferred = 0;
    for (i = 0; i < 16; i++)
      readBuffer[i] = 0;

    ftStatus = FT4222_SPIMaster_SingleReadWrite(ftHandle, readBuffer,
                                                writeBuffer, sizeToTransfer,
                                                sizeTransferred, isEndTransaction);
    if (FT_OK != ftStatus) {
      printf("do_read failed!\n");
      return -1;
    } else {
      for (i = 0; i < 16; i++) {
        printf("read 0x%2d: 0x%2x\n", i, readBuffer[i]);
      }
      printf("\n");
    }

#if 1
#if 1
    sizeToTransfer = 1;
    writeBuffer[0] = 0x00;

    for (i = 0; i < 16; i++)
      readBuffer[i] = 0;

    while (readBuffer[0] == 0) {
      ftStatus = FT4222_SPIMaster_SingleReadWrite(ftHandle, readBuffer,
                                                  writeBuffer, sizeToTransfer,
                                                  sizeTransferred, isEndTransaction);
      if (FT_OK != ftStatus) {
        printf("do_read failed!\n");
        return -1;
      } else {
//        printf("read in loop 0x%2d: 0x%2x\n", i, readBuffer[0]);
//	sleep(1);
      }
    }
printf("read out loop 0x%2d: 0x%2x\n", 0, readBuffer[0]);
#endif
    sizeToTransfer = 15;
    isEndTransaction = 1;
    writeBuffer[ 0] = 0x00;
    writeBuffer[ 1] = 0x00;
    writeBuffer[ 2] = 0x00;
    writeBuffer[ 3] = 0x00;
    writeBuffer[ 4] = 0x00;
    writeBuffer[ 5] = 0x00;
    writeBuffer[ 6] = 0x00;
    writeBuffer[ 7] = 0x00;
    writeBuffer[ 8] = 0x00;
    writeBuffer[ 9] = 0x00;
    writeBuffer[10] = 0x00;
    writeBuffer[11] = 0x00;
    writeBuffer[12] = 0x00;
    writeBuffer[13] = 0x00;
    writeBuffer[14] = 0x00;
    writeBuffer[15] = 0x00;

    *sizeTransferred = 0;
    for (i = 0; i < 16; i++)
      readBuffer[i] = 0;

    ftStatus = FT4222_SPIMaster_SingleReadWrite(ftHandle, readBuffer,
                                                writeBuffer, sizeToTransfer,
                                                sizeTransferred, isEndTransaction);
    if (FT_OK != ftStatus) {
      printf("do_read failed!\n");
      return -1;
    } else {
      for (i = 0; i < 16; i++) {
        printf("read2 0x%2d: 0x%2x\n", i, readBuffer[i]);
      }
      printf("\n");
    }
#endif
    for (i = 0; i < 8; i++) {
      ret_data = ret_data << 8;
      ret_data = ret_data | readBuffer[i];
    }

    free(sizeTransferred);
    free(writeBuffer);
    free(readBuffer);
    fprintf (stderr, "Done Reading address 0x%010llx, Got data 0x%016llx\n", addr, ret_data);
    return ret_data;
}

#else

//#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <unistd.h>

#include "ftd2xx.h"
#include "libft4222.h"
#include "common.h"
std::vector< FT_DEVICE_LIST_INFO_NODE > g_FT4222DevList;

static uint64 spi_do_read(FT4222_SPIMode mode, FT_HANDLE ftHandle, uint64 addr)
{
    uint8 *  readBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint8 *  writeBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint16 * sizeTransferred = (uint16 *) malloc (sizeof(uint16));
    BOOL     isEndTransaction = 1;
    uint16   sizeToTransfer = 32;
    FT_STATUS ftStatus;
    uint8    status = 0;
	int i;
    uint64   ret_data = 0;

	memset(readBuffer, 0, 100);
	memset(writeBuffer, 0, 100);

    * sizeTransferred = 0;
#if 0
    writeBuffer[ 0] = 0x02; // write
    writeBuffer[ 1] = (addr >> 32) & 0xff;
    writeBuffer[ 2] = (addr >> 24) & 0xff;
    writeBuffer[ 3] = (addr >> 16) & 0xff;
    writeBuffer[ 4] = (addr >>  8) & 0xff;
    writeBuffer[ 5] = addr & 0xffUL;//((addr & 0xffffffffffffffff) >>  0) & 0xff;
    writeBuffer[ 6] = 0x12; // Turn around byte: 1010 0101
    writeBuffer[ 7] = 0x34;
    writeBuffer[ 8] = 0x56;
    writeBuffer[ 9] = 0x78;
    writeBuffer[10] = 0xa1;
    writeBuffer[11] = 0xb2;
    writeBuffer[12] = 0xc3;
    writeBuffer[13] = 0xd4;
    writeBuffer[14] = 0x0;
    writeBuffer[15] = 0x0;

    ftStatus = FT4222_SPIMaster_SingleReadWrite(ftHandle, readBuffer,
                                                writeBuffer, sizeToTransfer,
                                                sizeTransferred, isEndTransaction);

      if (FT_OK != ftStatus) {
        printf("do_read failed!\n");
		return 0;
      }
	  sleep(1);
#endif
#if 0
	for (i=0; i<32; i++) {
		writeBuffer[i] = 0xa0+i;
	}

#else
#if 0
    writeBuffer[ 0] = 0x00; 
    writeBuffer[ 1] = 0;
    writeBuffer[ 2] = 0;
    writeBuffer[ 3] = 0;
    writeBuffer[ 4] = 0;
    writeBuffer[ 5] = 0;
    writeBuffer[ 6] = 0x0; // Turn around byte: 1010 0101
    writeBuffer[ 7] = 0x0;
    writeBuffer[ 8] = 0x0;
    writeBuffer[ 9] = 0x0;
#if 0
#if 1
    writeBuffer[10] = addr & 0xffUL;//((addr & 0xffffffffffffffff) >>  0) & 0xff;
    writeBuffer[11] = (addr >> 8) & 0xff;
    writeBuffer[12] = (addr >> 16) & 0xff;
    writeBuffer[13] = (addr >> 24) & 0xff;
    writeBuffer[14] = (addr >> 32) & 0xff;
#else
    writeBuffer[10] = (addr >> 32) & 0xffUL;//((addr & 0xffffffffffffffff) >>  0) & 0xff;
    writeBuffer[11] = (addr >> 24) & 0xff;
    writeBuffer[12] = (addr >> 16) & 0xff;
    writeBuffer[13] = (addr >> 8) & 0xff;
    writeBuffer[14] = (addr) & 0xff;
#endif
    writeBuffer[15] = 0x1;// Read
#else
#if 0
    writeBuffer[26] = addr & 0xffUL;//((addr & 0xffffffffffffffff) >>  0) & 0xff;
    writeBuffer[27] = (addr >> 8) & 0xff;
    writeBuffer[28] = (addr >> 16) & 0xff;
    writeBuffer[29] = (addr >> 24) & 0xff;
    writeBuffer[30] = (addr >> 32) & 0xff;
#else
    writeBuffer[26] = (addr >> 32) & 0xffUL;//((addr & 0xffffffffffffffff) >>  0) & 0xff;
    writeBuffer[27] = (addr >> 24) & 0xff;
    writeBuffer[28] = (addr >> 16) & 0xff;
    writeBuffer[29] = (addr >> 8) & 0xff;
    writeBuffer[30] = (addr) & 0xff;
#endif
    writeBuffer[31] = 0x1;// Read
#endif

#else
    writeBuffer[ 0] = 0x01; // Read
#if 1
    writeBuffer[ 1] = (addr >> 32) & 0xff;
    writeBuffer[ 2] = (addr >> 24) & 0xff;
    writeBuffer[ 3] = (addr >> 16) & 0xff;
    writeBuffer[ 4] = (addr >>  8) & 0xff;
    writeBuffer[ 5] = addr & 0xffUL;//((addr & 0xffffffffffffffff) >>  0) & 0xff;
#else
    writeBuffer[ 1] = addr & 0xffUL;
    writeBuffer[ 2] = (addr >> 8) & 0xff;
    writeBuffer[ 3] = (addr >> 16) & 0xff;
    writeBuffer[ 4] = (addr >>  24) & 0xff;
    writeBuffer[ 5] = (addr >> 32) & 0xff;//((addr & 0xffffffffffffffff) >>  0) & 0xff;
#endif
    writeBuffer[ 6] = 0x0; // Turn around byte: 1010 0101
    writeBuffer[ 7] = 0x0;
    writeBuffer[ 8] = 0x0;
    writeBuffer[ 9] = 0x0;
    writeBuffer[10] = 0x0;
    writeBuffer[11] = 0x0;
    writeBuffer[12] = 0x0;
    writeBuffer[13] = 0x0;
    writeBuffer[14] = 0x0;
    writeBuffer[15] = 0x0;
#endif
#endif
//    do {
      //fprintf (stderr, "Try Reading address 0x%llx\n", addr);
      *sizeTransferred = 0;

do {
      ftStatus = FT4222_SPIMaster_SingleReadWrite(ftHandle, readBuffer,
                                                  writeBuffer, sizeToTransfer,
                                                  sizeTransferred, isEndTransaction);
      if (FT_OK != ftStatus) {
        printf("do_read failed!\n");
      } else {
#if 1
		for (i = 0; i < 32; i++) {
			printf("read 0x%2d: 0x%2x\n", i, readBuffer[i]);
		}
		printf("\n");
#endif
      }
//	sleep(1);
} while(0);
while(1) {	sleep(2);};
      // for (int i = 7; i < 16; i ++)
      //   printf("readBuffer[%d] = 0x%x\n", i, readBuffer[i]);
      // printf("readBuffer[15] = 0x%x\n", readBuffer[15]);
//    } while (0);

    for (i = 0; i < 8; i++) {
      ret_data = ret_data << 8;
      ret_data = ret_data | readBuffer[i + 7];
    }

	free(sizeTransferred);
	free(writeBuffer);
	free(readBuffer);
    //fprintf (stderr, "Done Reading address 0x%010llx, Got data 0x%016llx\n", addr, ret_data);
    return ret_data;
}

#endif

#if 0 // good seq. for write
    writeBuffer[ 0] = 0x0; 
    writeBuffer[ 1] = 0x0;
    writeBuffer[ 2] = 0x10; // data7
    writeBuffer[ 3] = 0x20; // data6
    writeBuffer[ 4] = 0x30; // data5
    writeBuffer[ 5] = 0x40; // data4
    writeBuffer[ 6] = 0x50; // data3
    writeBuffer[ 7] = 0x60; // data2
    writeBuffer[ 8] = 0x70; // data1
    writeBuffer[ 9] = 0x80; // data0
    writeBuffer[10] = (addr >> 32) & 0xffUL;//((addr & 0xffffffffffffffff) >>  0) & 0xff;
    writeBuffer[11] = (addr >> 24) & 0xff;
    writeBuffer[12] = (addr >> 16) & 0xff;
    writeBuffer[13] = (addr >> 8) & 0xff;
    writeBuffer[14] = (addr) & 0xff;
	writeBuffer[15] = 0x2;// Write
#endif

static uint64 spi_do_read2(FT4222_SPIMode mode, FT_HANDLE ftHandle, uint64 addr)
{
    uint8 *  readBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint8 *  writeBuffer = (uint8 *) malloc (sizeof(uint8) * 100);
    uint16 * sizeTransferred = (uint16 *) malloc (sizeof(uint16));
    BOOL     isEndTransaction = 0;
    uint16   sizeToTransfer = 16;
    FT_STATUS ftStatus;
    uint8    status = 0;
  int i;
    uint64   ret_data = 0;

    *sizeTransferred = 0;

	memset(readBuffer, 0, 100);
	memset(writeBuffer, 0, 100);
#if 1
#if 1
    writeBuffer[ 0] = 0x0; 
    writeBuffer[ 1] = 0x0;
    writeBuffer[ 2] = 0x0; // data7
    writeBuffer[ 3] = 0x0; // data6
    writeBuffer[ 4] = 0x0; // data5
    writeBuffer[ 5] = 0x0; // data4
    writeBuffer[ 6] = 0x0; // data3
    writeBuffer[ 7] = 0x0; // data2
    writeBuffer[ 8] = 0x0; // data1
    writeBuffer[ 9] = 0x0; // data0
#if 1
#if 0// 1st
    writeBuffer[10] = addr & 0xffUL;//((addr & 0xffffffffffffffff) >>  0) & 0xff;
    writeBuffer[11] = (addr >> 8) & 0xff;
    writeBuffer[12] = (addr >> 16) & 0xff;
    writeBuffer[13] = (addr >> 24) & 0xff;
    writeBuffer[14] = (addr >> 32) & 0xff;
#else // 2nd
    writeBuffer[10] = (addr >> 32) & 0xffUL;//((addr & 0xffffffffffffffff) >>  0) & 0xff;
    writeBuffer[11] = (addr >> 24) & 0xff;
    writeBuffer[12] = (addr >> 16) & 0xff;
    writeBuffer[13] = (addr >> 8) & 0xff;
    writeBuffer[14] = (addr) & 0xff;
#endif
	writeBuffer[15] = 0x1;// Read
#else
    writeBuffer[14] = (addr >> 32) & 0xffUL;//((addr & 0xffffffffffffffff) >>  0) & 0xff;
    writeBuffer[15] = (addr >> 24) & 0xff;
    writeBuffer[16] = (addr >> 16) & 0xff;
    writeBuffer[17] = (addr >> 8) & 0xff;
    writeBuffer[18] = (addr) & 0xff;
#endif
#endif
#endif
#if 0 // good seq. for write
    writeBuffer[ 0] = 0x0; 
    writeBuffer[ 1] = 0x0;
    writeBuffer[ 2] = 0x10; // data7
    writeBuffer[ 3] = 0x20; // data6
    writeBuffer[ 4] = 0x30; // data5
    writeBuffer[ 5] = 0x40; // data4
    writeBuffer[ 6] = 0x50; // data3
    writeBuffer[ 7] = 0x60; // data2
    writeBuffer[ 8] = 0x70; // data1
    writeBuffer[ 9] = 0x80; // data0
    writeBuffer[10] = (addr >> 32) & 0xffUL;//((addr & 0xffffffffffffffff) >>  0) & 0xff;
    writeBuffer[11] = (addr >> 24) & 0xff;
    writeBuffer[12] = (addr >> 16) & 0xff;
    writeBuffer[13] = (addr >> 8) & 0xff;
    writeBuffer[14] = (addr) & 0xff;
	writeBuffer[15] = 0x2;// Write
#endif


#if 1
    ftStatus = FT4222_SPIMaster_SingleWrite(ftHandle, 
                                                writeBuffer, sizeToTransfer,
                                                sizeTransferred, isEndTransaction);

    if (FT_OK != ftStatus || *sizeTransferred != sizeToTransfer) {
      printf("do_write failed!\n");
      return -1;
    } else {
      printf("do_write ok!\n");
    }

	isEndTransaction = 1;
	sizeToTransfer = 32;

    memset(writeBuffer, 0, sizeToTransfer);

    ftStatus = FT4222_SPIMaster_SingleReadWrite(ftHandle, readBuffer,
                                                writeBuffer, sizeToTransfer,
                                                sizeTransferred, isEndTransaction);

    if (FT_OK != ftStatus || *sizeTransferred != sizeToTransfer) {
      printf("do_read failed!\n");
      return -1;
    } else {
      for (i = 0; i < sizeToTransfer; i++) {
        printf("read 0x%2d: 0x%2x\n", i, readBuffer[i]);
      }
      printf("\n");
    }
#else
	isEndTransaction = 1;
    ftStatus = FT4222_SPIMaster_SingleReadWrite(ftHandle, readBuffer,
                                                writeBuffer, sizeToTransfer,
                                                sizeTransferred, isEndTransaction);

    if (FT_OK != ftStatus || *sizeTransferred != 16) {
      printf("do_read failed!\n");
      return -1;
    } else {
      for (i = 0; i < 16; i++) {
        printf("read 0x%2d: 0x%2x\n", i, readBuffer[i]);
      }
      printf("\n");
    }

#endif

    for (i = 0; i < 8; i++) {
      ret_data = ret_data << 8;
      ret_data = ret_data | readBuffer[i];
    }

    free(sizeTransferred);
    free(writeBuffer);
    free(readBuffer);
    fprintf (stderr, "Done Reading address 0x%010llx, Got data 0x%016llx\n", addr, ret_data);
    return ret_data;
}

int main (int argc, char const *argv[])
{
  int doSingle = 1;
  uint8 *  readBuffer;
  uint8 *  writeBuffer;
  uint8    singleWriteBytes = 1;
  uint16   multiWriteBytes = 13;
  uint16   multiReadBytes = 8;
  uint32 * sizeOfRead;
  std::string status;

  uint16   sizeToTransfer = 23;
  uint16 * sizeTransferred;
  BOOL     isEndTransaction;

  FT_STATUS ftStatus;
  FT_HANDLE ftHandle;

  if (doSingle == 0)
    ftHandle = setup(SPI_IO_DUAL, CLK_DIV_16,
                                     CLK_IDLE_LOW, CLK_LEADING);
  else
    ftHandle = setup(SPI_IO_SINGLE, CLK_DIV_256,
                                     CLK_IDLE_LOW, CLK_LEADING);

  printf("ftHandle = 0x%x!\n", ftHandle);

  spi_do_read2(SPI_IO_SINGLE, ftHandle, 0x10UL);
//  spi_do_read(SPI_IO_SINGLE, ftHandle, 0x80000000UL);
//  spi_do_read2(SPI_IO_SINGLE, ftHandle, 0x860000UL);
//  spi_do_read(SPI_IO_SINGLE, ftHandle, 0x12345678UL);

  FT4222_UnInitialize(ftHandle);
  FT_Close(ftHandle);
  return 0;
}
