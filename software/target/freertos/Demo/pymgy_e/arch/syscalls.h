#ifndef SYSCALLS_H
#define SYSCALLS_H

#include <stdint.h>

#define SYS_write 64
#define SYS_exit 93
#define SYS_timer 1234

uint64_t vSyscallToHost(long which, long arg0, long arg1, long arg2);

void vSyscallInit(void);

#endif /* SYSCALLS_H */
