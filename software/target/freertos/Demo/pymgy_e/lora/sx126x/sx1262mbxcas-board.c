/*!
 * \file      sx1262mbxcas-board.c
 *
 * \brief     Target board SX1262MBXCAS shield driver implementation
 *
 * \copyright Revised BSD License, see section \ref LICENSE.
 *
 * \code
 *                ______                              _
 *               / _____)             _              | |
 *              ( (____  _____ ____ _| |_ _____  ____| |__
 *               \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 *               _____) ) ____| | | || |_| ____( (___| | | |
 *              (______/|_____)_|_|_| \__)_____)\____)_| |_|
 *              (C)2013-2017 Semtech
 *
 * \endcode
 *
 * \author    Miguel Luis ( Semtech )
 *
 * \author    Gregory Cristian ( Semtech )
 */
#include <stdlib.h>
#include "hal.h"
#include "board-config.h"
//#include "board.h"
//#include "delay.h"
#include "radio.h"
#include "sx126x-board.h"

/*!
 * Antenna switch GPIO pins objects
 */
Gpio_t AntPow;
Gpio_t DeviceSel;

/*!
 * Debug GPIO pins objects
 */
#if defined( USE_RADIO_DEBUG )
Gpio_t DbgPinTx;
Gpio_t DbgPinRx;
#endif

static void sx126xSpiRW1Basize(LoraRW_t rw, uint8_t a1, uint8_t *buffer, uint16_t size)
{
    RVHAL_ASSERT_PARAM(size < RVHAL_SPIM_FIFO_SIZE_MAX);
    
    if (LORA_WRITE == rw)
        rvHal_spim_write_1262(&a1, 1, buffer, size);
}

static void sx126xSpiRW2Basize(LoraRW_t rw, uint8_t a1, uint8_t a2, uint8_t *buffer, uint16_t size)
{
    RVHAL_ASSERT_PARAM(size < (RVHAL_SPIM_FIFO_SIZE_MAX - 1));
    
    uint8_t abuffer[2];
    abuffer[0] = a1;
    abuffer[1] = a2;
    
    if (LORA_WRITE == rw)
        rvHal_spim_write_1262(abuffer, 2, buffer, size);
    else
        rvHal_spim_read_1262(abuffer, 2, buffer, size);
}

static void sx126xSpiRW3Basize(LoraRW_t rw, uint8_t a1, uint8_t a2, uint8_t a3, uint8_t *buffer, uint16_t size)
{
    RVHAL_ASSERT_PARAM(size < (RVHAL_SPIM_FIFO_SIZE_MAX - 2));
    
    uint8_t abuffer[3];
    abuffer[0] = a1;
    abuffer[1] = a2;
    abuffer[2] = a3;
    
    if (LORA_WRITE == rw)
        rvHal_spim_write_1262(abuffer, 3, buffer, size);
    else
        rvHal_spim_read_1262(abuffer, 3, buffer, size);
}

static void sx126xSpiRW4Basize(LoraRW_t rw, uint8_t a1, uint8_t a2, uint8_t a3, uint8_t a4, uint8_t *buffer, uint16_t size)
{
    RVHAL_ASSERT_PARAM(size < (RVHAL_SPIM_FIFO_SIZE_MAX - 3));
    
    uint8_t abuffer[4];
    abuffer[0] = a1;
    abuffer[1] = a2;
    abuffer[2] = a3;
    abuffer[3] = a4;
    
    if (LORA_READ == rw)
        rvHal_spim_read_1262(abuffer, 4, buffer, size);
}

void SX126xIoInit( void )
{
//    GpioInit( &SX126x.Spi.Nss, RADIO_NSS, PIN_OUTPUT, PIN_PUSH_PULL, PIN_NO_PULL, 1 );
    GpioInit( &SX126x.BUSY, RADIO_BUSY, PIN_INPUT, PIN_PUSH_PULL, PIN_NO_PULL, 0 );
    GpioInit( &SX126x.DIO1, RADIO_DIO_1, PIN_INPUT, PIN_PUSH_PULL, PIN_NO_PULL, 0 );
//    GpioInit( &DeviceSel, RADIO_DEVICE_SEL, PIN_INPUT, PIN_PUSH_PULL, PIN_NO_PULL, 0 );
}

void SX126xIoIrqInit( DioIrqHandler dioIrq )
{
    GpioSetInterrupt( &SX126x.DIO1, IRQ_RISING_EDGE, IRQ_HIGH_PRIORITY, dioIrq );
}

void SX126xIoDeInit( void )
{
//    GpioInit( &SX126x.Spi.Nss, RADIO_NSS, PIN_OUTPUT, PIN_PUSH_PULL, PIN_NO_PULL, 1 );
    GpioInit( &SX126x.BUSY, RADIO_BUSY, PIN_INPUT, PIN_PUSH_PULL, PIN_NO_PULL, 0 );
    GpioInit( &SX126x.DIO1, RADIO_DIO_1, PIN_INPUT, PIN_PUSH_PULL, PIN_NO_PULL, 0 );
}

void SX126xIoDbgInit( void )
{
#if defined( USE_RADIO_DEBUG )
    GpioInit( &DbgPinTx, RADIO_DBG_PIN_TX, PIN_OUTPUT, PIN_PUSH_PULL, PIN_NO_PULL, 0 );
    GpioInit( &DbgPinRx, RADIO_DBG_PIN_RX, PIN_OUTPUT, PIN_PUSH_PULL, PIN_NO_PULL, 0 );
#endif
}

void SX126xIoTcxoInit( void )
{
    // No TCXO component available on this board design.
}

uint32_t SX126xGetBoardTcxoWakeupTime( void )
{
    return BOARD_TCXO_WAKEUP_TIME;
}

void SX126xReset( void )
{
    GpioInit( &SX126x.Reset, RADIO_RESET, PIN_OUTPUT, PIN_PUSH_PULL, PIN_PULL_UP, 1 );

    DelayMs( 10 );
    GpioWrite( &SX126x.Reset, 0 );
    DelayMs( 20 );
    GpioWrite( &SX126x.Reset, 1 );
    DelayMs( 10 );
}

void SX126xWaitOnBusy( void )
{
    while( GpioRead( &SX126x.BUSY ) == 1 );
}

void SX126xWakeup( void )
{
    CRITICAL_SECTION_BEGIN( );
#if 1
    sx126xSpiRW2Basize(LORA_READ, RADIO_GET_STATUS, 0, NULL, 0);
#else
    GpioWrite( &SX126x.Spi.Nss, 0 );

    SpiInOut( &SX126x.Spi, RADIO_GET_STATUS );
    SpiInOut( &SX126x.Spi, 0x00 );

    GpioWrite( &SX126x.Spi.Nss, 1 );
#endif    
    // Wait for chip to be ready.
    SX126xWaitOnBusy( );

    CRITICAL_SECTION_END( );
}

void SX126xWriteCommand( RadioCommands_t command, uint8_t *buffer, uint16_t size )
{
    SX126xCheckDeviceReady( );

#if 1
    sx126xSpiRW1Basize(LORA_WRITE, ( uint8_t )command, buffer, size);
#else    
    GpioWrite( &SX126x.Spi.Nss, 0 );

    SpiInOut( &SX126x.Spi, ( uint8_t )command );

    for( uint16_t i = 0; i < size; i++ )
    {
        SpiInOut( &SX126x.Spi, buffer[i] );
    }

    GpioWrite( &SX126x.Spi.Nss, 1 );
#endif
    if( command != RADIO_SET_SLEEP )
    {
        SX126xWaitOnBusy( );
    }
}

uint8_t SX126xReadCommand( RadioCommands_t command, uint8_t *buffer, uint16_t size )
{
    uint8_t status = 0;

    SX126xCheckDeviceReady( );
#if 1
    sx126xSpiRW2Basize(LORA_READ, ( uint8_t )command, 0, buffer, size);
#else
    GpioWrite( &SX126x.Spi.Nss, 0 );

    SpiInOut( &SX126x.Spi, ( uint8_t )command );
    status = SpiInOut( &SX126x.Spi, 0x00 );
    for( uint16_t i = 0; i < size; i++ )
    {
        buffer[i] = SpiInOut( &SX126x.Spi, 0 );
    }

    GpioWrite( &SX126x.Spi.Nss, 1 );
#endif
    SX126xWaitOnBusy( );

    return status;
}

#define MOD_BY_4B(a)	((a) - (((a)>>2)<<2))
void SX126xWriteRegisters( uint16_t address, uint8_t *buffer, uint16_t size )
{
    SX126xCheckDeviceReady( );
#if 1
    RVHAL_ASSERT_PARAM(size > 0);

	uint16_t mod, first_size;

	mod = MOD_BY_4B(size-1);
	first_size = size - mod;

	/* first_size == 1B, 5B, 9B ... (+3B asize) algin */
    sx126xSpiRW3Basize(LORA_WRITE, RADIO_WRITE_REGISTER, ( address & 0xFF00 ) >> 8, address & 0x00FF, buffer, first_size);

	if (0 != mod) {
		int i;
		buffer += first_size;
		address += first_size;

		/* send byte by byte (1+3B align) */
		for (i=0; i<mod; address++, buffer++, i++) {			
		    sx126xSpiRW3Basize(LORA_WRITE, RADIO_WRITE_REGISTER, ( address & 0xFF00 ) >> 8, address & 0x00FF, buffer, 1);
		}
	}
#else
    GpioWrite( &SX126x.Spi.Nss, 0 );
    
    SpiInOut( &SX126x.Spi, RADIO_WRITE_REGISTER );
    SpiInOut( &SX126x.Spi, ( address & 0xFF00 ) >> 8 );
    SpiInOut( &SX126x.Spi, address & 0x00FF );
    
    for( uint16_t i = 0; i < size; i++ )
    {
        SpiInOut( &SX126x.Spi, buffer[i] );
    }

    GpioWrite( &SX126x.Spi.Nss, 1 );
#endif
    SX126xWaitOnBusy( );
}

void SX126xWriteRegister( uint16_t address, uint8_t value )
{
    SX126xWriteRegisters( address, &value, 1 );
}

void SX126xReadRegisters( uint16_t address, uint8_t *buffer, uint16_t size )
{
    SX126xCheckDeviceReady( );
#if 1
    sx126xSpiRW4Basize(LORA_READ, RADIO_READ_REGISTER, ( address & 0xFF00 ) >> 8, address & 0x00FF, 0, buffer, size);
#else
    GpioWrite( &SX126x.Spi.Nss, 0 );

    SpiInOut( &SX126x.Spi, RADIO_READ_REGISTER );
    SpiInOut( &SX126x.Spi, ( address & 0xFF00 ) >> 8 );
    SpiInOut( &SX126x.Spi, address & 0x00FF );
    SpiInOut( &SX126x.Spi, 0 );
    for( uint16_t i = 0; i < size; i++ )
    {
        buffer[i] = SpiInOut( &SX126x.Spi, 0 );
    }
    GpioWrite( &SX126x.Spi.Nss, 1 );
#endif
    SX126xWaitOnBusy( );
}

uint8_t SX126xReadRegister( uint16_t address )
{
    uint8_t data;
    SX126xReadRegisters( address, &data, 1 );
    return data;
}

void SX126xWriteBuffer( uint8_t offset, uint8_t *buffer, uint8_t size )
{
    SX126xCheckDeviceReady( );
#if 1
    sx126xSpiRW2Basize(LORA_WRITE, RADIO_WRITE_BUFFER, offset, buffer, size);
#else
    GpioWrite( &SX126x.Spi.Nss, 0 );

    SpiInOut( &SX126x.Spi, RADIO_WRITE_BUFFER );
    SpiInOut( &SX126x.Spi, offset );
    for( uint16_t i = 0; i < size; i++ )
    {
        SpiInOut( &SX126x.Spi, buffer[i] );
    }
    GpioWrite( &SX126x.Spi.Nss, 1 );
#endif
    SX126xWaitOnBusy( );
}

void SX126xReadBuffer( uint8_t offset, uint8_t *buffer, uint8_t size )
{
    SX126xCheckDeviceReady( );
#if 1
    sx126xSpiRW3Basize(LORA_READ, RADIO_READ_BUFFER, offset, 0, buffer, size);
#else
    GpioWrite( &SX126x.Spi.Nss, 0 );

    SpiInOut( &SX126x.Spi, RADIO_READ_BUFFER );
    SpiInOut( &SX126x.Spi, offset );
    SpiInOut( &SX126x.Spi, 0 );
    for( uint16_t i = 0; i < size; i++ )
    {
        buffer[i] = SpiInOut( &SX126x.Spi, 0 );
    }
    GpioWrite( &SX126x.Spi.Nss, 1 );
#endif
    SX126xWaitOnBusy( );
}

void SX126xSetRfTxPower( int8_t power )
{
    SX126xSetTxParams( power, RADIO_RAMP_40_US );
}

uint8_t SX126xGetDeviceId( void )
{
#if 0
    if( GpioRead( &DeviceSel ) == 1 )
    {
        return SX1261;
    }
    else
#endif
    {
        return SX1262;
    }
}

void SX126xAntSwOn( void )
{
// TODO:    GpioInit( &AntPow, RADIO_ANT_SWITCH_POWER, PIN_OUTPUT, PIN_PUSH_PULL, PIN_PULL_UP, 1 );
}

void SX126xAntSwOff( void )
{
// TODO:    GpioInit( &AntPow, RADIO_ANT_SWITCH_POWER, PIN_OUTPUT, PIN_PUSH_PULL, PIN_PULL_UP, 0 );
}

bool SX126xCheckRfFrequency( uint32_t frequency )
{
    // Implement check. Currently all frequencies are supported
    return true;
}

#if defined( USE_RADIO_DEBUG )
void SX126xDbgPinTxWrite( uint8_t state )
{
    GpioWrite( &DbgPinTx, state );
}

void SX126xDbgPinRxWrite( uint8_t state )
{
    GpioWrite( &DbgPinRx, state );
}
#endif

#ifdef TEST_DEMO_SPI_LORA
void lora_spi_test(void)
{
    SX126xReset( );

/*
 * expected result:
addr1: 0x6bc, value: 0x1d
addr1: 0x6bd, value: 0xf
addr1: 0x6be, value: 0x10
addr1: 0x6bf, value: 0x21
addr1: 0x6c0, value: 0x97

addr2: 0x6bc, value: 0xa0
addr2: 0x6bd, value: 0xa1
addr2: 0x6be, value: 0xa2
addr2: 0x6bf, value: 0x21
addr2: 0x6c0, value: 0x97
*/

	int i, err = 0;
	uint8_t buf[5];
	uint8_t buf1[5] = {0x1d, 0xf, 0x10, 0x21, 0x97};
	uint8_t buf2[5] = {0xa0, 0xa1, 0xa2, 0x21, 0x97};

	SX126xReadRegisters(0x6bc, buf, sizeof(buf));
	for (i=0; i<sizeof(buf); i++) {
		RVHAL_LORA_LOG_INFO("addr1: 0x%x, value: 0x%x\n", 0x6bc+i, buf[i]);
		if (buf[i] != buf1[i])
			err = 1;
		buf[i] = 0xa0+i;
	}
	RVHAL_LORA_LOG_INFO("\n");
	SX126xWriteRegisters(0x6bc, buf, 3);

	SX126xReadRegisters(0x6bc, buf, sizeof(buf));
	for (i=0; i<sizeof(buf); i++) {
		RVHAL_LORA_LOG_INFO("addr2: 0x%x, value: 0x%x\n", 0x6bc+i, buf[i]);
		if (buf[i] != buf2[i])
			err = 1;
	}

	if (err)
    	RVHAL_LORA_LOG_INFO("[FAIL] ");
	else
    	RVHAL_LORA_LOG_INFO("[PASS] ");
	RVHAL_LORA_LOG_INFO("[SPI] [read & write] [read & write the lora chip sx1262 registers] ...\n");		
}
#endif
