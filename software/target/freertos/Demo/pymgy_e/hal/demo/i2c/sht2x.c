#include <string.h>
#include <math.h> // TODO: floor
#include "hal.h"
#include "clib.h"
#include "demo.h"

#ifdef TEST_DEMO_I2C

#define I2C_ADR         0x40

enum{
	TYPE_T_MEASUREMENT_HM    = 0xE3,
	TYPE_RH_MEASUREMENT_HM   = 0xE5
};

#define LITTLE_ENDIAN

typedef unsigned char   u8t;      ///< range: 0 .. 255
typedef signed char     i8t;      ///< range: -128 .. +127
																    	
typedef unsigned short  u16t;     ///< range: 0 .. 65535
typedef signed short    i16t;     ///< range: -32768 .. +32767

typedef float    ft; 

typedef union {
  u16t u16;               // element specifier for accessing whole u16
  i16t i16;               // element specifier for accessing whole i16
  struct {
    #ifdef LITTLE_ENDIAN  // Byte-order is little endian
    u8t u8L;              // element specifier for accessing low u8
    u8t u8H;              // element specifier for accessing high u8
    #else                 // Byte-order is big endian
    u8t u8H;              // element specifier for accessing low u8
    u8t u8L;              // element specifier for accessing high u8
    #endif
  } s16;                  // element spec. for acc. struct with low or high u8
} snt16;


static int sht2x_read(unsigned char type, unsigned char *rxdata)
{
	u8t tx[1];
	struct i2c_msg msgs[2] = {
		{
			.addr = I2C_ADR,
			.flags = 0,
			.len = 1,
			.buf = tx,
		},
		{
			.addr = I2C_ADR,
			.flags = I2C_M_RD,
			.len = 4,
			.buf = rxdata,
		},
	};
	int ret;

	tx[0] = type;
	ret = rvHal_i2c_transfer(0, msgs, 2);
	if (ret < 0)
		return -1;

	//RVHAL_HAL_LOG_INFO("i2c rx 0x%x, 0x%x, 0x%x\n", rxdata[0], rxdata[1], rxdata[2]);

	return 0;
}

#define         POLYNOMIAL      0x131      //P(x)=x^8+x^5+x^4+1 = 100110001

typedef enum{
	ACK_ERROR                = 0x01,
	TIME_OUT_ERROR           = 0x02,
	CHECKSUM_ERROR           = 0x04,
	UNIT_ERROR               = 0x08
}etError;

static unsigned char SHT2x_CheckCrc(unsigned char data[], unsigned char nbrOfBytes, unsigned char checksum)
{
	unsigned char crc = 0,byteCtr=0,bit=0;	

	for (byteCtr = 0; byteCtr < nbrOfBytes; ++byteCtr)
	{ 
		crc ^= (data[byteCtr]);
		for (bit = 8; bit > 0; --bit)
		{ 
			if (crc & 0x80) crc = (crc << 1) ^ POLYNOMIAL; 
			else crc = (crc << 1);
		}
	}
	if (crc != checksum) return CHECKSUM_ERROR;  
	else return 0;
}

static unsigned char SHT2x_MeasureHM(unsigned char eSHT2xMeasureType, snt16 *pMeasurand)    
{
	unsigned char  data[3] = {0, 0, 0};    
	unsigned char  error=0;    
	
	error=sht2x_read(eSHT2xMeasureType, data);
	if(0 == error)
	{
		pMeasurand->s16.u8H=data[0];
		pMeasurand->s16.u8L=data[1];
		error=SHT2x_CheckCrc (data,2,data[2]);
	}
	return error;
}

//==============================================================================
static float SHT2x_CalcRH(u16t u16sRH)
//==============================================================================
{
  ft humidityRH;              // variable for result

  u16sRH &= ~0x0003;          // clear bits [1..0] (status bits)
  //-- calculate relative humidity [%RH] --

  humidityRH = -6.0 + 125.0/65536 * (ft)u16sRH; // RH= -6 + 125 * SRH/2^16
  return humidityRH;
}

//==============================================================================
static float SHT2x_CalcTemperatureC(u16t u16sT)
//==============================================================================
{
  ft temperatureC;            // variable for result

  u16sT &= ~0x0003;           // clear bits [1..0] (status bits)

  //-- calculate temperature
  temperatureC= -46.85 + 175.72/65536 *(ft)u16sT; //T= -46.85 + 175.72 * ST/2^16
  return temperatureC;
}

unsigned char SHT2x_MeasureData(void)  
{
	snt16 temp,humi;
	unsigned char error=0;
	short temperature, humidity;

	error=SHT2x_MeasureHM(TYPE_T_MEASUREMENT_HM,&temp);

	if(0 == error)
	{
		temperature = (short)floor(SHT2x_CalcTemperatureC(temp.u16));
		if (-47 == temperature) // TODO:
	    	RVHAL_HAL_LOG_INFO("[FAIL] ");
		else
	    	RVHAL_HAL_LOG_INFO("[PASS] ");
    	RVHAL_HAL_LOG_INFO("[I2C] [read & write & interrupt] [get the Temperature from SHT2X: %d `C] ...\n", temperature);		
	}

	error=SHT2x_MeasureHM(TYPE_RH_MEASUREMENT_HM,&humi);
	if(0 == error)
	{
		humidity = (short)floor(SHT2x_CalcRH(humi.u16));
		if (-6 == humidity) // TODO:
	    	RVHAL_HAL_LOG_INFO("[FAIL] ");
		else
	    	RVHAL_HAL_LOG_INFO("[PASS] ");
    	RVHAL_HAL_LOG_INFO("[I2C] [read & write & interrupt] [get the Humidity from SHT2X: %d] ...\n", humidity);		
	}

	if (error)
	    RVHAL_HAL_LOG_INFO("Error CRC \n");		
	
	return error;  
}
#else
unsigned char SHT2x_MeasureData(void)
{
	return 0;
}
#endif
