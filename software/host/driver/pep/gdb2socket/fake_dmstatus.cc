#include <cassert>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <memory>

#include "fake_dmstatus.h"
#include "../../../beak/riscv/opcodes.h"
#include "../../../beak/riscv/mmu.h"
#include "../../../beak/riscv/sim.h"

#include "../../../beak/riscv/debug_defines.h"
#include "../../../beak/riscv/debug_rom_defines.h"


#if 0
#  define D(x) x
#else
#  define D(x)
#endif

// Return the number of bits wide that a field has to be to encode up to n
// different values.
// 1->0, 2->1, 3->2, 4->2
static unsigned field_width(unsigned n)
{
  unsigned i = 0;
  n -= 1;
  while (n) {
    i++;
    n >>= 1;
  }
  return i;
}

///////////////////////// debug_module_t

debug_module_t::debug_module_t() :
  nprocs(1),
  //config(config),
  //abstract_rti(10),
  //support_abstract_csr_access(1),
  program_buffer_bytes(4 + 4*2),
  debug_progbuf_start(debug_data_start - program_buffer_bytes),
  debug_abstract_start(debug_progbuf_start - debug_abstract_size*4),
  custom_base(0),
  // The spec lets a debugger select nonexistent harts. Create hart_state for
  // them because I'm too lazy to add the code to just ignore accesses.
  hartsellen(field_width(1)),
  hart_state(1 << field_width(1)),
  hart_array_mask(1)
{
  D(fprintf(stderr, "debug_data_start=0x%x\n", debug_data_start));
  D(fprintf(stderr, "debug_progbuf_start=0x%x\n", debug_progbuf_start));
  D(fprintf(stderr, "debug_abstract_start=0x%x\n", debug_abstract_start));

  assert(nprocs <= 1024);

  program_buffer = new uint8_t[program_buffer_bytes];

  memset(debug_rom_flags, 0, sizeof(debug_rom_flags));
  memset(program_buffer, 0, program_buffer_bytes);
  program_buffer[4*2] = ebreak();
  program_buffer[4*2+1] = ebreak() >> 8;
  program_buffer[4*2+2] = ebreak() >> 16;
  program_buffer[4*2+3] = ebreak() >> 24;
  memset(dmdata, 0, sizeof(dmdata));

  write32(debug_rom_whereto, 0,
          jal(ZERO, debug_abstract_start - DEBUG_ROM_WHERETO));

  memset(debug_abstract, 0, sizeof(debug_abstract));

  reset();
}

debug_module_t::~debug_module_t()
{
  delete[] program_buffer;
}


void debug_module_t::reset()
{

  dmcontrol = {0};

  dmstatus = {0};
  dmstatus.impebreak = true;
  dmstatus.authenticated = true;
  dmstatus.version = 2;

  abstractcs = {0};
  abstractcs.datacount = sizeof(dmdata) / 4;
  abstractcs.progbufsize = 0;

  abstractauto = {0};

  sbcs = {0};

}

void debug_module_t::add_device(bus_t *bus) {
}

bool debug_module_t::load(reg_t addr, size_t len, uint8_t* bytes)
{
	//addr = DEBUG_START + addr;
	
	//if (addr >= DEBUG_ROM_ENTRY &&
	//    (addr + len) <= (DEBUG_ROM_ENTRY + debug_rom_raw_len)) {
	//  memcpy(bytes, debug_rom_raw + addr - DEBUG_ROM_ENTRY, len);
	//  return true;
	//}
	
	if (addr >= DEBUG_ROM_FLAGS && ((addr + len) <= DEBUG_ROM_FLAGS + 1024)) {
	  memcpy(bytes, debug_rom_flags + addr - DEBUG_ROM_FLAGS, len);
	  return true;
	}
	
	if (addr >= debug_abstract_start && ((addr + len) <= (debug_abstract_start + sizeof(debug_abstract)))) {
	  memcpy(bytes, debug_abstract + addr - debug_abstract_start, len);
	  return true;
	}
	
	if (addr >= debug_data_start && (addr + len) <= (debug_data_start + sizeof(dmdata))) {
	  memcpy(bytes, dmdata + addr - debug_data_start, len);
	  return true;
	}
	
	//if (addr >= debug_progbuf_start && ((addr + len) <= (debug_progbuf_start + program_buffer_bytes))) {
	//  memcpy(bytes, program_buffer + addr - debug_progbuf_start, len);
	//  return true;
	//}
	
	return false;

}

bool debug_module_t::store(reg_t addr, size_t len, const uint8_t* bytes)
{ 

	//addr = DEBUG_START + addr;
	
	if (addr >= debug_data_start && (addr + len) <= (debug_data_start + sizeof(dmdata))) {
		memcpy(dmdata + addr - debug_data_start, bytes, len);
		return true;
	}


	if (addr == DEBUG_ROM_HALTED) {
		assert (len == 4);
  		if (!hart_state[0].halted) {
  		  hart_state[0].halted = true;
  		        // TODO: What if the debugger comes and writes dmcontrol before the
  		        // halt occurs?
  		}
		////printf("select hart = %d to halt\n",dmcontrol.hartsel);
		if (dmcontrol.hartsel == 0) {
			if (0 == (debug_rom_flags[0] & (1 << DEBUG_ROM_FLAG_GO))){
				if (dmcontrol.hartsel == 0) {
					abstract_command_completed = true;
					////printf("abstract command complete\n");
				}
			}
		}
		////printf("set debug_rom_halted\n");
		return true;
    }

	if (addr == DEBUG_ROM_GOING) {
		assert(len == 4);
		debug_rom_flags[0] &= ~(1 << DEBUG_ROM_FLAG_GO);
		////printf("clean debug_rom_going\n");

		uint32_t arg1 = 0;
		if (get_field(command, AC_ACCESS_MEMORY_AAMPOSTINCREMENT)){
			arg1 = read32(dmdata, 1);
			write32(dmdata, 1, arg1 + get_field(command, AC_ACCESS_MEMORY_AAMSIZE)*2);
		}

		return true;
	}

	if (addr == DEBUG_ROM_RESUMING) {
		assert (len == 4);
    	hart_state[0].halted = false;
    	hart_state[0].resumeack = true;
		debug_rom_flags[0] &= ~(1 << DEBUG_ROM_FLAG_RESUME);
    	return true;
	}

	if (addr == DEBUG_ROM_EXCEPTION) {
		if (abstractcs.cmderr == CMDERR_NONE) {
  		  abstractcs.cmderr = CMDERR_EXCEPTION;
  		}
		return true;
	}

  return true;
}

void debug_module_t::write32(uint8_t *memory, unsigned int index, uint32_t value)
{
	uint8_t* base = memory + index * 4;
  	base[0] = value & 0xff;
  	base[1] = (value >> 8) & 0xff;
  	base[2] = (value >> 16) & 0xff;
  	base[3] = (value >> 24) & 0xff;
}

uint32_t debug_module_t::read32(uint8_t *memory, unsigned int index)
{
	uint8_t* base = memory + index * 4;
	uint32_t value = ((uint32_t) base[0]) |
    (((uint32_t) base[1]) << 8) |
    (((uint32_t) base[2]) << 16) |
    (((uint32_t) base[3]) << 24);
  return value;
}

//processor_t *debug_module_t::processor(unsigned hartid) const
//{
//  //vcore_processor_t *proc = NULL;
//  //try {
//  //  proc = sim->get_core(hartid);
//  //} catch (const std::out_of_range&) {
//  //}
//  //return proc;
//  return NULL;
//}

bool debug_module_t::hart_selected(unsigned hartid) const
{
	return hartid == 0;
}

unsigned debug_module_t::sb_access_bits()
{
  return 0;
}

void debug_module_t::sb_autoincrement()
{
}

void debug_module_t::sb_read()
{
}

void debug_module_t::sb_write()
{
}

bool debug_module_t::dmi_read(unsigned address, uint32_t *value)
{
	uint32_t result = 0;

	if (address >= DMI_DATA0 && address < DMI_DATA0 + abstractcs.datacount) {
		unsigned i = address - DMI_DATA0;
		result = read32(dmdata, i);
		if (abstractcs.busy) {
			result = -1;
		}
		
		if (abstractcs.busy && abstractcs.cmderr == CMDERR_NONE) {
			abstractcs.cmderr = CMDERR_BUSY;
		}
		
		if (!abstractcs.busy && ((abstractauto.autoexecdata >> i) & 1)) {
			perform_abstract_command();
		}
	} else if (address >= DMI_PROGBUF0 && address < DMI_PROGBUF0 + 1) {
		//printf("program buffer not supported");
  	} else {
		switch (address) {
		case DMI_DMCONTROL:
		{
			result = set_field(result, DMI_DMCONTROL_HALTREQ, dmcontrol.haltreq);
			result = set_field(result, DMI_DMCONTROL_RESUMEREQ, dmcontrol.resumereq);
			result = set_field(result, DMI_DMCONTROL_HARTSELHI,dmcontrol.hartsel >> DMI_DMCONTROL_HARTSELLO_LENGTH);
			result = set_field(result, DMI_DMCONTROL_HASEL, dmcontrol.hasel);
			result = set_field(result, DMI_DMCONTROL_HARTSELLO, dmcontrol.hartsel);
			result = set_field(result, DMI_DMCONTROL_HARTRESET, dmcontrol.hartreset);
			result = set_field(result, DMI_DMCONTROL_NDMRESET, dmcontrol.ndmreset);
			result = set_field(result, DMI_DMCONTROL_DMACTIVE, dmcontrol.dmactive);
		}
		break;
		case DMI_DMSTATUS:
    	{
  		    dmstatus.allhalted = true;
    	    dmstatus.anyhalted = false;
  		    dmstatus.allrunning = true;
    	    dmstatus.anyrunning = false;
    	    dmstatus.allnonexistant = true;
    	    dmstatus.allresumeack = true;
    	    dmstatus.anyresumeack = false;

			if (hart_selected(0)) { 
				dmstatus.allnonexistant = false;
    	    	if (hart_state[0].resumeack) {
					//printf("DM read: resume ack received\n");
					dmstatus.anyresumeack = true;
    	    	} else {
					dmstatus.allresumeack = false;
    	    	}
    	    	if (hart_state[0].halted) {
					//printf("DM read: halt request received\n");
					dmstatus.allrunning = false;
					dmstatus.anyhalted = true;
    	    	} else {
					dmstatus.allhalted = false;
					dmstatus.anyrunning = true;
    	    	}
			}

    	    // We don't allow selecting non-existant harts through
    	    // hart_array_mask, so the only way it's possible is by writing a
    	    // non-existant hartsel.
    	    dmstatus.anynonexistant = (dmcontrol.hartsel >= nprocs);

  		    dmstatus.allunavail = false;
  		    dmstatus.anyunavail = false;
			////printf("debug dmi_read::: dmstatus = %x\n", dmstatus);

    	    result = set_field(result, DMI_DMSTATUS_IMPEBREAK,dmstatus.impebreak);
    	    result = set_field(result, DMI_DMSTATUS_ALLHAVERESET,hart_state[0].havereset);
    	    result = set_field(result, DMI_DMSTATUS_ANYHAVERESET,hart_state[0].havereset);
  		    result = set_field(result, DMI_DMSTATUS_ALLNONEXISTENT, dmstatus.allnonexistant);
  		    result = set_field(result, DMI_DMSTATUS_ALLUNAVAIL, dmstatus.allunavail);
  		    result = set_field(result, DMI_DMSTATUS_ALLRUNNING, dmstatus.allrunning);
  		    result = set_field(result, DMI_DMSTATUS_ALLHALTED, dmstatus.allhalted);
    	    result = set_field(result, DMI_DMSTATUS_ALLRESUMEACK, dmstatus.allresumeack);
    	    result = set_field(result, DMI_DMSTATUS_ANYNONEXISTENT, dmstatus.anynonexistant);
    	    result = set_field(result, DMI_DMSTATUS_ANYUNAVAIL, dmstatus.anyunavail);
    	    result = set_field(result, DMI_DMSTATUS_ANYRUNNING, dmstatus.anyrunning);
    	    result = set_field(result, DMI_DMSTATUS_ANYHALTED, dmstatus.anyhalted);
    	    result = set_field(result, DMI_DMSTATUS_ANYRESUMEACK, dmstatus.anyresumeack);
    	    result = set_field(result, DMI_DMSTATUS_AUTHENTICATED, dmstatus.authenticated);
    	    result = set_field(result, DMI_DMSTATUS_AUTHBUSY, dmstatus.authbusy);
    	    result = set_field(result, DMI_DMSTATUS_VERSION, dmstatus.version);
    	}
		break;
		case DMI_ABSTRACTCS:
			result = set_field(result, DMI_ABSTRACTCS_CMDERR, abstractcs.cmderr);
        	result = set_field(result, DMI_ABSTRACTCS_BUSY, abstractcs.busy);
        	result = set_field(result, DMI_ABSTRACTCS_DATACOUNT, abstractcs.datacount);
        	result = set_field(result, DMI_ABSTRACTCS_PROGBUFSIZE,abstractcs.progbufsize);
        	break;
		case DMI_ABSTRACTAUTO:
			result = set_field(result, DMI_ABSTRACTAUTO_AUTOEXECPROGBUF, abstractauto.autoexecprogbuf);
        	result = set_field(result, DMI_ABSTRACTAUTO_AUTOEXECDATA, abstractauto.autoexecdata);
			printf("--DM    ::: dmi_read addr = %0x, value = %0x\n", address, *value);
        	break;
		case DMI_COMMAND:
			result = 0;
        	break;
		case DMI_HARTINFO:
			result = set_field(result, DMI_HARTINFO_NSCRATCH, 1);
			result = set_field(result, DMI_HARTINFO_DATAACCESS, 1);
			result = set_field(result, DMI_HARTINFO_DATASIZE, abstractcs.datacount);
			result = set_field(result, DMI_HARTINFO_DATAADDR, debug_data_start);
			break;
		case DMI_SBCS:
			result = set_field(result, DMI_SBCS_SBVERSION, sbcs.version);
  			result = set_field(result, DMI_SBCS_SBREADONADDR, sbcs.readonaddr);
  			result = set_field(result, DMI_SBCS_SBACCESS, sbcs.sbaccess);
  			result = set_field(result, DMI_SBCS_SBAUTOINCREMENT, sbcs.autoincrement);
  			result = set_field(result, DMI_SBCS_SBREADONDATA, sbcs.readondata);
  			result = set_field(result, DMI_SBCS_SBERROR, sbcs.error);
  			result = set_field(result, DMI_SBCS_SBASIZE, sbcs.asize);
  			result = set_field(result, DMI_SBCS_SBACCESS128, sbcs.access128);
  			result = set_field(result, DMI_SBCS_SBACCESS64, sbcs.access64);
  			result = set_field(result, DMI_SBCS_SBACCESS32, sbcs.access32);
  			result = set_field(result, DMI_SBCS_SBACCESS16, sbcs.access16);
  			result = set_field(result, DMI_SBCS_SBACCESS8, sbcs.access8);
    	default:
    	  result = 0;
    	  return true;
    	}
  }
	*value = result;
	//printf("--DM    ::: dmi_read addr = %0x, value = %0x\n", address, *value);
	return true;
}

void debug_module_t::run_test_idle()
{
	if (rti_remaining > 0) {
		rti_remaining--;
	}
	if (rti_remaining == 0 && abstractcs.busy && abstract_command_completed) {
		abstractcs.busy = false;
	}
}

static bool is_fpu_reg(unsigned regno)
{
  return 0;
}

bool debug_module_t::perform_abstract_command()
{

	if (abstractcs.cmderr != CMDERR_NONE)
		return true;
	if (abstractcs.busy) {
		abstractcs.cmderr = CMDERR_BUSY;
		return true;
	}

	if ((command >> 24) == 0) {
		// register access
  		unsigned size = get_field(command, AC_ACCESS_REGISTER_AARSIZE);
  		bool write = get_field(command, AC_ACCESS_REGISTER_WRITE);
  		unsigned regno = get_field(command, AC_ACCESS_REGISTER_REGNO);
		//printf("--DM reg::: command: size = %d, write = %d, regno = %x\n", size, write, regno);

		if (!hart_state[dmcontrol.hartsel].halted) {
  		  abstractcs.cmderr = CMDERR_HALTRESUME;
  		  return true;
  		}

		////printf("perform abstracts command: halted hart\n"); 

		unsigned i = 0;
		if (get_field(command, AC_ACCESS_REGISTER_TRANSFER)) {
			////printf("perform abstracts command: transfer assert\n"); 

			//if (regno < 0x1000 && support_abstract_csr_access) {
			if (regno < 0x1000) {
    		    //write32(debug_abstract, i++, csrw(S0, CSR_DSCRATCH));

				if (write) {
					switch (size) {
						case 2:
							write32(debug_abstract, i++, lw(S0, ZERO, debug_data_start));
    		    	  	  	break;
    		    	  	default:
							abstractcs.cmderr = CMDERR_NOTSUP;
    		    	  	  	return true;
    		    	}
    		    	write32(debug_abstract, i++, csrw(S0, regno));

				} else {
					write32(debug_abstract, i++, csrr(S0, regno));
					switch (size) {
						case 2:
							write32(debug_abstract, i++, sw(S0, ZERO, debug_data_start));
					    	break;
						default:
							abstractcs.cmderr = CMDERR_NOTSUP;
					    	return true;
					}
				}
			} else if (regno >= 0x1000 && regno < 0x1020) {
				unsigned regnum = regno - 0x1000;
				
				switch (size) {
					case 2:
						if (write)
							write32(debug_abstract, i++, lw(regnum, ZERO, debug_data_start));
				  	  	else
							write32(debug_abstract, i++, sw(regnum, ZERO, debug_data_start));
				  	  	break;
				  	default:
						abstractcs.cmderr = CMDERR_NOTSUP;
				  	  	return true;
				}
			} else {
				abstractcs.cmderr = CMDERR_NOTSUP;
				return true;
			}
		}

		if (get_field(command, AC_ACCESS_REGISTER_POSTEXEC)) {
			write32(debug_abstract, i,
				jal(ZERO, debug_progbuf_start - debug_abstract_start - 4 * i));
			i++;

			////printf("perform abstracts command: post execution\n"); 
		} else {
			write32(debug_abstract, i++, ebreak());

			////printf("perform abstracts command: add ebreak index=%d\n",i); 
		}

		debug_rom_flags[dmcontrol.hartsel] |= 1 << DEBUG_ROM_FLAG_GO;

		////printf("perform abstracts command: debug rom go\n"); 

  		//rti_remaining = abstract_rti;
  		rti_remaining = 0;
  		abstract_command_completed = false;

  		abstractcs.busy = true;
	} else if ((command >> 24) == 2) {
		//abstract memory access
  		unsigned size = get_field(command, AC_ACCESS_MEMORY_AAMSIZE);
  		bool vtl = get_field(command, AC_ACCESS_MEMORY_AAMVIRTUAL);
  		bool write = get_field(command, AC_ACCESS_MEMORY_WRITE);
  		bool postincrement = get_field(command, AC_ACCESS_MEMORY_AAMPOSTINCREMENT);
		//printf("--DM mem::: size = %d, write = %d, virtual = %d, postincrement = %d\n", size, write, vtl, postincrement);

		unsigned i = 0;

		if (write) {
			////printf("perform abstracts command: write memory\n"); 

			switch (size) {
				case 1:
					//read addr
					write32(debug_abstract, i++, lh(S1, ZERO, debug_data_start + 4));
					//read data
					write32(debug_abstract, i++, lh(S0, ZERO, debug_data_start));
					write32(debug_abstract, i++, sh(S0, ZERO, S1));
    		  	  	break;
				case 2:
					//read addr
					write32(debug_abstract, i++, lw(S1, ZERO, debug_data_start + 4));
					//read data
					write32(debug_abstract, i++, lw(S0, ZERO, debug_data_start));
					write32(debug_abstract, i++, sw(S0, ZERO, S1));
    		  	  	break;
    		  	default:
					abstractcs.cmderr = CMDERR_NOTSUP;
    		  	  	return true;
    		}

		} else {
			////printf("perform abstracts command: read memory\n"); 

			switch (size) {
				case 1:
					write32(debug_abstract, i++, lh(S1, ZERO, debug_data_start + 4));
					write32(debug_abstract, i++, lh(S0, ZERO, S1));
					write32(debug_abstract, i++, sh(S0, ZERO, debug_data_start));
			    	break;
				case 2:
					write32(debug_abstract, i++, lw(S1, ZERO, debug_data_start + 4));
					write32(debug_abstract, i++, lw(S0, ZERO, S1));
					write32(debug_abstract, i++, sw(S0, ZERO, debug_data_start));
			    	break;
				default:
					abstractcs.cmderr = CMDERR_NOTSUP;
			    	return true;
			}
		}
		////printf("perform abstracts command: post execution\n"); 
		write32(debug_abstract, i++, ebreak());

		debug_rom_flags[dmcontrol.hartsel] |= 1 << DEBUG_ROM_FLAG_GO;

		////printf("perform abstracts command: debug rom go\n"); 

  		rti_remaining = 0;
  		abstract_command_completed = false;

  		abstractcs.busy = true;
	} else {
		abstractcs.cmderr = CMDERR_NOTSUP;
	}

	////printf("perform abstracts command: done\n"); 
	return true;
}

bool debug_module_t::dmi_write(unsigned address, uint32_t value)
{
	printf("--DM    ::: dmi_write addr = %0x, value = %0x\n", address, value);

	if (!dmstatus.authenticated && address != DMI_AUTHDATA && address != DMI_DMCONTROL)
		return false;

	if (address >= DMI_DATA0 && address < DMI_DATA0 + abstractcs.datacount) {
		unsigned i = address - DMI_DATA0;
		if (!abstractcs.busy)
			write32(dmdata, address - DMI_DATA0, value);

		if (abstractcs.busy && abstractcs.cmderr == CMDERR_NONE) {
			abstractcs.cmderr = CMDERR_BUSY;
		}

		if (!abstractcs.busy && ((abstractauto.autoexecdata >> i) & 1)) {
			perform_abstract_command();
		}
		return true;

	} else if (address >= DMI_PROGBUF0 && address < DMI_PROGBUF0 + 1) {
		//printf("Error, not supported DMI_PROGBUF");
		return false;

	} else {
	    switch (address) {
		case DMI_DMCONTROL:
		{
			if (!dmcontrol.dmactive && get_field(value, DMI_DMCONTROL_DMACTIVE))
				reset();
			dmcontrol.dmactive = get_field(value, DMI_DMCONTROL_DMACTIVE);
			if (!dmstatus.authenticated || !dmcontrol.dmactive)
				return true;
	
			dmcontrol.haltreq = get_field(value, DMI_DMCONTROL_HALTREQ);
			dmcontrol.resumereq = get_field(value, DMI_DMCONTROL_RESUMEREQ);
			dmcontrol.hartreset = get_field(value, DMI_DMCONTROL_HARTRESET);
			dmcontrol.ndmreset = get_field(value, DMI_DMCONTROL_NDMRESET);
			dmcontrol.hasel = 0;
			dmcontrol.hartsel = get_field(value, DMI_DMCONTROL_HARTSELHI) << DMI_DMCONTROL_HARTSELLO_LENGTH;
			dmcontrol.hartsel |= get_field(value, DMI_DMCONTROL_HARTSELLO);
			dmcontrol.hartsel &= (1L<<hartsellen) - 1;
	
	
			if (hart_selected(0)) {
				if (get_field(value, DMI_DMCONTROL_ACKHAVERESET)) {
					hart_state[0].havereset = false;
				}
				if (dmcontrol.haltreq) {
					////printf("DM write: halt req\n");
				}
				if (dmcontrol.resumereq) {
					hart_state[0].resumeack = false;
					////printf("DM write: resume req\n");
				}
			}
			return true;
		}
		case DMI_COMMAND:
			command = value;
			//printf("\n");
			//printf("-------------------DM enter abstract perform---------------------\n");
			return perform_abstract_command();
		case DMI_ABSTRACTCS:
			abstractcs.cmderr = (cmderr_t) (((uint32_t) (abstractcs.cmderr)) & (~(uint32_t)(get_field(value, DMI_ABSTRACTCS_CMDERR))));
			return true;
		case DMI_ABSTRACTAUTO:
			abstractauto.autoexecprogbuf = get_field(value,DMI_ABSTRACTAUTO_AUTOEXECPROGBUF);
			abstractauto.autoexecdata = get_field(value,DMI_ABSTRACTAUTO_AUTOEXECDATA);
			return true;
	
		}
	}
}

void debug_module_t::proc_reset(unsigned id)
{
  hart_state[0].havereset = true;
  hart_state[0].halted = false;
  hart_state[0].haltgroup = 0;
}

#ifdef __cplusplus
extern "C" {
#endif
//debug_module_t* dm_new()
//{
//    //printf("dm new\n");
//	//dm = new debug_module_t();
//	std::unique_ptr<debug_module_t> debug_module(new debug_module_t());
//    //printf("dm == %p\n", debug_module.get());
//	debug_module->proc_reset(0);
//	return debug_module.get();
//}
void dm_new(debug_module_t* dm)
{
    //printf("dm new\n");
	dm = new debug_module_t();
    //printf("dm == %p\n", dm);
	dm->proc_reset(0);
}
#ifdef __cplusplus
}
#endif
