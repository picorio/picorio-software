/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

/* RISCV includes */
#include "arch/syscalls.h"
#include "arch/clib.h"

#include "hal.h"
#include "demo.h"
#include "system_config.h"

#define APP_SYSTEM_TEST

static void vIOTestTask( void *pvParameters __unused)
{
	RVHAL_OS_LOG_INFO("TEST DEMO for IO functions ...\n");

#ifdef TEST_DEMO_GPIO
	RVHAL_OS_LOG_INFO("\n------- gpio test -------\n");
	gpio_test();
	rvHal_delay(500);
#endif	

#ifdef TEST_DEMO_I2C
	RVHAL_OS_LOG_INFO("\n------- i2c test -------\n");
	//while(1) 
	{
		SHT2x_MeasureData();
	}
	rvHal_delay(500);
#endif	

#ifdef TEST_DEMO_SPI_LORA
	RVHAL_OS_LOG_INFO("\n------- spi lora test -------\n");
	extern void lora_spi_test(void);
	lora_spi_test();
	rvHal_delay(500);
#endif

#ifdef TEST_DEMO_LORA_PINGPONG
	RVHAL_OS_LOG_INFO("\n------- lora pingpong test -------\n");
	PingpongMain();
#endif

	RVHAL_OS_LOG_INFO("\nTEST IO functions done ...\n");

	while(1) {
		RVHAL_OS_LOG_INFO("------- TEST -------\n");
		vTaskDelay(1000);
	}
}


static void vDemoTask( void *pvParameters __unused)
{
	RVHAL_OS_LOG_INFO("\nDemo task ...\n");

	while(1) {
		RVHAL_OS_LOG_INFO("------- Demo -------\n");
		vTaskDelay(2000);
	}
}



static void App_System_Test(void)
{
#ifdef TEST_DEMO_IO
#ifdef TEST_DEMO_SPI_FLASH
	xTaskCreate( vSpiNorTestTask, "SpiNorTestTask", 2048, NULL, ( configMAX_PRIORITIES - 2 ), NULL );
#else /* ifndef TEST_DEMO_SPI_FLASH */
	xTaskCreate( vIOTestTask, "IOTestTask", 2048, NULL, ( configMAX_PRIORITIES - 2 ), NULL );
#endif	
#endif
	xTaskCreate( vDemoTask, "vDemoTask", 2048, NULL, ( configMAX_PRIORITIES - 3 ), NULL );
}

static __unused void App_Tasks_Create(void)
{
	/* create your tasks depend on your system requirement */

#if 0 /* demo tasks ... */
	xTaskCreate( vAppTask1, "vAppTask1", 2048, NULL, ( configMAX_PRIORITIES - 20 ), NULL );
	xTaskCreate( vAppTask2, "vAppTask2", 1024, NULL, ( configMAX_PRIORITIES - 10 ), NULL );
	xTaskCreate( vAppTask3, "vAppTask3", 2048, NULL, ( configMAX_PRIORITIES - 12 ), NULL );
#endif
}

/*
 * must implement
*/
void rvHalCB_app_entry(void)
{
#ifdef APP_SYSTEM_TEST
	App_System_Test();
#else
	App_Tasks_Create();
#endif
}
