#!/bin/sh

LOOPCOUNT=1
TESTCOUNT=1
WAITIME=1
BOARD="$1"
FILES=$PROJ_ROOT/sw-suite/vcorefp_torture/tysom3_config/*
PKFILES=$PROJ_ROOT/sw-suite/vcorefp_torture/tysom3_pk_config/*

reset_board()
{
# Reset board
if [[ $BOARD == "tysom3" ]]; then
  sshpass -p 'root' ssh root@192.168.0.100 sh -c '/home/root/sd_card/do_reset'
elif [[ $BOARD == "104" ]]; then
  (printf 'reset' | $PROJ_ROOT/tool/driver/es1y_fpga_104/debug-socket.o) &
  sleep 5
  pkill -9 debug-socket.o
  pkill -9 debug-socket.o
elif [[ $BOARD == "118" ]]; then
  (printf 'reset' | $PROJ_ROOT/tool/driver/es1y_fpga_118/debug-socket.o) &
  sleep 5
  pkill -9 debug-socket.o
  pkill -9 debug-socket.o
fi
}

kill_fesvr2socket()
{
# Kill if pass N seconds
pkill -9 fesvr2socket.o
pkill -9 fesvr2socket.o
kill -9 $1
}

pass_fail_processing()
{
mkdir -p /work/users/regression/es1y_fpga_regr/torture_$BOARD_$(date '+%Y-%m-%d')/test$2
# If pass remove everything, else move to unique test directory
if [ "$1" -gt "0" ]
then
  if [ "$1" -gt "2" ]
  then
    echo "$(date) Torture Test $2 TIMEOUT" >> /work/users/regression/es1y_fpga_regr/torture_$BOARD_$(date '+%Y-%m-%d')/torture.log
    mv $3 /work/users/regression/es1y_fpga_regr/torture_$BOARD_$(date '+%Y-%m-%d')/test$2/test.elf
    mv $4 /work/users/regression/es1y_fpga_regr/torture_$BOARD_$(date '+%Y-%m-%d')/test$2/test.c
    mv $5 /work/users/regression/es1y_fpga_regr/torture_$BOARD_$(date '+%Y-%m-%d')/test$2/test$2.log
  else
    echo "$(date) Torture Test $2 FAILED" >> /work/users/regression/es1y_fpga_regr/torture_$BOARD_$(date '+%Y-%m-%d')/torture.log
    mv $3 /work/users/regression/es1y_fpga_regr/torture_$BOARD_$(date '+%Y-%m-%d')/test$2/test.c
    mv $4 /work/users/regression/es1y_fpga_regr/torture_$BOARD_$(date '+%Y-%m-%d')/test$2/test.elf
    mv $5 /work/users/regression/es1y_fpga_regr/torture_$BOARD_$(date '+%Y-%m-%d')/test$2/test$2.log
  fi
else
  make clean
  rm -f $5
  echo "$(date) Torture Test $2 passed" >> /work/users/regression/es1y_fpga_regr/torture_$BOARD_$(date '+%Y-%m-%d')/torture.log
fi
}

test_num=0

vp_nonpk_tests()
{
# Non-pk tests
for f in $FILES
do
  for (( j = 0 ; j < $TESTCOUNT; j++ ))
  do
    reset_board
    # Create and run vp test
    cd $PROJ_ROOT/sw-suite/vcorefp_torture

    if [[ $BOARD == "tysom3" ]]; then
      make clean compilebig CONFIG=$f
      make tysom |& tee test$test_num.log &
    elif [[ $BOARD == "118" ]]; then
      make clean compile CONFIG=$f
      make fpga_118 |& tee test$test_num.log &
    else
      make clean compile CONFIG=$f
      make fpga_104 |& tee test$test_num.log &
    fi

    # Check pass or fail
    PID=$!
    sleep $WAITIME
    python3 post_process.py -f test$test_num.log > processed_test$test_num.log
    $PROJ_ROOT/verif/common/regression/fpga/job_check.sh -i $PROJ_ROOT/sw-suite/vcorefp_torture/processed_test$test_num.log -p "PASSED" -f "FAILED" -t 1
    RSLT=$?
    SW_SUITE=$PROJ_ROOT/sw-suite/vcorefp_torture/
    kill_fesvr2socket "$PID"
    pass_fail_processing "$RSLT" "$test_num" "$SW_SUITE/rand_instr_test.elf" "$SW_SUITE/rand_instr_test.c" "$SW_SUITE/processed_test$test_num.log"
    test_num=$(($test_num + 1))
    cd -
  done
done
}

vp_pk_tests()
{
# pk tests
for f in $PKFILES
do
  for (( j = 0 ; j < $TESTCOUNT; j++ ))
  do
    reset_board
    # Create test
    cd $PROJ_ROOT/sw-suite/vcorefp_torture
    make clean compilepk CONFIG=$f

    if [[ $BOARD == "tysom3" ]]; then
      make tysompk |& tee test$test_num.log &
    elif [[ $BOARD == "118" ]]; then
      make fpga_118 |& tee test$test_num.log &
    else
      make fpga_104 |& tee test$test_num.log &
    fi

    # Check pass or fail
    PID=$!
    sleep $WAITIME
    python3 post_process.py -f test$test_num.log > processed_test$test_num.log
    $PROJ_ROOT/verif/common/regression/fpga/job_check.sh -i $PROJ_ROOT/sw-suite/vcorefp_torture/processed_test$test_num.log -p "PASSED" -f "FAILED" -t 1
    RSLT=$?

    kill_fesvr2socket "$PID"
    pass_fail_processing "$RSLT" "$test_num" "$SW_SUITE/rand_instr_test.elf" "$SW_SUITE/rand_instr_test.c" "$SW_SUITE/processed_test$test_num.log"

    test_num=$(($test_num + 1))
    cd -
  done
done
}

amo_all_exception_tests()
{
for (( j = 0 ; j < $TESTCOUNT; j++ ))
do
  reset_board
  # Create test
  cd $PROJ_ROOT/sw-suite/orv/amo_all_exception
  ./create_tests.sh

  for f in $PROJ_ROOT/sw-suite/orv/amo_all_exception/elfs/*
  do
    reset_board
    if [[ $BOARD == "tysom3" ]]; then
      ($PROJ_ROOT/tool/driver/es1y_fpga_tysom3/fesvr2socket.o ++$f ++set_vp0_rst_pc=80000000 ++load_pk ++release_vp0_reset ++release_bank0_reset ++release_bank1_reset ++release_vp0_early_reset ++set_0_to_bank0_vldram ++set_0_to_bank1_vldram ++set_0_to_vp0_l1_tagram ++log_step=fpga |& tee test$test_num.log) &
    elif [[ $BOARD == "118" ]]; then
      ($PROJ_ROOT/tool/driver/es1y_fpga_118/fesvr2socket.o ++$f ++set_vp0_rst_pc=80000000 ++load_pk ++release_vp0_reset ++release_l2_reset ++release_vp0_early_reset ++set_0_to_l2_vldram ++set_0_to_vp0_l1_tagram |& tee test$test_num.log) &
    else
      ($PROJ_ROOT/tool/driver/es1y_fpga_104/fesvr2socket.o ++$f ++set_vp0_rst_pc=80000000 ++load_pk ++release_vp0_reset ++release_bank0_reset ++release_bank1_reset ++release_vp0_early_reset ++set_0_to_bank0_vldram ++set_0_to_bank1_vldram ++set_0_to_vp0_l1_tagram |& tee test$test_num.log) &
    fi

    # Check pass or fail
    PID=$!
    $PROJ_ROOT/verif/common/regression/fpga/job_check.sh -i $PROJ_ROOT/sw-suite/orv/amo_all_exception/test$test_num.log -p "PASSED" -f "FAILED" -t $WAITIME
    RSLT=$?

    kill_fesvr2socket "$PID"
    pass_fail_processing "$RSLT" "$test_num" "$f" "$f" "$PROJ_ROOT/sw-suite/orv/amo_all_exception/test$test_num.log"
    test_num=$(($test_num + 1))
  done
  cd -
done
}

amo_timer_tests()
{
for (( j = 0 ; j < $TESTCOUNT; j++ ))
do
  reset_board
  # Create test
  cd $PROJ_ROOT/sw-suite/orv/amo_timer
  ./create_tests.sh

  for f in $PROJ_ROOT/sw-suite/orv/amo_timer/elfs/*
  do
    reset_board
    if [[ $BOARD == "tysom3" ]]; then
      ($PROJ_ROOT/tool/driver/es1y_fpga_tysom3/fesvr2socket.o ++$f ++set_vp0_rst_pc=80000000 ++load_pk ++release_vp0_reset ++release_bank0_reset ++release_bank1_reset ++release_vp0_early_reset ++set_0_to_bank0_vldram ++set_0_to_bank1_vldram ++set_0_to_vp0_l1_tagram ++log_step=fpga |& tee test$test_num.log) &
    elif [[ $BOARD == "118" ]]; then
      ($PROJ_ROOT/tool/driver/es1y_fpga_118/fesvr2socket.o ++$f ++set_vp0_rst_pc=80000000 ++load_pk ++release_vp0_reset ++release_l2_reset ++release_vp0_early_reset ++set_0_to_l2_vldram ++set_0_to_vp0_l1_tagram |& tee test$test_num.log) &
    else
      ($PROJ_ROOT/tool/driver/es1y_fpga_104/fesvr2socket.o ++$f ++set_vp0_rst_pc=80000000 ++load_pk ++release_vp0_reset ++release_bank0_reset ++release_bank1_reset ++release_vp0_early_reset ++set_0_to_bank0_vldram ++set_0_to_bank1_vldram ++set_0_to_vp0_l1_tagram |& tee test$test_num.log) &
    fi

    # Check pass or fail
    PID=$!
    $PROJ_ROOT/verif/common/regression/fpga/job_check.sh -i $PROJ_ROOT/sw-suite/orv/amo_timer/test$test_num.log -p "PASSED" -f "FAILED" -t $WAITIME
    RSLT=$?

    kill_fesvr2socket "$PID"
    pass_fail_processing "$RSLT" "$test_num" "$f" "$f" "$PROJ_ROOT/sw-suite/orv/amo_timer/test$test_num.log"
    test_num=$(($test_num + 1))
  done
  cd -
done
}

vcore_all_exception_tests()
{
for (( j = 0 ; j < $TESTCOUNT; j++ ))
do
  reset_board
  # Create test
  cd $PROJ_ROOT/sw-suite/orv/vcore_all_exception
  ./create_tests.sh

  for f in $PROJ_ROOT/sw-suite/orv/vcore_all_exception/elfs/*
  do
    reset_board
    if [[ $BOARD == "tysom3" ]]; then
      ($PROJ_ROOT/tool/driver/es1y_fpga_tysom3/fesvr2socket.o ++$f ++set_vp0_rst_pc=80000000 ++load_pk ++release_vp0_reset ++release_bank0_reset ++release_bank1_reset ++release_vp0_early_reset ++set_0_to_bank0_vldram ++set_0_to_bank1_vldram ++set_0_to_vp0_l1_tagram ++log_step=fpga |& tee test$test_num.log) &
    elif [[ $BOARD == "118" ]]; then
      ($PROJ_ROOT/tool/driver/es1y_fpga_118/fesvr2socket.o ++$f ++set_vp0_rst_pc=80000000 ++load_pk ++release_vp0_reset ++release_l2_reset ++release_vp0_early_reset ++set_0_to_l2_vldram ++set_0_to_vp0_l1_tagram |& tee test$test_num.log) &
    else
      ($PROJ_ROOT/tool/driver/es1y_fpga_104/fesvr2socket.o ++$f ++set_vp0_rst_pc=80000000 ++load_pk ++release_vp0_reset ++release_bank0_reset ++release_bank1_reset ++release_vp0_early_reset ++set_0_to_bank0_vldram ++set_0_to_bank1_vldram ++set_0_to_vp0_l1_tagram |& tee test$test_num.log) &
    fi

    # Check pass or fail
    PID=$!
    $PROJ_ROOT/verif/common/regression/fpga/job_check.sh -i $PROJ_ROOT/sw-suite/orv/vcore_all_exception/test$test_num.log -p "PASSED" -f "FAILED" -t $WAITIME
    RSLT=$?

    kill_fesvr2socket "$PID"
    pass_fail_processing "$RSLT" "$test_num" "$f" "$f" "$PROJ_ROOT/sw-suite/orv/vcore_all_exception/test$test_num.log"
    test_num=$(($test_num + 1))
  done
  cd -
done
}

vcore_timer_tests()
{
for (( j = 0 ; j < $TESTCOUNT; j++ ))
do
  reset_board
  # Create test
  cd $PROJ_ROOT/sw-suite/orv/vcore_timer
  ./create_tests.sh

  for f in $PROJ_ROOT/sw-suite/orv/vcore_timer/elfs/*
  do
    reset_board
    if [[ $BOARD == "tysom3" ]]; then
      ($PROJ_ROOT/tool/driver/es1y_fpga_tysom3/fesvr2socket.o ++$f ++set_vp0_rst_pc=80000000 ++load_pk ++release_vp0_reset ++release_bank0_reset ++release_bank1_reset ++release_vp0_early_reset ++set_0_to_bank0_vldram ++set_0_to_bank1_vldram ++set_0_to_vp0_l1_tagram ++log_step=fpga |& tee test$test_num.log) &
    elif [[ $BOARD == "118" ]]; then
      ($PROJ_ROOT/tool/driver/es1y_fpga_118/fesvr2socket.o ++$f ++set_vp0_rst_pc=80000000 ++load_pk ++release_vp0_reset ++release_l2_reset ++release_vp0_early_reset ++set_0_to_l2_vldram ++set_0_to_vp0_l1_tagram |& tee test$test_num.log) &
    else
      ($PROJ_ROOT/tool/driver/es1y_fpga_104/fesvr2socket.o ++$f ++set_vp0_rst_pc=80000000 ++load_pk ++release_vp0_reset ++release_bank0_reset ++release_bank1_reset ++release_vp0_early_reset ++set_0_to_bank0_vldram ++set_0_to_bank1_vldram ++set_0_to_vp0_l1_tagram |& tee test$test_num.log) &
    fi

    # Check pass or fail
    PID=$!
    $PROJ_ROOT/verif/common/regression/fpga/job_check.sh -i $PROJ_ROOT/sw-suite/orv/vcore_timer/test$test_num.log -p "PASSED" -f "FAILED" -t $WAITIME
    RSLT=$?

    kill_fesvr2socket "$PID"
    pass_fail_processing "$RSLT" "$test_num" "$f" "$f" "$PROJ_ROOT/sw-suite/orv/vcore_timer/test$test_num.log"
    test_num=$(($test_num + 1))
  done
  cd -
done
}

# start server
if [[ $BOARD == "104" ]]; then
  (printf 'source /work/users/dtu/pygmy-es1x/tool/driver/es1y_fpga_104/script/server.tcl' | vivado -mode tcl) &
  sleep 30
elif [[ $BOARD == "118" ]]; then
  (printf 'source /work/users/dtu/pygmy-es1x/tool/driver/es1y_fpga_118/script/server.tcl' | vivado -mode tcl) &
  sleep 30
fi

# Reset board
if [[ $BOARD == "tysom3" ]]; then
  sshpass -p 'root' ssh root@192.168.0.100 sh -c '/home/root/sd_card/start_regr_server.sh' &
elif [[ $BOARD == "104" ]]; then
  (printf 'reset' | $PROJ_ROOT/tool/driver/es1y_fpga_104/debug-socket.o) &
  sleep 5
  pkill -9 debug-socket.o
  pkill -9 debug-socket.o
elif [[ $BOARD == "118" ]]; then
  (printf 'reset' | $PROJ_ROOT/tool/driver/es1y_fpga_118/debug-socket.o) &
  sleep 5
  pkill -9 debug-socket.o
  pkill -9 debug-socket.o
fi

if [[ $BOARD == "tysom3" ]]; then
  WAITIME=45 
elif [[ $BOARD == "118" ]]; then
  WAITIME=200 
elif [[ $BOARD == "104" ]]; then
  WAITIME=200 
fi

# Run tests
mkdir -p /work/users/regression/es1y_fpga_regr/torture_$BOARD_$(date '+%Y-%m-%d')
for (( i = 0 ; i < $LOOPCOUNT; i++ ))
do
  #vp_nonpk_tests 
  #vp_pk_tests
  echo "RUNNING AMO_ALL_EXCEPTION_TESTS" >> /work/users/regression/es1y_fpga_regr/torture_$BOARD_$(date '+%Y-%m-%d')/torture.log
  amo_all_exception_tests

  echo "RUNNING AMO_TIMER_TESTS" >> /work/users/regression/es1y_fpga_regr/torture_$BOARD_$(date '+%Y-%m-%d')/torture.log
  amo_timer_tests

  echo "RUNNING VCORE_ALL_EXCEPTION_TESTS" >> /work/users/regression/es1y_fpga_regr/torture_$BOARD_$(date '+%Y-%m-%d')/torture.log
  vcore_all_exception_tests

  echo "RUNNING AMO_TIMER_TESTS" >> /work/users/regression/es1y_fpga_regr/torture_$BOARD_$(date '+%Y-%m-%d')/torture.log
  vcore_timer_tests

done
