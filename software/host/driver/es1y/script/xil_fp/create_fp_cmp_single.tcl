##################################################################
# CREATE IP XIL_fp_cmp_single
##################################################################

set floating_point XIL_fp_cmp_single
create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 -module_name $floating_point

set_property -dict { 
  CONFIG.Operation_Type {Compare}
  CONFIG.C_Compare_Operation {Condition_Code}
  CONFIG.A_Precision_Type {Single}
  CONFIG.C_A_Exponent_Width {8}
  CONFIG.C_A_Fraction_Width {24}
  CONFIG.Result_Precision_Type {Custom}
  CONFIG.C_Result_Exponent_Width {4}
  CONFIG.C_Result_Fraction_Width {0}
  CONFIG.C_Mult_Usage {No_Usage}
  CONFIG.Flow_Control {NonBlocking}
  CONFIG.Has_RESULT_TREADY {false}
  CONFIG.C_Latency {2}
  CONFIG.C_Rate {1}
  CONFIG.Has_ACLKEN {true}
  CONFIG.Has_ARESETn {true}
} [get_ips $floating_point]

##################################################################

