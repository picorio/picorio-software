#ifndef __HTIF_ES1Y_H
#define __HTIF_ES1Y_H
#include "htif_proj.h"
#include "station_orv32.h"
#include "station_cache.h"
#include "station_slow_io.h"
#include "station_sdio.h"
#include "station_dma.h"

//class htif_proj;

class pygmy_e : public htif_proj {
  typedef void (pygmy_e::*func_pointer)(std::string key);
  private:
    //helpers
    //std::vector<uint64_t> get_breakpoints(std::string breakpoints);
    void set_breakpoints(std::vector<uint64_t> station_en_addr, std::vector<uint64_t> station_addr, std::vector<uint64_t> breakpoints);
    // list of functions
    void program_pll(std::string key);
    // RUN
    void bg();
    void reset_sdio(std::string key);  
    // SDIO
    void clear_nth_bit(int64_t addr, int position);
    void set_nth_bit(int64_t addr, int position);
    void set_n_bits(int64_t addr, int end_position, int start_position);
    void clear_n_bits(int64_t addr, int end_position, int start_position);
    bool nth_bit_set(int64_t addr, int position);
    bool n_bits_nonzero(int64_t addr, int end_position, int start_position);
    uint32_t get_n_bits(int64_t addr, int end_position, int start_position);
    bool card_insertion_chk(int64_t header);
    void card_detection(int64_t header);
    void host_controller_setup_sequence(int64_t header);
    void host_controller_clock_setup_sequence(int64_t header);
    void card_clock_supply_sequence(int64_t header);
    void card_clock_stop_sequence(int64_t header);
    void clock_frequency_change_sequence(int64_t header);
    void card_interface_detection(int64_t header);
    bool tuning_sequence(int64_t header) ;
    void mode1_retuning_flow_sequence(int64_t header) ;
    void auto_tuning_sequence(int64_t header) ;
    void software_tuning_sequence(int64_t header);
    void bus_timing_setting(int64_t header);
    void sdio_init(std::string key);  
    // SDIO ENDS
    void incr_test_io_freq(std::string key);
    void config_l2(std::string key);
    void config_l1(std::string key);
    void chip_config(std::string key);
    void release_bank0_reset(std::string key);
    void release_bank1_reset(std::string key);
    void release_reset_for_l2(std::string key);
    void release_orv32_early_reset(std::string key);
    void set_0_to_orv32_l1_tagram(std::string key);
    void load_kernel(std::string key);
    void set_magic_mem_address(std::string key);
    void set_reset_pc(const char * target);
    void set_orv32_reset_pc(std::string key);
    void load_rom(std::string key);
    void release_orv32_reset(std::string key);
    void io_reg_test(std::string key);
    void enable_flash_fsm(std::string key);
    void enable_boot_fsm(std::string key);
    void read_from_flash(std::string key);
    void trigger_afterwards(std::string key);
    void test_scan_f(std::string key);
    struct func_t {
      std::string key;
      pygmy_e::func_pointer ptr;
   
    };
    std::vector<func_t> boot_sequence;
  public:
    pygmy_e(htif_t * ht) : htif_proj("pygmy_e", ht){
        boot_sequence.push_back((func_t){"incr_test_io_freq=", &pygmy_e::incr_test_io_freq});
        boot_sequence.push_back((func_t){"chip_config=", &pygmy_e::chip_config});
        boot_sequence.push_back((func_t){"program_pll="  ,&pygmy_e::program_pll});
        boot_sequence.push_back((func_t){"config_l2=", &pygmy_e::config_l2});
        boot_sequence.push_back((func_t){"release_bank0_reset", &pygmy_e::release_bank0_reset});
        boot_sequence.push_back((func_t){"release_bank1_reset", &pygmy_e::release_bank1_reset});
        boot_sequence.push_back((func_t){"release_l2_reset", &pygmy_e::release_reset_for_l2});
        boot_sequence.push_back((func_t){"reset_sdio"  ,&pygmy_e::reset_sdio});
        boot_sequence.push_back((func_t){"sdio_init"  ,&pygmy_e::sdio_init});
        boot_sequence.push_back((func_t){"io_reg_test", &pygmy_e::io_reg_test});
        boot_sequence.push_back((func_t){"enable_boot_fsm", &pygmy_e::enable_boot_fsm});
        boot_sequence.push_back((func_t){"config_l1=", &pygmy_e::config_l1});
        boot_sequence.push_back((func_t){"release_orv32_early_reset", &pygmy_e::release_orv32_early_reset});
        boot_sequence.push_back((func_t){"set_0_to_orv32_l1_tagram", &pygmy_e::set_0_to_orv32_l1_tagram});
        boot_sequence.push_back((func_t){"load_pk", &pygmy_e::load_kernel});
        boot_sequence.push_back((func_t){"set_orv32_rst_pc=", &pygmy_e::set_orv32_reset_pc});
        boot_sequence.push_back((func_t){"load_rom", &pygmy_e::load_rom});
        boot_sequence.push_back((func_t){"enable_flash_fsm", &pygmy_e::enable_flash_fsm});
        boot_sequence.push_back((func_t){"release_orv32_reset", &pygmy_e::release_orv32_reset});
        boot_sequence.push_back((func_t){"trigger_afterwards", &pygmy_e::trigger_afterwards});
        boot_sequence.push_back((func_t){"test_scan_f", &pygmy_e::test_scan_f});
        boot_sequence.push_back((func_t){"read_from_flash", &pygmy_e::read_from_flash});
        auto it = this->boot_sequence.begin();
        for(;it != this->boot_sequence.end(); ++it){
          this->ht->argmap.insert({it->key.c_str(), ""});
        }
        this->ht->argmap.insert({"freeze", ""});
        this->ht->argmap.insert({"test_bp=", ""});
        this->ht->argmap.insert({"bp_pc=", ""});
        this->ht->argmap.insert({"background_traffic", ""});
        this->ht->argmap.insert({"disable_pc_chk", ""});
        this->ht->argmap.insert({"ic_bypass", ""});
        this->ht->argmap.insert({"tlb_bypass", ""});
        this->ht->argmap.insert({"snapshot", ""});
        this->ht->argmap.insert({"plic_interrupt", ""});
        this->ht->argmap.insert({"log_step=", ""});
        this->ht->argmap.insert({"snap_period=", ""});
        this->ht->argmap.insert({"snap_initial=", ""});
        this->ht->argmap.insert({"linux_rom", ""});
        this->ht->argmap.insert({"test_scu", ""});
        this->ht->argmap.insert({"test_bg=", ""});
    } 
  void setup();
  ~pygmy_e();
  
  friend class htif_t;
};

// COPY OF mem_t from ours-beak/devices needed for making the dts
class mem_t{
 public:
  mem_t(size_t size) : len(size) {
    if (!size)
      throw std::runtime_error("zero bytes of target memory requested");
    //data = (char*)calloc(1, size);
    //if (!data)
    //  throw std::runtime_error("couldn't allocate " + std::to_string(size) + " bytes of target memory");
  }
  mem_t(const mem_t& that) = delete;
  ~mem_t() {
  }
  char* contents() { return data; }
  size_t size() { return len; }

 private:
  char* data;
  size_t len;
};

#endif
