#ifndef HAL_INCLUDE_IO_H
#define HAL_INCLUDE_IO_H

/********************************
				rtc
*********************************/

void __rvHal_rtc_init(void);

/********************************
				uart
*********************************/

void __rvHal_uart_init(void);

/********************************
				spi
*********************************/

#define RVHAL_SPIM_FIFO_SIZE_MAX	32

void __rvHal_spi_init(void);

void rvHal_spim_read( unsigned short addr, unsigned char *buffer, unsigned char size);
void rvHal_spim_write( unsigned short addr, unsigned char *buffer, unsigned char size);

void rvHal_spim_read_1262( unsigned char *abuffer, unsigned char asize, unsigned char *buffer, unsigned char size);
void rvHal_spim_write_1262( unsigned char *abuffer, unsigned char asize, unsigned char *buffer, unsigned char size);

/********************************
				i2c
*********************************/

/*
 * struct i2c_msg: flags
*/
#define I2C_M_RD		0x0001	/* read data, from slave to master */
					/* I2C_M_RD is guaranteed to be 0x0001! */
#define I2C_M_TEN		0x0010	/* this is a ten bit chip address */
#define I2C_M_DMA_SAFE		0x0200	/* the buffer of this message is DMA safe */
					/* makes only sense in kernelspace */
					/* userspace buffers are copied anyway */
#define I2C_M_RECV_LEN		0x0400	/* length will be first received byte */
#define I2C_M_NO_RD_ACK		0x0800	/* if I2C_FUNC_PROTOCOL_MANGLING */
#define I2C_M_IGNORE_NAK	0x1000	/* if I2C_FUNC_PROTOCOL_MANGLING */
#define I2C_M_REV_DIR_ADDR	0x2000	/* if I2C_FUNC_PROTOCOL_MANGLING */
#define I2C_M_NOSTART		0x4000	/* if I2C_FUNC_NOSTART */
#define I2C_M_STOP		0x8000	/* if I2C_FUNC_PROTOCOL_MANGLING */

struct i2c_msg {
	unsigned short addr;	/* slave address			*/
	unsigned short flags;	/* see above */
	unsigned short len;		/* msg length				*/
	unsigned char *buf;		/* pointer to msg data			*/
};

void __rvHal_i2c_init(void);

/*!
 * @discussion execute a single or combined I2C message.
 * @param dev_num i2c device number, see enum RVHAL_i2c_num (only support I2C0 till now).
 * @param msgs One or more messages to execute before STOP is issued to
 *	terminate the operation; each message begins with a START.
 * @param num Number of messages to be executed, at least is 1.
 * @return 0: ok, negative: fail
 */
int rvHal_i2c_transfer(unsigned int dev_num, struct i2c_msg *msgs, int num);

/********************************
				gpio
*********************************/

struct irq_gpio_handler_t
{
	void *context;
	void (*hook)(void *context);
};

struct gpio_desc
{
	unsigned int pin;
    struct irq_gpio_handler_t handler;
};

enum RVHAL_gpio_type
{
    GPIO_PIN_INPUT = 0,
    GPIO_PIN_OUTPUT,
};

enum RVHAL_gpio_int_type
{
    GPIO_INT_TYPE_LEVEL = 0,
    GPIO_INT_TYPE_EDGE,
};
 
enum RVHAL_gpio_int_polarity
{
    GPIO_INT_POLARITY_LOW = 0,
    GPIO_INT_POLARITY_HIGH,
};

void __rvHal_gpio_init(void);

/*!
 * @discussion initialize gpio pin descriptior.
 * @param dgpio gpio descriptor.
 * @param pin pin number[0, 31].
 * @param type see enum RVHAL_gpio_type.
 * @param value if type is GPIO_PIN_OUTPUT, it is [0, 1] by default.
 */
void rvHal_gpio_init( struct gpio_desc *dgpio, unsigned int pin, unsigned int type, unsigned int value );

/*!
 * @discussion set gpio pin interrupt attribution.
 * @param dgpio gpio descriptor.
 * @param level see enum RVHAL_gpio_int_type.
 * @param polarity see enum RVHAL_gpio_int_polarity.
 * @param irqHandler gpio pin callback handler.
 * @param context context param for this gpio pin.
 */
void rvHal_gpio_set_interrupt( struct gpio_desc *dgpio, unsigned int level, unsigned int polarity, void (*irqHandler)(void*), void *context);

/*!
 * @discussion remove gpio pin interrupt attribution.
 * @param dgpio gpio descriptor.
 */
void rvHal_gpio_remove_interrupt( struct gpio_desc *dgpio );

/*!
 * @discussion gpio pin output level.
 * @param dgpio gpio descriptor.
 * @param value [0, 1].
 */
void rvHal_gpio_write( struct gpio_desc *dgpio, unsigned int value );

/*!
 * @discussion gpio pin input level.
 * @param dgpio gpio descriptor.
 * @return value [0, 1].
 */
unsigned int rvHal_gpio_read( struct gpio_desc *dgpio );

/*!
 * @discussion toggle gpio pin output level.
 * @param dgpio gpio descriptor.
 */
void rvHal_gpio_toggle( struct gpio_desc *dgpio );
#endif /* HAL_INCLUDE_IO_H */

