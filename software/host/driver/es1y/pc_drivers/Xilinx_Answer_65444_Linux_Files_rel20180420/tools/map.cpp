#include <map>
// for uint*_t data types
#include <stdint.h>

// for print statements
#include <stdio.h>
// write to files
#include <fstream>
#include <cstdio>

#define AXI_DATA_BUS_WIDTH_IN_BYTES 64

uint8_t * mallocWord() {
	return (uint8_t *) malloc(sizeof(uint8_t)*AXI_DATA_BUS_WIDTH_IN_BYTES);
}

int main() {
	std::map<int, uint8_t *> sb;
	uint8_t *dataArr = mallocWord();
	sb[0] = dataArr;
  sb[1] = dataArr+0x20;
  sb[2] = dataArr+0x40;


  FILE *of;
  of = fopen("sb.log", "w");


	std::map<int, uint8_t *>::iterator  it=sb.begin();
  while(it != sb.end()){
    printf("%x %p\n", it->first, it->second);
    fprintf(of, "%x %p\n", it->first, it->second);
    
    it++;
  }
  fclose(of);

	return 0;

}
