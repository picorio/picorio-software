#include "pygmy_e.h"
#include <string.h>
#include <iostream>
#include <sstream>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <fstream>
#include <sstream>
#include <vector>

static uint8_t reg[8];
static time_t ltime;

#ifndef TARGET_ARCH
# define TARGET_ARCH "riscv64-unknown-elf"
#endif

#if !defined(PREFIX) && defined(__PCONFIGURE__PREFIX)
# define PREFIX __PCONFIGURE__PREFIX
#endif


#ifndef TARGET_DIR
# define TARGET_DIR "/" TARGET_ARCH "/bin/"
#endif

#define INSNS_PER_RTC_TICK 1
#define CPU_HZ 1000000000 
#define DRAM_BASE          0x80000000
#define CLINT_BASE         STATION_DMA_BASE_ADDR
#define CLINT_SIZE         0x000c0000
#define PLIC_BASE          STATION_ORV32_BASE_ADDR
#define PLIC_SIZE          0x000002a8
#define NUM_OF_INTR 22
#define PGSHIFT 12
#define PGSIZE  (1 << PGSHIFT)
#define DTC "/usr/bin/dtc"

// HELPERS
std::vector<std::string> split(const std::string& s, char delimiter)
{
   std::vector<std::string> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   while (std::getline(tokenStream, token, delimiter))
   {
      tokens.push_back(token);
   }
   return tokens;
}

char* multi_tok(char *s, char * delimiter)
{
    static char *string;
    if (s != NULL)
        string = s;

    if (string == NULL)
        return string;
    char *end = strstr(string, delimiter);
    if (end == NULL) {
        char *temp = string;
        string = NULL;
        return temp;
    }

    char *temp = string;

    *end = '\0';
    string = end + strlen(delimiter);
    return temp;
}

std::vector<std::string> split_multi(const std::string& s, char * delimiter)
{
  std::vector<std::string> tokens;
  char *c_s = new char[s.length() + 1];
  strcpy(c_s, s.c_str());
  char * token = multi_tok(c_s, delimiter);
  std::string str;
  while (token != NULL) {
    str.assign(token);
    tokens.push_back(str);
    //printf("%s\n", token);
    token = multi_tok(NULL, delimiter);
  }
  delete [] c_s;
  return tokens;

}

std::vector<uint64_t> get_breakpoints(std::string breakpoints){
  std::vector<uint64_t> breakpoint_vals;
  std::string delimiter = "_";
  size_t pos = 0;
  size_t sz;
  std::string bp;
  while ((pos=breakpoints.find(delimiter)) != std::string::npos) {
    bp = breakpoints.substr(0,pos);
    breakpoint_vals.push_back(std::stoul(bp));
    breakpoints.erase(0, pos + delimiter.length());
  }
  return breakpoint_vals;
}

void pygmy_e::set_breakpoints(std::vector<uint64_t> station_en_addr, std::vector<uint64_t> station_addr, std::vector<uint64_t> breakpoints){
  for (int i = 0; i < breakpoints.size(); i++){
    int idx = random() % station_en_addr.size();
    uint64_t en_addr = station_en_addr.at(idx);
    uint64_t addr = station_addr.at(idx);
    station_en_addr.erase(station_en_addr.begin() + idx);
    station_addr.erase(station_addr.begin() + idx);
    this->ht->OURSBUS_WRITE(en_addr, 0x00000001);
    this->ht->OURSBUS_WRITE(addr, breakpoints.at(i));
  }
}

static std::vector<std::pair<reg_t, mem_t*>> make_mems(const char* arg)
{
  // handle legacy mem argument
  char* p;
  auto mb = strtoull(arg, &p, 0);
  if (*p == 0) {
    reg_t size = reg_t(mb) << 20;
    if (size != (size_t)size)
      throw std::runtime_error("Size would overflow size_t");
    return std::vector<std::pair<reg_t, mem_t*>>(1, std::make_pair(reg_t(DRAM_BASE), new mem_t(size)));
  }

  // handle base/size tuples
  std::vector<std::pair<reg_t, mem_t*>> res;
  while (true) {
    auto base = strtoull(arg, &p, 0);
    if (!*p || *p != ':')
      exit(-1);
    auto size = strtoull(p + 1, &p, 0);
    if ((size | base) % PGSIZE != 0)
      exit(-1);
    res.push_back(std::make_pair(reg_t(base), new mem_t(size)));
    if (!*p)
      break;
    if (*p != ',')
      exit(-1);
    arg = p + 1;
  }
  return res;
}

static std::string dts_compile(const std::string& dts)
{
  // Convert the DTS to DTB
  int dts_pipe[2];
  pid_t dts_pid;

  fflush(NULL); // flush stdout/stderr before forking
  if (pipe(dts_pipe) != 0 || (dts_pid = fork()) < 0) {
    std::cerr << "Failed to fork dts child: " << strerror(errno) << std::endl;
    exit(1);
  }

  // Child process to output dts
  if (dts_pid == 0) {
    close(dts_pipe[0]);
    int step, len = dts.length();
    const char *buf = dts.c_str();
    for (int done = 0; done < len; done += step) {
      step = write(dts_pipe[1], buf+done, len-done);
      if (step == -1) {
        std::cerr << "Failed to write dts: " << strerror(errno) << std::endl;
        exit(1);
      }
    }
    close(dts_pipe[1]);
    exit(0);
  }

  pid_t dtb_pid;
  int dtb_pipe[2];
  if (pipe(dtb_pipe) != 0 || (dtb_pid = fork()) < 0) {
    std::cerr << "Failed to fork dtb child: " << strerror(errno) << std::endl;
    exit(1);
  }

  // Child process to output dtb
  if (dtb_pid == 0) {
    dup2(dts_pipe[0], 0);
    dup2(dtb_pipe[1], 1);
    close(dts_pipe[0]);
    close(dts_pipe[1]);
    close(dtb_pipe[0]);
    close(dtb_pipe[1]);
    execl(DTC, DTC, "-O", "dtb", 0);
    std::cerr << "Failed to run " DTC ": " << strerror(errno) << std::endl;
    exit(1);
  }

  close(dts_pipe[1]);
  close(dts_pipe[0]);
  close(dtb_pipe[1]);

  // Read-out dtb
  std::stringstream dtb;

  int got;
  char buf[4096];
  while ((got = read(dtb_pipe[0], buf, sizeof(buf))) > 0) {
    dtb.write(buf, got);
  }
  if (got == -1) {
    std::cerr << "Failed to read dtb: " << strerror(errno) << std::endl;
    exit(1);
  }
  close(dtb_pipe[0]);

  // Reap children
  int status;
  waitpid(dts_pid, &status, 0);
  if (!WIFEXITED(status) || WEXITSTATUS(status) != 0) {
    std::cerr << "Child dts process failed" << std::endl;
    exit(1);
  }
  waitpid(dtb_pid, &status, 0);
  if (!WIFEXITED(status) || WEXITSTATUS(status) != 0) {
    std::cerr << "Child dtb process failed" << std::endl;
    exit(1);
  }

  return dtb.str();
}

std::string make_dts(size_t insns_per_rtc_tick, size_t cpu_hz,
                     uint64_t num_cores, uint64_t max_xlen, std::string isa,
                     std::vector<std::pair<reg_t, mem_t*>> mems)
{
  std::stringstream s;
  reg_t plicbs = PLIC_BASE;
  reg_t plicsz = PLIC_SIZE;

  s << std::dec <<
         "/dts-v1/;\n"
         "\n"
         "/ {\n"
         "  #address-cells = <2>;\n"
         "  #size-cells = <2>;\n"
         "  compatible = \"ucbbar,spike-bare-dev\";\n"
         "  model = \"ucbbar,spike-bare\";\n"
         "  cpus {\n"
         "    #address-cells = <1>;\n"
         "    #size-cells = <0>;\n"
         "    timebase-frequency = <" << (cpu_hz/insns_per_rtc_tick) << ">;\n";
  for (size_t i = 0; i < num_cores; i++) {
    s << "    CPU" << i << ": cpu@" << i << " {\n"
         "      device_type = \"cpu\";\n"
         "      reg = <" << i << ">;\n"
         "      status = \"okay\";\n"
         "      compatible = \"riscv\";\n"
         //"      riscv,isa = \"" << procs[i]->get_isa_string() << "\";\n"
         "      riscv,isa = \"" << isa << "\";\n"
         //"      mmu-type = \"riscv," << (procs[i]->get_max_xlen() <= 32 ? "sv32" : "sv39") << "\";\n"
         // max_xlen == 0 means max_vlen == 32
         "      mmu-type = \"riscv," << (max_xlen <= 0 ? "sv32" : "sv39") << "\";\n"
         "      clock-frequency = <" << cpu_hz << ">;\n"
         "      CPU" << i << "_intc: interrupt-controller {\n"
         "        #interrupt-cells = <1>;\n"
         "        interrupt-controller;\n"
         "        compatible = \"riscv,cpu-intc\";\n"
         "      };\n"
         "    };\n";
  }
  s <<   "  };\n";
  for (auto& m : mems) {
    s << std::hex <<
         "  memory@" << m.first << " {\n"
         "    device_type = \"memory\";\n"
         "    reg = <0x" << (m.first >> 32) << " 0x" << (m.first & (uint32_t)-1) <<
                   " 0x" << (m.second->size() >> 32) << " 0x" << (m.second->size() & (uint32_t)-1) << ">;\n"
         "  };\n";
  }
  s <<   "  soc {\n"
         "    #address-cells = <2>;\n"
         "    #size-cells = <2>;\n"
         "    compatible = \"ucbbar,spike-bare-soc\", \"simple-bus\";\n"
         "    ranges;\n";
#if 1//plic
         s<<"    plic0: interrupt-controller@" << plicbs << " {\n"
         "      #interrupt-cells = <1>;\n"
         "      compatible = \"rivai,plic-1.0.0\";\n"
         "      interrupt-controller;\n"
         "      reg = <0x" << (plicbs >> 32) << " 0x" << (plicbs & (uint32_t)-1) <<
                     " 0x" << (plicsz >> 32) << " 0x" << (plicsz & (uint32_t)-1) << ">;\n";
         s<<std::dec<<"      riscv,ndev = <" << NUM_OF_INTR << ">;\n"
         "    };\n";
#endif
         s <<std::hex<<"    clint@" << CLINT_BASE << " {\n"
         "      compatible = \"riscv,clint0\";\n"
         "      interrupts-extended = <";
  for (size_t i = 0; i < num_cores; i++)
    s << "&CPU" << i << "_intc 3 &CPU" << i << "_intc 7 ";
  reg_t clintbs = CLINT_BASE;
  reg_t clintsz = CLINT_SIZE;
  s << std::hex << ">;\n"
         "      reg = <0x" << (clintbs >> 32) << " 0x" << (clintbs & (uint32_t)-1) <<
                     " 0x" << (clintsz >> 32) << " 0x" << (clintsz & (uint32_t)-1) << ">;\n"
         "    };\n"
         "  };\n"
         "  htif {\n"
         "    compatible = \"ucb,htif0\";\n"
         "  };\n"
         "};\n";
  return s.str();
}

//static uint32_t make_dtb(uint64_t data, std::vector<std::pair<reg_t, mem_t*>> mems, uint64_t max_xlen, std::string isa, uint32_t * ret_rom[])
static uint32_t make_dtb(uint64_t data, std::vector<std::pair<reg_t, mem_t*>> mems, uint32_t * ret_rom[])
{
  uint64_t start_pc = 0x80000000;
  std::string isa_arr[] = {"i","m","a", "f", "d", "c"};
  uint8_t isa_length = 6;
  const int reset_vec_size = 8;
  
  uint8_t num_cores = data & 0xff;
  //fprintf(stderr, "DATA %016x\n", data);
  fprintf(stderr, "NUM CORES %x\n", num_cores);
  uint8_t max_xlen = (data >> 8) & 0x1;
  uint64_t hz = ((data >> 9) & 0xffff)*1000000;
  fprintf(stderr, "HZ %d\n", hz);
  uint8_t isa = (data >> 25) & 0x1;
  std::string isa_str = "rv32";
  if (isa == 0x1) {
    isa_str = "rv64";
  }
  
  isa = (data >> 26) & 0x3f;
  //fprintf(stderr, "ISA %02x\n", isa);
  for (int i= 0; i < isa_length ; ++i) {
    if (((isa >> i) & 0x1) == 0x1) {
      isa_str = isa_str + isa_arr[i]; 
    }
  }
  fprintf(stderr, "ISA: %s\n", isa_str.c_str());

  //start_pc = start_pc == reg_t(-1) ? get_entry_point() : start_pc;
  uint64_t start_pc_dtb = start_pc;

  uint32_t reset_vec[reset_vec_size] = {
    0x297,                                      // auipc  t0,0x0
    0x28593 + (reset_vec_size * 4 << 20),       // addi   a1, t0, &dtb
    0xf1402573,                                 // csrr   a0, mhartid
    //get_core(0)->get_xlen() == 32 ?
    max_xlen == 0 ? // 0 means 32, otherwise it's 64
      0x0182a283u :                             // lw     t0,24(t0)
      0x0182b283u,                              // ld     t0,24(t0)
    0x28067,                                    // jr     t0
    0,
    (uint32_t) (start_pc_dtb & 0xffffffff),
    (uint32_t) (start_pc_dtb >> 32)
  };

  std::vector<char> rom((char*)reset_vec, (char*)reset_vec + sizeof(reset_vec));

  std::string dts = make_dts(INSNS_PER_RTC_TICK, hz, num_cores, max_xlen, 
                             isa_str, mems);
  std::string dtb = dts_compile(dts);

  rom.insert(rom.end(), dtb.begin(), dtb.end());

  const int align = 0x1000;
  rom.resize((rom.size() + align - 1) / align * align);
  
  uint64_t size = ((rom.size()/4) + 1);
  *ret_rom = new uint32_t[size];
  uint32_t tmp = 0;
  int idx = 0;
  int count = 0;
  //fprintf(stderr, "ROM INFO:\n");
  for (std::vector<char>::const_iterator i = rom.begin(); i != rom.end(); ++i) {
    tmp = tmp >> 8;
    tmp = tmp | ((uint32_t)(*i & 0xff) << 24);
    if (idx % 4 == 3) {
      //fprintf(stderr, "0x%08x\n", tmp);
      (*ret_rom)[count] = tmp;
      count++;
    }
    idx ++;
  }
  if ((idx % 4) != 0) {
    while ((idx % 4) != 3) {
      tmp = tmp >> 8;
      idx++;
    }
    //fprintf(stderr, "0x%08x\n", tmp);
    (*ret_rom)[count] = tmp;
    count++;
  }

  return count;
  //return ret_rom;
}

// MAIN START UP FUNCTIONS
void pygmy_e::setup(){
  std::vector<func_t>::iterator it = this->boot_sequence.begin();
  for (;it != this->boot_sequence.end(); ++it){
    if (strcmp(it->key.c_str(), "load_pk") == 0) {
      func_pointer run_function = it->ptr;
      (this->*run_function)(this->ht->argmap[it->key.c_str()]);
    } else {
      if(this->ht->argmap[it->key.c_str()].length()>0){
        func_pointer run_function = it->ptr;
        (this->*run_function)(this->ht->argmap[it->key.c_str()]);
      }
    }
  }
}

void pygmy_e::chip_config(std::string key) {
  char* pEnd;
  uint64_t data = 0;
  std::string config_str = this->ht->argmap["chip_config="];
  //std::vector<std::string> configs = split(config_str, '_');
  std::string delim = "__";
  char *c_delim = new char[delim.length() + 1];
  strcpy(c_delim, delim.c_str());
  std::vector<std::string> configs = split_multi(config_str, c_delim);
  delete [] c_delim;

  //for (auto vit = configs.begin(); vit != configs.end(); ++vit) {
  //  fprintf(stderr, " %s ", (*vit).c_str());
  //}
  for (auto vit = configs.begin(); vit != configs.end(); ++vit) {
    std::vector<std::string> config = split(*vit, '_');
    // bits[0:7] for number of cores
    if (config[0].compare("nc") == 0) {
      uint8_t num_cores = std::strtol(config[1].c_str(), NULL, 10); 
      data |= num_cores;
    } 
    // bit[8] is for xlen (64/32)
    else if (config[0].compare("xlen") == 0) {
      uint64_t xlen = std::strtol(config[1].c_str(), NULL, 10);
      if (xlen == 64) {
        data |= (0x1 << 8);
      }
      else {
        data &= ~(0x1 << 8);
      }
    } 
    // bit[9:24] for frequency
    else if (config[0].compare("f") == 0) {
      uint16_t frequency = std::strtol(config[1].c_str(), NULL, 10);
      data |= (frequency << 9);
    }
    // bit[25:31] for isa
    else if (config[0].compare("isa") == 0) {
      std::string isa = config[1];
    
      // Bit 25 high means it is rv64 otherwise rv32
      if (isa.find("64") != std::string::npos) {
        data |= (0x1 << 25);
      }
      else if (isa.find("32") != std::string::npos) {
        data &= ~(0x1 << 25);
      }
      // Bit 26 high means i extension is enabled
      if (isa.find("i") != std::string::npos) {
        data |= (0x1 << 26);
      }
      // Bit high 27 means m extension is enabled
      if (isa.find("m") != std::string::npos) {
        data |= (0x1 << 27);
      }
      // Bit high 28 means a extension is enabled
      if (isa.find("a") != std::string::npos) {
        data |= (0x1 << 28);
      }
      // Bit high 29 means f extension is enabled
      if (isa.find("f") != std::string::npos) {
        data |= (0x1 << 29);
      }
      // Bit high 30 means d  extension is enabled
      if (isa.find("d") != std::string::npos) {
        data |= (0x1 << 30);
      }
      // Bit high 31 means c  extension is enabled
      if (isa.find("c") != std::string::npos) {
        data |= (0x1 << 31);
      }
    } 
  }
  //std::string mode = this->ht->argmap["log_step="];
  //if (mode.compare("beak") == 0) { 
    this->ht->OURSBUS_WRITE(STATION_DMA_SYSTEM_CFG_ADDR, data); 
  //}
}

void pygmy_e::program_pll(std::string key) {
  ltime = time(NULL);
  fprintf(stderr, "Start Programming PLL @ %s\n", asctime(localtime(&ltime)));

  // Keep RESET and BYPASS
  char* fin;
  char* fout;
  char* pEnd;
  char* tmp;
  char pll_str[256];
  uint64_t nr, nf, od, nb;
  strcpy(pll_str, key.c_str());

  fin = strtok(pll_str, "_");
  fout = strtok(NULL, "_");

  char cmd[256];
  char cfg[256];
  sprintf(cmd, "/work/tech/TCI_PLL/TCITSMCN28HPCPGPMPLLA1_calc.csh -n %s %s | awk '{ if (NR == 3) print $1,$2,$3,$4 }'", fin, fout);
  FILE* result = popen(cmd, "r");

  fgets(cfg, 255, result);
  fprintf(stderr, "cmd = %s\ncfg = %s\n", cmd, cfg);

  tmp = strtok(cfg, " ");
  nr = std::strtoul(tmp, &pEnd, 10);
  tmp = strtok(NULL, " ");
  nf = std::strtoul(tmp, &pEnd, 10);
  tmp = strtok(NULL, " ");
  od = std::strtoul(tmp, &pEnd, 10);
  tmp = strtok(NULL, " ");
  nb = std::strtoul(tmp, &pEnd, 10);

  // Program
  this->ht->OURSBUS_WRITE(STATION_SLOW_IO_SCU_FUNC_CLKR_ADDR, nr - 1);
  this->ht->OURSBUS_WRITE(STATION_SLOW_IO_SCU_FUNC_CLKF_ADDR, nf - 1);
  this->ht->OURSBUS_WRITE(STATION_SLOW_IO_SCU_FUNC_CLKOD_ADDR, od - 1);
  this->ht->OURSBUS_WRITE(STATION_SLOW_IO_SCU_FUNC_BWADJ_ADDR, nb - 1);
  this->ht->OURSBUS_WRITE(STATION_SLOW_IO_SCU_FUNC_INTFB_ADDR, 1);

  // Assert PLL_PROG_DONE
  this->ht->OURSBUS_WRITE(STATION_SLOW_IO_SCU_FUNC_PROG_DONE_ADDR, 1);

  // Wait for Lock
  usleep(10);

  // Wait for Refclk
  usleep(1);

  // Drop PLL_BYPASS and Set CLK_SEL
  this->ht->OURSBUS_WRITE(STATION_SLOW_IO_SCU_FUNC_BYPASS_ADDR, 0);
  // Drop PLL_RESET
  this->ht->OURSBUS_WRITE(STATION_SLOW_IO_SCU_FUNC_RESET_ADDR, 0);
  this->ht->OURSBUS_WRITE(STATION_SLOW_IO_SCU_FUNC_CLK_SEL_ADDR, 1);

  ltime = time(NULL);
  fprintf(stderr, "Done Programming PLL @ %s\n", asctime(localtime(&ltime)));
}

void pygmy_e::reset_sdio(std::string key) {
  uint64_t rdata = 0x0;
  ltime = time(NULL);
  fprintf(stderr, "Start Reseting SDIO @ %s\n", asctime(localtime(&ltime)));
  // Core reset
  this->ht->OURSBUS_WRITE(STATION_SDIO_S2ICG_PLLCLK_EN_ADDR, 0x1);
  for (int i = 0; i < 3; i++) {
    this->ht->OURSBUS_READ_4B(STATION_SDIO_S2ICG_PLLCLK_EN_ADDR, rdata);
  }
  this->ht->OURSBUS_WRITE(STATION_SDIO_S2B_RESETN_ADDR, 0x1);
  while (rdata != 0x1) {
    this->ht->OURSBUS_READ_4B(STATION_SDIO_S2B_RESETN_ADDR, rdata);
  }
  ltime = time(NULL);
  fprintf(stderr, "Done Reseting SDIO @ %s\n", asctime(localtime(&ltime)));
}

void pygmy_e::config_l2(std::string key) {
  uint64_t ctrl_reg;
  ltime = time(NULL);
  fprintf(stderr, "Start Configuring L2 @ %s\n", asctime(localtime(&ltime)));
  if(this->ht->argmap["config_l2="] == "test") {
    ctrl_reg = (0x5555ul << 28);
    this->ht->OURSBUS_WRITE(STATION_CACHE_S2B_CTRL_REG_ADDR_0, ctrl_reg);
    this->ht->OURSBUS_WRITE(STATION_CACHE_S2B_CTRL_REG_ADDR_1, ctrl_reg);
    ctrl_reg = (0x0000ul << 28);
    this->ht->OURSBUS_WRITE(STATION_CACHE_S2B_CTRL_REG_ADDR_0, ctrl_reg);
    this->ht->OURSBUS_WRITE(STATION_CACHE_S2B_CTRL_REG_ADDR_1, ctrl_reg);
  } else if(this->ht->argmap["config_l2="] == "shutdown") {
    ctrl_reg = (0xfffful << 28);
    this->ht->OURSBUS_WRITE(STATION_CACHE_S2B_CTRL_REG_ADDR_0, ctrl_reg);
    this->ht->OURSBUS_WRITE(STATION_CACHE_S2B_CTRL_REG_ADDR_1, ctrl_reg);
  } else if(this->ht->argmap["config_l2="] == "sleep") {
    ctrl_reg = (0x5555ul << 28);
    this->ht->OURSBUS_WRITE(STATION_CACHE_S2B_CTRL_REG_ADDR_0, ctrl_reg);
    this->ht->OURSBUS_WRITE(STATION_CACHE_S2B_CTRL_REG_ADDR_1, ctrl_reg);
  } else if(this->ht->argmap["config_l2="] == "lp") {
    int n_way;
    int sd[7]; // way 0 is always on
    n_way = (random() % 7) + 1;
    ctrl_reg = 0;
    for (int i = 0; i < n_way; i++) {
      sd[i] = (random() % 2) + 2; // 2-sleep, 3-sd
      ctrl_reg = ctrl_reg | (sd[i] << (2*(7 - i)));
    }
    ctrl_reg = (ctrl_reg << 28);
    this->ht->OURSBUS_WRITE(STATION_CACHE_S2B_CTRL_REG_ADDR_0, ctrl_reg);
    this->ht->OURSBUS_WRITE(STATION_CACHE_S2B_CTRL_REG_ADDR_1, ctrl_reg);
    fprintf(stderr, "L2 LP mode, ctrl_reg = %llx", ctrl_reg);
  }
  ltime = time(NULL);
  fprintf(stderr, "Done Configuring L2 @ %s\n", asctime(localtime(&ltime)));
}


void pygmy_e::incr_test_io_freq(std::string key) {
      std::string config_str = this->ht->argmap["incr_test_io_freq="];
        uint64_t value = atoi(config_str.c_str());

          printf("incr_test_io_freq: 0x%lx\n", value);
          this->ht->OURSBUS_WRITE(STATION_SDIO_S2B_TEST_IO_CLKDIV_HALF_DIV_LESS_1_ADDR, value);
}

void pygmy_e::config_l1(std::string key) {
  ltime = time(NULL);
  fprintf(stderr, "Start Configuring L1 @ %s\n", asctime(localtime(&ltime)));
  if(this->ht->argmap["config_l1="] == "nobypass") {
    this->ht->OURSBUS_WRITE(STATION_ORV32_S2B_CFG_BYPASS_IC_ADDR, 0);
  }
  ltime = time(NULL);
  fprintf(stderr, "Done Configuring L1 @ %s\n", asctime(localtime(&ltime)));
}

#define release_bank_reset(proj, target) \
  void proj::release_bank##target##_reset(std::string key) { \
    uint64_t rdata = 0; \
    ltime = time(NULL); \
    fprintf(stderr, "Start Releasing Reset for Bank %0d @ %s\n", target, asctime(localtime(&ltime))); \
    char arg[100]; \
    sprintf(arg, "release_bank%d_reset", #target); \
    this->ht->OURSBUS_WRITE(STATION_CACHE_S2ICG_CLK_EN_ADDR_##target, 1); \
    for (int i = 0; i < 3; i++) { \
      this->ht->OURSBUS_READ_4B(STATION_CACHE_S2ICG_CLK_EN_ADDR_##target, rdata); \
    } \
    this->ht->OURSBUS_WRITE(STATION_CACHE_S2B_RSTN_ADDR_##target, 1); \
    while (rdata != 1) { \
      this->ht->OURSBUS_READ_4B(STATION_CACHE_S2B_RSTN_ADDR_##target, rdata); \
    } \
    ltime = time(NULL); \
    fprintf(stderr, "Done Releasing Reset for Bank %0d @ %s\n", target, asctime(localtime(&ltime))); \
   } \

release_bank_reset(pygmy_e, 0)
release_bank_reset(pygmy_e, 1)

void pygmy_e::release_reset_for_l2(std::string key) {
  release_bank0_reset(key);
  release_bank1_reset(key);
}

#define release_early_reset(proj, target, addr) \
  void proj::release_##target##_early_reset(std::string key) { \
    ltime = time(NULL); \
    fprintf(stderr, "Start Releasing Early Reset for %s @ %s\n", #target, asctime(localtime(&ltime))); \
    char arg[100]; \
    sprintf(arg, "release_%s_early_reset", #target); \
    this->ht->OURSBUS_WRITE(addr, 1); \
    ltime = time(NULL); \
    fprintf(stderr, "Done Releasing Early Reset for %s @ %s\n", #target, asctime(localtime(&ltime))); \
  }

release_early_reset(pygmy_e, orv32, STATION_ORV32_S2B_EARLY_RSTN_ADDR)

#define set_zero_to_l1_tagram(proj, target, addr_way0, addr_way1) \
  void proj::set_0_to_##target##_l1_tagram(std::string key) { \
    uint64_t wdata_way0, wdata_way1; \
    uint64_t rdata_way0, rdata_way1; \
    ltime = time(NULL); \
    fprintf(stderr, "Start Zeroing Out TAG RAM for %s L1 @ %s\n", #target, asctime(localtime(&ltime))); \
    for (int index = 0; index < 128; index ++) { \
      wdata_way0 = (random() << 32) + random(); \
      wdata_way1 = (random() << 32) + random(); \
      this->ht->OURSBUS_WRITE(addr_way0 + index * 8, wdata_way0); \
      this->ht->OURSBUS_WRITE(addr_way1 + index * 8, wdata_way1); \
      this->ht->OURSBUS_READ_4B(addr_way0 + index * 8, rdata_way0); \
      this->ht->OURSBUS_READ_4B(addr_way1 + index * 8, rdata_way1); \
      if ((rdata_way0 != (wdata_way0 & 0xffffff)) || (rdata_way1 != (wdata_way1 & 0xffffff))) { \
        fprintf(stderr, "Error: TAG RAM %s access wrong, index = %0d, wdata_way0 = 0x%lx, wdata_way1 = 0x%lx, rdata_way0 = 0x%lx, rdata_way1 = 0x%lx\n", #target, index, wdata_way0, wdata_way1, rdata_way0, rdata_way1); \
      } \
      this->ht->OURSBUS_WRITE(addr_way0 + index * 8, 0); \
      this->ht->OURSBUS_WRITE(addr_way1 + index * 8, 0); \
    } \
    ltime = time(NULL); \
    fprintf(stderr, "Done Zeroing Out TAG RAM for %s L1 @ %s\n", #target, asctime(localtime(&ltime))); \
  }

set_zero_to_l1_tagram(pygmy_e, orv32, STATION_ORV32_IC_TAG_WAY_0_ADDR, STATION_ORV32_IC_TAG_WAY_1_ADDR)

void pygmy_e::load_kernel(std::string key){
//   std::map<std::string, uint64_t> symbols;
  ltime = time(NULL);
  if (this->ht->argmap["load_pk"].length() > 0) {
    fprintf(stderr, "Start Loading Kernel @ %s\n", asctime(localtime(&ltime)));
    this->ht->symbols = load_elf(this->ht->path.c_str(), &(this->ht->mem), &(this->ht->entry));
    //this->ht->symbols_s = load_elf_s(this->ht->path.c_str(), &(this->ht->mem), &(this->ht->entry));
    //for (auto it = this->ht->addr_to_symbols.begin(); it != this->ht->addr_to_symbols.end(); ++it){
    //  fprintf(stderr, "KEY: %016x VALUE: %s \n", it->first, it->second.c_str());
    //}
    //fprintf(stderr,"--------------------\n");
    if (this->ht->argmap["load_2nd_file"].length() == 0) {
      this->ht->program_symbols = this->ht->symbols;
      //this->ht->program_symbols_s = this->ht->symbols_s;
      //this->ht->program_symbols = load_elf_symbols_only(this->ht->path.c_str(), &this->ht->mem, &(this->ht->entry));
      //for (auto it = this->ht->program_symbols.begin(); it != this->ht->program_symbols.end(); ++it){
      //  fprintf(stderr, "KEY: %s VALUE: %016x \n", it->first.c_str(), it->second);
      //}
    }
  } else {
    if (this->ht->argmap["filename1"].length() > 0) {
      fprintf(stderr, "Start Loading Kernel Symbols @ %s\n", asctime(localtime(&ltime)));
      this->ht->symbols = load_elf_symbols_only(this->ht->path.c_str(), &this->ht->mem, &(this->ht->entry));
    }
  }
  for (std::map<std::string, uint64_t>::iterator it = this->ht->symbols.begin(); it != this->ht->symbols.end(); ++it) {
    if (it->first.substr(0, 6) == "tohost") {
      if (it->first.length() == 6) {
        this->ht->tohost_addr = it->second;
      } else if (it->first.length() > 6) {
        if (it->first.substr(6, 1) == ".") {
          this->ht->tohost_addr = it->second;
        }
      }
    }
    if (it->first.substr(0, 8) == "fromhost") {
      if (it->first.length() == 8) {
        this->ht->fromhost_addr = it->second;
      } else if (it->first.length() > 8) {
        if (it->first.substr(8, 1) == ".") {
          this->ht->fromhost_addr = it->second;
        }
      }
    }
  }
  if ((this->ht->tohost_addr == 0) && (this->ht->fromhost_addr == 0)) {
    fprintf(stderr, "warning: tohost and fromhost symbols not in ELF; can't communicate with target\n");
  }
  ltime = time(NULL);
  if (this->ht->argmap["load_pk"].length() > 0) {
    fprintf(stderr, "Done Loading Kernel @ %s\n", asctime(localtime(&ltime)));
  } else {
    fprintf(stderr, "Done Loading Kernel Symbols @ %s\n", asctime(localtime(&ltime)));
  }
  if (this->ht->argmap["filename2"].length() > 0) {
    std::string path;
    if (access(this->ht->argmap["filename2"].c_str(), F_OK) == 0)
      path = this->ht->argmap["filename2"];
    else if (this->ht->argmap["filename2"].find('/') == std::string::npos)
    {
      std::string test_path = PREFIX TARGET_DIR + this->ht->argmap["filename2"];
      if (access(test_path.c_str(), F_OK) == 0)
        path = test_path;
    }

    if (path.empty())
      throw std::runtime_error("could not open " + this->ht->argmap["filename2"]);

    ltime = time(NULL);
    if (this->ht->argmap["load_2nd_file"].length() > 0) {
      fprintf(stderr, "Start Loading 2nd File as ELF, Symbols will be put in program_symbols @ %s\n", asctime(localtime(&ltime)));
      this->ht->program_symbols = load_elf(path.c_str(), &this->ht->mem, &(this->ht->entry));
      this->ht->program_symbols_s = load_elf_s(path.c_str(), &this->ht->mem, &(this->ht->entry));
    } else {
      fprintf(stderr, "Start Loading 2nd File Symbols, will be put in program_symbols @ %s\n", asctime(localtime(&ltime)));
      this->ht->program_symbols = load_elf_symbols_only(path.c_str(), &this->ht->mem, &(this->ht->entry));
      this->ht->program_symbols_s = load_elf_symbols_only_s(path.c_str(), &this->ht->mem, &(this->ht->entry));
    }
    ltime = time(NULL);
    if (this->ht->argmap["load_2nd_file"].length() > 0) {
      fprintf(stderr, "Done Loading 2nd File as ELF @ %s\n", asctime(localtime(&ltime)));
    } else {
      fprintf(stderr, "Done Loading 2nd File Symbols @ %s\n", asctime(localtime(&ltime)));
    }
  }

  if (this->ht->argmap["filename3"].length() > 0) {
    std::string path;
    if (access(this->ht->argmap["filename3"].c_str(), F_OK) == 0)
      path = this->ht->argmap["filename3"];
    else if (this->ht->argmap["filename3"].find('/') == std::string::npos)
    {
      std::string test_path = PREFIX TARGET_DIR + this->ht->argmap["filename3"];
      if (access(test_path.c_str(), F_OK) == 0)
        path = test_path;
    }

    if (path.empty())
      throw std::runtime_error("could not open " + this->ht->argmap["filename3"]);
    ltime = time(NULL);
    if (this->ht->argmap["load_3rd_file"].length() > 0) {
      fprintf(stderr, "Start Loading 3rd File as ELF, Symbols will be put in program_symbols @ %s\n", asctime(localtime(&ltime)));
      this->ht->program_symbols = load_elf(path.c_str(), &this->ht->mem, &(this->ht->entry));
      this->ht->program_symbols_s = load_elf_s(path.c_str(), &this->ht->mem, &(this->ht->entry));
    } else {
      fprintf(stderr, "Start Loading 3rd File Symbols, will be put in program_symbols @ %s\n", asctime(localtime(&ltime)));
      this->ht->program_symbols = load_elf_symbols_only(path.c_str(), &this->ht->mem, &(this->ht->entry));
      this->ht->program_symbols_s = load_elf_symbols_only_s(path.c_str(), &this->ht->mem, &(this->ht->entry));
    }
    ltime = time(NULL);
    if (this->ht->argmap["load_3rd_file"].length() > 0) {
      fprintf(stderr, "Done Loading 3rd File as ELF @ %s\n", asctime(localtime(&ltime)));
    } else {
      fprintf(stderr, "Done Loading 3rd File Symbols @ %s\n", asctime(localtime(&ltime)));
    }
  }
  for (auto it = this->ht->symbols_s.begin(); it != this->ht->symbols_s.end(); ++it){
    this->ht->addr_to_symbols[it->second] = it->first;
    //fprintf(stderr, "KEY: %s VALUE: %016x \n", it->first.c_str(), it->second);
  }
  auto it = this->ht->symbols_s.find("_payload_start");
  int64_t offset = 0;
  if (it != this->ht->symbols_s.end()) {
    auto vit = this->ht->program_symbols_s.find("_start");
    //offset = it->second - vit->second;
    offset = vit->second - it->second;
  }
  for (auto it = this->ht->program_symbols_s.begin(); it != this->ht->program_symbols_s.end(); ++it) {
    this->ht->addr_to_program_symbols[it->second + offset] = it->first;
    //fprintf(stderr, "KEY: %s VALUE: %016x \n", it->first.c_str(), it->second);
  }
}

#define set_reset_pc(proj, target, addr) \
  void proj::set_##target##_reset_pc(std::string key){ \
    ltime = time(NULL); \
    fprintf(stderr, "Start Setting Reset PC for %s @ %s\n", #target, asctime(localtime(&ltime))); \
    size_t sz; \
    char arg[100]; \
    sprintf(arg, "set_%s_rst_pc=", #target); \
    uint64_t rst_pc = std::stoul(this->ht->argmap[arg], &sz, 16); \
    this->ht->OURSBUS_WRITE(addr, rst_pc); \
    ltime = time(NULL); \
    fprintf(stderr, "Done Setting Reset PC for %s @ %s\n", #target, asctime(localtime(&ltime))); \
  }

set_reset_pc(pygmy_e, orv32, STATION_ORV32_S2B_CFG_RST_PC_ADDR)

void pygmy_e::load_rom(std::string key){
  ltime = time(NULL);
  fprintf(stderr, "Start Loading ROM @ %s\n", asctime(localtime(&ltime)));
  uint64_t data; 
  std::string mode = this->ht->argmap["log_step="];
 // if (mode.compare("beak") == 0) { 
 //   if (this->ht->argmap["chip_config="].length() > 0) {
       this->ht->OURSBUS_READ(STATION_DMA_SYSTEM_CFG_ADDR, data); 
 //   } else {
 //     // Set default value, simplify sim arg
 //     data = 0x000000001e04b101;
 //   }
 // } else {
 //   data = 0x000000001e04b101;
 // }
  uint32_t * rom;
  uint32_t rom_length = make_dtb(data, make_mems("2048"), &rom);
  //fprintf(stderr, "OUT %d:\n", rom_length);
  //for (int i= 0; i < rom_length; ++i){
  //  fprintf(stderr,"0x%08x\n", rom[i]);
  //}
  uint64_t addr;
  if (this->ht->argmap["linux_rom"].length()>0) {
    addr = 0x80016000;
  } else {
    addr = 0x80010000;
  }
  //for (int i = 0; i < 131; i++) {
  for (int i = 0; i < rom_length/2; i++) {
    reg[7] = (rom[i * 2 + 1] >> 24) & 0xff;
    reg[6] = (rom[i * 2 + 1] >> 16) & 0xff;
    reg[5] = (rom[i * 2 + 1] >>  8) & 0xff;
    reg[4] = (rom[i * 2 + 1] >>  0) & 0xff;
    reg[3] = (rom[i * 2 + 0] >> 24) & 0xff;
    reg[2] = (rom[i * 2 + 0] >> 16) & 0xff;
    reg[1] = (rom[i * 2 + 0] >>  8) & 0xff;
    reg[0] = (rom[i * 2 + 0] >>  0) & 0xff;
    this->ht->mem.write(addr, 8, (uint8_t*)reg);
    addr += 8;
  }
  if (rom_length % 2 == 1 ){
    reg[7] = (0x00 >> 24) & 0xff;
    reg[6] = (0x00 >> 16) & 0xff;
    reg[5] = (0x00 >>  8) & 0xff;
    reg[4] = (0x00 >>  0) & 0xff;
    reg[3] = (rom[rom_length-1 + 0] >> 24) & 0xff;
    reg[2] = (rom[rom_length-1 + 0] >> 16) & 0xff;
    reg[1] = (rom[rom_length-1 + 0] >>  8) & 0xff;
    reg[0] = (rom[rom_length-1 + 0] >>  0) & 0xff;
    this->ht->mem.write(addr, 8, (uint8_t*)reg);
  }
  ltime = time(NULL);
  //free(rom);
  fprintf(stderr, "Done Loading ROM @ %s\n", asctime(localtime(&ltime)));
}

// symbols
void pygmy_e::release_orv32_reset(std::string key){
    ltime = time(NULL);
    fprintf(stderr, "Start Releasing ORV32 Reset @ %s\n", asctime(localtime(&ltime)));
    // Set Breakpoints
    if (this->ht->argmap["test_bp="].length()>0){
      std::string mode = this->ht->argmap["test_bp="];
      if (mode.compare("step_through") == 0){
        this->ht->OURSBUS_WRITE(STATION_ORV32_S2B_DEBUG_STALL_ADDR,0x00000001);
      }
      std::vector<uint64_t> breakpoints = get_breakpoints(this->ht->argmap["bp_pc="]);
      std::vector<uint64_t> station_en_addr={STATION_ORV32_S2B_EN_BP_IF_PC_0_ADDR,
                                             STATION_ORV32_S2B_EN_BP_IF_PC_1_ADDR,
                                             STATION_ORV32_S2B_EN_BP_IF_PC_2_ADDR,
                                             STATION_ORV32_S2B_EN_BP_IF_PC_3_ADDR,
                                             STATION_ORV32_S2B_EN_BP_WB_PC_0_ADDR,
                                             STATION_ORV32_S2B_EN_BP_WB_PC_1_ADDR,
                                             STATION_ORV32_S2B_EN_BP_WB_PC_2_ADDR,
                                             STATION_ORV32_S2B_EN_BP_WB_PC_3_ADDR
                                            };
      std::vector<uint64_t> station_addr={STATION_ORV32_S2B_BP_IF_PC_0_ADDR,
                                          STATION_ORV32_S2B_BP_IF_PC_1_ADDR,
                                          STATION_ORV32_S2B_BP_IF_PC_2_ADDR,
                                          STATION_ORV32_S2B_BP_IF_PC_3_ADDR,
                                          STATION_ORV32_S2B_BP_WB_PC_0_ADDR,
                                          STATION_ORV32_S2B_BP_WB_PC_1_ADDR,
                                          STATION_ORV32_S2B_BP_WB_PC_2_ADDR,
                                          STATION_ORV32_S2B_BP_WB_PC_3_ADDR
                                          };
      set_breakpoints(station_en_addr, station_addr, breakpoints);
    }
    this->ht->OURSBUS_WRITE(STATION_ORV32_S2B_EARLY_RSTN_ADDR, 1);
    this->ht->OURSBUS_WRITE(STATION_ORV32_S2B_RSTN_ADDR, 1);
    ltime = time(NULL);
    fprintf(stderr, "Done Releasing ORV32 Reset @ %s\n", asctime(localtime(&ltime)));
}

// IO reg access
void pygmy_e::io_reg_test(std::string key){
  fprintf(stderr, "Start io reg testing @ %s\n", asctime(localtime(&ltime)));
  uint64_t wdata;
  uint64_t rdata = 0;
  uint64_t addr;
  uint64_t orig_data = 0;

  // QSPIM
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_QSPIM_BLOCK_REG_ADDR + 0x5c, rdata);
  if (rdata != 0x3430322a) {
    fprintf(stderr, "Error: qspim version id wrong, rdata = 0x%x\n", rdata);
  }
  // SSPIM0
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_SSPIM0_BLOCK_REG_ADDR + 0x5c, rdata);
  if (rdata != 0x3430312a) {
    fprintf(stderr, "Error: sspim0 version id wrong, rdata = 0x%x\n", rdata);
  }
  // SSPIM1
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_SSPIM1_BLOCK_REG_ADDR + 0x5c, rdata);
  if (rdata != 0x3430312a) {
    fprintf(stderr, "Error: sspim1 version id wrong, rdata = 0x%x\n", rdata);
  }
  // SSPIM2
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_SSPIM2_BLOCK_REG_ADDR + 0x5c, rdata);
  if (rdata != 0x3430312a) {
    fprintf(stderr, "Error: sspim2 version id wrong, rdata = 0x%x\n", rdata);
  }
  // SPIS
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_SPIS_BLOCK_REG_ADDR + 0x5c, rdata);
  if (rdata != 0x3430312a) {
    fprintf(stderr, "Error: spis version id wrong, rdata = 0x%x\n", rdata);
  }
  // UART0
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_UART0_BLOCK_REG_ADDR + 0xf8, rdata);
  if (rdata != 0x3430312a) {
    fprintf(stderr, "Error: uart0 lsr reg reset value wrong, rdata = 0x%x\n", rdata);
  }
  // UART1
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_UART1_BLOCK_REG_ADDR + 0xf8, rdata);
  if (rdata != 0x3430312a) {
    fprintf(stderr, "Error: uart1 lsr reg reset value wrong, rdata = 0x%x\n", rdata);
  }
  // UART2
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_UART2_BLOCK_REG_ADDR + 0xf8, rdata);
  if (rdata != 0x3430312a) {
    fprintf(stderr, "Error: uart2 lsr reg reset value wrong, rdata = 0x%x\n", rdata);
  }
  // UART3
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_UART3_BLOCK_REG_ADDR + 0xf8, rdata);
  if (rdata != 0x3430312a) {
    fprintf(stderr, "Error: uart3 lsr reg reset value wrong, rdata = 0x%x\n", rdata);
  }
  // I2SM
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_I2SM_BLOCK_REG_ADDR + 0x1f8, rdata);
  if (rdata != 0x3131302a) {
    fprintf(stderr, "Error: i2sm version id wrong, rdata = 0x%x\n", rdata);
  }
  // I2SS0
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_I2SS0_BLOCK_REG_ADDR + 0x1f8, rdata);
  if (rdata != 0x3131302a) {
    fprintf(stderr, "Error: i2ss0 version id wrong, rdata = 0x%x\n", rdata);
  }
  // I2SS1
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_I2SS1_BLOCK_REG_ADDR + 0x1f8, rdata);
  if (rdata != 0x3131302a) {
    fprintf(stderr, "Error: i2ss1 version id wrong, rdata = 0x%x\n", rdata);
  }
  // I2C0
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_I2C0_BLOCK_REG_ADDR + 0xf8, rdata);
  if (rdata != 0x3230312a) {
    fprintf(stderr, "Error: i2c0 version id wrong, rdata = 0x%x\n", rdata);
  }
  // I2C1
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_I2C1_BLOCK_REG_ADDR + 0xf8, rdata);
  if (rdata != 0x3230312a) {
    fprintf(stderr, "Error: i2c1 version id wrong, rdata = 0x%x\n", rdata);
  }
  // I2C2
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_I2C2_BLOCK_REG_ADDR + 0xf8, rdata);
  if (rdata != 0x3230312a) {
    fprintf(stderr, "Error: i2c2 version id wrong, rdata = 0x%x\n", rdata);
  }
  // GPIO
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_GPIO_BLOCK_REG_ADDR + 0x6c, rdata);
  if (rdata != 0x3231322a) {
    fprintf(stderr, "Error: gpio version id wrong, rdata = 0x%x\n", rdata);
  }
  // RTC
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_RTC_BLOCK_REG_ADDR + 0x1c, rdata);
  if (rdata != 0x3230362a) {
    fprintf(stderr, "Error: rtc version id wrong, rdata = 0x%x\n", rdata);
  }
  // TIMERS
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_TIMERS_BLOCK_REG_ADDR + 0xac, rdata);
  if (rdata != 0x3231312a) {
    fprintf(stderr, "Error: timers version id wrong, rdata = 0x%x\n", rdata);
  }
  // WDT
  this->ht->OURSBUS_READ_4B(STATION_SLOW_IO_WDT_BLOCK_REG_ADDR + 0xf8, rdata);
  if (rdata != 0x3131302a) {
    fprintf(stderr, "Error: timers version id wrong, rdata = 0x%x\n", rdata);
  }

  // SDIO
  if (this->ht->argmap["reset_sdio"].length() > 0) {
    fprintf(stderr, "Start Accessing SDIO\n");
    wdata = random();
    this->ht->OURSBUS_WRITE(STATION_SDIO_MSHC_CTRL_ADDR + 0x34, wdata);
    this->ht->OURSBUS_READ_4B(STATION_SDIO_MSHC_CTRL_ADDR + 0x34, rdata);
    if ((rdata & 0xff) != (wdata & 0xff)) {
      fprintf(stderr, "Error: SDIO Reg access data mismatch, wdata = 0x%x, rdata = 0x%x\n", wdata, rdata);
    }
  }
  fprintf(stderr, "Done io reg testing @ %s\n", asctime(localtime(&ltime)));
}

void pygmy_e::enable_flash_fsm(std::string key){
  ltime = time(NULL);
  fprintf(stderr, "Start Enabling Flash FSM @ %s\n", asctime(localtime(&ltime)));
  this->ht->OURSBUS_WRITE(STATION_SLOW_IO_S2B_BOOT_FROM_FLASH_ENA_ADDR, 1);
  this->ht->OURSBUS_WRITE(STATION_SLOW_IO_S2B_BOOT_FROM_FLASH_ENA_SW_CTRL_ADDR, 1);
  fprintf(stderr, "Done Enabling Flash FSM @ %s\n", asctime(localtime(&ltime)));
}

void pygmy_e::enable_boot_fsm(std::string key){
  ltime = time(NULL);
  fprintf(stderr, "Start Enabling Flash FSM @ %s\n", asctime(localtime(&ltime)));
  this->ht->OURSBUS_WRITE(STATION_SLOW_IO_S2B_BOOTUP_ENA_ADDR, 1);
  this->ht->OURSBUS_WRITE(STATION_SLOW_IO_S2B_BOOTUP_ENA_SW_CTRL_ADDR, 1);
  fprintf(stderr, "Done Enabling Flash FSM @ %s\n", asctime(localtime(&ltime)));
}

void pygmy_e::read_from_flash(std::string key){
  uint64_t rdata;
  ltime = time(NULL);
  fprintf(stderr, "Start Reading from flash @ %s\n", asctime(localtime(&ltime)));
  this->ht->OURSBUS_READ(STATION_SLOW_IO_FLASH_ADDR, rdata);
  if (rdata != 0x8967452301efcdab) {
    fprintf(stderr, "Error: Flash content at addr 0x0 wrong, rdata = 0x%x\n", rdata);
  }
  this->ht->OURSBUS_READ(STATION_SLOW_IO_FLASH_ADDR + 0x8, rdata);
  if(rdata != 0x1111111111111111) {
    fprintf(stderr, "Error: Flash content at addr 0x8 wrong, rdata = 0x%x\n", rdata);
  }
}

void pygmy_e::trigger_afterwards(std::string key){
  // Trigger after everything is done
    ltime = time(NULL);
    fprintf(stderr, "Start Triggering Afterwards @ %s\n", asctime(localtime(&ltime)));
    reg[0] = 0x00;
    reg[1] = 0x00;
    reg[2] = 0x00;
    reg[3] = 0x00;
    reg[4] = 0x00;
    reg[5] = 0x00;
    reg[6] = 0x00;
    reg[7] = 0x00;
    this->ht->mem.write(0xffffffff00, 8, (uint8_t*)reg);
    ltime = time(NULL);
    fprintf(stderr, "Done Triggering Afterwards @ %s\n", asctime(localtime(&ltime)));
}

void pygmy_e::test_scan_f(std::string key){
  ltime = time(NULL);
  fprintf(stderr, "Start Test Scan Functional Mode @ %s\n", asctime(localtime(&ltime)));
  this->ht->OURSBUS_WRITE(STATION_ORV32_S2B_DEBUG_STALL_ADDR, 1);
  this->ht->OURSBUS_WRITE(0xfffffff000, 0);
  this->ht->OURSBUS_WRITE(STATION_ORV32_S2B_DEBUG_STALL_ADDR, 0);
  this->ht->OURSBUS_WRITE(STATION_ORV32_S2B_DEBUG_RESUME_ADDR, 1);
  ltime = time(NULL);
  fprintf(stderr, "Done Test Scan Functional Mode @ %s\n", asctime(localtime(&ltime)));
}


