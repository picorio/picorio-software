// FPGA CMD
#ifndef _FPGA_CMDS_H
#define _FPGA_CMDS_H
        #define CMD_RESET                 0xb0
        #define CMD_DDR_READ              0x25
        #define CMD_DDR_WRITE             0x24
        #define CMD_BRAM_READ             0x15
        #define CMD_BRAM_WRITE            0x14
        #define CMD_SPI_READ              0x05
        #define CMD_SPI_WRITE             0x04
        #define CMD_SPI_STATUS            0xaa
#endif
