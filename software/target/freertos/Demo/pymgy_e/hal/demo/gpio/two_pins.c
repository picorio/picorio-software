#include <string.h>
#include "hal.h"
#include "clib.h"
#include "demo.h"

#ifdef TEST_DEMO_GPIO

#define GPIO_TEST_PIN_OUT	11
#define GPIO_TEST_PIN_IN	12
#define GPIO_TEST_TIMES		32

static void gpio_test_int( void* context )
{
	RVHAL_HAL_LOG_INFO("[PASS] [GPIO] [interrupt] [%s] ...\n", (char *)context);	
}

void gpio_test(void)
{
	int i,j,v,err = 0;
	char *int_context = "gpio pin 31 interrupt by rising edge from gpio pin 30";
	static struct gpio_desc out, in;

    rvHal_gpio_init( &out, GPIO_TEST_PIN_OUT, GPIO_PIN_OUTPUT, 0 );
    rvHal_gpio_init( &in, GPIO_TEST_PIN_IN, GPIO_PIN_INPUT, 0 );

	/* test write & read */
	for (i=0; i<GPIO_TEST_TIMES; i++) {
		v = i & 1;
		rvHal_gpio_write(&out, v);
		for (j=0; j<10000; j++) {;}
		if (v != (int)rvHal_gpio_read(&in)) {
			err = 1;
			break;
		}
	}

	if (err)
		RVHAL_HAL_LOG_INFO("[FAIL] ");
	else
		RVHAL_HAL_LOG_INFO("[PASS] ");
	RVHAL_HAL_LOG_INFO("[GPIO] [read & write] [gpio pin 30 output 0 & 1 to gpio pin 31 for 32 times] ...\n");

	/* test interrupt */
	rvHal_gpio_write(&out, 0);
    rvHal_gpio_set_interrupt( &in, GPIO_INT_TYPE_EDGE, GPIO_INT_POLARITY_HIGH, gpio_test_int, int_context);	
	for (j=0; j<10000; j++) {;}
	rvHal_gpio_write(&out, 1);
	rvHal_gpio_write(&out, 0);
}
#else
void gpio_test(void)
{}
#endif

