#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <vector>
#include <string>
#include <string.h>
#include <map>

#include "common.h"

std::map<std::string, uint8_t> orid_map = {
  {"rb",      0b100000},
  {"mmem",    0b100001},
  {"mp",      0b100010},
  {"ddr",     0b100011},
  {"dt",      0b100101},
  {"dma",     0b100111},
  {"l2_vld",  0b101000},
  {"l2_dirty",0b101001},
  {"l2_tag",  0b101010},
  {"l2_data", 0b101011},
  {"l2_lru",  0b101100},
  {"l2_hpm",  0b101101}
};


uint64_t addr2oraddr (std::string target, uint64_t addr) {
  uint64_t oraddr = 0;
  uint64_t base_addr = 0;
  uint64_t sram_offset = 0;

  base_addr = STATION_CACHE_BASE_ADDR;
  sram_offset = STATION_CACHE_DBG_DATARAM_OFFSET;
  oraddr = base_addr + sram_offset + (addr & 0x7fff); //1to1 map 32K sram addr space

  return oraddr;
}

