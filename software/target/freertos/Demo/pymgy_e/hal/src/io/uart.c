#include <string.h>
#include "FreeRTOS.h"
#include "common.h"
#include "rtlgen.h"
#include "hal.h"
#include "clib.h"

#define UART_DLL_ADDR (0x0)
#define UART_THR_ADDR (0x0)
#define UART_RBR_ADDR (0x0)
#define UART_IER_ADDR (0x4)
#define UART_DLH_ADDR (0x4)
#define UART_FCR_ADDR (0x8)
#define UART_IIR_ADDR (0x8)
#define UART_LCR_ADDR (0xc)
#define UART_MCR_ADDR (0x10)
#define UART_LSR_ADDR (0x14)
#define UART_DLF_ADDR (0xc0)

#define UART_LCR_DLAB           0x80 /* Divisor latch access bit */
#define UART_LSR_TEMT           0x40 /* Transmitter empty */
#define UART_LSR_THRE           0x20 /* Transmit-hold-register empty */
#define BOTH_EMPTY (UART_LSR_TEMT | UART_LSR_THRE)

#define IID_MODEM_STATUS            (0x0)
#define IID_NO_INTERRUPT_PENDING    (0x1)
#define IID_THR_EMPTY               (0x2)
#define IID_RECEIVED_DATA_AVAILABLE (0x4)
#define IID_RECEIVER_LINE_STATUS    (0x6)
#define IID_BUSY_DETECT             (0x7)
#define IID_CHARACTER_TIMEOUT       (0xc)

#define RT_FIFO_CHAR_1       (0x0<<6)
#define RT_FIFO_QUARTER_FULL (0x1<<6)
#define RT_FIFO_HALF_FULL    (0x2<<6)
#define RT_FIFO_FULL_2       (0x3<<6)
#define TET_FIFO_QUARTER_FULL (0x2<<4)
#define DMAM_MODE0           (0x0<<3)
#define DMAM_MODE1           (0x1<<3)
#define XFIFOR_RESET         (0x1<<2)
#define RFIFOR_RESET         (0x1<<1)
#define FIFOE_ENABLED        (0x1<<0)

#ifdef BOARD_FPGA_104
#define UART_CONSOLE_BAUDRATE		1500000
#else
#define UART_CONSOLE_BAUDRATE		500000
#endif
#define UART_CLK					FREQ_PCLK

#if 0 /* backup */
//#define UART_CONSOLE_BAUDRATE  3000000//115200 //not work
//#define UART_CLK     (200000000)
//#define UART_CONSOLE_BAUDRATE  9600//115200
//#define UART_CLK     (100000000)
//#define UART_CONSOLE_BAUDRATE  500000//115200
//#define UART_CLK     (200000000)
#define UART_CONSOLE_BAUDRATE  500000//115200
#define UART_CLK     (40000000)
//#define UART_CONSOLE_BAUDRATE  9600//115200
//#define UART_CLK     (20000000)
#endif

#define DIV_ROUND_CLOSEST(x, divisor)(			\
{							\
	typeof(x) __x = x;				\
	typeof(divisor) __d = divisor;			\
	(((typeof(x))-1) > 0 ||				\
	 ((typeof(divisor))-1) > 0 ||			\
	 (((__x) > 0) == ((__d) > 0))) ?		\
		(((__x) + ((__d) / 2)) / (__d)) :	\
		(((__x) - ((__d) / 2)) / (__d));	\
}							\
)

static unsigned int reg_uart1 = STATION_SLOW_IO_UART1_BLOCK_REG_ADDR;

void rvHal_uart_tx_poll(int id, uint8_t ch)
{
	uint64_t reg = reg_uart1;// TODO:
	unsigned int status;

	write32(reg+UART_THR_ADDR, ch);

	for (;;) {
		status = read32(reg+UART_LSR_ADDR);
		if ((status & BOTH_EMPTY) == BOTH_EMPTY)
			break;
	}
}

#define UART1_ENABLE_RX_INT
#ifdef UART1_ENABLE_RX_INT
static void uart_irq_handler(void)
{
        uint32_t rdata;
        uint32_t reg = reg_uart1;// TODO:
        rdata = read32(reg+UART_IIR_ADDR) & 0xf;

        switch(rdata)
        {
                case IID_RECEIVED_DATA_AVAILABLE:
                case IID_CHARACTER_TIMEOUT:
                        rdata = read32(reg+UART_RBR_ADDR);
                        printf("uart rx: %u - %c\n", rdata, rdata);
#if 0
                        rdata=read32(reg+UART_IER_ADDR);
                        rdata &= ~1;
                        write32(reg+UART_IER_ADDR, rdata);
#endif
                        break;

                default:
                        printf("uart int id: 0x%x\n", rdata);
                        break;
        }
}

static struct irq_ext_handler_t ext_irq_uart_handler = {
        .hook           = uart_irq_handler,
};
#endif

void __rvHal_uart_init(void)
{
	uint64_t reg = reg_uart1;
	unsigned int divisor;
	unsigned char c;
	unsigned int rdata;

	write32(reg+UART_LCR_ADDR, 0x3);
	write32(reg+UART_IER_ADDR, 0);
	write32(reg+UART_FCR_ADDR, 0);
	write32(reg+UART_MCR_ADDR, 3);

	divisor = DIV_ROUND_CLOSEST(UART_CLK, 16 * UART_CONSOLE_BAUDRATE);
	c = read32(reg+UART_LCR_ADDR);
	write32(reg+UART_LCR_ADDR, c | UART_LCR_DLAB);
	write32(reg+UART_DLL_ADDR, divisor & 0xff);
	write32(reg+UART_DLH_ADDR, (divisor >> 8) & 0xff);
	write32(reg+UART_LCR_ADDR, c & ~UART_LCR_DLAB);

#ifdef UART1_ENABLE_RX_INT
        rdata = read32(reg+UART_FCR_ADDR);
        rdata &= ~(3<<6); //only rx char 1
        rdata |= RFIFOR_RESET | XFIFOR_RESET | FIFOE_ENABLED /*| TET_FIFO_QUARTER_FULL | RT_FIFO_QUARTER_FULL*/;
        write32(reg+UART_FCR_ADDR, rdata); // fifo enable

        rdata = read32(reg+UART_IER_ADDR);
        rdata &= ~2; //disable tx int
        rdata |= /*(1<<7) | 2 | */1; //enable rx int
        write32(reg+UART_IER_ADDR, rdata);

        __rvHal_request_irq_ext(6, &ext_irq_uart_handler);
#endif
}
