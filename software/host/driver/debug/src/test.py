from multiprocessing import Process, Queue
import time

def f(q):
    while (1):
        setup
            wait for accept
        if (q.empty() == False):
            print(q.get())    # prints "[42, None, 'hello']"

if __name__ == '__main__':
    q = Queue()
    p = Process(target=f, args=(q,))
    p.start()
    q.put(['abc'])
    q.put(['ab'])
    q.put(['ac'])
    q.put(['bc'])
    q.put(['abc'])
    q.put(['ac'])
    q.put(['bc'])

