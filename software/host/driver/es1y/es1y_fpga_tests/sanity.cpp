// for uint*_t data types
#include <stdint.h>
// for print statements
#include <inttypes.h>
#include <stdio.h>
// for ctrl-c interrupt handling
#include <csignal>
// write to files
#include <cstdio>

#include "../tools/pcie_driver.c"
#define WORD_SIZE AXI_DATA_BUS_WIDTH_IN_BYTES //size in bytes
#define MEMORY_SIZE 0x80000 // 512kb bram
#define NUM_ADDRESSES MEMORY_SIZE/WORD_SIZE

#define DEBUG

#define STATION_ORV32_S2B_CFG_RST_PC_ADDR 0x00000000
using namespace std;

void clearbuf (uint8_t * buf, int len) {
  for (int i = 0; i < len; i++) {
    buf[i] = 0x00;
  }
}

int main() {  
  uint32_t addr;
  uint64_t rddata;
  uint64_t wrdata;
  uint8_t * buf;
  char * test_name = new char;
  test_name = "sanity_test";

  int test_id = 3;

  buf = (uint8_t *) malloc (sizeof(uint8_t) * AXI_DATA_BUS_WIDTH_IN_BYTES);

if (test_id >= 0) {
  printf("Running %s...\n",test_name);
  wrdata = 0x0000000000000000;
  do_reset(wrdata);
  sleep(1);
}
if (test_id >= 1) {
  wrdata = 0x0000000000000001;
  do_reset(wrdata);
  sleep(1);  
}
if (test_id >= 2) {
  //Read Oursring PCIE DBG
  addr = DBG_REG_ADDR;
  printf("%s: Reading default dbg value from PCIE2OURSRING...\n",test_name);
  clearbuf(buf, AXI_DATA_BUS_WIDTH_IN_BYTES);
  printf("%s: Clear buf %x %x %x %x...\n",test_name,buf[0],buf[1],buf[2],buf[3]);
  pcie_read(addr,buf);
  printf("%s: Read buf %x %x %x %x...\n",test_name,buf[0],buf[1],buf[2],buf[3]);
}
if (test_id >= 3) {
  //Read Oursring PCIE DBG
  //addr = CMD_REG_ADDR;
  //printf("%s: Reading default dbg value from PCIE2OURSRING...\n",test_name);
  //clearbuf(buf, AXI_DATA_BUS_WIDTH_IN_BYTES);
  //printf("%s: Clear buf %x %x %x %x...\n",test_name,buf[0],buf[1],buf[2],buf[3]);
  //pcie_read(addr,buf);
  //printf("%s: Read buf %x %x %x %x...\n",test_name,buf[0],buf[1],buf[2],buf[3]);
  //buf[0] = 0xda;
  //pcie_write(addr,buf);
  //printf("%s: Read buf %x %x %x %x...\n",test_name,buf[0],buf[1],buf[2],buf[3]);
  //clearbuf(buf, AXI_DATA_BUS_WIDTH_IN_BYTES);
  //printf("%s: Clear buf %x %x %x %x...\n",test_name,buf[0],buf[1],buf[2],buf[3]);
  //pcie_read(addr,buf);
  //printf("%s: Read buf %x %x %x %x...\n",test_name,buf[0],buf[1],buf[2],buf[3]);

  //Read Es1y
  addr =  0x00000000;
  rddata = 0x0000000000000000;
  rddata = do_read(addr);
  printf("%s: READ  Addr=%x Data=%lx\n",test_name,addr,rddata);
  wrdata = 0xdeadfacedeadface;
  printf("%s: WRITE Addr=%x Data=%lx\n",test_name,addr,wrdata);
  do_write(addr,wrdata);
  rddata = 0x0000000000000000;
  rddata = do_read(addr);
  printf("%s: READ Addr=%x Data=%lx\n",test_name,addr,rddata);
  printf("%s: Exit! \n",test_name);
 }  
}

