// RIVAI: GPL-2.0-only
#if defined _SPI_DW_
#include "spi.h"
#include "common.h"
#include "station_slow_io.h"
typedef enum {
	MSG_END = 0,
	MSG_START = 1,
	MSG_CMD_WRITTEN = 2,
	MSG_CMD_READED = 3,
	MSG_DATA_WRITTEN = 4,
	MSG_DATA_READED = 5,
}TRANSFER_QUEUE_FLAG_t; 

static TRANSFER_QUEUE_FLAG_t  transfer_queue_flag = MSG_END;
static u32 cmd_padded_cnt = 0;
static u16 merge_happened = 0;

static void *merge_backup_tx = 0;
static u32 merge_backup_len = 0;
static u32 merge_backup_len_ori = 0;
static u32 merge_sended_cnt = 0;
#define ENTRY_UNIT (4)
extern struct spi_mem spi_mem_obj;

/* Slave spi_dev related */
struct chip_data {
	u8 tmode;		/* TR/TO/RO/EEPROM */
	u8 type;		/* SPI/SSP/MicroWire */

	u8 poll_mode;		/* 1 means use poll mode */

	u16 clk_div;		/* baud rate divider */
	u32 speed_hz;		/* baud rate */
	void (*cs_control)(u32 command);
};

static struct dw_spi dws_spi_instance = {
	.regs = STATION_SLOW_IO_SSPIM1_BLOCK_REG_ADDR, // STATION_SLOW_IO_QSPIM_BLOCK_REG_ADDR, // STATION_SLOW_IO_SPIM_BLOCK_REG_ADDR; // SPI0;
	.num_cs = 1,
	.max_freq = 50000000, //50Mhz
	.current_freq = 2000000, //2Mhz
};

static struct chip_data chip_data_instance = {
	.tmode = SPI_TMOD_TR,
	.type = SPI_FRF_SPI,
	.poll_mode = 1,
	.clk_div = 4,  //fixme: can be changed dynamically?
	.speed_hz = 0, //dynamic changed to dw_spi->current_freq
};

void dw_spi_dump_regs(struct dw_spi *dws)
{
	//printf("CTRL0: \t\t0x%08x\n", dw_readl(dws, DW_SPI_CTRL0));
	//printf("CTRL1: \t\t0x%08x\n", dw_readl(dws, DW_SPI_CTRL1));
	//printf("SSIENR: \t0x%08x\n", dw_readl(dws, DW_SPI_SSIENR));
	//printf("SER: \t\t0x%08x\n", dw_readl(dws, DW_SPI_SER));
	//printf("BAUDR: \t\t0x%08x\n", dw_readl(dws, DW_SPI_BAUDR));
	//printf("TXFTLR: \t0x%08x\n", dw_readl(dws, DW_SPI_TXFLTR));
	//printf("RXFTLR: \t0x%08x\n", dw_readl(dws, DW_SPI_RXFLTR));
	//printf("TXFLR: \t\t0x%08x\n", dw_readl(dws, DW_SPI_TXFLR));
	//printf("RXFLR: \t\t0x%08x\n", dw_readl(dws, DW_SPI_RXFLR));
	//printf("SR: \t\t0x%08x\n", dw_readl(dws, DW_SPI_SR));
	//printf("IMR: \t\t0x%08x\n", dw_readl(dws, DW_SPI_IMR));
	//printf("ISR: \t\t0x%08x\n", dw_readl(dws, DW_SPI_ISR));
	//printf("DMACR: \t\t0x%08x\n", dw_readl(dws, DW_SPI_DMACR));
	//printf("DMATDLR: \t0x%08x\n", dw_readl(dws, DW_SPI_DMATDLR));
	//printf("DMARDLR: \t0x%08x\n", dw_readl(dws, DW_SPI_DMARDLR));
	//printf("CS_OVERRIDE: \t0x%08x\n", dw_readl(dws, DW_SPI_CS_OVERRIDE));

}

/* Return the max entries we can fill into tx fifo */
static inline u32 tx_max(struct dw_spi *dws)
{
	u32 tx_left, tx_room, rxtx_gap;

	tx_left = (dws->tx_end - dws->tx);
	tx_room = (dws->fifo_len) - (dw_readl(dws, DW_SPI_TXFLR)<<2);

	/*
	 * Another concern is about the tx/rx mismatch, we
	 * though to use (dws->fifo_len - rxflr - txflr) as
	 * one maximum value for tx, but it doesn't cover the
	 * data which is out of tx/rx fifo and inside the
	 * shift registers. So a control from sw point of
	 * view is taken.
	 */
	rxtx_gap =  ((dws->rx_end - dws->rx) - (dws->tx_end - dws->tx));

	//printf("dws->rx_end = 0x%x, dws->rx = 0x%x, dws->tx_end = 0x%x, dws->tx = 0x%x. \n", dws->rx_end, dws->rx, dws->tx_end, dws->tx);
	//printf("func: %s, line: %d. tx_left = %d, tx_room = %d, dws->fifo_len = %d, rxtx_gap = %d \n", __func__, __LINE__, tx_left, tx_room, dws->fifo_len, rxtx_gap);
	return min3(tx_left, tx_room, (u32) (dws->fifo_len - rxtx_gap));
}

/* Return the max entries we should read out of rx fifo */
static inline u32 rx_max(struct dw_spi *dws)
{
	u32 rx_left = (dws->rx_end - dws->rx); //fixme: rx_left should include cmd responding len;
	u32 rx_fifo = 0;
	u32 min_value;
	if(MSG_CMD_WRITTEN == transfer_queue_flag && 1 == merge_happened) {
		//printf("func: %s, ignore rx \n", __func__);
		//note: FIXME if ignore happened, rx read shoulbe be added to next rx read;
	} else {
		while(rx_fifo < rx_left) 
			rx_fifo = (dw_readl(dws, DW_SPI_RXFLR) << 2);
	}
	min_value = min(rx_left, rx_fifo);
	//printf("func: %s, line: %d. rx_left = %d dws->rx_end = 0x%x, dws->rx = 0x%x rxflr = 0x%x  rx_fifo = 0x%x, min_value = 0x%x \n", __func__, __LINE__, rx_left, dws->rx_end, dws->rx, dw_readl(dws, DW_SPI_RXFLR), rx_fifo, min_value);
	return min_value; //for fifo level, need multipy 4B
}

static void dw_writer(struct dw_spi *dws)
{
	u32 max = tx_max(dws); //unit is byte
	u32 txw = 0;
	u32 tmp1_tx_data = 0;
	u32 tmp2_tx_data = 0;
	u32 tmp3_tx_data = 0;
	u32 transfer_max = 0;
	u32 pad_cnt = 0;
	static u8 merge_backup1 = 0;
	static u8 merge_backup2 = 0;
	static u8 merge_backup3 = 0;
	static u8 merge_backup4 = 0;
	
	//printf("func: %s, line: %d. max = %d transfer_max = %d cmd_padded_cnt = %d \n", __func__, __LINE__, max, transfer_max, cmd_padded_cnt);
	if (MSG_CMD_READED == transfer_queue_flag && 1 == merge_happened) {
		pad_cnt = cmd_padded_cnt; //for cmd + buf case: 1,2,3, where cmd take xx bytes cases;
		//printf("func: %s, line: %d: merge_backup_tx = 0x%x, merge_backup_len = 0x%x \n", __func__, __LINE__, merge_backup_tx, merge_backup_len);
		while(merge_backup_len >= 4) {
			txw = ___constant_swab32(*(u32 *)(merge_backup_tx));
			dw_write_io_reg(dws, DW_SPI_DR, txw);
			merge_backup_tx += 4;
			merge_backup_len -= 4;
			merge_sended_cnt += 4;
			//printf("func: %s, line: %d: merge_backup_tx = 0x%x, merge_backup_len = 0x%x \n", __func__, __LINE__, merge_backup_tx, merge_backup_len);
		}
		switch(pad_cnt) {
			case 3:
				tmp1_tx_data = (*(u8 *)(dws->tx));
				dws->tx += 1;
			case 2:
				tmp2_tx_data = (*(u8 *)(dws->tx));
				dws->tx += 1;
			case 1:
				tmp3_tx_data = (*(u8 *)(dws->tx));
				dws->tx += 1;
				if(3 == pad_cnt)
					txw = (merge_backup1 << 24 | tmp1_tx_data << 16 | tmp2_tx_data << 8 | tmp3_tx_data << 0);
				else if(2 == pad_cnt)
					txw = (merge_backup2 << 24 | merge_backup1 << 16 | tmp2_tx_data << 8 | tmp3_tx_data << 0);
				else if(1 == pad_cnt)
					txw = (merge_backup3 << 24 | merge_backup2 << 16| merge_backup1 << 8 | tmp3_tx_data << 0);
				else
					txw = (merge_backup4 << 24 | merge_backup3 << 16 | merge_backup2 << 8| merge_backup1 << 0);
				break;
			default:
				//printf("err: pad_cnt = %d \n", pad_cnt);
				break;

		}
		//printf("notice: after merge txw = %x \n", txw);
		
		if (pad_cnt != 0) { 
			max -= pad_cnt;
			dw_write_io_reg(dws, DW_SPI_DR, txw);
		} else {
			//max do not need -4;
			//max -= 4;
			//cmd_padded_cnt = 4;
		}
		dws->need_merge = 0;
		//merge_happened = 0;
		pad_cnt = 0;
		merge_backup_tx = 0;
		merge_backup_len = 0;
		//printf("func: %s, line: %d. left max = %d transfer_max = %d \n", __func__, __LINE__, max, transfer_max);

	}

	//for max < 0 case: max < pad_cnt will result in this case 
	if (max < 0)
		max = 0;
	transfer_max = max%(ENTRY_UNIT) ? (ENTRY_UNIT) : 0;
	transfer_max += ((max>>2)<<2); 
	pad_cnt = transfer_max - max; //for case 1,2,3 
	if (MSG_START == transfer_queue_flag) {
		cmd_padded_cnt = transfer_max - max; //maybe 0
		merge_sended_cnt = 0;
		merge_backup_len_ori = 0;
	}	

	//printf("func: %s, line: %d. left max = %d transfer_max = %d pad_cnt = %d need_merge = %d \n", __func__, __LINE__, max, transfer_max, pad_cnt, dws->need_merge);
	while (transfer_max) {
		/* Set the tx word if the transfer's original "tx" is not null */
		if (dws->tx_end - dws->len) {
			if (dws->n_bytes == 1)
				txw = (*(u8 *)(dws->tx)) ;
			else if(dws->n_bytes == 2)
				txw = *(u16 *)(dws->tx);
			else if(dws->n_bytes == 4) {
				if(dws->need_merge) {
					//backup dws->tx data
					if(dws->len >= 4) {
						merge_backup_tx = dws->tx;
						merge_backup_len = dws->len;
						merge_backup_len_ori = dws->len;
						//printf("merge_backup_tx = %x \n", merge_backup_tx);
						dws->tx += ((dws->len>>2)<<2);
						transfer_max -= ((dws->len>>2)<<2);
						max = (dws->len) % (ENTRY_UNIT);
					}	
					switch (max) {
						case 3:
							merge_backup3 = (*(u8 *)(dws->tx));
							dws->tx += 1;
						case 2:
							merge_backup2 = (*(u8 *)(dws->tx));
							dws->tx += 1;
						case 1:
							merge_backup1 = (*(u8 *)(dws->tx));
							dws->tx += 1;
							break;
						default:
							//printf("err: max %d \n", max);
							break;
					}
					
					//merge need happen
					merge_happened = 1;				
				} else {
					if(pad_cnt) { //for case 1, 2, 3, MSB first, least bit pad zero.
						//printf("err: pad_cnt = %d will never happend \n", pad_cnt);
						switch(pad_cnt) {
							case 1:
								tmp1_tx_data = (*(u8 *)(dws->tx));
								dws->tx += 1;
							case 2:
								tmp2_tx_data = (*(u8 *)(dws->tx));
								dws->tx += 1;
							case 3:
								tmp3_tx_data = (*(u8 *)(dws->tx));
								dws->tx += 1;
								if(tmp1_tx_data != 0)
									txw = (tmp1_tx_data << 24 | tmp2_tx_data << 16 | tmp3_tx_data << 8);
								else if(tmp2_tx_data != 0)
									txw = (tmp2_tx_data << 24 | tmp3_tx_data << 16);
								else if(tmp3_tx_data != 0)
									txw = (tmp3_tx_data << 24);
								pad_cnt = 0;
								break;
							default:
								//printf("err: pad_cnt = %d \n", pad_cnt);
								break;

						}
					} else {
						txw = ___constant_swab32(*(u32 *)(dws->tx));
						dws->tx += 4;
						//printf("func: %s, line: %d. dws->tx = %x \n", __func__, __LINE__, dws->tx);
					}
				}
			}
		} else {
			dws->tx += 4;
			if(dws->tx > dws->tx_end)
				dws->tx = dws->tx_end;
		}
		if(0 != transfer_max) {
			transfer_max -= 4;
		}
		if (0 == dws->need_merge) {
			dw_write_io_reg(dws, DW_SPI_DR, txw);
		}
	}
	if (MSG_CMD_READED == transfer_queue_flag) {
		//dw_spi_dump_regs(&dws_spi_instance);
		transfer_queue_flag = MSG_DATA_WRITTEN; //after all data written, enable SER
		//rvHal_sleep(5000);	
		dw_writel(dws, DW_SPI_SER, 1);
	}
	if (MSG_START == transfer_queue_flag) {
		transfer_queue_flag = MSG_CMD_WRITTEN;
		if(0 == dws->need_merge) {
			//dw_spi_dump_regs(&dws_spi_instance);
			dw_writel(dws, DW_SPI_SER, 1);
		}
	}
	
}

static void dw_reader(struct dw_spi *dws)
{
	u32 max = rx_max(dws); //fixme: when need to do flow control? always; when merge happend or stage happened?   
	u32 rxw = 0;
	u8 data = 0;
	u8 bit_pos = 0;
	u32 pad_cnt = 0;
	u32 transfer_max = 0;


	//printf("func: %s, line: %d. max = %d transfer_max = %d pad_cnt = %d transfer_queue_flag = %d \n", __func__, __LINE__, max, transfer_max, pad_cnt, transfer_queue_flag);
#if 0	
	if (0 == max) { //data not ready, just return  and retry;
	 	while ( ( read32(reg_spim+SPIM_SR_ADDR) & SPIM_SR_BUSY) );
		return ;
	}
#endif
	if (MSG_DATA_WRITTEN == transfer_queue_flag)
	{
		if (1 == merge_happened) {
			pad_cnt = cmd_padded_cnt; 
		} 
	}

	if(MSG_CMD_WRITTEN == transfer_queue_flag && 1 == merge_happened) {
		dws->rx += dws->n_bytes; //no data to read, so just update index
		dws->rx = dws->rx_end;
	}

	if(max > 0) {
		max += (merge_backup_len_ori);	
		transfer_max = max%4 ? 4 : 0;
		transfer_max += ((max>>2)<<2);
		//printf("func: %s, line: %d. max = %d merge_sended_cnt = 0x%x, transfer_max = %d pad_cnt = %d \n", __func__, __LINE__, max, merge_sended_cnt, transfer_max, pad_cnt);
	}
	while (transfer_max) {
		/* Care rx only if the transfer's original "rx" is not null */
		if (dws->rx_end - dws->len) {
			rxw = dw_read_io_reg(dws, DW_SPI_DR);
			if (dws->n_bytes == 1)
				*(u8 *)(dws->rx) = rxw;
			else if(dws->n_bytes == 2)
				*(u16 *)(dws->rx) = rxw;
			else if(dws->n_bytes == 4) {
				//ignore pad cnt bytes data
				if (merge_sended_cnt >= 4) {
					merge_sended_cnt -= 4;
				} else {
					switch (pad_cnt)
					{
						case 0:
							if(max > 0) {
								data = (rxw & 0xff000000) >> 24;
								*(u8 *)(dws->rx) = data;
								dws->rx += 1;
								max--;
							}
						case 3:
							if(max > 0) {
								data = (rxw & 0xff0000) >> 16;
								*(u8 *)(dws->rx) = data;
								dws->rx += 1;
								max--;
							}
						case 2:
							if(max > 0) {
								data = (rxw & 0xff00) >> 8;
								*(u8 *)(dws->rx) = data;
								dws->rx += 1;
								max--;
							}
						case 1:
							if(max > 0) {
								data = (rxw & 0xff);
								*(u8 *)(dws->rx) = data;
								dws->rx += 1;
								max--;
							}
						case 4:
							pad_cnt = 0;
							break;
						default:
							//printf("err: pad_cnt = %d \n", pad_cnt);
							break;

					}
				}
			}
		} else { //actually, you donot know whether or not need receive data at send cmd period
			if (MSG_CMD_WRITTEN == transfer_queue_flag){  
				if(1 == merge_happened) {//fixme, when need_merge? no write, so, no need to read;
					//printf("func: %s, line: %d. notice, merge happened. \n", __func__, __LINE__);
				}
			}
			
			if (MSG_DATA_WRITTEN == transfer_queue_flag || MSG_DATA_READED == transfer_queue_flag){
		
				rxw = dw_read_io_reg(dws, DW_SPI_DR);
				//printf("func: %s, line: %d. just clear rx fifo. \n", __func__, __LINE__);
			}
			if(MSG_DATA_WRITTEN == transfer_queue_flag && 4 == pad_cnt) { 
				//dws->rx += dws->n_bytes;
				pad_cnt = 0;
			} else {
				dws->rx += dws->n_bytes;
			}
		}
		transfer_max -= 4;
	}
	
	if (MSG_DATA_WRITTEN == transfer_queue_flag)
	{
		transfer_queue_flag = MSG_DATA_READED;
		merge_happened = 0;				
	}
	if (MSG_CMD_WRITTEN == transfer_queue_flag) 
	{
		transfer_queue_flag = MSG_CMD_READED;
	}
	
}

void check_status(struct dw_spi *dws, char *info)
{
	u32 status = 1;
	while( 0 != status) {
		//printf("func: %s, line: %d. %s \n", __func__, __LINE__, info);
		dw_writel(dws, 0x60, 0x05000000);
		dw_writel(dws, 0x10, 0x1);
		dw_readl(dws, 0x00);
		dw_readl(dws, 0x00);
		dw_readl(dws, 0x00);
		dw_readl(dws, 0x00);
		dw_readl(dws, 0x00);
		dw_readl(dws, 0x00);
		dw_readl(dws, 0x00);
		dw_readl(dws, 0x00);
		status = dw_readl(dws, 0x60);
		dw_writel(dws, 0x10, 0x0);
		status  &= 0x00ff0000;
	}

	return ;
}

void write_enable(struct dw_spi *dws)
{
	dw_writel(dws, 0x08, 0x0);
	dw_writel(dws, 0x00, 0x70000);
	dw_writel(dws, 0x08, 0x1);

	dw_writel(dws, 0x60, 0x06);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);
	
	dw_writel(dws, 0x08, 0x0);
	dw_writel(dws, 0x00, 0x1f0000);
	dw_writel(dws, 0x08, 0x1);
}

void write_disable(struct dw_spi *dws)
{
	dw_writel(dws, 0x08, 0x0);
	dw_writel(dws, 0x00, 0x70000);
	dw_writel(dws, 0x08, 0x1);

	dw_writel(dws, 0x60, 0x04);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);
	
	dw_writel(dws, 0x08, 0x0);
	dw_writel(dws, 0x00, 0x1f0000);
	dw_writel(dws, 0x08, 0x1);
}
void test_case1(struct dw_spi *dws)
{
	dw_writel(dws, 0x08, 0x0);
	dw_writel(dws, 0x00, 0x1f0000);
	dw_writel(dws, 0x14, 2);
	dw_writel(dws, 0x08, 0x1);
	
	write_disable(dws);
#if 1
	check_status(dws, "before erase sector");
	write_enable(dws);
	dw_writel(dws, 0x60, 0xd8000000);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);
	
	check_status(dws, "before real program page");
	write_enable(dws);
	dw_writel(dws, 0x60, 0x02000000);
	dw_writel(dws, 0x60, 0x33333333);
	dw_writel(dws, 0x60, 0x77777777);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);
#endif
	check_status(dws, "before read page");
	dw_writel(dws, 0x60, 0x03000000);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);
}
void test_case2(struct dw_spi *dws)
{
	u32 status = 0;
	dw_writel(dws, 0x08, 0x0);
	dw_writel(dws, 0x00, 0x1f0000);
	dw_writel(dws, 0x14, 2);
	dw_writel(dws, 0x08, 0x1);
#if 1
#if 0
BEGIN:
	delay_cnt(15000);	
	dw_writel(dws, 0x60, 0x05000000);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	status = dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);

	dw_writel(dws, 0x08, 0x0);
	dw_writel(dws, 0x00, 0x70000);
	dw_writel(dws, 0x08, 0x1);
	dw_writel(dws, 0x60, 0x80);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);
	if( status & 0x00ff0000)
		goto BEGIN;
#endif
#if 0	
	dw_writel(dws, 0x60, 0x06);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);
	
	dw_writel(dws, 0x60, 0xc7);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);
#endif
	check_status(dws, "before erase sector");
	dw_writel(dws, 0x08, 0x0);
	dw_writel(dws, 0x00, 0x70000);
	dw_writel(dws, 0x08, 0x1);
	dw_writel(dws, 0x60, 0x06);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);
	
	dw_writel(dws, 0x08, 0x0);
	dw_writel(dws, 0x00, 0x1f0000);
	dw_writel(dws, 0x08, 0x1);
	
	//printf("func: %s, line: %d erase sector. \n", __func__, __LINE__);
	dw_writel(dws, 0x60, 0x20000000);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);
#if 1	
ERASE_SECTOR_BUSY_FLAG:
	check_status(dws, "before program page");	
	dw_writel(dws, 0x08, 0x0);
	dw_writel(dws, 0x00, 0x70000);
	dw_writel(dws, 0x08, 0x1);
	dw_writel(dws, 0x60, 0x06);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);
	
	dw_writel(dws, 0x08, 0x0);
	dw_writel(dws, 0x00, 0x1f0000);
	dw_writel(dws, 0x08, 0x1);
	
	//printf("func: %s, line: %d program page. \n", __func__, __LINE__);
	dw_writel(dws, 0x60, 0x02000000);
	dw_writel(dws, 0x60, 0x11111111);
	dw_writel(dws, 0x60, 0x33333333);
	dw_writel(dws, 0x60, 0x55555555);
	dw_writel(dws, 0x60, 0x77777777);
	dw_writel(dws, 0x60, 0x99999999);
	dw_writel(dws, 0x60, 0xbbbbbbbb);
	dw_writel(dws, 0x60, 0xdddddddd);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);
#endif
#endif
	check_status(dws, "before read page");
	//printf("func: %s, line: %d read page. \n", __func__, __LINE__);
	dw_writel(dws, 0x60, 0x03000000);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x60, 0x12345678);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);
}

void test_case3(struct dw_spi *dws)
{
	u32 loop_cnt = 0;
 	u32 len = 256 + 4;
	u32 rx_fifo = 0;

	dw_writel(dws, 0x08, 0x0);
	dw_writel(dws, 0x00, 0x1f0300);
	dw_writel(dws, 0x14, 10);
	dw_writel(dws, 0x18, 4);
	dw_writel(dws, 0x1c, 4);
	dw_writel(dws, 0x18, 4);
	dw_writel(dws, 0x10, 0x1);
	dw_writel(dws, 0xf4, 0x218);
	dw_writel(dws, 0x04, 63); //len = (63+1)*4
	dw_writel(dws, 0x08, 0x1);
	
	//write_disable(dws);
	//check_status(dws, "before read page");
	//notice: can not add print info, otherwise, the fifo will overflow;
	dw_writel(dws, 0x60, 0x03000000);
	while(loop_cnt < len) {
		rx_fifo = (dw_readl(dws, DW_SPI_RXFLR));
       		//printf("func: %s, line: %d rx_fifo = %d. \n", __func__, __LINE__, rx_fifo);
		loop_cnt += (rx_fifo<<2);
		while(rx_fifo--)
			dw_readl(dws, 0x60);
	}
	return ;
}

/* Must be called inside pump_transfers() */
static int poll_transfer(struct dw_spi *dws)
{
       do {
               dw_writer(dws);
               dw_reader(dws);
               //cpu_relax();
       } while (dws->rx_end > dws->rx);
	
	return 0;
}

int dw_spi_transfer_one(struct spi_controller *master,
		struct spi_device *spi, struct spi_transfer *transfer)
{
	struct dw_spi *dws = &dws_spi_instance;
	struct chip_data *chip = &chip_data_instance;
	u8 imask = 0;
	u16 txlevel = 0;
	u32 cr0 = 0;
	int ret;
	int i = 0;

	dws->tx = (void *)transfer->tx_buf;
	dws->tx_end = dws->tx + transfer->len;
	dws->rx = transfer->rx_buf;
	dws->rx_end = dws->rx + transfer->len;
	dws->len = transfer->len;
	dws->need_merge = transfer->need_merge;

	if (dws->tx != 0 && dws->len < 6) {
		for(i = dws->tx; i < dws->tx_end; i++)
		{
			//printf("dws->tx content = %x \n", *(u8 *)i);
		}
	}
	
	spi_enable_chip(dws, 0);
	/* Handle per transfer options for bpw and speed */
	if (transfer->speed_hz != dws->current_freq) {
		transfer->speed_hz = dws->current_freq;
		if (transfer->speed_hz != chip->speed_hz) {
			/* clk_div doesn't support odd number */
			//printf("func: %s, line: %d.  dws->max_freq = %d,  transfer->speed_hz = %d \n", __func__, __LINE__, dws->max_freq, transfer->speed_hz);
			//printf("FIXME: func: %s, line: %d. chip->speed_hz = %d,  chip->clk_div = %d \n", __func__, __LINE__, chip->speed_hz, chip->clk_div);
			chip->clk_div = 2;
			chip->speed_hz = transfer->speed_hz;
			//printf("func: %s, line: %d. chip->speed_hz = %d,  chip->clk_div = %d \n", __func__, __LINE__, chip->speed_hz, chip->clk_div);
		}
		dws->current_freq = transfer->speed_hz;
		spi_set_clk(dws, chip->clk_div);
		//printf("func: %s, line: %d.  dws->current_freq = %d,  chip->clk_div = %d \n", __func__, __LINE__, dws->current_freq, chip->clk_div);
	}

	if (MSG_START == transfer_queue_flag && !(dws->need_merge)) {
		dws->n_bytes = dws->len;
	}
	else {
		dws->n_bytes = 4;
	}

	/* Default SPI mode is SCPOL = 0, SCPH = 0 */
	cr0 = (spi->bits_per_word - 1)
		| (chip->type << SPI_FRF_OFFSET)
		| ((((spi->mode & SPI_CPOL) ? 1 : 0) << SPI_SCOL_OFFSET) |
			(((spi->mode & SPI_CPHA) ? 1 : 0) << SPI_SCPH_OFFSET))
		| (chip->tmode << SPI_TMOD_OFFSET)
		| (((dws->n_bytes<<3) -1) << SPI_DFS32_OFFSET);

	/*
	 * Adjust transfer mode if necessary. Requires platform dependent
	 * chipselect mechanism.
	 */
	if (chip->cs_control) {
		if (dws->rx && dws->tx)
			chip->tmode = SPI_TMOD_TR;
		else if (dws->rx)
			chip->tmode = SPI_TMOD_RO;
		else
			chip->tmode = SPI_TMOD_TO;

		cr0 &= ~SPI_TMOD_MASK;
		cr0 |= (chip->tmode << SPI_TMOD_OFFSET);
	}

	if (MSG_START == transfer_queue_flag && !(dws->need_merge)) {
		//update cr0
		dw_writel(dws, DW_SPI_CTRL0, 0x70000);
		//dw_spi_dump_regs(dws);
		spi_enable_chip(dws, 1);	
		//do tansfer
		dw_write_io_reg(dws, DW_SPI_DR, (*(u8 *)(dws->tx)));
		dw_writel(dws, DW_SPI_SER, 1);
		dw_read_io_reg(dws, DW_SPI_DR);
		

	} else {
		dw_writel(dws, DW_SPI_CTRL0, cr0);
		//dw_spi_dump_regs(dws);
		spi_enable_chip(dws, 1);	
		//if (chip->poll_mode)
		poll_transfer(dws);
	}

	return 0;
}
#if 0
/* This may be called twice for each spi dev */
static int dw_spi_setup(struct spi_device *spi)
{
	struct dw_spi_chip *chip_info = NULL;
	static struct chip_data chip;

	/* Fixme: Only alloc on first setup */
	spi_set_ctldata(spi, &chip);
	/*
	 * Protocol drivers may change the chip settings, so...
	 * if chip_info exists, use it
	 */
	chip_info = spi->controller_data;

	/* chip_info doesn't always exist */
	if (chip_info) {
		if (chip_info->cs_control)
			chip.cs_control = chip_info->cs_control;

		chip.poll_mode = chip_info->poll_mode;
		chip.type = chip_info->type;
	}

	chip.tmode = SPI_TMOD_TR;

	return 0;
}
#endif

/* Restart the controller, disable all interrupts, clean rx fifo */
static void spi_hw_init(struct device *dev, struct dw_spi *dws)
{
	spi_reset_chip(dws);

	/*
	 * Try to detect the FIFO depth if not set by interface driver,
	 * the depth could be from 2 to 256 from HW spec
	 */
	if (!dws->fifo_len) {
		u32 fifo;

		for (fifo = 1; fifo < 256; fifo++) {
			dw_writel(dws, DW_SPI_TXFLTR, fifo);
			if (fifo != dw_readl(dws, DW_SPI_TXFLTR))
				break;
		}
		dw_writel(dws, DW_SPI_TXFLTR, 0);

		dws->fifo_len = (fifo == 1) ? 0 : fifo;
		dws->fifo_len *=4;
		//printf("Detected FIFO size: %u bytes\n", dws->fifo_len);
	}

	/* enable HW fixup for explicit CS deselect for Amazon's alpine chip */
	if (dws->cs_override)
		dw_writel(dws, DW_SPI_CS_OVERRIDE, 0xF);
}

void dw_spi_set_cs(struct spi_device *spi, bool enable)
{
	struct dw_spi *dws = spi_controller_get_devdata(spi->controller);

	if (enable) {
		//dw_writel(dws, DW_SPI_SER, BIT(spi->chip_select));
		/* For poll mode just disable all interrupts */
		spi_mask_intr(dws, 0xff);
		transfer_queue_flag = MSG_START;
	} else {
		dw_writel(dws, DW_SPI_SER, 0);
		transfer_queue_flag = MSG_END;
	}

}

int dw_spi_add_host(struct device *dev, struct dw_spi *dws)
{
	static struct spi_controller master;
	int ret;
	dws->master = &master;
	dws->type = SSI_MOTO_SPI;

	spi_controller_set_devdata(&master, dws);


	master.use_gpio_descriptors = true;
	master.mode_bits = SPI_CPOL | SPI_CPHA | SPI_LOOP;
	master.bits_per_word_mask =  SPI_BPW_RANGE_MASK(4, 16);
	master.bus_num = dws->bus_num;
	master.num_chipselect = dws->num_cs;
	master.set_cs = dw_spi_set_cs;
	master.transfer_one = dw_spi_transfer_one;
	master.max_speed_hz = dws->max_freq;
	master.flags = SPI_MASTER_GPIO_SS;

	if (dws->set_cs)
		master.set_cs = dws->set_cs;

	/* Basic HW init */
	spi_hw_init(NULL, &dws_spi_instance);

	//link between master and device;	
	spi_mem_obj.spi->controller = &master;

	return 0;
}

u32 read_status_x(struct dw_spi *dws, u8 index)
{
	u32 status = 0;
	u32 val = (0x65000000 | (index << 16));
	dw_writel(dws, 0x08, 0x0);
	dw_writel(dws, 0x00, 0x1f0000);
	dw_writel(dws, 0x14, 2);
	dw_writel(dws, 0x08, 0x1);
	
	dw_writel(dws, 0x60, val);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	status = dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);
	status  &= 0x000000ff;
	//printf("func: %s: status = 0x%x \n", __func__, status);
	
	return status;	
}

void write_status4(struct dw_spi *dws, u8 val)
{
	dw_writel(dws, 0x08, 0x0);
	dw_writel(dws, 0x00, 0x70000);
	dw_writel(dws, 0x14, 2);
	dw_writel(dws, 0x08, 0x1);
	
	dw_writel(dws, 0x60, 0x71);
	dw_writel(dws, 0x60, 0x04);
	dw_writel(dws, 0x60, val);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);
}

void at25xe161d_set_lpm(void)
{
	u8 lpm = 0;
	//printf("func:%s  \n", __func__);
	write_enable(&dws_spi_instance);	
	lpm = (u8)read_status_x(&dws_spi_instance, 4);
	//printf("func: %s current lpm = 0x%x \n", __func__, lpm);
	
	lpm &= 0x7f;
	write_status4(&dws_spi_instance, lpm);
	
	lpm = (u8)read_status_x(&dws_spi_instance, 4);
	//printf("func: %s current lpm = 0x%x \n", __func__, lpm);
}

void at25xe161d_clr_lpm(void)
{
	u8 lpm = 0;
	u8 index = 0;
	//printf("func:%s  \n", __func__);

	write_enable(&dws_spi_instance);	
	lpm = (u8)read_status_x(&dws_spi_instance, 4);
	//printf("func: %s current lpm = 0x%x \n", __func__, lpm);
	
	lpm |= 0x80;
	write_status4(&dws_spi_instance, lpm);
	
	//printf("func: %s after clear \n", __func__);
	lpm = (u8)read_status_x(&dws_spi_instance, 4);
	//printf("func: %s current lpm = 0x%x \n", __func__, lpm);
}

void at25xe161d_enter_lpm(void)
{	
	struct dw_spi *dws = &dws_spi_instance;

	dw_writel(dws, 0x08, 0x0);
	dw_writel(dws, 0x00, 0x70000);
	dw_writel(dws, 0x14, 2);
	dw_writel(dws, 0x08, 0x1);
	
	dw_writel(dws, 0x60, 0xb9);
	dw_writel(dws, 0x10, 0x1);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x00);
	dw_readl(dws, 0x60);
	dw_writel(dws, 0x10, 0x0);
	
}
void spi_init(void)
{
	//dw_spi_dump_regs(&dws_spi_instance);
//	*(uint32_t *)(STATION_FLASH_S2B_MODE_SEL_AUTO0_MAN1_ADDR) = 1;
#if 0
	*(uint64_t *)(STATION_SLOW_IO_SSP_SHARED_SEL_6_ADDR) = 4;
	*(uint64_t *)(STATION_SLOW_IO_SSP_SHARED_SEL_7_ADDR) = 4;
	*(uint64_t *)(STATION_SLOW_IO_SSP_SHARED_SEL_8_ADDR) = 4;
	*(uint64_t *)(STATION_SLOW_IO_SSP_SHARED_SEL_9_ADDR) = 4;
#endif
	uint32_t clkdiv_sel = read32(STATION_DMA_S2B_SLOW_IO_CLKDIV_DIVCLK_SEL_ADDR);
	//printf("func: %s, clkdiv_sel = 0x%x \n", clkdiv_sel);
	write32(STATION_DMA_S2B_SLOW_IO_CLKDIV_DIVCLK_SEL_ADDR, 0);
	dw_spi_add_host(NULL, &dws_spi_instance);
	//dw_spi_dump_regs(&dws_spi_instance);
#if 0 //work
	test_case1(&dws_spi_instance);
	return 0;
#endif
#if 0 //work
	test_case2(&dws_spi_instance);
	return 0;
#endif
#if 0 //not work
	test_case3(&dws_spi_instance);
	return 0;
#endif
}

void spi_nor_backup()
{
	//dw_spi_transfer_one();
}
#endif

