#!/usr/bin/env bash

i="1"

export PATH=/work/home/lpcui/work2/pygmy-es1x/tool/riscv/bin/:$PATH

cd /work/home/lpcui/work2/pygmy-es1x/tool/driver/pygmy_e
make reset0

while true; do
  cd /work/home/lpcui/work2/pygmy-es1x/tool/driver/pygmy_e
  test=`sed -n "$i p" pygmy_e_isa.list`
  echo "test=$test"
  ISA_TEST="/work/home/lpcui/work2/pygmy-es1x//tool/riscv/riscv32-unknown-elf/share/riscv-tests/isa_latest/$test"
  echo "ISA_TEST=$ISA_TEST"
  make -e orv_isa_test ISA_TEST=$ISA_TEST |& tee $test.log &
  PID=$!
  /work/home/lpcui/work2/pygmy-es1x/verif/common/regression/fpga/job_check.sh -i /work/home/lpcui/work2/pygmy-es1x/tool/driver/pygmy_e/$test.log -p "PASSED" -f "FAILED" -t 20
  RSLT=$?
  pkill -9 fesvr2socket.o
  pkill -9 fesvr2socket.o
  kill -9 $PID
  echo "RSLT=$RSLT"
  if [ "$RSLT" -gt "0" ]
  then
    if [ "$RSLT" -gt "2" ]
    then
      echo "$(date) ISA Test $test TIMEOUT, rerun = $rerun" >> /work/home/lpcui/work2/pygmy-es1x/tool/driver/pygmy_e/isa_test_fail.log
    else
      echo "$(date) ISA Test $test FAILED" >> /work/home/lpcui/work2/pygmy-es1x/tool/driver/pygmy_e/isa_test_fail.log
    fi
  else
    echo "$(date) ISA Test $test passed" >> /work/home/lpcui/work2/pygmy-es1x/tool/driver/pygmy_e/isa_test_pass.log
  fi

  if [ "$i" -gt "46" ]
  then
      i="1"
  else
      i=$(($i+1))
  fi
  cd /work/home/lpcui/work2/pygmy-es1x/tool/driver/pygmy_e
 make reset0
done
