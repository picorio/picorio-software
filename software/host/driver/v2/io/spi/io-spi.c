#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>

#include "common.h"
#include "common-spi.h"

struct spi_channel_t {
	struct io_channel_t io;

	FT_HANDLE ftHandle;
	FT_HANDLE ftHandle_gpio;
	FT4222_SPIMode mode;
};

#ifdef SPI_CLK_DIV_BASE
#define SPI_CLK_DIV		CLK_DIV_512
#else
#define SPI_CLK_DIV		CLK_DIV_32
#endif

static int spi_setup(struct io_channel_t *io)
{
	struct spi_channel_t *spi = (struct spi_channel_t *)io_get_priv_channel(io);
	FT4222_SPIMode mode = SPI_IO_SINGLE; /* can be from parse_args() */
	FT_HANDLE ftHandle = NULL;

	ftHandle = gpio_setup();
	if (NULL == ftHandle)
		return -1;
	spi->ftHandle_gpio = ftHandle;

	ftHandle = setup(mode, SPI_CLK_DIV, CLK_IDLE_LOW, CLK_LEADING);
	if (NULL == ftHandle) {
		FT_Close(spi->ftHandle_gpio);
		return -1;
	}

	spi->ftHandle = ftHandle;
	spi->mode = mode;
	return 0;
}

static void spi_reset(struct io_channel_t *io)
{
	struct spi_channel_t *spi = (struct spi_channel_t *)io_get_priv_channel(io);

	gpio_reset(spi->ftHandle_gpio);	
}

static int spi_do_status_check(struct io_channel_t *io)
{
	struct spi_channel_t *spi = (struct spi_channel_t *)io_get_priv_channel(io);

	do_status_check(spi->mode, spi->ftHandle);

	return 0; // TODO:
}

static void spi_do_write(struct io_channel_t *io, uint64_t addr, uint64_t data)
{
	struct spi_channel_t *spi = (struct spi_channel_t *)io_get_priv_channel(io);
//printf("%s: addr(0x%lx), data(0x%lx) ... \n", __func__, addr, data);
	do_write(spi->mode, spi->ftHandle, addr, data);
}

static uint64_t spi_do_read(struct io_channel_t *io, uint64_t addr)
{
	struct spi_channel_t *spi = (struct spi_channel_t *)io_get_priv_channel(io);

	return do_read(spi->mode, spi->ftHandle, addr);
}

static void spi_close(struct io_channel_t *io)
{
	struct spi_channel_t *spi = (struct spi_channel_t *)io_get_priv_channel(io);

	FT4222_UnInitialize(spi->ftHandle);
	FT_Close(spi->ftHandle);

	FT4222_UnInitialize(spi->ftHandle_gpio);
	FT_Close(spi->ftHandle_gpio);

	free(spi->io.ops);
	spi->io.ops = NULL;
	free(spi);
}

static void io_ops_init(struct io_ops_t *ops)
{
	ops->parse_args = NULL;
	ops->setup = spi_setup;
	ops->reset = spi_reset;
	ops->do_status_check = spi_do_status_check;
	ops->do_write = spi_do_write;
	ops->do_write_burst = NULL;
	ops->do_read = spi_do_read;
	ops->close = spi_close;
}

struct io_channel_t * io_open(void)
{
	struct io_ops_t *ops = (struct io_ops_t *)malloc(sizeof(*ops));
	if (NULL == ops)
		return NULL;

	struct spi_channel_t *spi = (struct spi_channel_t *)malloc(sizeof(*spi));
	if (NULL == spi) {
		free(ops);
		return NULL;
	}
	memset(spi, 0, sizeof(*spi));

	io_ops_init(ops);
	spi->io.ops = ops;
	spi->io.priv = spi;

	return &(spi->io);
}

