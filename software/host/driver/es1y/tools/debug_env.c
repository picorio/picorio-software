#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <iostream>

#include "common.h"
#include "pcie_driver.h"
#include "spi_driver.h"
#include "driver.h"
#include "pygmy_es1y_addr_translater.h"

int main (int argc, char const *argv[])
{
  setup();
  while (1) {
    std::string cmd;
    std::cout << "Please enter command: (All Data in HEX no matter 0x is added or not)\n: ";
    std::getline(std::cin, cmd);
    std::string delimiter = " ";
    size_t pos = 0;
    std::vector <std::string> tokenQ;
    int dma_cmd_vld = 0;
  
    while ((pos = cmd.find(delimiter)) != std::string::npos) {
      tokenQ.push_back(cmd.substr(0, pos));
      cmd.erase(0, pos + delimiter.length());
    }
    if (!cmd.empty())
      tokenQ.push_back(cmd);
  
    if ((tokenQ.size() == 1) && (tokenQ[0] == "status")) {
      do_status_check();
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "do_while")) {
      while (1) {
       do_write (0x2080, 0xa5a5a5a5a5a5a5a5);
       //do_status_check();
      }
    } else if ((tokenQ.size() == 1) && (tokenQ[0] == "setpc")) {
      do_write(STATION_VP_S2B_CFG_RST_PC_ADDR_0, 0x80000000);
    } else if ((tokenQ.size() == 0) || (tokenQ[0] == "help") || (tokenQ[0] == "h")) {
      std::cout << "This is Help Info\n";
    } else if ((tokenQ[0] == "quit") || (tokenQ[0] == "q") || (tokenQ[0] == "exit")) {
      break;
    } else if ((tokenQ.size() == 3) && (tokenQ[0] == "read")) {
      char * p;
      uint64_t data = 0;
      uint64_t addr = strtoul(tokenQ[1].c_str(), & p, 16);
      std::string target = tokenQ[2];
      if (*p == 0) {
        if (target == "rb") {
          data = do_read(addr);
        } else if (target == "dma") {
          do_write(STATION_DMA_S2B_DMA_DEBUG_ADDR_ADDR, addr);
          do_write(STATION_DMA_S2B_DMA_DEBUG_REQ_TYPE_ADDR, 0);
          dma_cmd_vld = 1;
          do_write(STATION_DMA_DMA_DEBUG_CMD_VLD_ADDR, dma_cmd_vld);
          while (dma_cmd_vld == 1)
            dma_cmd_vld = do_read(STATION_DMA_DMA_DEBUG_CMD_VLD_ADDR);
          data = do_read(STATION_DMA_B2S_DMA_DEBUG_RDATA_ADDR);
        } else if (target == "dt") {
          do_write(STATION_DT_DBG_ADDR_ADDR, addr);
          data = do_read(STATION_DT_DBG_DATA_ADDR);
        } else {
          data = do_read(addr2oraddr(target, addr));
        }
        printf("Do Read to Addr 0x%llx, Got Data 0x%llx\n", addr, data);
      }
    } else if ((tokenQ.size() == 4) && (tokenQ[0] == "write")) {
      char * p;
      uint64_t addr = strtoul(tokenQ[1].c_str(), & p, 16);
      uint64_t data = strtoul(tokenQ[2].c_str(), & p, 16);
      std::string target = tokenQ[3];
      if (*p == 0) {
        if (target == "rb") {
          do_write(addr, data);
        } else if (target == "dma") {
          do_write(STATION_DMA_S2B_DMA_DEBUG_ADDR_ADDR, addr);
          do_write(STATION_DMA_S2B_DMA_DEBUG_REQ_TYPE_ADDR, 2);
          do_write(STATION_DMA_S2B_DMA_DEBUG_WR_DATA_ADDR, data);
          dma_cmd_vld = 1;
          do_write(STATION_DMA_DMA_DEBUG_CMD_VLD_ADDR, dma_cmd_vld);
          while (dma_cmd_vld == 1)
            dma_cmd_vld = do_read(STATION_DMA_DMA_DEBUG_CMD_VLD_ADDR);
        } else if (target == "dt") {
          do_write(STATION_DT_DBG_ADDR_ADDR, addr);
          do_write(STATION_DT_DBG_DATA_ADDR, data);
        } else {
          do_write(addr2oraddr(target, addr), data);
        }
        printf("Do Write to Addr 0x%llx with Data 0x%llx\n", addr, data);
      }
    } else if ((tokenQ.size() == 4) && (tokenQ[0] == "dump")) {
      char * p;
      uint64_t data = 0;
      uint64_t addr_lo = strtoul(tokenQ[1].c_str(), & p, 16);
      uint64_t addr_hi = strtoul(tokenQ[2].c_str(), & p, 16);
      std::string target = tokenQ[3];
      if (*p == 0) {
        for (uint64_t addr = addr_lo; addr <= addr_hi; addr += 8) {
          if (target == "rb") {
            data = do_read(addr);
          } else if (target == "dma") {
            do_write(STATION_DMA_DBG_ADDR_ADDR, addr);
            data = do_read(STATION_DMA_DBG_RDATA_ADDR);
          } else if (target == "dt") {
            do_write(STATION_DT_DBG_ADDR_ADDR, addr);
            data = do_read(STATION_DT_DBG_DATA_ADDR);
          } else {
            data = do_read(addr2oraddr(target, addr));
          }
          printf("0x%llx: 0x%08llx\n", addr + 0, (data >>  0) & 0xffffffff);
          printf("0x%llx: 0x%08llx\n", addr + 4, (data >> 32) & 0xffffffff);
        }
      }
    } else {
      std::cout << "Unrecognized Command; Please use help or h to see supported command list.\n";
    }
  }
  return 0;
}
