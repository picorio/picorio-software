#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>

#include "common.h"

int io_parse_args(struct io_channel_t *io, int argc, char const **argv)
{
	struct io_ops_t *ops = io->ops;

	if (ops->parse_args)
		return ops->parse_args(io, argc, argv);

	return 0;
}

int io_setup(struct io_channel_t *io)
{
	struct io_ops_t *ops = io->ops;

	if (ops->setup)
		return ops->setup(io);

	return 0;
}

void io_reset(struct io_channel_t *io)
{
	struct io_ops_t *ops = io->ops;

	if (ops->reset)
		ops->reset(io);
}

int io_do_status_check(struct io_channel_t *io)
{
	struct io_ops_t *ops = io->ops;

	if (ops->do_status_check)
		return ops->do_status_check(io);

	return 0;
}

void io_do_write(struct io_channel_t *io, uint64_t addr, uint64_t data)
{
	struct io_ops_t *ops = io->ops;

	if (ops->do_write)
		ops->do_write(io, addr, data);
}

void io_do_write_burst(struct io_channel_t *io, uint64_t addr, uint64_t* data, int len)
{
	struct io_ops_t *ops = io->ops;

	if (ops->do_write_burst)
		ops->do_write_burst(io, addr, data, len);
}

uint64_t io_do_read(struct io_channel_t *io, uint64_t addr)
{
	struct io_ops_t *ops = io->ops;

	if (ops->do_read)
		return ops->do_read(io, addr);

	/* error? */
	return 0UL;
}

void io_close(struct io_channel_t *io)
{
	struct io_ops_t *ops = io->ops;

	if (ops->close)
		ops->close(io);
}
