#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <string.h>
#include <iostream>

#include "ftd2xx.h"
#include "libft4222.h"
#include "debug_tick.h"
#include "common.h"
#include "pygmy_es1_addr_translater.h"

#include <time.h>

#define LOOP_COUNT 1000000

int mem_test (FILE* f, FT4222_SPIMode mode, FT_HANDLE ftHandle, std::string target) {
  uint64_t addr;
  uint64_t data;
  uint64_t rcvd;
  int errcnt = 0;
  int err = 0;
//  for (int i = 0; i < LOOP_COUNT; i++) {
  for (uint32_t i = 0; i < 100000; i++) {
//    addr = (random() % 0xfffff) << 3;
//    addr = (i % 0xfffff) << 3;
    addr = i;
    err = 0;
    // data = 0
    data = 0;
    fprintf(stderr, "Writing 0x%lx to address 0x%lx\n", data, addr);
    do_write(mode, ftHandle, addr2obaddr(target, addr), data);
    rcvd = do_read(mode, ftHandle, addr2obaddr(target, addr));
    fprintf(stderr, "Reading address 0x%lx, get 0x%lx\n", addr, rcvd);
    if (rcvd != data) {
      fprintf(f, ">>> DATA=0 FAIL at address 0x%llx, rcvd = 0x%llx, data = 0x%llx\n", addr, rcvd, data);
      err = 1;
    }
    // data = '1
    data = 0xffffffffffffffff;

    //fprintf(stderr, "Writing 0x%lx to address 0x%lx\n", data, addr);
    do_write(mode, ftHandle, addr2obaddr(target, addr), data);
    rcvd = do_read(mode, ftHandle, addr2obaddr(target, addr));
    //fprintf(stderr, "Reading address 0x%lx, get 0x%lx\n", addr, rcvd);
    if (rcvd != data) {
      fprintf(f, ">>> DATA='1 FAIL at address 0x%llx, rcvd = 0x%llx, data = 0x%llx\n", addr, rcvd, data);
      err = 1;
    }

    // data = random
    data = ((0UL + rand()) << 32) + rand();

    //fprintf(stderr, "Writing 0x%lx to address 0x%lx\n", data, addr);
    do_write(mode, ftHandle, addr2obaddr(target, addr), data);
    rcvd = do_read(mode, ftHandle, addr2obaddr(target, addr));
    //fprintf(stderr, "Reading address 0x%lx, get 0x%lx\n", addr, rcvd);
    if (rcvd != data) {
      fprintf(f, ">>> DATA=rand FAIL at address 0x%llx, rcvd = 0x%llx, data = 0x%llx\n", addr, rcvd, data);
      err = 1;
    }

    data = ~data; // flip the data and try again
    //fprintf(stderr, "Writing 0x%lx to address 0x%lx\n", data, addr);
    do_write(mode, ftHandle, ob_addr, data);
    rcvd = do_read(mode, ftHandle, ob_addr);
    //fprintf(stderr, "Reading address 0x%lx, get 0x%lx\n", addr, rcvd);
    if (rcvd != data) {
      fprintf(f, ">>> DATA=rand FAIL at address 0x%llx, rcvd = 0x%llx, data = 0x%llx\n", addr, rcvd, data);
      err = 1;
    }
    errcnt += err;
    if (err == 0) fprintf(f, ">>> PASS at address 0x%lx\n", addr);
    if (i && i%1000 == 0) fprintf(stderr, ">>> %d err_cnt %d\n", i, errcnt);
    fflush(f);
  }
  return errcnt;
}

int main (int argc, char const *argv[])
{
  FILE* f = fopen("mem_tester_err.log", "w");
  time_t start_time, end_time;
  start_time = time(NULL);
  unsigned seed = 1; // Default seed
  int result;
  std::string status;
  FT4222_SPIMode mode = SPI_IO_SINGLE;

  FT_HANDLE ftHandle = setup(mode, CLK_DIV_64, CLK_IDLE_LOW, CLK_LEADING);
  if (ftHandle == NULL)
    return 0;

  do_status_check(mode, ftHandle);

  if (argc < 2) {
    // Print Usage
  } else {
    for (int i = 0; i < argc; i++) {
      if (std::strncmp(argv[i], "seed=", 5) == 0) {
        seed = strtol(argv[i] + 5, NULL, 16);
        break;
      }
    }

    fprintf(stderr, "Using seed = %x\n", seed);
    srand(seed);
    for (int i = 0; i < argc; i++) {
      if (std::strcmp(argv[i], "l2") == 0) {
        fprintf(stderr, "Start testing using L2DA\n");
        // Drop L2 Reset
        do_write(mode, ftHandle, 0x8000000000 + 8 * 0x1a, 0x0);
        result = mem_test(f, mode, ftHandle, "l2_data");
        if (result != 0x0) {
          fprintf(stderr, "Test L2 FAILED!\n");
        } else {
          fprintf(stderr, "Test L2 PASSED!\n");
        }
      } else if (std::strcmp(argv[i], "dt") == 0) {
        // Manually reset DDR
        // Drop L2 Reset
        do_write(mode, ftHandle, 0x8000000000 + 8 * 0x1a, 0x0);
        fprintf(stderr, "Start testing using DT\n");
        result = mem_test(f, mode, ftHandle, "dt");
        if (result != 0x0) {
          fprintf(stderr, "Test DT FAILED!\n");
        } else {
          fprintf(stderr, "Test DT PASSED!\n");
        }
      } else if (std::strcmp(argv[i], "dma") == 0) {
        // Drop L2 Reset
        do_write(mode, ftHandle, 0x8000000000 + 8 * 0x1a, 0x0);
        fprintf(stderr, "Start testing using DMA\n");
        result = mem_test(f, mode, ftHandle, "dma");
        if (result != 0x0) {
          fprintf(stderr, "Test DMA FAILED!\n");
        } else {
          fprintf(stderr, "Test DMA PASSED!\n");
        }
      }
    }
  }
    
  end_time = time(NULL);
  printf("Start Time: %s\n", asctime(localtime(&start_time)));
  printf("End Time: %s\n", asctime(localtime(&end_time)));
  printf("ERR CNT: %s\n", result);
  fprintf(f, "Start Time: %s\n", asctime(localtime(&start_time)));
  fprintf(f, "End Time: %s\n", asctime(localtime(&end_time)));
  fprintf(f, "ERR CNT: %s\n", result);

  FT4222_UnInitialize(ftHandle);
  FT_Close(ftHandle);
  return result;
}
