#ifndef HAL_DEMO_H
#define HAL_DEMO_H

void gpio_test(void);
unsigned char SHT2x_MeasureData(void);
void vSpiNorTestTask(void *pvParameters);

#endif /* HAL_DEMO_H */

